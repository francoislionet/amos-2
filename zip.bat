del c:\temp\amos2-win-%1.rar
del c:\temp\amos2-macos-%1.rar
del c:\temp\amos2-linux-%1.rar

rar a -r c:\temp\amos2-win-%1.rar fonts\*.*
rar a -r c:\temp\amos2-win-%1.rar utilities\*.*
rar a -r c:\temp\amos2-win-%1.rar documentation\*.*
rar a -r c:\temp\amos2-win-%1.rar runtime\*.*
rar a -r -xapplications\*.*\html c:\temp\amos2-win-%1.rar applications\*.*
rar a -r -xdemos\*.*\html c:\temp\amos2-win-%1.rar demos\*.*
rar a -r c:\temp\amos2-win-%1.rar default\*.*
rar a -r c:\temp\amos2-win-%1.rar templates\*.*
rar a -r c:\temp\amos2-win-%1.rar .vscode\*.*
rar a c:\temp\amos2-win-%1.rar "run.bat" "keyboard_shortcuts.js" "amosc-x64.exe" "amosc-x86.exe" "Change Log.pdf" "Quick Start.pdf" "AMOS-2.code-workspace" "amos2import.exe" "amos2import-x86.exe"

rar a -r c:\temp\amos2-macos-%1.rar fonts\*.*
rar a -r c:\temp\amos2-macos-%1.rar documentation\*.*
rar a -r c:\temp\amos2-macos-%1.rar utilities\*.*
rar a -r c:\temp\amos2-macos-%1.rar runtime\*.*
rar a -r -xapplications\*.*\html c:\temp\amos2-macos-%1.rar applications\*.*
rar a -r -xdemos\*.*\html c:\temp\amos2-macos-%1.rar demos\*.*
rar a -r c:\temp\amos2-macos-%1.rar default\*.*
rar a -r c:\temp\amos2-macos-%1.rar templates\*.*
rar a -r c:\temp\amos2-macos-%1.rar .vscode\*.*
rar a c:\temp\amos2-macos-%1.rar "run.bat" "keyboard_shortcuts.js" "amosc-macos" "Quick Start.pdf" "Change Log.pdf" "AMOS-2.code-workspace"

rar a -r c:\temp\amos2-linux-%1.rar fonts\*.*
rar a -r c:\temp\amos2-linux-%1.rar documentation\*.*
rar a -r c:\temp\amos2-linux-%1.rar utilities\*.*
rar a -r c:\temp\amos2-linux-%1.rar runtime\*.*
rar a -r -xapplications\*.*\html c:\temp\amos2-linux-%1.rar applications\*.*
rar a -r -xdemos\*.*\html c:\temp\amos2-linux-%1.rar demos\*.*
rar a -r c:\temp\amos2-linux-%1.rar default\*.*
rar a -r c:\temp\amos2-linux-%1.rar templates\*.*
rar a -r c:\temp\amos2-linux-%1.rar .vscode\*.*
rar a c:\temp\amos2-linux-%1.rar "run.bat" "keyboard_shortcuts.js" "amosc-linux-x64" "amosc-linux-x86" "Quick Start.pdf" "Change Log.pdf" "AMOS-2.code-workspace"


