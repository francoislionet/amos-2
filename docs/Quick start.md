# Quick start



## Installation

The current version is still in development, I have not yet programmed an installer.

Download the latest version for your platform : Windows / MacOS / Linux from amos2.org (well if you read this, you must have done it somehow!)

Or download the whole project on Bitbucket at :
https://bitbucket.org/francoislionet/amos-2/src/master/

Unzip the whole amos-2 directory on your hard-drive as a **NEW** directory if you already have one (important!). Note: do not put it in 'Program files' or 'Programs (x86)' as they are write protected folders.


## Editing

I strongly suggest to use Visual Studio Code as an editor. It is the one I use, if you do to, you will
be synchronized with my changes.

A Visual Studio workspace file can be found at the root of the "AMOS-2" folder.

Once VSCode installed, simply double click on the workspace file to open AMOS-2.

If you are new to AMOS-2, install the keyboard shortcuts as explained on Youtube there : 
https://youtu.be/lsxe-3O6NDs

Then code, and press CTRL-F2 to compile the current application, and CTRL-F1 to run it in the browser (you must be editing the .AMOS file or the system will not work).


## Structure of the amos-2 directory
The amos-2 directory contains a series of sub-directories:

- `runtime` : contains the source-code of the AMOS 2 runtime engine

- `compiler` : the compiler itself

- `default` : contains default stuff for compilation, I suggest not to put anything here during development.

- `documentation` : all files are sorted using simple filenames for the moment. Partly done by cutting and pasting from the original AMOS Pro manual, not complete, but much more info (maybe a little outdated after the intensive refactoring of last week for version 0.7-05052019)...

- `applications` : contains an application that I use for debugging. You can put your applications there.

- `fonts` : contains the definitions of both Google and Amiga fonts. See `Change Log.pdf` of version 0.7 - 05052019 for more information.

- `templates` : contains various files, including in V0.7 - 05052019 ready to use manifests.

- `demos` : all the show-able demos up to today. Waiting for more from you guys! :wink:

- `utilities` : various utilities uses by amos2convert executable

  

## Your first application, "Hello world"

To create a new application :

- Copy the "empty_application" folder from the 'templates' folder at the location you want on your machine
- Rename the new folder to "hello_world". 
- Choose the manifest adapted to your application in the "amos-2/templates" folder (first use, suggested = manifest-1200.hjson)
- Copy it at the root of your new application folder
- Rename it to "manifest.hjson"
- Open Visual Studio Code, load "your_application/main.amos" 
- Type the magic words:
  Print "Hello world!"
- Hit CTRL-F2
- Hit CTRL-F1
- Be amazed at how good a programmer you are! 



## Compile and run the demos!

Under Visual Studio Code :

- Load thee .AMOS source code of the demos contained in the `amos-2/demos` folder

- Hit CTRL-F2

- Hit CTRL-F1

- Enjoy!

  

## You can also compile directly from the command line:

- Open a command line window

- Change the directory to the root of the amos-2 directory

- Compile your application by typing:

  - Windows:
    `amosc-x64 path_to_your_application_folder` _or_
    `amosc-x86 path_to_your_application_folder` if you have Windows 32 bits

  - MacOs
    `amosc-macos path_to_your_application_folder`
    **Note :** for the compiler out of the ZIP file to work, you have to change the property of the executable file to `executable` those properties not being saved in the ZIP

  - Linux:
    `amosc-linux-x64 path_to_your_application_folder` _or_
    'amosc-linux-x32 path_to_your_application_folder` if you have a 32 bits version of Linux
  - If everything goes right, the compiler should indicate a successfull compilation
  - Open a file explorer, and display the content of application folder. You will
    see that the compiler has created a new "HTML" folder. You will find in it the components of
    your compiled application, and an index.html file at the root


  **To run your application :**

  - Open your favourite browser (note: AMOS 2 has only been tested on Chrome)

  - Enter the complete path (including the drive) to the "index.html" file. In our case, if you
    have installed the amos-2 directory at the root of your C: drive, the path would be
    `C:/amos-2/applications/hello_world/HTML/index.html`

  - Press ENTER

  - The classic AMOS screen should appear with the magic words: "Hello world"!

  - Repeat the process to create other applications.

  **To import an original AMOS application**

  For Windows users only for the moment...

  - Copy the .AMOS application at the root of the amos-2 directory right out of the ADF disc

  - Type the command:
    `amos2import.exe myApplication.AMOS`

    The amos2import utility will load the amos program, create a new directory
    in the 'applications' directory, save the source code of the application as
    main.amos, create the manifest (**warning** in version 0.7 the manifest will not work, you have to choose it manually in the `templates` folder!!! ), and extract the content of the banks (for the
    moment only the sprites bank). 
    You will have to add the fonts manually in the manifest, see `Change Log.pdf` for version 0.7 - 05052019 for more info.

    Your application is ready to compile.

  

## The compiler command-line options as of today

- Command line syntax: 

  amosc [ options ] "path_to_application_folder" ... where options can be:

  - -fullscreen: will set the app to be full-screen, bypassing the information from the manifest

  - -fullpage: will set the app to be full page bypassing the options of the manifest
  - -speed "speed": will bypass the compilation/speed parameter of the manifest. Possible values ar "fast", "fair", safe" and "graphics" (please read the comments in a manifest for more info)
  - -saveto "save_to_path": indicate the path where to save the HTML folder containing the compiled application (default being the path to the folder of the app itself)
  - -log "path_to_log_file": accepted by the compiler but not implemented in this version. Saves the output of the compilation to a file.
  - -h / -help / --help: display the syntax of the command line (not really implemented, just accepted)
  - work in progress, more options to come!

  

