
# AMOS 2 Compiler Change Log
## *By Francois Lionet - francois@francoislio.net*

For information on how to compile, read the 'Quick Start.pdf' file...

# List of changes



## *AMOS 2 Compiler version 0.7 - 07/08/2019*

Urgent corrections after the publication of the latest version.

### Manifest

- No more commas in manifests

- Correction of the version number in `templates/manifest-PC.hjson`, I forgot to change it...

- New manifest for NTSC modes:  `manifest-1200-NTSC.hjson` and `manifest-1200-NTSC-Fullpage.hjson`, and for PAL modes: `manifest-1200-PAL.hjson` and `manifest-1200-PAL-Fullpage.hjson`  in the `amos-2/templates` folder.
  In NTSC mode, you have 40 more lines of display, and the size of the default screen is 320 x 240 and not 320 x 200 as in PAL, with 30 lines of text instead of 25...

- **New :** property `display.bodyBackgroundColor` : if the application is NOT in full-page or full-screen mode, indicates the colour of the `body` element of the page. Basically what is around the display canvas. If this value is not defined, the colour used is then `display.backgroundColor`

- **New :** property `display.bodyBackgroundImage` : if the application is NOT in full-page or full-screen mode, this property contains the path to an image to use as the background of the web-page (all that is around the canvas). This property overwrites the `display.bodyBackgroundColor` property, but when you resize the browser window, the `bodyBackgroundColor` can appear.

  If this property is absent or empty, then no image will be displayed.

  Please note that I have bought the rights of the `amos-2/runtime/resources/star_night.jpeg` image contained in this distribution from Adobe, but you have not! I strongly advise to replace it by another one when you want to publish your applications (legal can of worms: I have bought it for a program that makes other programs, is it allowed?)...
  Anyway, it looks really cool today! :smile: It should be big enough for 4K applications.
  Soon, videos as background! :v:

- **New :** property `default.screen.window.cursorImage` : contains the path to the PNG image to use as cursor for the text windows. Allowing to have an AMOS-like cursor in Amiga emulation, and a more modern shape for PC. You can also have it point to your own image, and the path can be absolute, or pointing to the folder of your application.
  You will now find two images in the `amos-2/runtime/resources` folder : `cursor_pc.png` and `cursor_amiga.png`

- **Bug :** you can now have two variables with the same name in different procedures

### Runtime

- Support of full-page / full-screen mode for Amiga-emulated applications
- Proper support of NTSC and PAL
- **New :** = Display Height... If in Amiga emulation, returns 311 in PAL or 261 in NTSC, as in the original AMOS. If in PC mode, returns the current height of the display canvas, allowing you to find the real height of the display area in full-screen mode where it follows the size of the browser window.... Please note that the height of the canvas only reflects the height of the browser window and not the actual drawing area, where black-bars can be drawn above and below to preserve the geometry of the application if `display.keepProportions` is set to `true` in the manifest...
- **New in AMOS-2 :** = Display Width... In Amiga mode, returns the same value in both NTSC and PAL mode, 342. In PC mode, returns the current width of the canvas, with the same remarks as for `Display Height`
- **New :** =NTSC, returns `true` if the application is running in Amiga mode and if the `display.tvStandard` property of the manifest is set to `"ntsc"`
- **Bug :** the `Proc` token used individually for a simple procedure call reported a syntax error
- **Bug :** Bobs and Sprites were not scaled along with the display (new bug since refactoring)
- **Bug :** several bugs corrected in file-system
- **Bug :** variable from the parameters of the procedures were not the ones that the code inside of the procedure was using
- **New :** any new screen will always have a font assigned, the first font in the list of fonts in the order of the manifest. Google Fonts come first, so if you have both, it will always be the first Google Font on the list. If you have NO font in the list of fonts, then it will be the default Text Window font.

### Compiler

- No error message was displayed if the compiler did not find or load the manifest of the application, more generally, compilation errors were not displayed.

  

## *AMOS 2 Compiler version 0.7 -  05/08/2019*

Load of changes in this version, I proceeded to a complete refactoring of both the compiler and the runtime. I hope I will not forget anything...  anyway, please re-test all the bugs you have posted in the AMOS-2 Development and Debugging Facebook group and tell me if they are corrected or not...

As usual for a version after such intense refactoring, problems can appear. I have tested (and changed the manifest so they are ready to compile) all the current demos, but who knows. I will correct them asap if any blocking bug is found...

## New

### Demos

- **brow fox over lazy dog** : Google Fonts in actions. All available fonts included. Have a look at the source code.
- **infinite balls** : the demo for you to play with, now with proper fonts. Please report how many bobs you can have at a good frame-rate in the group! (and wait for true hardware acceleration, it will be even bigger! :smile: ​)

### Compiler

- New version: 0.7... Getting there!:wink:

- Command line options:

  Command line syntax: 

  amosc.exe [ options ] "path_to_application_folder" ... where options can be:

  - -fullscreen: will set the app to be full-screen, bypassing the information from the manifest

  - -fullpage: will set the app to be full page bypassing the options of the manifest
  - -speed "speed": will bypass the compilation/speed parameter of the manifest. Possible values ar "fast", "fair", safe" and "graphics" (please read the comments in a manifest for more info)
  - -saveto "save_to_path": indicate the path where to save the HTML folder containing the compiled application (default being the path to the folder of the app itself)
  - -log "path_to_log_file": accepted by the compiler but not implemented in this version. Saves the output of the compilation to a file.
  - -h / -help / --help: display the syntax of the command line (not really implemented, just accepted)
  - more to come!

- The compiler now checks for the existence of the compiled application at start, preventing problems in Visual Studio Code when you hit CTRL-F2 with a non .AMOS source opened (used to create a "HTML" folder at wrong locations, happened to me many times, head-aches to find!)

- Creation of a "Filesaver" system that only updates the files that need to be saved, saving a lot of time during compilation. In "Verbose" mode (the current mode of the compiler in this version) the list of files actually saved is displayed. Check for yourself, a change in the .AMOS file will only induce the saving of the "application.js" file after an initial compilation. The system used a new file at the root of the application, "files.hjson"

- The compiler now displays the time used for compilation (only visible if you use it from command line)

- Fonts: improved handling of both Google and Amiga fonts. 

- Sprites: collision masks are now only created for .PNG images. For big images use JPG (they will work the same and will be displayable as sprite or bob, but will only have boxed-collision detection compilation will be faster and compiled application size smaller)

- There should not be anymore undefined error and warning messages. If you encounter one, please report in the development group, along with the source code that  generated it

- OR and AND (with complex expressions) use to produce bad code, that did not crash but reported wrong results. Suggestion, for complex expressions like 
  `If A = 2 And B <> 3 Or C = 4 Then...` _use parenthesis_ (I always do, even in Javascript):

  `If ( A = 2 And B = 2 ) Or C = 4 Then...`

  Please note that I fear there is an un-corrected bug when you use `||` as operator. Just use OR for this version.

- New compilation warning messages:

  - "Font not found"
  - "Unnecessary files found in folder" : generated when the compiler find for example extra files in the font folder where there should only  be folders
  - "Font not supported" : AMOS-2 only supports monochrome Amiga fonts (for the moment)
  - "Files at root of file system folder are not compiled" : the root of the Filesystem folder should only contain directories
  - "Missing folder" : one folder like 'resources/sprites' is missing from your application

### Organisation of the directories

- **New :** Global "amos-2/templates" directory.

  This directory will contain various kind of files and directories. In today's version, you can find pre-created manifests for your various needs.

  - "manifest-1200.hjson" : for emulation of AMOS running on an Amiga 1200 with the orange screen and the Topaz font.

  - "manifest-PC.hjson" : a PC application centred in the browser and displaying the name of the application and  the "Made with AMOS-2" message. The default screen is 1280 x 720 and the default window font is "IBM Plex Mono"

  - "manifest-PC-Fullpage-1280x710.hjson" : displays the application in full-page within the browser, with a default screen of 1280x720 and a default window font of "IBM Plex Mono". Press F11 to go full-screen.

  - "manifest-PC-Fullpage-1920x1080.hjson" : displays the application in full-page within the browser with a default Full-HD screen and a default window font of "IBM Plex Mono". Press F11 to go full-screen.

    _To create a new application today, follow this procedure:_

    - Copy the "amos-2/Templates/empty_application" folder where you want (no need anymore for it to be in the "applications" directory) and rename it

    - Choose the manifest in the "amos-2/templates" directory

    - Copy it into your new application folder and rename it to "manifest.hjson"

      Note that even if the system is still functional, I have removed the manifest from the "amos-2/default" folder, this was generating more problems than benefits. Please check that the file is not there before reporting bugs! :wink:

    **If you create cool manifests for various resolutions, please use the same format in names. I will soon create a visual utility (in AMOS-2 of course) that will allow you to define the parameters of your application with choices on screen, and that will save the  manifest in the new application. 
    And remember: SHARE!**

- **New: ** Global "amos-2/Fonts" directory containing both the Amiga and Google fonts that will be available for compilation of any application. This directory contains two sub-directories:

  - "amiga" : this directory contains the definition of the Amiga fonts. To add a new font, just create a sub-directory with the EXACT name of the font in lowercase, and drag and drop in the directory the font that you will find in the Amiga fonts directory out of the ADF virtual disc. It should only contain the various sizes as individual files, any other kind of file will report a compilation warning.

    Only monochrome fonts are supported in this version.

    **If you find new Amiga fonts, please share them with the community, I will add them to the default distribution, goal being to have all the fonts ever created on the Amiga in this directory (fonts there do not affect the size of the compiled application).**

  - "google" : this directory contains the definition of the Google fonts. To add a new font, please read the file "amos-2/templates/font.definition" file, it is a simple text file, everything is explained, and I will make a video about this soon. A lot of cool fonts are already added, selected individually by yours truly thinking on how they would be cool in games... 135 in today's version. 
    Compile and run the "amos-2/demos/brow fox over lazy dog" demo to see them in action.

    **Here too, if you add cool Google Fonts that are not present today, share! The more the better!**

    _Important warning_: many Google Fonts are *NOT* free. I had no time to look at their licence. If you use them in a commercial application (I guess that open-source / public domain is fine), check the licence! I cannot be held responsible for any copyright infringement!

- **New :** Global "amos-2/run/resources" directory, that contains mandatory files for the runtime to function. The first file you will find there is the definition of the shape of the text window cursor.

  A simple .png file that is resized and remapped to the animated colors of the cursor before being drawn. This picture should be small for the system to be fast. Transparent colors in the png will not appear, true white ($FFFFFF) will be remapped to the color of the animation as listed in the manifest, and any other color will appear as it is defined.

  More files to come here as development progresses.

### Runtime

- `Palette / Set Colour` : now correctly handle the "compilation/useShortColors" flag from the manifest (still bugs in the previous version)

- **Bug :**`Every Proc` used to generate a syntax error

- **New :** proper support of Amiga and Google fonts

  - Amiga fonts : Amiga Fonts and now properly displayed, and can be any height. Remember that they are not-vectorized, and only have the definition for some (or just one) of the heights. If you ask for a height that is not defined in the `Set Font` instruction, the compiler will choose the most approaching height _above_ the demanded height (or below if no choice), and will scale the letters during the rendering process. The closer to the original font height, the better the result.

    It is normal to see pixels for Amiga fonts.

    It is in my list to have the compiler vectorize the font during compilation, transforming therefore old chunky Amiga fonts with pixels visible into nice and smooth modern fonts. It will be very cool! "Transmitting the spirit of the Amiga", my goal!

    So nice to see those fonts back! As said above, please find all the fonts you can and share them!

  - Google Fonts : Google Fonts are  embedded into the application itself. This means that can even work off-line, a great plus for your applications. They will load always and be very fast to load as well. Note that `Set Font` instruction only returns when the font is actually loaded, so you can use the font immediately after. (bug in version 0.7, you have to do a CLS and another `Set Font` for it to work, will investigate soon). Of course, if the font is already loaded, it will be very fast (same remark for Amiga fonts).

    Both Google and Amiga fonts can be found in the compiled application "HTML/resources/fonts" folder. Only the necessary fonts are copied.

- `Get Font` can now be used with `Get Font "name_of_the_font", size`, allowing you to directly access the font without having to get the list of fonts first. Much simpler! To be useable in your application, fonts must be added to the manifest prior to compilation (see the "manifest" section)

- **New:** `= Get Font Number` returns the total number of fonts included in the application (you had no way of knowing that before but than checking the string returned by `= font$()` )

- `= font$( number ) / = Font$( "name_of_font" )` : you can now use the name of the font as parameter. If the value "fonts/listFonts" in the manifest is equal to "amiga", the format of the string returned by the function will be a 40 character string as on the original AMOS. If the value is "pc", the returned string can be any length, and contain more information about the font, like the available weights and styles for Google fonts.

- `Text` now have an extra parameter, `tags` that indicate how to display the string. This string should contain tags similar to the Javascript Canvas properties related to text display, and can take the following values:

  - horizontal formatting: #start, #left (default), #center, #right, #end 

    ![Alt text](./media/text_align3.png "Vertical tags")

    

  -  vertical formatting: #top, #bottom, #middle, #alphabetic (default), #hanging

    ![Alt text](./media/text_baseline.png "Vertical tags")

    Examples of use: 

    `Text 320, 100, "Hello AMOS-2!"` -> text is aligned on the left and baseline

    `Text 320, 100, "You look nicer today!", "#center"` -> text is centred in X and on the baseline

    `Text 320, 100, "Yeah I had a lifting last week!", "#right #top"` -> text is aligned on the right and top

    Note that if you use several tags for horizontal or vertical, only the last one will be used. Unrecognised tags will report no error and have no effect (maybe I should have then report errors)...

- **New: ** `Set Alpha alpha_value` sets the semi-transparency of all the graphics instructions in the screen after being used, including  `Text`. alpha_value being an integer ranging from 0 (fully transparent, nothing is drawn) to 255 (fully opaque). For example, to draw a semi-transparent circle, use:

  `Set Alpha 128 : Ink 2 : Circle 320, 200, 200`

- `Hot Spot` now updates all sprites and bobs on the screen automatically

- **New :** 

  `Remap RGB_value To RGBA_value`

  `Remap RGB_value To RGBA_value, x1, y1 To x2, y2`

  Performs a pixel per pixel remapping of the whole current screen or a zone in the current screen. 

  RGB_value is an integer in the form of $RGB (manifest compilation/useShortColor = true) or $RRGGBB (compilation/useShortColor = false). An eventual alpha value will be ignored.

  RGBA_value is an integer in the form of $RGBA or $RRGGBBAA

  Warning, this operation can take a long time for large screens in PC mode. For 'Amiga-resolution' screens, it is rather fast (depending on the display/screenScale property of the manifest, a value of 1 having the internal bitmap of the screen to the same resolution as the one indicated in the `Screen Open` instruction, but a value of 4 having the internal screen for a `Screen Open 1, 640, 480...` being in fact 2560 x 1920... remember that when you use this instruction)... 
  This can be used today to create palette animation effects.

- **New:** `Set Block Alpha block_number, alpha_value` allows you to set a block semi-transparent. alpha_value being an integer ranging from 0 (totally transparent) to 255 (totally opaque) - not tested.

- **Bug: ** `= Colour()` did not take into account the manifest flags 'compilation/useShortColors'

- **Bug :** an error in the parameters instructions that wait for completion (such as `Set Font`) crashed the compiled application at Javascript level.

- **New :** support of full-page and full-screen for compiled applications.

- **Temporary :** the `Stop` instruction generates a Javascript 'debugger' call to help me debug. Be warned if you use it with Chrome debugger open! (will have no effect if not, and won't stop the application).

- **Bug :** wrong remapping of the numpad keys to Amiga number keys (not important for compatibility, no numpad on the Amiga! :wink: )

- **Bug :** the application now receives key-presses without having to click on the canvas.
- **Bug :** `= X Screen / = Y Screen` used to round the value to an integer
- Fonts are now available when the application starts, no need to do `Get Disc Font / Get Rom Font` anymore.
- **Renderer** : correction of several bugs, handling of full-page and full-screen mode with black bars (see manifest), possible resizing of the application in real time, faster rendering of rotated and scaled bobs and sprites over non-rotated screens (will be slower is the screen is rotated, and actually do not even try to rotate a screen in version 0.7 it will give un-predictable results).
- **Bug :** the application used to crash if you opened a screen with more colors than the value of the default screen as defined in the manifest.
- **Bug :** `Get Bob Palette / Get Sprite Palette` crashed. Note that for today, the palette of the sprite bank is not extracted, so the result will be the palette of the default screen.
- `Cls color [ x1, y1 To x2, y2 ]` now takes into account the value of the `Set Alpha` instruction. For transparency, you have to set the screen  transparent mode before, with `Set Transparent 0` . Have a look at how I do it in the Infinite Balls demo for the control panel.
- **Bug :** `= Colour( index )` used to crash
- **Important :** `= Point( x, y ) will _only_ give good results if the internal screens are NOT scaled (manifest: display/screenScale = 1). Nothing I can do about it, will be corrected when real Amiga plane-based emulation is implemented, and _only_ in Amiga emulation, behaviour due to the anti-aliasing process in the Canvas, I cannot rely on any RGB value and cannot find them always in the palette to return the index of the color.
- **Bug :** width of line was not properly set in `Bar and Box`
- **Bug :**  `Set Pattern` now work properly as on the Amiga
- **Bug :** collision detection did not work after some code lost. Restored.
- Text Windows cursor can now be semi-transparent and have a different shape (see manifest)
- Text Windows default font can now be a Google Font or an Amiga Font (see manifest). Only choose mono fonts, without that, the cursor will not follow the text.
- **Note :** `Print` is slow, some refactoring still to be done! Use `Text`, much faster for real time text display.



### Manifest

The simplest for you is to have a look inside of one of the default manifest files contained in the "amos-2/templates" folder, everything is explained in the comments. 

**Warning :** the manifest now is on V6. Applications with the old manifest V5 will not compile anymore. Read above and choose the correct manifest for your application. Make sure that the "amos-2/default" folder does _not_ contain any "manifest.hjson" file, as it will mess things.

In brief :

- **New :** font section, when you insert the name of the fonts to include in the application. Two sub-sections : 

  - "amiga" : list of Amiga fonts to include
  - "google" : list of Google Fonts to include

  Just insert the name of the font in lower case, respecting spaces. The font must be present in the "amos-2/fonts" directory, or you will get a compilation error.

- Text Window font now uses a real-font, and not anymore a tinkered one as I was doing so far. Display is much more precise for Amiga fonts.

- Text Window Cursor can be semi-transparent with semi-transparency animated as well. It does not need anymore one specific color to function, color #3 of the palette is now free for you to use. The definition of the image use for the cursor is located in the "amos-2/runtime/run/resources" folder. I will add soon in a new version of the manifest the possibility to choose the picture from the manifest by indicating its path (should have done it already, sorry for that), enabling the original "horizontal bar" and the new "semi-transparent block" or any kind of cursor, including with graphics, in your application.

  

### On next week to-do list

- Implement multiple .AMOS source code support and the `Include` instruction

- Implement extension support (nearly all done with `Instruction` and `function` keywords), principle = one file = one extension, using a hidden `include` from the above work. 

  I will from now on, program all the new instructions as separate files, and the compiler will link them. I also need to migrate the current instructions and functions to the new system. Much better design, open for infinite expandability, and it will reduce the size of the compiled application runtime immensely as only the necessary code will be copied and not the whole stuff as up to today...

  And **YOU** (I hope) will be able to start real help on the coding side, by implementing the original AMOS extensions, in simple Basic code! (nearly as fast as native Javascript, no reason not to use it!)... 

  Who wants to do AMOS-3D? Should be simple, and much faster than on the Amiga even if not using the graphic card... ?

- Finally do `Load Iff` and more generally `Load Image` that will load everything and in future version SVG and vector images (to be used in bobs, will be nice and I hope not too slow).

- Have the compiler **directly** understand .AMOS applications without the need of using a conversion utility (it will work on MacOS Bill :wink: and Linux too). Simple to make, just convert the existing C code to Javascript. And then you will be able to do `amosc my_game.AMOS`, the compiler erasing the temporary folder at the end, you will only get as a result the HTML folder ready to be ran.

- Implement sound instructions, using Monkey as root library, a stable, open-source, and very cool Javascript library for quality sound support that works on every platform.

- And more surprises, but they would not be surprises if I told them to you would they? :grin:

  

Pfew, what a week this was! Hope you like this version! :kissing_smiling_eyes:

Amsterdam, here we come! :v:

Francois






## *AMOS 2 Compiler version 0.6 - 17/07/2019*

## New
- Infinite Balls demo! Yeah!

## Bugs corrected
- Optimization of renderer for non rotated screens, MUCH faster
- Wait VLB used to wait TWO vbl 


## *AMOS 2 Compiler version 0.6 - 16/07/2019*
- Change of version
- Implementation of warnings
- First warning: 'variable not assigned before use'
- Get Fonts called at start of application
- Compiler should work on Linux and MacOS, special thanks to Matt Diley for finding tghe reason! (Y)
- Integration in Visual Studio Code. How to use, watch: https://youtu.be/lsxe-3O6NDs
- Compiler _should_ run on Linux and MacOS, please confirm. Thanks Matt Dilley! (Y)
- Faster PLOT instruction 
- CLS now locates the cursor at 0,0 after erasing the screen

## Bugs corrected
- //*** : compiler no more crashes when it finds such remark 
- If -condition- Then PROCEDURE_CALL ":" Else ... used to produce a syntax error if no column was added after the procedure call
- If -condition-  Then PROCEDURE_CALL Else ... produced wrong code with unpredicatable results (no crash though)
- ROL / ROR were not transmitting the carry
- "Print "Hello"    Rem on the same line" ... Used to generate syntax error
- Many bugs and crashes removed in font handling and display, Base Line now finally correct
- Line number of reported error used to be +1



## *AMOS 2 Compiler version 0.5 - 13/07/2019*

## Bugs corrected
- Set Transparent crashed
- Restore, Goto, Gosub, On, Every now support calculated labels
- Every did not work at all (due to lost code)
- Label Not Defined error message was incorrect



## *AMOS 2 Compiler version 0.5 - 08/07/2019*

## New

- Organisation of the Documentation folder
- AMOS-2 Visual Studio workspace file in the "AMOS-2" folder, press F5 to launch the compiler on the Applications/debugging application (can be changed in Visual Studio Code debugger settings)
- New folder "templates" that contain files to include in your projects (more to  come, please help by finding cool Amiga Fonts, images, sprites, sounds, icons, etc.)
- Cleaning of the Fonts folder, now Roboto Google Font is present by default, and one Amiga Font, Courrier. Other fonts can be found in the "templates/Amiga Fonts" directory. To import a new Google Font, please read the chapter of the documentation, "2.1. Fonts and Directory Structure".
- JPG pictures can now be used in the sprites bank, collision detections with bobs and sprites displaying those image is boxed only.
- Paste Bob X, Y, Image, Scale
- Paste Bob X, Y, Image, ScaleX, ScaleY : new parameters to scale the image while pasting. I will add soon rotation and skew.

## Bugs corrected

- FPS Indicator display was flashing
- Several big problems crashing the compiler and / or the application with Global variables, specially arrays
- Crashes within procedures with global/shared variables
















## *AMOS 2 Compiler version 0.5 - 27/05/2019.2*

- Demos folder cleaned, all demos now work, and are ready-compiled.
- Removal of several display bugs in the process

## *AMOS 2 Compiler version 0.5 - 24/05/2019.2*

## Bug removed
- The default manifest was badly configured, making all applications display badly.

## *AMOS 2 Compiler version 0.5 - 24/05/2019*

## New
- infamous Boobs demo updated with two boobs by Marvin and Marcel :)
- new pages of docs in the doc folder

## *AMOS 2 Compiler version 0.5 - 18/05/2019*

## New
- infamous Boobs demo updated with two boobs by Marvin and Marcel! :)
- new compiler option :  --fullpage=true/false
  	...compiles in Full Page mode: the application will take the whole browser page, keeping proportions or not.
- new compiler option :  --keepproportions=true/fasle 
  	...if full page or full screen, with keep the proportions of the original applications by applying black bars on the top and bottom or the sides. The color of the bars being defined in the manifest
- new manifest option : display.fullpage = true / false (manifest version unchanged)
- new manifest option : display.keepProportions = true / false


## *AMOS 2 Compiler version 0.2 - 05/05/2019*

### Implementation of AMAL!
### Direct import of original AMOS applications!
### Collision detection works without a server!

### New
- All tokens are now compilable, they will just have no effect (I will print a warning soon for non-implemented instructions)
- Collision detections now works without the need of a server: the compiler now saves the collision masks directly into the code, therefore the AMOS 2 runtime does not need anymore to scan the images, ovepassing the 'origin-policy' protections of the browsers.
- Every XXX Gosub
- Every XXX Proc
- Every On
- Every Off
- ChanAn
- ChanMv
- Channel To Sprite
- Channel To Bob
- Channel To Screen (not tested)
- Channel To Screen Display (not tested)
- Channel To Screen Offset (not tested)
- AmReg
- Amal On 
- Amal Off
- Amal Freeze (not tested)
- AmalErr
- AmalErr$
- Amal
- Synchro On (not tested)
- Synchro Off (not tested)
- Synchro (not tested)
- Update On
- Update Off
- Update
- Update Every
- Gamepad Map Buttons (more info in a later version)
- Gamepad Map Axes (more info in a later version)
- Gamepad Map Triggers (more info in a later version)

### Bugs corrected
- Many! :)

## *AMOS 2 Compiler version 0.2 - 04/04/2019*

Implementation of joystick and gamepads! Have a look at the 'debugging' application contained in the distribution...

### New
- = JUp
- = JDown
- = JRight
- = JLeft
- = Fire
- = Gamepad Connected( PADNUMBER ) ***NEW IN AMOS2!***

	PADNUMBER is an integer which value from 1 to 4, you can connect up to 4 gamepads to the machine...

	Returns TRUE if the gamepad is connected.

- = Gamepad Button( PADNUMBER, BUTTONNUMBER ) ***NEW IN AMOS2!***

	BUTTONNUMBER is an integer number from 0 to 15, with the following mapping of the buttons:
	- 0 : Bottom button in right cluster ('A' on XBox controller, used by the Fire function)
	- 1 : Right button in right cluster ( 'B' on XBox controller)
	- 2 : Left button in right cluster ('X' on XBox controller)
	- 3 : Top button in right cluster ('Y' on XBox controller)
	- 4 : Top left front button
	- 5 : Top right front button
	- 6 : Bottom left front button
	- 7 : Bottom right front button
	- 8 : Left button in center cluster (gamepad left 'control' button)
	- 9 : Right button in center cluster (gamepad right 'control' button)
	- 10 : Left stick pressed button
	- 11 : Right stick pressed button
	- 12 : Top button in left cluster (JUp)
	- 13 : Bottom button in left cluster (JDown)
	- 14 : Left button in left cluster (JLeft)
	- 15 : Right button in left cluster (JRight)

- = Gamepad Axe( PADNUMBER, AXENUMBER ) ***NEW IN AMOS2!***

	AXENUMBER is an integer number from 0 to 3, indicating which axe to report. The function returns a floating point value from -1 to 1.
	- 0 : Horizontal axis for left stick
	- 1 : Vertical axis for left stick
	- 2 : Horizontal axis for right stick
	- 3 : Vertical axis for right stick

- = Gamepad Trigger( PADNUMBER, TRIGGERNUMBER ) ***NEW IN AMOS2!***

	TRIGGERNUMBER is an integer number from 0 to 1, indicating which trigger to report. The function returns a floating point value from 0 to 1.
	- 0 : Left trigger
	- 1 : Right trigger

### Bugs removed
- Bob Off NUMBER and Sprite Off NUMBER do not generate an error anymore if the Bob or Sprite did not exist

## *AMOS 2 Compiler version 0.2 - 31/03/2019*

Collision detection implemented! Please note that this version does not handle collisions with rotated sprites and bobs. It does handle though collisions with SCALED sprites and bobs. I will implement rotations later, as it is a rather complicated job... I do not think also that it will ever support SKEW for collision detection...

For a demo of collision detection, just run the 'debugging' application contained in this distribution.

---

***IMPORTANT NOTE:*** to handle collision detection, I need to obtain from the browser the pixels of the images of the sprites and bobs. Problem is that Chrome does not allow that and reports a 'Cross-domain error' when you try to do that when the html file is played from the local disc. Stupid protection of copyrights! For you, if you try to run the 'debugging' application in Chrome without a server, AMOS will report an 'Internal error'...

The only solution for pixel-precise collision detection to work under Chrome, is to run your application from a local webserver, and instead of entering the path to the index.html file, enter its URL in the localhost.

If you want to have pixel-precise collisions for your application in Chrome, you have to install a small local webserver. I suggest to use 'Fenix', it is very simple and works great. http://fenixwebserver.com/

After the installation of Fenix, choose 'New server' and enter the path to your 'applications' directory within the amos-2 directory. Start the server. And to run your application in the browser, just type in the URL bar:

`http://localhost/myapplication/html/index.html`

Firefox does not have this protection, and pixel precise collision detection should work fine without server (not tested).

You can also, if you do not want to install a server, set the sprites/collisionBoxed value to 'true' in the manifest (see later).

I will in the (very) near future, solve this problem with an Electron application that will run your amos-compiled apps directly, OUT of the browser. You will simply have to type:

`amos2 myApplication`, and your application will run as native in its own window.

---

New version of the manifest: "5".

New entry in the manifest: 'Sprites'.

```
// Sprites and bobs
sprites:
{
	collisionBoxed: false,			// Only use box collision detection, fast but imprecise
	collisionPrecision: 1,			// Pixel-precision of the collisions. 1 = pixel per pixel, 0.5 = 2 by 2, 0.25 = 4 by 4 etc. 0.25 is good for large sprites and 16 times faster...
						// Set it to 1 if your game contains small sprites... (for total compatibility with Amiga, leave it to 1, images are small)
	collisionAlphaThreshold: 1,		// Level of alpha for which a pixel is considered 'not transparent' and will generate a collision (0 being transparent, and 255 being opaque)
},
```
### New
- = Sprite Col
- = SpriteBob Col
- = BobSprite Col
- = Bob Col
- = Col
- = Rnd now has different behaviors depending on the type of the value passed to it.
	- If the value is an integer, the behavior is the same as in original AMOS, Rnd( value ) returns an integer from 0 to value (included)
	- If the value is a floating point number (meaning that there is something after the decimal dot, 10.0 being treated as integer), it returns a floating point value from
0 to value (excluded). For example, Rnd( 10.00000001 ) will return random floating point numbers from 0 to 10 included... (with maybe a very very little chance of returning a number above 10, but hey! it's good to live dangerously :)

### Bugs removed
- applications now work even if there is no images in the resources/sprites folder
- "indian" is now called "endian" in the manifest
- bobs were not clipped to the screen they belong to

## *AMOS 2 Compiler version 0.2 - 29/03/2019*

### Bugs removed
- The compiler produced a bad 'index.html' if no Google Font was defined in the 'resources/fonts' folder of the application (or in the default folder)
- index.html was trying to load a non-existent file: 'filetemplate.js', generating a non-critical error in Chrome debugger console.

## *AMOS 2 Compiler version 0.2 - 28/03/2019*

New! Sprites! Have a look at the spritesdemo in the demos folder (or the 'debugging' application! :)
New! Bob and screen scale and rotations! (veeery powerful! :)

### New
- Sprite
- Sprite Off
- Sprite Priority
- Sprite Update On
- Sprite Update Off
- Sprite Update
- ***NEW IN AMOS2*** Sprite Scale spriteNumber, xScale#, yScale# - Scales the sprite. 1.0 => no scale, 0.5 => half, 2.0 => double
- ***NEW IN AMOS2*** Sprite Rotate spriteNumber, angle# - Rotates the sprite around its hot-spot
- ***NEW IN AMOS2*** Sprite Skew spriteNumber, xSkew#, ySkew# - Sets the horizontal and vertical skew. 0 => no skew
- Get Sprite Palette
- Get Sprite
- Del Sprite
- Ins Sprite
- = X Sprite
- = Y Sprite
- = I Sprite
- Set Sprite Buffer (does nothing)
- ***NEW IN AMOS2*** Bob Rotate bobNumber, angle# - Rotates the bob around its hot-spot
- ***NEW IN AMOS2*** Bob Scale bobNumber, xScale#, yScale# - Scales the bob. 1.0 => no scale, 0.5 => half, 2.0 => double
- ***NEW IN AMOS2*** Bob Skew bobNumber, xSkew#, ySkew# - Sets the horizontal and vertical skew. 0 => no skew
- ***NEW IN AMOS2*** Screen Hot Spot mask / Screen Hot Spot screenNumber, mask / Screen Hot Spot screenNumber, xHot, yHot - Sets the hot-spot of the screen. Coordinates are screen coordinates. mask: cf Hot Spot instruction... Note, this affects the position on the display, and a new Screen Display has to be done after this instruction. Default hot-spot = 0, 0.
- ***NEW IN AMOS2*** Screen Rotate angle# / Screen Rotate screenNumber, angle# - Rotates the screen around its hot-spot and its content (text, graphics, bobs etc.)
- ***NEW IN AMOS2*** Screen Scale xScale#, yScale# / Screen Scale screenNumber, xScale#, yScale# - Scales the screen and its content (text, graphics, bobs etc.)
- ***NEW IN AMOS2*** Screen Skew xSkew#, ySkew# / Screen Skew screenNumber, xSkew#, ySkew#  - Sets the horizontal and vertical skew. 0 => no skew... Note: if bobs are present in the screen, they will be improperly skewed. Working on this.
- Get Sprite Palette / Get Bob Palette now sets colors 16 to 32 of a 16 colors screen to a copy of the colors from 0 to 15, as in original AMOS

### New version of the manifest, version 4
- If you are using a manifest local to your application (overwriting the default manifest), you have to change the version number to 4 or your will receive a compiler error message.
- New : FPS indicator for debugging purpose! To activate it, it is located in the 'display' section :

	- fps: true						// Display FPS indicator on screen
	- fpsFont: "12px Verdana",		// Font to use for FPS indicator
	- fpsColor: "#FFFF00",			// Color of the FPS indicator
	- fpsX: 10,						// X position of the FPS indicator
	- fpsY: 16						// Y position of the FPS indicator

### Bugs removed
- Wait Key left the key in the input buffer
- Print / Print # caused internal error in runtime if more than one string was printed
- Compiler internal error in some cases when affecting a value to an integer variable

## *AMOS 2 Compiler version 0.1 - 23/03/2019*

### New
- Open Random
- Field
- Get
- Put

### Bugs removed
- Lof returned 'undefined'
- Input # and Print # were not using the same logic as the original AMOS


## *AMOS 2 Compiler version 0.1 - 22/03/2019*

### New
- Open Out
- Open In
- Append
- Print # (only for sequential files)
- Input # (only for sequential files)
- Line Input # (only for sequential files)
- Set Input
- = Pof = Note: Pof( value ) = position not tested! :)
- = Lof
- = Eof

### Bugs removed
- Bit manipulations instructions and functions did not work!


## *AMOS 2 Compiler version 0.1 - 21/03/2019*

### New
- Bset
- Bclr
- Bchg
- Btst
- Rol.B
- Rol.W
- Rol.L
- Ror.B
- Ror.W
- Ror.L
- Optimisation of setting an integer variable for constants (used to add Math.floor)

### Bugs removed
- Bsave and Bload did not actually worked! ;)

## *AMOS 2 Compiler version 0.1 - 20/03/2019*

### New
- BSave, and generic local saving in the browser. When you save a file in AMOS 2, the file data is in fact, saved in the LocalStorage area of your browser. The LocalStorage is a persistent memory spacve reserved by the browser for each web-site. The data is saved from session to session, and as long as the URL is the same, is preserved for the next sessions. This space is not very large, 5 to 10MB for each application, but on an Amiga point of view this is huge. Bsave, and every SAVE instructions in AMOS 2 saves the DATA of the file in that space, while preserving the file location in the file system directory structure.
Example:
Reserve as Work 10, 10000
For P = 0 To 9999
	Poke Start( 10 ) + P, Rnd( 255 )
Next
Bsave "Dh0:data/myfile.bin", Start( 10 ) To Start( 10 ) + Length( 10 )
Dir
...>
Directory of Dh0:data/
	myfile.bin				10000
... close the tab, re-open it:
Dir
Directory of Dh0:data/
	myfile.bin				10000
... the file is still there! (should be! :)
LocalStorage is an easy way to save persistent data, it does need a server. I will implement later in the development a true save on the user's local machine (when applications are packaged), AND on the server where the application is located (by creating a small WebSocket server that will receive the file).

Please note that DFree and Disc Info$ return the available space in the LocalStorage area. Under Chrome, you should get a response of 5000 * 1024 bytes.


## *AMOS 2 Compiler version 0.1 - 13/03/2019*

File System!

You can now have a directory called 'filesystem' just beside the 'resources' directory of your application and/or in the 'default' directory of the compiler, in this case, the files contained in the 'default/filesystem' folder will be available to all the applications compiled by the compiler.
This directory can ONLY contain sub-directories: each sub-directory defines a DRIVE that will be available from within your AMOS 2 application.
For example: 'DF0', 'DH1" etc.
If you call the directory 'application' then this directory will be the default directory of your application (for example, a DIR command will return its content).
Then you can put in those directories files or nested folders with files: the hierarchy will be reflected as a normal drive.

From within your AMOS program, all disc command work as on the Amiga. The format of path is similar to the one of the Amiga:
DRIVE:DIRECTORY/SUB-DIRECTORY/FILE.EXTENSION

You will also be able to SAVE files in those directories, in the next version! :)
... instructions like Load Iff will work just as on the Amiga! You will be able to load ABK of course. Total compatibility.

How are the files stored?
In the HTML folder of your compiled application, you will find a list of numbered js files. Each file contains the data of the original file in base64 forma (1/3 bigger). When you load a file, AMOS 2 loads the JS file and uncrypt the data. Why JS files? So that you do not need a server! JS files can be loaded locally without server, and they ALSO work on a server.

Internal files?
I have also programmed an option in the compiler (not available yet), to store files directly into the source code of the application. If this option is selected, everything is loaded when the index.html file is loaded in the browser. And later, 'loading' a file from within the AMOS application is instant.... because it is already loaded!
This option will be great if you want your program to be reactive when it runs. Once everything loaded, nothing to load anymore.

Please note: this version of the compiler already contains files in the filesystem folders of the debugging application an in the default folder. Maybe you should clean them! (no time this morning, late!)

### New
- Dir
- = Dir First$
- = Dir Next$
- Rename
- = Exist
- Set Dir
- Bload
- Reserve As Work
- Reserve As Data
- Reserve As Chip Work
- Reserve As Chip Data
- Erase
- Start
- Length
- Poke
- Doke
- Loke
- Peek
- Deek
- Leek
- Peek$ (for the moment only ASCII, works for UTF-8 for characters under 128)
- Poke$
... see you tomorrow for the end of the file system. It has taken longer than expected, but it was worth it!

## *AMOS 2 Compiler version 0.1 - 12/03/2019*

Support of Google Fonts! Have a look at the Sine Scroller example located in the 'applications/debugging' directory!

### Bugs removed:

### New
- Set Stack. Does nothing.

## *AMOS 2 Compiler version 0.1 - 11/03/2019*

Support of Google Fonts! Have a look at the Sine Scroller example located in the 'applications/debugging' directory!

### Bugs removed:
- The cursor was flashing only in the currently activated screen
- Procedures were not recursive

### New
- Plot has been accelerated
- Support of Google Fonts!
To include a Google Font in your application:
* Select a font on Google Font website
* Copy the file located in 'application/empty_application/resources/fonts/Roboto.googlefont' in your application's 'resources/fonts' directory (or inthe default directory of the compiler)
* Rename the file to the name of the font (case sensitive!)
* Copy the information provided by the Google Font website inside
* The Google Font will be available as a 'Disc' font, Amiga fonts being 'Rom' fonts

- Set Font now accepts more parameters: Set Font number, Height, Weight, Italic
* Height: for Google Font, indicates the height
* Weight: for Google Fonts, indicates the weight. Javascript type parameters, example: 'bold', 100, 300 etc.
* Italic: "italic" to put the font in italic

## *AMOS 2 Compiler version 0.1 - 08/03/2019*

### Bugs removed:
- val returned zero for hexadecimal and binary values

### New

## *AMOS 2 Compiler version 0.1 - 07/03/2019*

### Bugs removed:
- restore without label caused a syntax error
- palette and default palette did not include the last value
- X ^ Y reported a syntax error
- get bob did not set color 0 as transparent

### New


## *AMOS 2 Compiler version 0.1 - 05/03/2019*

### Bugs removed:
- Mid$/Left$/Right$ used as a function reported a syntax error
- Paste Bob parameters were not in the correct order
- Paste Bob and Put Bob were anti-aliasing the paste sprite
- The speed of the loop now stays stable even with heavy graphics and does not slow down anymore

### New

## *AMOS 2 Compiler version 0.1 - 04/03/2019*

LOTS of stuff in this version!

*NEW Amiga Fonts!*
Just copy the font, the .font and the folder into a directory called 'fonts' located in your application 'resources' directory. The fonts will be available in your application after you have done a Get Fonts / Get Disc Fonts / Get Rom Fonts. You can also copy the font in the default folder of the compiler (see later).
Please note that this version of the compiler only support monochrome bitmap Amiga fonts. An error message during compilation will be displayed if another type of font is found. To come: Google fonts, in a very simple manner! :)

*NEW Sprite Bank*
To include an image in your sprite bank, just name it '1.png', '2.png' etc. and the image will be available in the sprites bank of your AMOS application, and you will be able to use it with bobs and sprites.
Images included in the 'default' directory of the compiler will also be included, but REPLACED if another image with a similar name exists in your application 'resources/sprites' directory, by the image of the application.

### Bugs removed:

### New
- New 'default' directory, located in the same directory as the compiler. If defined, this directory can contain a default manifest and a default resources folder: elements from the resources folder's banks will added to the compiled application. Use: put all the fonts there, and a default manifest so that you do not have to define one for each application. If a manifest also exists in the application's folder, the values of the application's manifest override the one of the default manifest. Same for fonts or images or sounds: the ones defined in the application's resource folder override the one of the default folder.
- Get Fonts
- Get Disc Fonts
- Get Rom Fonts
- Set Font. Note: Set Font accepts a second parameter, HEIGHT, forcing the height of the font, even if it is not defined in the font itself (it will use the largest font definition and zoom it)
- Text
- = Font$
- = Text Length
- Get Block
- Put Block
- Del Block
- Get CBlock
- Put CBlock
- Del CBlock
- Curs Pen
- Set Curs
- = Key State
- = Key Shift
- Key Speed. Does nothing.
- Bob
- Paste Bob
- Put Bob
- Del Bob
- Ins Bob
- Get Bob
- Get Bob Palette
- Hot Spot
- = X Bob
- = Y Bob
- = I Bob
- Priority On
- Priority Off
- Priority Reverse On
- Priority Reverse Off
- Bob Update Off
- Bob Update On
- Bob Update
- Limit Bob
- Bob Show [number]. *New in AMOS2!*
- Bob Hide [number]. *New in AMOS2!*
- Bob Draw. Does nothing.
- Bob Clear. Does nothing.



## **AMOS 2 Compiler version 0.1 - 28/02/2019**

### Bugs removed:
- restore label generated a syntax error
- Dual Playfield now sets the correct screen display priority
- Dual Playfield screen 1 (the one in the back), now has its
  palette shifted from 8 (colors 0 to 7 become colors 8 to 15),
  but ONLY for draw operations.
  CONSEQUENCE IN AMOS 2: you MUST set the screens to Dual Playfield
  BEFORE doing drawing operations, otherwise the shift in the palette
  will not be taken into account.
  If it breaks too many programs, I will remap the colors in the
  bitmap iself.
- Dual Priority does the same

### New


## **AMOS 2 Compiler version 0.1 - 27/02/2019**

### Bugs removed:
- Mouse Zone made the compiled application crash
- Border$ did not wrap
- Paper and Pen where not set to draw a border with Border$
- After creating, moving or destroying windows, the windows and cursor did not appear anymore
- Activating a window after its size was changed since the last wind save displayed garbage
- Def Scroll made the application crash
- Sort did not work on array of numbers
- Centre left a dead cursor at the position before the call and did not position the cursor at the end of the string

### New

## **AMOS 2 Compiler version 0.1 - 26/02/2019**

### Bugs removed:

### New
- Dual Priority
- X Text
- Y Text
- = Zone$
- = Border$
- Stop
- = Errn
- Add
- Line Input
- = Flip$
- = String$
- Sort
- = Match()
- Direct (end the program)
- Edit (end the program)
- Randomize
  If Randomize is used, a MerseneTwister random number generator is initialized with the randomize seed.
  This ensure consistant production of quality random numbers, repeatable with the same seed. Great for demos! (make
  the game run on the random generator, if no bugs or different inputs, it will do the same each time!)
- Auto View On
- Auto View Off
- View
- Get Palette
- Set Line
- Scanshift
- Put Key
- Clear Key
- Mouse coordinates are now reported as a floating point number with between pixels precision (if the AMOS screen is zoomed!)
- Mouse coordinates are no longer set to zero when the pointer leaves the AMOS application screen (stay as they were on exit)
- = Mouse Wheel *new*. Returns -1, 0 or 1 (and multiples) when the mouse wheel is moved. This value is reset after having been consulted.

## **AMOS 2 Compiler version 0.1 - 24/02/2019**

Screens complete!

### Bugs removed:
- Scroll Off did not work in some cases
- Colour use as a function reported a syntax error
- Manifest saved on a Mac reported an 'illegal manifest' during compilation
- Compiler error reporting was screwed
- Paper and Pen reported 'illegal text window parameter' errors for a number of color greater than the numberof colors in the screen, instead of using the modulo of the color to the numkber of colors of the screen as in original AMOS
- Big cleanup in the manifest (you have to change it again). Now it is OK! Parameters in 'emulation' are simpler and make much more sense, have a look!
- Big cleanup in the hardware / software coordinates system. Should work fine now, please test hZone, mouse coordinates and this kind of functions...

### New
- Screen Copy
- Screen Clone
- Screen Display
- = Screen Colour
- = Screen Width
- = Screen Height
- Screen To Front
- Screen To Back
- Screen Hide
- Screen Show
- = X Hard
- = Y Hard
- = X Screen
- = Y Screen
- Screen
- Dual Playfield
- Default Palette
- Colour back
- Clip
- Scroll
- Def Scroll
- Set Transparent
- Stop Transparent
- Forgot about Dual Priority. For next version!

## **AMOS 2 Compiler version 0.1 - 22/02/2019**

Warning: the format of the manifest has changed! You will get a compilation error if you do not replace yoru manifest by the one contained in the 'empty_application' folder.

### Bugs removed:
- Input created bugs for loops later in the application

### New
- Screen Open
- Screen Close
- Screen Display
- Screen Offset
- Lower, Hires, Laced and for AMOS 2, HalfBright
- Changes in the manifest will be documented over the week-end


## **AMOS 2 Compiler version 0.1 - 21/02/2019**

Warning: the format of the manifest has changed! You will get a compilation error if you do not replace yoru manifest by the one contained in the 'empty_application' folder.

### Bugs removed:
- Procedures with more than one parameter reported a syntax error
- Hexadecimal numbers reported a syntax error for some letters between A and F
- Some syntax error in expressions were not reported and compiled into invalid code
- You can now compile without a 'resources' folder, but a warning message is displayed

### New
- New compilation message indicating the syntax and speed manifest parameters
- manifest: compilation/mode changed to 'speed'...
- manifest: new entry in 'compilation': 'syntax'. Please note that only Colors and Palette implement this today. Can take the following values:
* 'amos1.3' -> supports all original AMOS 1.3 commands, refuses AMOSPro and AMOS 2 commands. Total graphical emulation of the Amiga, remapped graphics with copper simulation, palette rotation and color animations. Function parameters must follow the original values, example colors are in the form $RGB and not $RRGGBB.
* 'amosPro' -> as above, accepts AMOSPro commands, refuses AMOS 2 commands, will refuse any new AMOS 2 instruction.
* 'amos2' -> 'modern' mode. True color graphics, no limits in screen size and positions, no palette animation, no copper effects, no hardware limitations (for example screen positions starting at a multiple of 16 etc.). MUCH faster mode. Parameters with modern values, example colors are in the form $RRGGBB.
- support of Amiga screen hardware resolution (more doc to come)
- support of 'hires' and 'laced' in manifest
- separation of browser canvas scaling (can be anything), and internal screen scaling (to improve precision of graphics) without influence on coordinates inside of the application


## **AMOS 2 Compiler version 0.1 - 20/02/2019**

### Bugs removed:
- Shared and Global did not work

### New
- A variable does not need to be defined in the main code to be used as shared uin a procedure. It will be automatically created for you. Goal is to allow Instructions and Function to reserve variable with which they can communicate (like an array). Warning: you must be careful with names, not to interfere with the variable at the root level.
- Cannot read directory error message implemented

## **AMOS 2 Compiler version 0.1 - 18/02/2019**

### Bugs removed:
- Min and Max reported a syntax error

### New
- Instruction / End Instruction
- Function / End Function
- Double Buffer
- Screen Swap
- Files in the 'resources' folder of the application are now copied into the 'html/resources' folder when compiling
- Boing Waves Demo
- The 'Boob' instruction set in the Boing Waves demo, copy and paste it for you own demos!


## **AMOS 2 Compiler version 0.1 - 17/02/2019**

### Bugs removed:

### New
- Input$
- C type remarks: // and /* */ (can be nested, not like in C)
- Print Using complete
- Inclusion of Javascript code within the Basic code, between { and }, example:

Print "We are in Basic"
A$ = "Great!"
{
	console.log( "We are in Javascript! " + this.vars.A$ );		// Easy to get Basic variables!
}

## **AMOS 2 Compiler version 0.1 - 16/02/2019**

### Bugs removed:

### New
- Support of hexadecimal numbers, example $000F
- Support of binary numbers, example %00000001
- Print Using, only ~ supported in this version (the rest later!)

## **AMOS 2 Compiler version 0.1 - 15/02/2019**

### Bugs removed:
- Else If did not work (in runtime, compiled fine), tests were not executed in a sequence.

### New
- Mid$()= / Left$= / Right$= : Left$, Mid$ and Right$ as they worked in the original AMOS


## **AMOS 2 Compiler version 0.1 - 14/02/2019**

### Bugs removed:
- Some errors were not reported properly m(strange message)
- Parentheses at start of parameters in instructions caused a syntax error, example: locate ( A + B), ( C + D )
- AND and OR in evaluation only worked if both sides had the same type. Example this did not work: If A = "A" AND B = 2 Then...

### New
- empty_application now has the full 32 colour AMOS Palette


## **AMOS 2 Compiler version 0.1 - 13/02/2019**

### Bugs removed:
- Error messages were sometimes wrong in the compiled application

### New
- Input
- Timer, =Timer (reserved variables implemented)
- Key$, =Key$
- Scan$()

## **AMOS 2 Compiler version 0.1 - 12/02/2019**

### Bugs removed:
- The left of windows with borders was filled with the color of the paper
- Wind Size had problem when increasing the size of the window horizontally
- CUp, CLeft, CRight and CDown were not positionning the cursor if it went out of bounds, not scrolling the screen and crashed if the cursor went over the
- Curs Off / Curs On crashed

### New
- The colors of the flashing text cursor are now the same as in the original AMOS

## **AMOS 2 Compiler version 0.1 - 11/02/2019**

A lot of progress today!

### Bugs removed:
- Set Buffer reported a syntax error
- Max and Min reported a syntax error
- Scroll Off Was not wrapped to the top of the screen
- Ink did not accept 3 parameters
- Global and Shared did not accept more than one variable
- Global did not work with arrays

### New instructions:
- Remember X / Y
- Memorize X / Y
- Wind Save (implemented the proper way, as in AMOS)
- Wind Close
- Wind Move
- Wind Size
- CMove
- CLine
- Reserve Zone
- Reset Zone
- Set Zone
- = Pen$
- = Paper$
- = At( x, y )... Print At( 10, 10 )
- = CUp$
- = CDown$
- = CLeft$
- = CRight$
- = CMove$
- = X Graphic
- = Y Graphic
- = Zone
- = HZone
- = Scin
- = Mouse Screen
- = Mouse Zone

Please test the new text window instructions!

:)

## **AMOS 2 Compiler version 0.1 - 10/02/2019**

### Bugs removed:
- Else caused a syntax error
- Step caused a syntax error
- =Point did not point to the good pixel and return bad values
- Break Off had no effect
- Wait Vbl was not waiting for anything!

### New instructions:
- Not

## **AMOS 2 Compiler version 0.1 - 09/02/2019**

### Bugs removed:
- Expression evaluation: a function within a function generated a syntax error. Spent the whole morning on this, I had to re-write a lot of code, there was a logical error... :(
- Arrays with the same name as normal variables were not accepted

### New instructions:
- Wait Vbl

### New:
- You can now ommit parameters in instructions, like 'locate ,10'
- Have a look in the 'applications' folder, I have added demos
- If you want to create a new application, just copy the folder 'empty_application' and rename it!
- manifest: new entry 'compilation', in which you can set the compilation mode

-> safe: will never crash the browser, even in case of non controlled loops. Slow.

-> fair: *should* ;) never crash the browser and still provide a nice speed of execution. This is the default mode...

-> graphics: if you have a long loop (many turns of the loop) with time-consuming graphic instructions, this might crash the browser. In this case, use this mode. Very slow, but as most of the time is taken by the graphic instructions, this should have no effect.

-> fast: full speed Scotty! Long loops should have a Wait Vbl or a Wait command in them. Just try! If it does not work, use another mode...

## **AMOS 2 Compiler version 0.1 - 07/02/2019**

Bugs removed: (might have forgotten some, should keep a log!)
- & and | in condition evaluation reported an error
- And yes, I forgot all the other ones... :| but it works better and better each day!

New instructions:
- Palette
- Colour
- Wind Open
- Window
- Wind Save

Tomorrow: I complete the text windows. Should have done that today, but making the animated Julia demo was more fun!

Note: a complete list of what is implemented and not implemented will come soon. If you want to know, open the file 'compiler/tokens.js'. Every line where you can find a 'compile:' property is implemented!

## **AMOS 2 Compiler version 0.1 - 02/02/2019**

Bugs removed:
- Negative numbers at certain positions in expressions returned a syntax error
- HScroll did not move the entire line
- If without End If did not report a compilation error
- Bar used to draw the filled part out of the correct zone
- Consecutive +( in expressions returned a syntax error
- Plot did not set the position of the graphic cursor after being called
- Draw and Draw to of horizontal lines left a blank area of half a pixel on the left (but now antialiasing is back in line drawing... got to find a solution to this)
- Complex expressions with brackets could report syntax errors even if they were correct
- Arrays were not initialised with 0 or ""

New Instructions:
- Inc
- Dec
- =X Curs
- =Y Curs

## **AMOS 2 Compiler version 0.1 - 01/02/2019**

Bugs removed:
- Parameters were not passed to procedures
- End Proc did not support return parameters
- Mouse Key returned garbage
- Print "\" did not work
- Several instructions with no parameters on the same liune generated a syntax error
- The current instruction was still executed even in case of an error
- Antialiasing caused line function to work improperly
- Rnd( X ) used to report numbers up to X - 1 only
- Hscroll and Vscroll did not work
- While/Do/For/Repeat without Wend/Loop/Next/Until did not report error during compilation

New instructions of today!
- Wait
- =Bin$
- =Hex$
- =Instr
- Pop Prop ...  AND you can now do Pop Proc[ returnParameter ]
- Polyline
- Polygon

## **AMOS 2 Compiler version 0.1 - 31/01/2019**

Bugs removed:
- Negative numbers induced a syntax error in expression evaluation
- Ink used to report errors for ink number greater than the number of colors of the screen
- Comma in Print command reported a syntax error
- Assignement of Mid$, Left$, Right$ to a variable reported an error
- Box and Bar used to draw wrong width and height
- Tab$ had unpredictable behaviors
- For / Next loops with floating points numbers did not have the same behavior as original AMOS (wrong behavior, will not be present in future mode)

New instructions
- Wait Key
- =Asc(). Note: conversion table for Amiga specific characters not yet done, it returns the Javascript UTF-8 code.
- =Repeat$
- Pop (forgot this one in structure!)
- Swap
- True / False

## **AMOS 2 Compiler version 0.1 - 30/01/2019**

In this version, some bugs gone!

- Data: accepts large number of datas
- Else If: implemented
- Set tab: implemented
- Left$/Mid$Right$: work
- functions like Int() used to report syntax error
- Space$: works
- <> as operator (you can also use !=)
- Clw: reset the position of the cursor
- Tab$ works
- Val: returns 0 if string is not a number
- Str$ and Print 'number' now puts a space in front as in original AMOS
- Under On: works (note that 'Writing' is not implemented)


## From previous release ##

A lot of work done this week.

- Shared and Global keywords, allowing you to control how procedures deal with variables
- Dim, single and multidimensional arrays
- Data, Read, Restore
- Proper keyboard handling. You now end a program by pressing the classic CONTROL-C
- Inkey$: keycodes are converted to the Amiga equivalent to be compatible
- Mouse handling. X Mouse and Y Mouse as functions (you cannot set the position of the mouse for the moment)
- Change Mouse, Show, Show on, Hide, Hide on, Mouse Key, Mouse Click
- Error messages and error management in the running application. You now get proper 'Illegal function call' errors when you call a function with improper parameters
- Math function: Sgn, Abs, Int, PI#, Min, Max, Sin, Cos, Tan, Asin, Acos, Atan, Sqr, Log, Exp (not tested :) - still to do: Hsin, Hcos, HTan (trigonometrical functions only in degrees for the moment)
- Graphic functions: Plot, Draw, Draw to, Circle, Ellipse, Bar, Box, Gr Locate, =Xgr, = Ygr, Ink (but for the pattern parameter),
- New option (set as default in this version) to include the line number of each instruction in the compiled application, allowing proper runtime errors reporting (like 'Illegal function call error at line 10 column 5)

To sum up:
- Compiler -> 95% done
- Control instruction (like For/Next - If/Then etc) -> 100% done
- Procedures -> 100% done
- Error management (on error goto / on error proc / error reporting) -> 100% done
- Keyboard and mouse -> 50% done
- Graphic functions -> 75% done (still Paint and patterns to do)
- Text windows -> 50% done
- Mathematical functions -> 80% done
- String functions -> 75% done

The Helloworld application included in this folder contains a little (very little, please make some more impressive) demo of the new graphical and math functions.

AMOS 2 is taking shape nicely. Most of the work on the compiler itself is done, adding new instructions or function is a matter of minutes, I can concertrate on the runtime library that makes the program work.

My plan for the week to come:

- Finish the graphical instructions, implement graphic fonts
- Finish the remaining mathematical functions
- Complete the text Windows (Wind Open, Wind Close etc), find a better default font
- Implement Pixi.js as a renderer

If I manage to keep on this pace, I see Sprites, Bobs and AMAL completed at the end of February (can't wait to program AMAL, gonna be a lot of fun in Javascript!)... And we will be able to program the first real games in AMOS 2!

See you next week!

Francois

# How to use?

## Executables
You will find the executable version of the compiler at: https://drive.google.com/drive/folders/189QBsS_Py9TUWE4jfacxpkuwjQ4j1TBU?usp=sharing

- amosc-win.exe -> for Windows
- amosc-linux -> for Linux
- amosc-macos -> for MacOS

Yeah I know, these files are quite big. I will try to find another package maker for node. Anyway, they launch real quick. Copy the file at the root of the 'amos-2' directory.

## Try it!
This directory contains a Hello world application in the 'application' folder. To try the compiler, open a command line window, go in the directory where the compiler is located (normally the root of the 'amos-2' directory), and type:

amosc-win.exe ./applications/helloworld

You should obtain a message:
'Compilation successful.'

Now open a explorer, and look inside of the 'applications/helloworld' folder. You will find a 'html' folder that contains the compiled application ready to be launched, the entry point being the file 'index.html'. To launch this application, open your browser and type the full path to the index.html file in the address bar. The application should be displayed in your browser!

## Create your own AMOS 2 application

To create your own AMOS 2 application, follow the steps:

- copy the 'helloworld' folder into another folder in the 'applications' directory
- rename 'helloworld.amos' to 'myprogram.amos'
- open your favorite source code editor
- load the file 'manifest.hjson', it contains the default definition of the program. Change the section 'infos/start' and enter the name of your .amos file. You can also change the default screen size and color. The 'display' section contains the definition of the main canvas element that will contain the display. These information are copied into the 'index.html' file when compiling. Note the 'zoomX' and 'zoomY' values: they are mandatory to scale the default AMOS red screen (320 x 200) into a size that is compatible with today's resolution. I have not yet worked on full screen, it will come in the future.
- load the file 'myprogram.amos' and type your code! Please note that this is the very first public version of the compiler, I have not tested real code. Please consider it as a toy for you to play with, with simple code.
- Unlike the original AMOS, the names of the variable IS case-sensitive. So a variable name 'hello' will not be the same as 'Hello'. I think I will put that as an option for compilation.

Of course, in the future, you will be able to create a new application with a simple command:

amosc -new myprogram.amos ... and it will copy the manifest and create the proper folders.

## Note on the code produced

Once you have compiled a program, you will find the code in the file 'application.js' in the 'myapplication/html/run' folder. You might be surprised by the code produced, and how it is sliced in blocks. It is the only way to proceed to port a language such as Basic into Javascript. Javascript does not allow endless loops or gotos, so the program must be run 'from above' and control on the execution must be possible at each time.
- first to avoid taking all the processor time, and render Chrome inoperant.
- second to allow goto and error catching the Basic way, with On Error Goto...

### The compiler has for the moment two way of producing code.

- 'fast' code: if your program do not contain any 'On Error', the Javascript instructions are grouped as much as possible, and the number of block is reduced.
- 'slow' mode: if an 'On Error' instruction has been detected in the first pass of compilation, the compiler will produce one block of code per instruction. It is the only way to allow 'Resume' and 'Resume Next'. Such mode wuill be slower, I still have to do some benchmarking.

### Where is the main loop?

The code that calls all the blocks contained in 'application.js' can be seen in the file 'runtime/amos.js'. You will see that the loop is verfy simple and no time is lost between calling each block of code. You will also see that I artificially slow down the execution.

### About the speed of execution:

I am sure that many of you will try to have an idea of the speed of the code produced. It is actually quite fast. Of course, in order not to clutter the
browser, I limit the speed a little, but as you can see with the Amimated Julia demo, one can achieve quite nice speeds...
My first goal is to allow native Amiga AMOS programs to run, and I am sure that I will have to slow down the code even further to achieve compatibility.
The 'future' mode will have a much cleaner compilation (as some instructions like 'Goto' or 'Resume' will be forbidden, thus enabling me to produce much cleaner code.

### In the future

I will make a 'fastest' mode that will NOT cut the code into blocks, or only the minimum. The code produced will be then pure Javascript and the speed of execution will be comparable to the speed of real Javascript.

I will also implement the possibility to insert real Javascript code in the middle of the Basic code, for time-critical sections. Example:

```
Procedure GoFast[ A, B ]
    {
        // We are in Javascript now!
        this.vars.retValue = 0;                              // This variable will be accessible in Basic
        for ( var x = 0; x < 100000; x++ )
        {
            this.vars.retValue += this.vars.pA * this.vars.pB;         // Stupid example, but shows how to get the variables.
        }
    }
    ' Back to Basic!
    Param retValue                                      ' Return the variable defined in the Javascript portion
End Proc
```
- Hardware rendering with Pixi.js, 1000s of sprites, 100s of bobs
- Real 'classic Amiga emulation' mode with the same speed, palette animations, copper simulation and so on (found how to do it, should be fast)
- Proper tracker music support
- Implementation of the major AMOS extensions
- Amiga fonts support
- etc. etc. etc.

## Please help and share!

Although it is an early version, any report on things that are supposed to work and don't is welcomed. Just post your comments on Patreon.

Please share!

I am doing this project alone on my free time. It is a big project, and any support on Patreon is more than welcomed. I do not post on Facebook anymore to save time (a lot of time saved since I stopped going there), so you are the ONLY people who can make this project popular and help increase the number olf Patrons. Plkease share such posts in the Amiga forums on FB, on your page... everywhere you can!

And if you are not yet one of my Patrons, you are welcomed to help: www.patreon.com/francoislionet

All for today! See you next week for a new version. Until then I will post my progress day by day in my thread.

Francois
