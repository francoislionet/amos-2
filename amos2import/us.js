/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * Import AMOS errors
 *
 * List of messages US
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/05/2019
 */

const messages = require( './messages' );

texts =
[
    "converting_code:                       Converting code...",
    "converting_banks:                      Converting banks...",
    "creating_manifest:                     Creating manifest...",
    "file_error:                            Disk error, conversion failed. Please check the path...",
    "conversion_succesfull:                 Conversion succesfull. %1 sprite(s) converted to PNG."
];
errors =
// <ERRORS-START>
[

];

messages.addLanguage( 'US', texts, errors );