/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * Import AMOS
 *
 * The return of the Basic!
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 22/12/2018
 */
const version = '0.1 - 04/05/2019';

var Exec = require('child_process').execSync;
var Path = require('path')
const DateAndTime = require( 'date-and-time' );
const fs = require( 'fs' );
const mkdirp = require( 'mkdirp' );
const minimist = require( 'minimist' );
const pathSeparator = require( 'path' ).sep;
const HJSON = require( 'hjson' );
const messages = require( './messages' );
const utilities = require( './utilities' );
const us = require( './us' ) ;

var sourcePath, currentPath, destinationPath;
var options = {};

// Get command line arguments
var argv = minimist( process.argv.slice( 2 ) );
if ( argv[ '_' ].length == 0 )
{
	messages.print( 'title', version );
	messages.print( 'author', version );
	process.exit();
}
else if ( argv[ '_' ].length == 1 )
{
	sourcePath = argv[ '_' ][ 0 ];
}

// Make destination directories
sourcePath = utilities.replaceStringInText( sourcePath, '\\', '/' );
var applicationName = utilities.getFilename( sourcePath );
var currentPath = Path.resolve( process.cwd(), '.' );
currentPath = utilities.replaceStringInText( currentPath, '\\', '/' );
if ( utilities.getDirectory( sourcePath ) == '' )
    sourcePath = currentPath + '/' + applicationName + '.' + utilities.getFileExtension( sourcePath );
var destinationPath = currentPath + '/applications/' + applicationName;
var utilitiesPath = currentPath + '/utilities/windows/';
console.log( sourcePath );

try
{
    // Make the directory structure
    mkdirp.sync( destinationPath );
    mkdirp.sync( destinationPath + '/resources/images' );
    mkdirp.sync( destinationPath + '/resources/fonts');
    mkdirp.sync( destinationPath + '/filesystem');

    // Save the code of the application
    messages.print( 'converting_code' );
    var stdout = Exec( utilitiesPath + 'listamos.exe ' + sourcePath, { cwd: destinationPath } ).toString();
    fs.writeFileSync( destinationPath + '/main.amos', stdout, { encoding: 'utf8' } );

    // Save the content of the banks
    messages.print( 'converting_banks' );
    stdout = Exec( utilitiesPath + 'dumpamos.exe ' + sourcePath, { cwd: destinationPath } ).toString();;

    var spriteNumber = 0;
    var files = utilities.getDirectory( destinationPath + '/', false );
    for ( var f = 0; f < files.length; f++ )
    {
        var file = files[ f ];
        if ( !file.isDirectory )
        {
            if ( file.name.indexOf( 'sprite' ) == 0 )
            {
                var number = parseInt( file.name.substring( 6 ), 16 );
                spriteNumber++;
                stdout = Exec( utilitiesPath + 'Nuclex.ImageConverter ' + file.path + ' ' + destinationPath + '/resources/images/' + number + '.png', { cwd: destinationPath } ).toString();
            }
        }
    }

    // Clear the directory
    for ( var f = 0; f < files.length; f++ )
    {
        var file = files[ f ];
        if ( !file.isDirectory && file.name.indexOf( '.amos' ) < 0 )
        {
            fs.unlinkSync( file.path );
        }
    }

    // Create the manifest
    messages.print( 'creating_manifest' );
    var manifestPath = currentPath + '/default/manifest.hjson';
    var manifest = fs.readFileSync( manifestPath, { encoding: 'utf8' } );
    manifest = utilities.fixJson( manifest );
    manifest = HJSON.parse( manifest );
    manifest.infos.applicationName = applicationName;
    var now = new Date();
    manifest.infos.date = 'Created on ' + DateAndTime.format( now, 'ddd MMM DD YYYY' );
    var header = fs.readFileSync( currentPath + '/utilities/manifestheader.js', { encoding: 'utf8' } );
    var json = header + HJSON.stringify( manifest );
    fs.writeFileSync( destinationPath + '/manifest.hjson', json, { encoding: 'utf8' } );
}
catch ( error )
{
    messages.print( 'file_error' );
}
messages.print( 'conversion_succesfull', spriteNumber );
