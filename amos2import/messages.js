/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * Messages and errors
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/12/2018
 */

const utilities = require( './utilities' );

const messages = {};
const errors = {};
var language = 'US';

module.exports.addLanguage = function( lan, mes, err )
{
	messages[ lan ] = mes;
	errors[ lan ] = err;
}
module.exports.error = function( id, param1, param2, param3 )
{
	var error = getText( id, errors[ language ] );
	var atLine = getText( 'at_line', messages[ language ], param1, param2, param3 );
	console.error( error + ' ' + atLine );
};
module.exports.print = function( id, param1, param2, param3 )
{
	var message = getText( id, messages[ language ], param1, param2, param3 );
	console.error( message );
};
module.exports.getMessage = function( id, param1, param2, param3 )
{
	return getText( id, messages[ language ], param1, param2, param3 );
};
module.exports.getErrorList = function()
{
	return errors[ language ];
};
function getText( id, list, param1, param2, param3 )
{
	if ( typeof param1 == 'undefined' )
		param1 = '';
	if ( typeof param2 == 'undefined' )
		param2 = '';
	if ( typeof param3 == 'undefined' )
		param3 = '';
		
	id += ':';
	var message = 'Message not found ' + id + '(%1, %2, %3)';
	for ( var l = 0; l < list.length; l++ )
	{
		if ( list[ l ].indexOf( id ) == 0 )
		{
			message = utilities.trimString( list[ l ].substring( id.length ) );
			break;
		}
	}
	
	if ( message.indexOf( '%1' ) >= 0 )
		message = utilities.replaceStringInText( message, '%1', '' + param1 );
	if ( message.indexOf( '%2' ) >= 0 )
		message = utilities.replaceStringInText( message, '%2', '' + param2 );
	if ( message.indexOf( '%3' ) >= 0 )
		message = utilities.replaceStringInText( message, '%3', '' + param3 );

	return message;
};

