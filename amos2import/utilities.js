/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * Utilities
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/12/2018
 */
const fs = require( 'fs' );
PNG = require('pngjs').PNG;

function getDirectory( path, recursive, result )
{
	if ( !result )
		result = [];

	var files;
	try
	{
		files = fs.readdirSync( path );
	}
	catch( err )
	{
		var toto = 2;
	}
	if ( files )
	{
		for ( var f = 0; f < files.length; f++ )
		{
			var sPath = path + files[ f ];
			var stats = fs.statSync( sPath );
			if ( !stats.isDirectory() )
			{
				result.push(
				{
					name: files[ f ],
					path: sPath,
					isDirectory: false
				} );
			}
			else
			{
				if ( recursive )
				{
					var newResult = getDirectory( sPath + '/', recursive );
					result.push(
					{
						name: files[ f ],
						path: sPath,
						isDirectory: true,
						files: newResult
					} );
				}
				else
				{
					result.push(
					{
						name: files[ f ],
						path: sPath,
						isDirectory: true
					} );
				}
			}
		}
	}
	return result;
};
module.exports.getDirectory = getDirectory;

function convertArrayBufferToString( arraybuffer )
{
	var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	var bytes = new Uint8Array( arraybuffer ),
	i, len = bytes.length, base64 = "";

	for (i = 0; i < len; i+=3)
	{
		base64 += chars[bytes[i] >> 2];
		base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
		base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
		base64 += chars[bytes[i + 2] & 63];
	}

	if ((len % 3) === 2)
	{
		base64 = base64.substring(0, base64.length - 1) + "=";
	}
	else if (len % 3 === 1)
	{
		base64 = base64.substring(0, base64.length - 2) + "==";
	}
	return base64;
}
module.exports.convertArrayBufferToString = convertArrayBufferToString;

function replaceMissingProperties( destination, source )
{
	for ( var p in source )
	{
		if ( typeof destination[ p ] == 'undefined' )
		{
			destination[ p ] = source[ p ];
		}
		if ( isObject( source[ p ] ) )
		{
			replaceMissingProperties( destination[ p ], source[ p ] );
		}
	}
	return destination;
};
module.exports.replaceMissingProperties = replaceMissingProperties;

module.exports.replaceStringInText = function( text, mark, replacement )
{
	var pos = text.indexOf( mark );
	while( pos >= 0 )
	{
		text = text.substring( 0, pos ) + replacement + text.substring( pos + mark.length );
		pos = text.indexOf( mark );
	}
	return text;
};
module.exports.getFormattedDate = function()
{
    var date = new Date();

    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var str = day + '/' + month + '/' + date.getFullYear() + "-" + hour + ":" + min + ":" + sec;

    return str;
};
module.exports.extractCode = function( source, mark )
{
	var start = source.indexOf( '<' + mark + '-START>' );
	var end = source.indexOf( '<' + mark + '-END>' );
	if ( start >= 0 && end >= start )
	{
		var text = source.substring( start, end );
		var firstLine = findNextLine( text, 0 );
		var lastLine = findStartOfLine( text, text.length - 1 );
		return text.substring( firstLine, lastLine );
	}
	return '';
};
function findNextLine( text, pos )
{
	if ( pos >= text.length )
		return -1;

	while( text.charCodeAt( pos ) != 13 && text.charCodeAt( pos ) != 10 && pos < text.length )
		pos++;
	if ( text.charCodeAt( pos ) == 13 )
	{
		if ( pos + 1 < text.length && text.charCodeAt( pos + 1 ) == 10 )
			return pos + 2;
		return pos + 1;
	}
	return pos + 1;
};
module.exports.findNextLine = findNextLine;
function findStartOfLine( text, pos )
{
	while( text.charCodeAt( pos ) != 10 && pos > 0 )
		pos--;
	if ( pos > 0 )
	{
		pos++;
		return pos;
	}
	return -1;
};
module.exports.findStartOfLine = findStartOfLine;

function trimString( str, position )
{
	var start = 0;
	position = typeof position == 'undefined' ? { left: true, right: true } : position;
	if ( position.left )
	{
		while( start < str.length && ( str.charCodeAt( start ) == 32 || str.charCodeAt( start ) == 9 || str.charCodeAt( start ) == 10 || str.charCodeAt( start ) == 13 ) )
			start++;
	}
	var end = str.length;
	if ( position.right )
	{
		while( end > 0 && ( str.charCodeAt( end - 1 ) == 32 || str.charCodeAt( end - 1 ) == 9 || str.charCodeAt( end - 1 ) == 10 || str.charCodeAt( end - 1 ) == 13 ) )
			end--;
	}
	if ( end > start )
		return str.substring( start, end );
	return '';
}
module.exports.trimString = trimString;

function printLine( str )
{
	var result = '';
	for ( var p = 0; p < str.length; p++ )
	{
		if ( str.charAt( p ) == '\t' )
			result += '    ';
		else
			result += str.charAt( p );
	}
	return result;
}
module.exports.printLine = printLine;

function replaceSpacesByUnderscores( str )
{
	var result = '';
	for ( var p = 0; p < str.length; p++ )
	{
		var c = str.charAt( p );
		if ( c == ' ' )
			result += '_';
		else
			result += c;
	}
	return result;
}
module.exports.replaceSpacesByUnderscores = replaceSpacesByUnderscores;

function isObject( item )
{
    return typeof item != 'undefined' ? (typeof item === "object" && !Array.isArray( item ) && item !== null) : false;
};
module.exports.isObject = isObject;

function isArray( item )
{
    return typeof item != 'undefined' ? Array.isArray( item ) : false;
};
module.exports.isArray = isArray;

function getFilename( path )
{
	var posPoint = path.lastIndexOf( '.' );
	if ( posPoint < 0 )
		posPoint = path.length;

	var posSlash1 = path.lastIndexOf( '/' );
	var posSlash2 = path.lastIndexOf( '\\' );
	if ( posSlash1 >= 0 && posSlash2 >= 0 )
		posSlash1 = Math.max( posSlash1, posSlash2 ) + 1;
	else if ( posSlash1 < 0 && posSlash2 < 0 )
		posSlash1 = 0;
	else if ( posSlash1 < 0 )
		posSlash1 = posSlash2 + 1;
	else
		posSlash1++;

	return path.substring( posSlash1, posPoint );
};
module.exports.getFilename = getFilename;

function getFileExtension( path )
{
	var posPoint = path.lastIndexOf( '.' );
	if ( posPoint < 0 )
		return '';
	return path.substring( posPoint + 1 );
};
module.exports.getFileExtension = getFileExtension;

// Fix bug in HJSON library: if EOL=CR (Macintosh) put LF
function fixJson( json )
{
	var jsonFixed = '';
	for ( var p = 0; p < json.length; p++ )
	{
		jsonFixed += json.charAt( p );
		if ( json.charCodeAt( p ) == 13 && p + 1 < json.length && json.charCodeAt( p + 1 ) != 10 )
			jsonFixed += String.fromCharCode( 10 );
	}
	return jsonFixed;
}
module.exports.fixJson = fixJson;

function deleteFilesInDirectory( path, options )
{
	var files;
	var errors = [];
	options = typeof options == 'undefined' ? {} : options;
	try
	{
		files = fs.readdirSync( path );
	}
	catch( err ){}
	if ( files )
	{
		for ( var f = 0; f < files.length; f++ )
		{
			var fPath = path + files[ f ];
			var stats = fs.statSync( fPath );
			if ( !stats.isDirectory() )
			{
				try
				{
					fs.unlinkSync( fPath );
				}
				catch( err )
				{
					errors.push( { error: 'cannot_delete_file', parameter: fPath } );
				}
			}
			else if ( options.recursive )
			{
				deleteFilesInDirectory( fPath + '/', options );
				try
				{
					fs.unlinkSync( fPath );
				}
				catch( err )
				{
					errors.push( { error: 'cannot_delete_directory', parameter: fPath } );
				}
			}
		}
	}
	else
	{
		errors.push( { error: 'cannot_read_directory', parameter: path } );
	}
	if ( errors.length )
		return errors;
	return false;
};
module.exports.deleteFilesInDirectory = deleteFilesInDirectory;

function getCollisionMask( image, threeshold = 10 )
{
	var arrayBuffer = new ArrayBuffer( image.width * image.height );
	var dataView = new Uint8Array( arrayBuffer );
	for ( var y = 0; y < image.height; y++ )
	{
		for ( var x = 0; x < image.width; x++ )
		{
			if ( image.data[ ( y * image.width + x ) * 4 + 3 ] >= threeshold )
			{
				dataView[ y * image.width + x ] = 0xFF;
			}
		}
	}
	return { buffer: arrayBuffer, view: dataView, width: image.width, height: image.height };
};
module.exports.getCollisionMask = getCollisionMask;

function loadImage( path )
{
	try
	{
		var data = fs.readFileSync( path );
		return PNG.sync.read( data );
	}
	catch( error )
	{		
	}
	return null;
};
module.exports.loadImage = loadImage;
