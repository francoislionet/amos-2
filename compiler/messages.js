/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * Messages and errors
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/12/2018
 */

const utilities = require( './utilities' );

const messages = {};
const errors = {};
const warnings = {};
const compilerWarnings = {};
const compilerErrors = {};
var language = 'US';

// Levels of verbosity
const VERBOSE_QUIET = 0;
const VERBOSE_ERRORS = 1;
const VERBOSE_WARNINGS = 2;
const VERBOSE_MESSAGES = 3;
const VERBOSE_VERBOSE = 4;
module.exports.VERBOSE_QUIET = VERBOSE_QUIET;
module.exports.VERBOSE_ERRORS = VERBOSE_ERRORS;
module.exports.VERBOSE_WARNINGS = VERBOSE_WARNINGS;
module.exports.VERBOSE_MESSAGES = VERBOSE_MESSAGES;
module.exports.VERBOSE_VERBOSE = VERBOSE_VERBOSE;

// List of errors
var errorList = [];
var numberOfErrors = 0;
var numberOfWarnings = 0;
var verbosity = VERBOSE_MESSAGES;

// Set the level of verbosity
function setVerbosity( verbose )
{
	verbosity = verbose;
}
module.exports.setVerbosity = setVerbosity;

// Add the current language
function addLanguage( lan, mes, err, warn, compWarnings, compErrors )
{
	messages[ lan ] = mes;
	errors[ lan ] = err;
	warnings[ lan ] = warn;
	compilerWarnings[ lan ] = compWarnings;
	compilerErrors[ lan ] = compErrors;
};
module.exports.addLanguage = addLanguage;

// Error handling
function pushError( err, fatal )
{
	err.level = module.exports.MESSAGE_ERROR;
	errorList.push( err );
	if ( err.error || err.compilerError )
		numberOfErrors++;
	if ( err.warning || err.compilerWarning )
		numberOfWarnings++;
	if ( fatal )
		throw {};
}
module.exports.pushError = pushError;

// Simple output
function print_info( id, param1, param2, param3 )
{
	if ( verbosity >= VERBOSE_MESSAGES )
	{
		var message = getText( id, messages[ language ], param1, param2, param3 );
		console.log( message );
	}
};
module.exports.print_info = print_info;

function print_verbose( id, param1, param2, param3 )
{
	if ( verbosity >= VERBOSE_VERBOSE )
	{
		var message = getText( id, messages[ language ], param1, param2, param3 );
		console.log( message );
	}
};
module.exports.print_verbose = print_verbose;

function print_always( id, param1, param2, param3 )
{
	var message = getText( id, messages[ language ], param1, param2, param3 );
	console.log( message );
};
module.exports.print_always = print_always;



// Is there errors in the pile?
function isErrors()
{
	return numberOfErrors > 0;
}
module.exports.isErrors = isErrors;

// Is there warnings in the pile?
function isWarnings()
{
	return numberOfWarnings > 0;
}
module.exports.isWarnings = isWarnings;

// Clear the warnings
function clearWarnings()
{
	var temp = [];
	for ( var e = 0; e < errorList.length; e++ )
	{
		var err = errorList[ e ];
		if ( err.error || err.compilerError )
		{
			temp.push( err );
		}
	}
	errorList = temp;
}
module.exports.clearWarnings = clearWarnings;

// Prints out the errors
function printErrors( options )
{
	for ( var e = 0; e < errorList.length; e++ )
	{
		var err = errorList[ e ];
		if ( err.error )
		{
			error( err.error, err.file, err.line, err.column, options );
		}
		else if ( err.compilerError )
		{
			compilerError( err.compilerError, err.file, err.line, err.column, options );
		}
	}
}
module.exports.printErrors = printErrors;

// Print out the warnings
function printWarnings( options )
{
	for ( var e = 0; e < errorList.length; e++ )
	{
		var error =	errorList[ e ];
		if ( error.warning )
		{
			warning( error.warning, error.file, error.line, error.column, options );
		}
		else if ( error.compilerWarning )
		{
			compilerWarning( error.compilerWarning, error.file, error.line, error.column, options );
		}
	}
}
module.exports.printWarnings = printWarnings;

// Print compiler error
function compilerError( id, param1, param2, param3, options )
{
	var errorText = getText( 'compilation_error', messages[ language ] );
	var error = getText( id, compilerErrors[ language ], param1, param2, param3 );
	var message = param1 + ':1:1: ' + errorText + ': ' + error.toLowerCase();
	console.log( message );
};
module.exports.warning = warning;

// Print compiler warning
function compilerWarning( id, param1, param2, param3 )
{
	var warningText = getText( 'compilation_warning', messages[ language ] );
	var warning = getText( id, compilerWarnings[ language ], param1, param2, param3 );
	var message = param1 + ':1:1: ' + warningText + ': ' + warning.toLowerCase();
	console.log( message );
};
module.exports.warning = warning;

// Formatted warnings
// helloWorld.c:5:3: warning: implicit declaration of function ‘prinft’
function warning( id, param1, param2, param3, options )
{
	var warningText = getText( 'warning', messages[ language ] );
	var warning = getText( id, warnings[ language ], param1, param2, param3 );
	var message = param1 + ':' + param2 + ':' + param3 + ': ' + warningText + ': ' + warning.toLowerCase();
	console.log( message );
};
module.exports.warning = warning;

// Formatted errors
function error( id, param1, param2, param3, options )
{
	var errorText = getText( 'error', messages[ language ] );
	var error = getText( id, errors[ language ], param1, param2, param3 );
	var message = param1 + ':' + param2 + ':' + param3 + ': ' + errorText + ': ' + error.toLowerCase();
	console.log( message );
};
module.exports.error = error;

// Returns the text from ID
function getMessage( id, param1, param2, param3 )
{
	return getText( id, messages[ language ], param1, param2, param3 );
};
module.exports.getMessage = getMessage;

// Returns the whole list of runtime errors
function getErrorList()
{
	return errors[ language ];
};
module.exports.getErrorList = getErrorList;

// Computes a text
function getText( id, list, param1, param2, param3 )
{
	if ( typeof param1 == 'undefined' )
		param1 = '';
	if ( typeof param2 == 'undefined' )
		param2 = '';
	if ( typeof param3 == 'undefined' )
		param3 = '';

	id += ':';
	var message = 'Message not found ' + id + '(%1, %2, %3)';
	for ( var l = 0; l < list.length; l++ )
	{
		if ( list[ l ].indexOf( id ) == 0 )
		{
			message = utilities.trimString( list[ l ].substring( id.length ) );
			break;
		}
	}

	if ( message.indexOf( '%1' ) >= 0 )
		message = utilities.replaceStringInText( message, '%1', '' + param1 );
	if ( message.indexOf( '%2' ) >= 0 )
		message = utilities.replaceStringInText( message, '%2', '' + param2 );
	if ( message.indexOf( '%3' ) >= 0 )
		message = utilities.replaceStringInText( message, '%3', '' + param3 );

	if ( message == '.' )
		message = '';
	return message;
};
module.exports.getText = getText;
