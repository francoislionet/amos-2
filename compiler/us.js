/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * List of messages US
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/12/2018
 */

const messages = require( './messages' );

texts =
[
	"title0:												.",
	"title1:												AMOS-2 Compiler version %1",
	"title2:												By Francois 'Amos' Lionet (c) 2018-2019",
	"title3:												Website: https://www.amos2.org",
	"title4:												Support the project on Patreon: http://www.patreon.com/francoislionet",
	"title5:												---------------------------------------------------------------------",
	"creating_directory:									Creating directory: %1",
	"compiled_with:											Compiled with AMOS 2 compiler version %1 on the %2",
	"compilation_success:									Compilation successful, time: %1 seconds.",
	"compilation_failed:									Compilation failed, no code generated...",
	"compiling:												Compiling %1, emulation: %2, speed: %3.",
	"first_pass:											First pass...",
	"second_pass:											Second pass...",
	"compiling_images:										Compiling images...",
	"compiling_sounds:										Compiling sounds...",
	"compiling_fonts:										Compiling fonts...",
	"compiling_filesystem:									Compiling filesystem...",
	"error:													error",
	"warning:												warning",
	"compilation_error:										Error",
	"compilation_warning:									Warning",
	"compiling_extension:									Compiling extension %1",
	"removing_directory:									Removing directory: %1",
	"skip_line:												.",
	"filesaver_saving:										Filesaver saving: ",
	"filesaver_copying:										Filesaver copying: ",
];

compilerWarnings =
[
	"font_not_found:										Font not found",
	"garbage_found_in_folder:								Unnecessary files found in folder",
	"font_not_supported:									Font not supported",
	"file_at_root_filesystem:								Files at root of file system folder are not compiled",
	"screen_not_multiple_of_font_size:						Default screen dimensions are not a multiple of window font sizes",
	"missing_folder:										Missing folder",
];

compilerErrors =
[
	"application_does_not_exist:							AMOS-2 application does not exist.",
	"manifest_not_found:									Application manifest not found.",
	"cannot_delete_file:									Cannot delete file",
	"cannot_delete_directory:								Cannot delete directory",
	"illegal_manifest:										Illegal manifest",
	"illegal_extension_manifest:							Illegal extension manifest",
	"bad_version_of_manifest:								Bad version of manifest",
	"cannot_load_runtime_index:								Cannot load runtime index",
	"cannot_write_file:										Cannot write file",
	"cannot_copy_file:										Cannot copy file",
	"cannot_copy_directory:									Cannot copy directory",
	"cannot_load_file:										Cannot load file",
	"include_before_everything:								Include should be located before any other instruction"
];

warnings =
[
	"warning_variable_not_declared:							variable used without been declared",
];

errors =
[
    "",																									// 0
    "return_without_gosub:                      Return without gosub",                                  // 1
    "pop_without_gosub:                         Pop without gosub",                                     // 2
    "error_not_resumed:                         Error not resumed",                                     // 3
    "cant_resume_to_a_label:                    Can't resume to a label",                               // 4
    "no_on_error_proc:                          No ON ERROR PROC before this instruction",              // 5
    "resume_label_not_defined:                  Resume label not defined",                              // 6
    "resume_without_error:                      Resume without error",                                  // 7
    "error_procedure_must_resume:               Error procedure must resume to end",                    // 8
    "program_interrupted:                       Program interrupted",                                   // 9
    "procedure_not_closed:						Procedure not closed",									// 10
    "out_of_variable_space:                     Out of variable space",                                 // 11
    "cannot_open_math_libraries:                Cannot open math libraries",                            // 12
    "out_of_stack_space:                        Out of stack space",                                    // 13
    "procedure_not_opened:						Procedure not opened",                                  // 14
    "user_function_not_defined:                 User function not defined",                             // 15
    "illegal_user_function_call:                Illegal user function call",                            // 16
    "illegal_direct_mode:                       Illegal direct mode",                                   // 17
    "procedure_not_found:						Procedure not found",									// 18
    "instruction_not_implemented:				Instruction not implemented",							// 19
    "division_by_zero:                          Division by zero",                                      // 20
    "string_too_long:                           String too long",                                       // 21
    "syntax_error:                              Syntax error",                                          // 22
	"illegal_function_call:						Illegal function call",									// 23
	"out_of_memory:								Out of memory",											// 24
	"address_error:								Address error",											// 25
	"string_not_closed:							String not closed",										// 26
	"non_dimensionned_array:					Non dimensionned array",								// 27
	"array_already_dimensionned:				Array already dimensionned",							// 28
	"overflow:									Overflow",												// 29
	"bad_iff_format:							Bad IFF format",										// 30
	"iff_compression_not_recognised:			IFF compression not recognised",						// 31
	"cant_fit_picture:							Can't fit picture in current screen",					// 32
	"out_of_data:								Out of data", 											// 33
	"type_mismatch:								Type mismatch",											// 34
	"bank_already_reserved:						Bank already reserved",									// 35
	"bank_not_reserved:							Bank not reserved",										// 36
	"fonts_not_examined:						Fonts not examined",									// 37
	"menu_not_opened:							Menu not opened",										// 38
	"menu_item_not_defined:						Menu item not defined",									// 39
	"label_not_defined:							Label not defined",										// 40
	"not_data_after_this_label:					No data after this label",								// 41
	"procedure_already_defined:					Procedure already defined",								// 42
	"next_without_for:							Next without for",										// 43
	"font_not_available:						Font not available",									// 44
	"until_without_repeat:						Until without repeat",									// 45
	"block_not_defined:							Block no defined",										// 46
	"screen_not_opened:							Screen not opened",										// 47
	"illegal_screen_parameter:					Illegal screen parameter",								// 48
	"illegal_number_of_colours:					Illegal number of colours",								// 49
	"valid_screen_numbers:						Valid screen numbers range from 0 to 7",				// 50
	"too_many_colours_in_flash:					Too many colours in flash",								// 51
	"flash_declaration_error:					Flash declaration error",								// 52
	"shift_declaration_error:					Shift declaration error",								// 53
	"text_window_not_opened:					Text window not opened",								// 54
	"text_window_already_opened:				Text window already opened",							// 55
	"text_window_too_small:						Text window too small",									// 56
	"text_window_too_large:						Text window too large",									// 57
	"wend_without_while:						Wend without while",									// 58
	"bordered_text_windows_not_on_edge:			Bordered text windows not on edge of screen",			// 59
	"illegal_text_window_parameter:				Illegal text window parameter",							// 60
	"loop_without_do:							Loop without do",										// 61
	"text_window_0_cant_be_closed:				Text window 0 can't be closed",							// 62
	"this_windows_has_no_border:				This window has not border",							// 63
	"no_loop_to_exit_from:						No loop to exit from",									// 64
	"block_not_found:							Block not found",										// 65
	"illegal_block_parameters:					Illegal block parameters",								// 66
	"screens_cant_be_animated:					Screens can't be animated",								// 67
	"bob_not_defined:							Bob not defined",										// 68
	"screen_already_in_double_buffering:		Screen already in double buffering",					// 69
	"cant_set_dual_playfield:					Can't set dual playfield",								// 70
	"screen_not_in_dual_playfield:				Screen not in dual playfield mode", 					// 71
	"scrolling_not_defined:						Scrolling not defined",									// 72
	"no_zones_defined:							No zones defined",										// 73
	"icon_not_defined:							Icon not defined",										// 74
	"rainbow_not_defined:						Rainbow not defined",									// 75
	"copper_not_disabled:						Copper not disabled",									// 76
	"copper_list_too_long:						Copper list too long",									// 77
	"illegal_copper_parameter:					Illegal copper parameter",								// 78
	"file_already_exists:						File already exists",									// 79
	"directory_not_found:						Directory not found",									// 80
	"file_not_found:							File not found",										// 81
	"illegal_filename:							Illegal filename",										// 82
	"disc_is_not_validated:						Disc is not validated",									// 83
	"disc_is_write_protected:					Disc is write protected",								// 84
	"directory_not_empty:						Directory not empty",									// 85
	"device_not_available:						Device not available",									// 86
	"for_without_next:							For without next",										// 87
	"disc_full:									Disc full",												// 88
	"file_is_write_protected_against_deletion:	File is write protected against deletion",				// 89
	"file_is_write_protected:					File is write protected",								// 90
	"file_is_protected_against_reading:			File is protected against reading",						// 91
	"not_an_amigados_disc:						Not an AmogaDOS disc",									// 92
	"no_disc_in_drive:							No disc in drive",										// 93
	"io_error:									I/O error",												// 94
	"file_format_not_recognised:				File format not recognised",							// 95
	"file_already_opened:						File already opened",									// 96
	"file_not_opened:							File not opened",										// 97
	"file_type_mismatch:						File type mismatch",									// 98
	"input_too_long:							Input too long",										// 99
	"end_of_file:								End of file",											// 100
	"disc_error:								Disc error",											// 101
	"instruction_not_allowed_there:				Instruction not allowed there",							// 102
	"illegal_number_of_dimensions:				Illegal number of dimensions",							// 103
	"array_not_dimensionned:					Array not dimensionned",								// 104
	"sprite_error:								Sprite error",											// 105
    "function_not_implemented:					Function not implemented",								// 106
	"syntax_error_in_animation_string:			Syntax error in animation string",						// 107
	"next_without_for_in_animation_string:		Next without for in animation string",					// 108
	"label_not_defined_in_animation_string:		Label not defined in animation string",					// 109
	"jump_to_within_autotest:					Jump to/within autotest in animation string",			// 110
	"autotest_already_opened:					Autotest already opened",								// 111
	"instruction_only_valid_in_autotest:		Instruction only valid in autotest",					// 112
	"animation_string_too_long:					Animation string too long",								// 113
	"label_already_defined_in_animation_string:	Label already defined in animation string",				// 114
	"illegal_instruction_during_autotest:		Illegal instruction during autotest",					// 115
	"amal_bank_not_reserved:					AMAL bank not reserved",								// 116
	"internal_error:							Internal error",										// 117
	"unknown_error:								Unknown error",											// 118
	"cannot_load_file:							Cannot load file",										// 119
	"interface_error_bad_syntax:				Interface error: bad syntax",							// 120
	"interface_error_out_of_memory:				Interface error: out of memory",						// 121
	"interface_error_label_defined_twice:		Interface error: label defined twice",					// 122
	"interface_error_label_not_defined:			Interface error: label not defined",					// 123
	"interface_error_channel_already_defined:	Interface error: channel already defined",				// 124
	"interface_error_channel_not_defined:		Interface error: channel not defined",					// 125
	"interface_error_screen_modified:			Interface error: screen modified",						// 126
	"interface_error_variable_not_defined:		Interface error: variable not defined",					// 127
	"interface_error_illegal_function_call:		Interface error: illegal function call",				// 128
	"interface_error_type_mismatch:				Interface error: type mismatch",						// 129
	"interface_error_buffer_too_small:			Interface error: buffer too small",						// 130
	"interface_error_illegal_n_parameters:		Interface error: illegal number of parameters",			// 131
	"if_without_endif:							If without endif",										// 132
	"not_enough_loops_to_exit:					Not enough loops to exit",								// 133
	"no_loop_to_exit:							No loop to exit",										// 134
	"please_redo_from_start:					Please redo from start ",								// 135
	"instruction_not_opened:					Instruction not opened",								// 136
	"instruction_already_opened:				Instruction already opened",							// 137
	"function_not_opened:						Function not opened",									// 138
	"function already opened:					Function already opened",								// 139
	"device_already_opened:						Device already opened",									// 140
	"device_not_opened:							Device not opened",										// 141
	"device_cannot_be_opened:					Device cannot be opened",								// 142
	"command_not_supported_by_device:			Command not supported by device",						// 143
	"device_error:								Device errror",											// 144
	"serial_device_already_in_use:				Serial device already in use",							// 145
	"repeat_without_until,						Repeat without until",									// 146
	"invalid_baud_rate:							Invalid baud rate",										// 147
	"out_of_memory_serial_device:				Out of memory (serial device)",							// 148
	"bad_parameter:								Bad parameter",											// 149
	"hardware_data_overrun:						hardware data overrrun",								// 150
	"do_without_loop							Do without loop",										// 151
	"while_without_wend							While without wend",									// 152
	"cannot_read_directory:						Cannot read directory: ",								// 153
	"directory_of:								Directory of %1",										// 154
	"timeout_error:								Timeout error",											// 155
	"buffer_overflow:							Buffer overflow",										// 156
	"no_data_set_ready:							No data set ready",										// 157
	"do_without_loop:							Do without loop",										// 158
	"break_detected:							Break detected",										// 159
	"selected_unit_already_in_use:				Selected unit already in use",							// 160
	"user_canceled_request:						User canceled request",									// 161
	"printer_cannot_output_graphics:			Printer cannot output graphics",						// 162
	"while_without_wend:						While without wend",									// 163
	"illegal_print_dimensions:					Illegal print dimensions",								// 164
	"corrupted_file:							Corrupted file",										// 165
	"out_of_memory_printer_device:				Out of memory (printer device)",						// 166
	"out_of_internal_memory_printer_device:		Out of internal memory (printer device)",				// 167
	"library_already_opened:					Library already opened",								// 168
	"library_not_opened:						Library not opened",									// 169
	"cannot_open_library:						Cannot open library",									// 170
	"parallel_device_already_opened:			Parallel device already opened",						// 171
	"out_of_memory_parallel_device:				Out of memory (parallel device)",						// 172
	"invalid_parallel_parameter:				Invalid parallel parameter",							// 173
	"parallel_line_error:						Parallel line error",									// 174
	"drive_not_found:							Drive not found",										// 175
	"parallel_port_reset:						Parallel port reset",									// 176
	"parallel_initialisation_error:				Parallel initialisation error",							// 177
	"wave_not_defined:							Wave not defined",										// 178
	"sample_not_defined:						Sample not defined",									// 179
	"sample_bank_not_found:						Sample bank not found",									// 180
	"256_characters_for_a_wave:					256 characters for a wave",								// 181
	"wave_0_and_1_are_reserved:					Wave 0 and 1 are reserved",								// 182
	"music_bank_not_found:						Music bank not found",									// 183
	"music_not_defined:							Music not defined",										// 184
	"cant_open_narrator:						Can't open narrator",									// 185
	"not_a_tracker_module:						Not a tracker module",									// 186
	"cannot_load_med_library:					Cannot load med.library",								// 187
	"cannot_start_med_library:					Cannot start med.library",								// 188
	"not_a_med_module:							Not a med module",										// 189
	"at_line:									at line: ",												// 190
	"at_column:									column: ",												// 191
	"image_not_defined:							Image not defined",										// 192
	"arexx_port_already_opened:					Arexx port already opened",								// 193
	"arexx_library_not_found:					Arexx library not found",								// 194
	"cannot_open_arexx_port:					Cannot open Arexx port",								// 195
	"Arexx_port_not_opened:						Arexx port not opened",									// 196
	"no_arexx_message_waiting:					No Arexx message waiting",								// 197
	"arexx_message_not_answered_to:				Arexx message not answered to",							// 198
	"arexx_device_not_interactive:				Arexx device not interactive",							// 199
	"local_storage_not_available:				Local storage not available",							// 200
	"sprite_not_defined:						Sprite not defined",									// 201
	"fps_indicator:								%1 FPS",												// 202
	"every_too_long:							Every routine too long",								// 203
	"every_not_defined:							Every not defined",										// 204
	"every_already_defined:						Every already defined",									// 205
	"amal_channel_not_opened:					AMAL channel not opened",								// 203
	"global_variable_not_defined:				Global array not defined",								// 204
	"function_not_available_in_true_color_mode: Function not available in non-paletted graphical mode",	// 205
	"context_not_defined:						Property context not defined",							// 206
	"property_not_defined:						Property not defined"									// 207
];

messages.addLanguage( 'US', texts, errors, warnings, compilerWarnings, compilerErrors );
