/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * The compiler
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 03/12/2018
 */

const fs = require( 'fs' );
const utilities = require( './utilities' );
const tokens = require( './tokens' );
const messages = require( './messages' );

var sources = {};
var sourcesArray = [];
var extensions = {};
var extensionsArray = [];
var lines = [];
var tokenList = {};

const EXPFLAG_COMPARAISON 			= 0x00000001;
const EXPFLAG_ENDONCOLON 			= 0x00000002;
const EXPFLAG_ENDONSEMICOLON		= 0x00000004;
const EXPFLAG_ENDONCOMMA 			= 0x00000008;
const EXPFLAG_ENDONBRACKET 			= 0x00000010;
const EXPFLAG_ENDONSQUAREBRACKET	= 0x00000020;
const EXPFLAG_ENDONTO	 			= 0x00000040;
const EXPFLAG_ENDONAS	 			= 0x00000080;
const EXPFLAG_ENDONTHEN 			= 0x00000100;
const EXPFLAG_ENDONSTEP 			= 0x00000200;
const EXPFLAG_ENDONPROC 			= 0x00000400;
const EXPFLAG_ENDONGOTO 			= 0x00000800;
const EXPFLAG_ENDONGOSUB 			= 0x00001000;
const EXPFLAG_WANTNUMBER 			= 0x00010000;
const EXPFLAG_WANTSTRING 			= 0x00020000;
const EXPFLAG_SKIPNEXTTOKEN			= 0x00040000;
const EXPFLAG_ENDOFINSTRUCTION		= 0x00080000;

module.exports.init = function( options )
{
	for ( var t = 0; t < tokens.tokenTable.length; t++ )
	{
		var token = tokens.tokenTable[ t ];
		var index = utilities.replaceSpacesByUnderscores( token.token );
		tokenList[ index ] = token;
	}
	return true;
};
module.exports.compile = function( options )
{
	options.sections = [];
	options.instructions = {};
	options.functions = {};
	options.resourcesSubpath = '';
	lines = [];

	var code = '';

	// Load the source
	var source = utilities.loadFile( options.currentSourcePath, { encoding: 'utf8' } );

	// Load the library code
	var libraryPath = options.runtimePath + '/code.jstemplate';
	var library = utilities.loadFile( libraryPath, { encoding: 'utf8' } );

	// Header
	var header = utilities.extractCode( library, 'HEADER' );
	header = utilities.replaceStringInText( header, 'APPLICATIONNAME', options.manifest.infos.applicationName );
	header = utilities.replaceStringInText( header, 'AUTHORNAME', options.manifest.infos.author );
	header = utilities.replaceStringInText( header, 'VERSIONNUMBER', options.manifest.infos.version );
	header = utilities.replaceStringInText( header, 'DATE', options.manifest.infos.date );
	header = utilities.replaceStringInText( header, 'COPYRIGHT', options.manifest.infos.copyright );
	var infos = messages.getMessage( 'compiled_with', options.version, utilities.getFormattedDate() );
	header = utilities.replaceStringInText( header, 'COMPILERINFOS', infos );
	var manifestJson = JSON.stringify( options.manifest );
	header = utilities.replaceStringInText( header, 'MANIFEST', manifestJson );
	code += header;

	// Main section
	options.sections[ 0 ] =
	{
		type: 'main',
		main: true,
		start: 0,
		end: 0,
		blocks: [],
		variables: {},
		labels: {},
		pileIfs: [ null ],
		anchors: {},
		pile:[ null ],
		start: 0,
		number: 0,
		datas: [],
		dataPosition: 0,
		globals: []
	};

	// Preprocessor
	preprocessPass( source, options );

	// Compile base code
	zeroPass( options );
	source = '';
	options.sections[ 0 ].end = lines.length;
	if ( messages.isErrors() )
		throw {};

	// Compute the instructions!
	for ( var i in options.instructions )
	{
		var instruction = options.instructions[ i ];
		firstPass( instruction.startInside, instruction.end - 1, instruction, options );
		if ( messages.isErrors() )
			throw {};

		options.tabs = '\t\t';
		secondPass( instruction.startInside + 1, instruction.end - 1, instruction, options );
		if ( messages.isErrors() )
			throw {};
	}

	// Compute the functions!
	for ( var f in options.functions )
	{
		var func = options.functions[ f ];
		firstPass( func.start, func.end, func, options );
		if ( messages.isErrors() )
			throw {};

		options.tabs = '\t\t';
		secondPass( func.start + 1, func.end, func, options );
		if ( messages.isErrors() )
			throw {};
	}

	// First pass!
	messages.print_info( 'first_pass' );
	firstPass( 0, lines.length, options.sections[ 0 ], options );
	if ( messages.isErrors() )
		throw {};

	// Second pass!
	messages.print_info( 'second_pass' );
	options.tabs = '\t\t';
	for ( var p = 0; p < options.sections.length; p++ )
		secondPass( options.sections[ p ].start, options.sections[ p ].end, options.sections[ p ], options );
	if ( messages.isErrors() )
		throw {};

	// Initialisation of the root variables
	var section = options.sections[ 0 ];
	var comma = false;
	var doCode = false;
	var oneBased = ',0';
	for ( var v in section.variables )
	{
		var variable = section.variables[ v ];

		if ( !doCode )
		{
			code += '\tthis.vars=\n\t{\n';
			doCode = true;
		}
		if ( comma )
			code += ',\n';
		code += '\t\t' + v + ':';
		if ( !variable.isArray )
		{
			switch ( variable.type )
			{
				case '0':
					code += '0';
					break;
				case '1':
					code += '0.0';
					break;
				case '2':
					code += '""';
					break;
			}
		}
		else
		{
			switch ( variable.type )
			{
				case '0':
					code += 'new AArray(this.amos,0' + oneBased + ')';
					break;
				case '1':
					code += 'new AArray(this.amos,0.0' + oneBased + ')';
					break;
				case '2':
					code += 'new AArray(this.amos,""' + oneBased + ')';
					break;
			}
		}
		comma = true;
	}
	if ( doCode )
		code += '\n\t}\n';

	// Insertion of the blocks of code
	code += '\tthis.blocks=[];\n';
	for ( var b = 0; b < section.blocks.length; b++ )
	{
		code += '\tthis.blocks[' + b + ']=function()\n\t{\n';
		code += section.blocks[ b ];
		code += '\t};\n';
	}

	// Create the instructions
	for ( var p in options.instructions )
	{
		var instruction = options.instructions[ p ];
		code += '\tthis.i' + instruction.name + '=new ' + instruction.name + '(this.amos,this);\n';
	}

	// Create the functions
	for ( var p in options.functions )
	{
		var func = options.functions[ p ];
		code += '\tthis.f' + func.name + '=new ' + func.name + '(this.amos,this);\n';
	}

	// Insert the data
	code += insertDatas( section );

	// Insert the labels
	code += insertLabels( section );

	// Initialize all extensions
	var exit = utilities.extractCode( library, 'FOOTER' );
	code += '\tthis.amos.run(this,0,null);\n';
	for ( var e in options.extensions )
	{
		var extension = options.extensions[ e ];
		if ( extension.inUse )
		{
			code += '\tthis.amos.' + extension.name + '=new ' + extension.name + '(this.amos,this);\n';
		}
	}
	code += exit;

	// Creation of the procedures
	for ( var p = 1; p < options.sections.length; p++ )
		code += insertSection( options.sections[ p ] );

	// Creation of the instructions
	for ( var p in options.instructions )
		code += insertSection( options.instructions[ p ] );

	// Creation of the functions
	for ( var p in options.functions )
		code += insertSection( options.functions[ p ] );

	// Finished without errors!
	return code;
};

module.exports.compileExtension = function( options )
{
	var code = '';
	options.sections = [];
	options.instructions = {};
	options.functions = {};
	options.resourcesSubpath = options.extensionName.toLowerCase();
	lines = [];

	// Load the source
	var source = utilities.loadFile( options.currentSourcePath, { encoding: 'utf8' } );

	// Load the library code
	var libraryPath = options.runtimePath + '/code_extensions.jstemplate';
	var library = utilities.loadFile( libraryPath, { encoding: 'utf8' } );

	// Header
	var header = utilities.extractCode( library, 'HEADER' );
	header = utilities.replaceStringInText( header, 'APPLICATIONNAME', options.manifest.infos.applicationName );
	header = utilities.replaceStringInText( header, 'AUTHORNAME', options.manifest.infos.author );
	header = utilities.replaceStringInText( header, 'VERSIONNUMBER', options.manifest.infos.version );
	header = utilities.replaceStringInText( header, 'DATE', options.manifest.infos.date );
	header = utilities.replaceStringInText( header, 'COPYRIGHT', options.manifest.infos.copyright );
	var infos = messages.getMessage( 'compiled_with', options.version, utilities.getFormattedDate() );
	header = utilities.replaceStringInText( header, 'COMPILERINFOS', infos );
	var manifestJson = JSON.stringify( options.manifest );
	header = utilities.replaceStringInText( header, 'MANIFEST', manifestJson );
	header = utilities.replaceStringInText( header, 'EXTENSION_NAME', options.extensionName );
	code += header;

	// Main section
	options.sections[ 0 ] =
	{
		type: 'main',
		main: true,
		start: 0,
		end: 0,
		blocks: [],
		variables: {},
		labels: {},
		pileIfs: [ null ],
		anchors: {},
		pile:[ null ],
		start: 0,
		number: 0,
		datas: [],
		dataPosition: 0,
		globals: []
	};

		// Preprocessor
	preprocessPass( source, options );

	// Compile base code
	zeroPass( options );
	source = '';
	options.sections[ 0 ].end = lines.length;
	if ( messages.isErrors() )
		throw {};

	// First pass!
	messages.print_info( 'first_pass' );
	firstPass( 0, lines.length, options.sections[ 0 ], options );
	if ( messages.isErrors() )
		throw {};

	// Second pass!
	messages.print_info( 'second_pass' );
	options.tabs = '\t\t';
	for ( var p = 0; p < options.sections.length; p++ )
		secondPass( options.sections[ p ].start, options.sections[ p ].end, options.sections[ p ], options );
	if ( messages.isErrors() )
		throw {};

	// Compute the instructions!
	for ( var i in options.instructions )
	{
		var instruction = options.instructions[ i ];
		firstPass( instruction.startInside, instruction.end - 1, instruction, options );
		if ( messages.isErrors() )
			throw {};

		options.tabs = '\t\t\t';
		secondPass( instruction.startInside, instruction.end - 1, instruction, options );
		if ( messages.isErrors() )
			throw {};
	}

	// Compute the functions!
	for ( var f in options.functions )
	{
		var func = options.functions[ f ];
		firstPass( func.startInside, func.end, func, options );
		if ( messages.isErrors() )
			throw {};

		options.tabs = '\t\t\t';
		secondPass( func.startInside, func.end, func, options );
		if ( messages.isErrors() )
			throw {};
	}

	// Initialisation of the root variables
	var section = options.sections[ 0 ];
	var comma = false;
	var doCode = false;
	var oneBased = ',0';
	for ( var v in section.variables )
	{
		var variable = section.variables[ v ];

		if ( !doCode )
		{
			code += '\tthis.vars=\n\t{\n';
			doCode = true;
		}
		if ( comma )
			code += ',\n';
		code += '\t\t' + v + ':';
		if ( !variable.isArray )
		{
			switch ( variable.type )
			{
				case '0':
					code += '0';
					break;
				case '1':
					code += '0.0';
					break;
				case '2':
					code += '""';
					break;
			}
		}
		else
		{
			switch ( variable.type )
			{
				case '0':
					code += 'new AArray(this.amos,0' + oneBased + ')';
					break;
				case '1':
					code += 'new AArray(this.amos,0.0' + oneBased + ')';
					break;
				case '2':
					code += 'new AArray(this.amos,""' + oneBased + ')';
					break;
			}
		}
		comma = true;
	}
	if ( doCode )
		code += '\n\t}\n';

	// Insertion of the blocks of code
	code += '\tthis.blocks=[];\n';
	for ( var b = 0; b < section.blocks.length; b++ )
	{
		code += '\tthis.blocks[' + b + ']=function()\n\t{\n';
		code += section.blocks[ b ];
		code += '\t};\n';
	}

	// Creation of the instructions
	for ( var p in options.instructions )
		code += insertSection( options.instructions[ p ], '\t' );

	// Creation of the functions
	for ( var p in options.functions )
		code += insertSection( options.functions[ p ], '\t' );


	// Insert the data
	code += insertDatas( section );

	// Insert the labels
	code += insertLabels( section );

	// Initialize the instructions
	for ( var p in options.instructions )
	{
		var instruction = options.instructions[ p ];
		code += '\tthis.i' + instruction.name + '=new ' + instruction.name + '(this.amos,this);\n';
	}

	// Initialize the functions
	for ( var p in options.functions )
	{
		var func = options.functions[ p ];
		code += '\tthis.f' + func.name + '=new ' + func.name + '(this.amos,this);\n';
	}

	// Finish the code
	var exit = utilities.extractCode( library, 'FOOTER' );
	code += exit;

	// Creation of the procedures
	for ( var p = 1; p < options.sections.length; p++ )
		code += insertSection( options.sections[ p ] );

	// Finished without errors!
	return code;
};

function insertLabels( section, tab )
{
	tab  = typeof tab == 'undefined' ? '' : tab;

	var code = '';
	var l;
	var newCode = '';
	var done = false;
	for ( l in section.labels )
	{
		var label = section.labels[ l ];
		if ( done )
		{
			newCode += ',\n';
		}
		newCode += '\t\t' + tab + l + ':\n\t\t' + tab + '{\n';
		newCode += '\t\t\t' + tab + 'dataPosition:' + label.dataPosition + ',\n';
		newCode += '\t\t\t' + tab + 'labelBlock:' + label.labelBlock + '\n';
		newCode += '\t\t' + tab + '}';
		done = true;
	}
	if ( newCode != '' )
	{
		code += '\n\t' + tab + '// Labels...\n';
		code += '\t' + tab + 'this.labels=\n\t' + tab + '{\n';
		code += newCode;
		code += '\n\t' + tab + '};\n';
	}
	return code;
};
function insertDatas( section, tab )
{
	tab  = typeof tab == 'undefined' ? '' : tab;

	var code = '';
	if ( section.datas.length )
	{
		code += '\t' + tab + '// Datas...\n';
		code += '\t' + tab + 'this.dataPosition=0;\n'
		code += '\t' + tab + 'this.datas=\n\t' + tab + '[\n';
		var line = '\t\t' + tab;
		for ( var d = 0; d < section.datas.length; d++ )
		{
			if ( line.length > 80 )
			{
				code += line + '\n';
				line = '\t\t' + tab;
			}
			line += section.datas[ d ];
			if ( d < section.datas.length - 1 )
				line += ',';
		}
		code += line + '\n\t' + tab + '];\n';
	}
	return code;
};
function insertSection( section, tab )
{
	tab  = typeof tab == 'undefined' ? '' : tab;

	var code = '';
	code += tab + 'function ';
	if ( section.type == 'procedure' )
		code += 'proc';
	if ( section.type == 'main' )
		code += section.name + '(amos)\n' + tab + '{\n\t' + tab + 'this.root=this;\n\tthis.amos=amos;\n';
	else
		code += section.name + '(amos,root)\n' + tab + '{\n\t' + tab + 'this.amos=amos;\n\t' + tab + 'this.root=root;\n';
	code += '\t' + tab + 'var self=this;\n';
	code += '\t' + tab + 'this.reset=function()\n\t' + tab + '{\n';
	code += '\t\t' + tab + 'self.vars=\n\t\t' + tab + '{\n';

	// Initialisation of the variables
	var comma = false;
	var oneBased = ',0';
	for ( var v in section.variables )
	{
		var variable = section.variables[ v ];
		if ( !variable.global )
		{
			if ( comma )
				code += ',\n';
			code += '\t\t\t' + tab + v + ':';
			if ( !variable.isArray )
			{
				switch ( variable.type )
				{
					case '0':
						code += '0';
						break;
					case '1':
						code += '0.0';
						break;
					case '2':
						code += '""';
						break;
				}
			}
			else
			{
				switch ( variable.type )
				{
					case '0':
						code += 'new AArray(this.amos,0' + oneBased + ')';
						break;
					case '1':
						code += 'new AArray(this.amos,0.0' + oneBased + ')';
						break;
					case '2':
						code += 'new AArray(this.amos,""' + oneBased + ')';
						break;
				}
			}
			comma = true;
		}
	}
	code += '\n\t\t' + tab + '};\n';
	code += '\t' + tab + '};\n';

	// Insert the data
	insertDatas( section, tab );

	// Insert the labels
	insertLabels( section, tab );

	// Blocks of code
	code += '\t' + tab + 'this.blocks=[];\n';
	for ( var b = 0; b < section.blocks.length; b++ )
	{
		code += '\t' + tab + 'this.blocks[' + b + ']=function()\n\t' + tab + '{\n';
		code += section.blocks[ b ];
		code += '\t' + tab + '};\n';
	}
	code += tab + '};\n';
	return code;
};

// Preprocessor: convert into lines, scan for includes, remove uncompiled lines.
function preprocessPass( source, options )
{
	var previous = 0;
	var currentLine = 0;
	var line;
	var info = new information( options );
	info.pass = -1;
	lines = [];
	while( previous >= 0 )
	{
		try
		{
			// Extract line by line
			info.currentLine = currentLine;
			pos = utilities.findNextLine( source, previous );
			if ( pos <= 0 )
			{
				line = utilities.trimString( source.substring( previous ), { right: true } );
				lines[ currentLine++ ] = line;
			}
			else
			{
				line = utilities.trimString( source.substring( previous, pos ), { right: true } );
				lines[ currentLine++ ] = line;
			}
			previous = pos;

			/*
			// Spot includes at start of line
			if ( ( pos = line.toLowerCase().indexOf( 'include ' ) ) >= 0 )
			{
				info.extractNextWord( line );
				if ( info.type != 'string' )
					info.throwError( 'type_mismatch' );
				if ( info.text.substring( 0, 2 ) == './' )
				{
					if ( !sources[ info.text ] )
					{
						sources[ info.text ] = { type: 'main' };
						sourcesArray.push( info.text );
					}
				}
				else
				{
					if ( !extensions[ info.text ] )
					{
						extensions[ info.text ] = { type: 'extension' };
						extensionsArray.push( info.text );
					}
				}
			}
			*/
		}
		catch( error )
		{

		}
	}
	options.start = 0;
	options.end = currentLine;
}


// Pass zero: cut the source in lines, spot the procedures
function zeroPass( options )
{
	var line;
	var info = new information( options );
	var procedureOpen = false;
	var instructionOpen = false;
	var functionOpen = false;
	info.pass = 0;
	for ( info.currentLine = options.start; info.currentLine < options.end; info.currentLine++ )
	{
		try
		{
			var line = lines[ info.currentLine ];
			if ( line == '' )
				continue;

			// Spot the beginning of procedures
			info.position = 0;
			info.extractNextWord( line );
			if ( info.type == 'token' )
			{
				switch ( info.token.token )
				{
					case 'procedure':
						if ( procedureOpen )
							info.throwError( 'procedure_already_open');
						if ( instructionOpen )
							info.throwError( 'syntax_error' );
						if ( functionOpen )
							info.throwError( 'syntax_error' );

						info.extractNextWord( line );
						if ( info.type != 'variable' )
							info.throwError( 'syntax_error' );

						// New section!
						procedureOpen =
						{
							name: info.text,
							type: 'procedure',
							start: info.currentLine,
							blocks: [],
							variables: {},
							labels: {},
							anchors: {},
							pileIfs: [ null ],
							pile: [ null ],
							parameterTypes: [],
							parameterNames: [],
							number: options.sections.length,
							onError: false,
							datas: [],
							dataPosition: 0,
							globals: []
						};
						options.sections.push( procedureOpen );
						break;

					case 'end proc':
						if ( !procedureOpen )
							info.throwError( 'procedure_not_opened' );
						procedureOpen.end = info.currentLine + 1;
						procedureOpen = false;
						break;

					case 'instruction':
						if ( instructionOpen )
							info.throwError( 'instruction_already_open' );
						if ( functionOpen )
							info.throwError( 'syntax_error' );
						if ( procedureOpen )
							info.throwError( 'syntax_error' );

						info.extractNextWord( line );
						if ( info.type != 'string' )
							info.throwError( 'syntax_error' );

						// New section!
						instructionOpen =
						{
							name: utilities.replaceSpacesByUnderscores( info.text ).toLowerCase(),
							type: 'instruction',
							start: info.currentLine,
							startInside: info.currentLine + 1,
							blocks: [],
							variables: {},
							labels: {},
							anchors: {},
							pileIfs: [ null ],
							pile: [ null ],
							parameterTypes: [],
							parameterNames: [],
							parameterSeparators: [],
							number: options.sections.length,
							onError: false,
							datas: [],
							dataPosition: 0,
							globals: []
						};

						// Gather parameters
						info.peekNextWord( line );
						if ( !info.endOfInstruction )
						{
							if ( info.text != ',' )
								info.throwError( 'syntax_error' );
							info.setPeekNextWordEnd();
							while ( true )
							{
								info.extractNextWord( line );
								if ( info.type != 'variable' )
									info.throwError( 'syntax_error' );
								instructionOpen.parameterNames.push( info.text );
								instructionOpen.parameterTypes.push( info.returnType );
								info.peekNextWord( line );
								if ( info.text != ',' && info.textLower != 'to' )
									break;
								if ( info.text == ',' )
									instructionOpen.parameterSeparators.push( ',' );
								else
									instructionOpen.parameterSeparators.push( 'to' );
								info.setPeekNextWordEnd();
							}
						}
						options.instructions[ instructionOpen.name.toLowerCase() ] = instructionOpen;
						break;

					case 'end instruction':
						if ( !instructionOpen )
							info.throwError( 'instruction_not_opened' );
						instructionOpen.end = info.currentLine + 1;
						instructionOpen = false;
						break;

					case 'function':
						if ( functionOpen )
							info.throwError( 'function_already_open' );
						if ( instructionOpen )
							info.throwError( 'syntax_error' );
						if ( procedureOpen )
							info.throwError( 'syntax_error' );

						info.noArray = true;
						info.extractNextWord( line );
						if ( info.type != 'string' )
							info.throwError( 'syntax_error' );

						// New section!
						functionOpen =
						{
							name: utilities.replaceSpacesByUnderscores( info.text ).toLowerCase(),
							type: 'function',
							start: info.currentLine,
							startInside: info.currentLine + 1,
							blocks: [],
							variables: {},
							labels: {},
							anchors: {},
							pileIfs: [ null ],
							pile: [ null ],
							parameterTypes: [],
							parameterNames: [],
							parameterSeparators: [],
							number: options.sections.length,
							onError: false,
							datas: [],
							dataPosition: 0,
							globals: [],
							returnType: ''
						};

						// Gather parameters
						info.peekNextWord( line );
						if ( !info.endOfInstruction )
						{
							if ( info.text != ',' )
								info.throwError( 'syntax_error' );
							info.setPeekNextWordEnd();

							while ( true )
							{
								info.extractNextWord( line );
								if ( info.type != 'variable' )
									info.throwError( 'syntax_error' );
								functionOpen.parameterNames.push( info.text );
								functionOpen.parameterTypes.push( info.returnType );
								info.peekNextWord( line );
								if ( info.text != ',' && info.textLower != 'to' )
									break;
								if ( info.text == ',' )
									functionOpen.parameterSeparators.push( ',' );
								else
									functionOpen.parameterSeparators.push( 'to' );

								info.setPeekNextWordEnd();
							}
							if ( !info.endOfInstruction )
								info.throwError( 'syntax_error' );
						}
						else
						{
							info.setPeekNextWordEnd();
						}
						options.functions[ functionOpen.name.toLowerCase() ] = functionOpen;
						break;

					case 'end function':
						if ( !functionOpen )
							info.throwError( 'function_not_opened' );
						functionOpen.end = info.currentLine + 1;
						functionOpen = false;
						break;
				}
			}
			else
			{
				// Spot On Error, if detected-> slow mode!
				while ( !info.eol )
				{
					if ( info.type == 'token' && info.token.token == 'on error' )
					{
						if ( !procedureOpen )
							options.sections[ 0 ].onError = true;
						else
							procedureOpen.onError = true;
					}
					info.extractNextWord( line );
				}
			}
		}
		catch ( error )
		{
		}
	}
}

// First loop: procedures and their parameters, labels
function firstPass( start, end, section, options )
{
	var javascript = 0;
	var remarkCount = 0;
	var javascriptLine, remarkLine, remarkColumn;
	var info = new information( options, { blockNumber: 0, section: section, pass: 1 } );
	info.section.start = start;
	for ( info.currentLine = start; info.currentLine < end; info.currentLine++ )
	{
		var ifs = info.section.pileIfs[ info.section.pileIfs.length - 1 ];
		if ( ifs && ifs.then )
		{
			ifs.endIfBlock = ++info.blockNumber;
			info.section.pileIfs.pop();
		}

		var line = lines[ info.currentLine ];
		if ( line == '' )
			continue;

		// Insert Javascript code?
		info.position = 0;
		if ( remarkCount == 0 )
		{
			info.peekNextWord( line );
			if ( javascript == 0 )
			{
				if ( info.text == '{' )
				{
					javascript++;
					javascriptLine = info.currentLine;
					javascriptColumn = info.position;
					continue;
				}
			}
			else
			{
				if ( info.text == '{' )
					javascript++;
				if ( info.text == '}' )
					javascript--;
				continue;
			}
		}

		try
		{
			var checkLabel = true;
			var quit = false;
			while( !quit )
			{
				info.extractNextWord( line );
				if ( info.eol || info.remark )
					break;

				// A block remark?
				if ( info.text == '/*' )
				{
					remarkCount++;
					if ( remarkCount == 1 )
					{
						remarkLine = info.currentLine;
						remarkColumn = info.column;
					}
				}
				else if ( info.text == '*/' )
				{
					remarkCount--;
					continue;
				}
				if ( remarkCount > 0 )
					continue;

				// Look for a label
				if ( checkLabel && ( info.type == 'variable' || info.type == 'number' ) )
				{
					if ( ( info.type == 'variable' && line.charAt( info.position ) == ':' ) || info.type == 'number' )
					{
						info.position++;

						if ( info.section.labels[ info.text ] )
							info.throwError( 'label_already_defined' );
						if ( !info.section.onError )
							info.blockNumber++;
						info.section.labels[ info.text ] = { labelBlock: info.blockNumber, dataPosition: info.section.dataPosition };
						continue;
					}
				}
				checkLabel = false;

				switch ( info.type )
				{
					// A procedure call
					case 'procedure':
						info.peekNextWord( line );
						if ( info.eol || info.text == ':' || info.textLower == 'else' || info.type == 'remark' )
						{
							info.nextBlock( '' );
							break;
						}
						if ( info.text == '[' )
						{
							info.setPeekNextWordEnd();

							// Skip the parameters
							while( true )
							{
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONSQUAREBRACKET );
								info.peekNextWord( line );
								if ( info.text == ']' )
								{
									info.setPeekNextWordEnd();
									break;
								}
								if ( info.text != ',' )
									info.throwError( 'syntax_error' );
								info.setPeekNextWordEnd();
							}
						}
						else
						{
							info.throwError( 'syntax_error' );
						}
						info.blockNumber++;
						break;

					// A instruction call
					case 'instruction':
						var instructionDefinition = info.instructionDefinition;
						if ( !instructionDefinition )
							info.throwError( 'instruction_not_defined' );

						info.peekNextWord( line );
						if ( info.endOfInstruction )
						{
							info.blockNumber++;
							break;
						}

						info.setPeekNextWordEnd();

						info.waitingFunctionCount = 0;
						var param = 0;
						while( true )
						{
							info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA );
							info.peekNextWord( line );
							if ( info.endOfInstruction )
								break;
							if ( info.textLower != instructionDefinition.parameterSeparators[ param ]  )
								info.throwError( 'syntax_error' );
							info.setPeekNextWordEnd();
							param++;
						}
						info.blockNumber++;
						break;

					case 'function':
						info.throwError( 'syntax_error' );

					// An instruction?
					case 'token':
						var token = info.token;
						noScan = false;
						// Locate loops
						switch( token.token )
						{
							// An instruction
							case 'instruction':
								info.extractNextWord( line );
								if ( info.type != 'string' )
									info.throwError( 'internal_error' );
								var instruction = options.instructions[ utilities.replaceSpacesByUnderscores( info.textLower ) ];
								if ( info.section.type == 'main' )
								{
									info.currentLine = instruction.end - 1;
									info.position = line.length;
									break;
								}
								info.throwError( 'internal_error' );
								break;
/*
								// Skip parameters
								info.peekNextWord( line );
								if ( !info.endOfInstruction )
								{
									if ( info.text != ',' )
										info.throwError( 'syntax_error' );
									info.position = info.peekNextWordEnd;
									while( true )
									{
										info.extractNextWord( line );
										if ( info.type != 'variable' )
											info.throwError( 'syntax_error' );
										info.peekNextWord( line );
										if ( info.text != ',' && info.textLower != 'to' )
											break;
										info.setPeekNextWordEnd();
									}
								}
								if ( !info.endOfInstruction )
									info.throwError( 'syntax_error' );
*/

							case 'end instruction':
								if ( info.section.type != 'instruction' )
									info.throwError( 'internal_error' );
								info.peekNextWord( line );
								if ( !info.endOfInstruction )
									info.throwError( 'syntax_error' );
								quit = true;
								break;

							// An function
							case 'function':
								info.extractNextWord( line );
								if ( info.type != 'string' )
									info.throwError( 'type_mismatch' );
								var func = options.functions[ utilities.replaceSpacesByUnderscores( info.textLower ) ];
								if ( info.section.type == 'main' )
								{
									info.currentLine = func.end - 1;
									info.position = line.length;
									quit = true;
									break;
								}
								info.throwError( 'internal_error' );
								break;
/*
								// Gather parameters
								info.peekNextWord( line );
								if ( !info.endOfInstruction )
								{
									if ( info.text != ',' )
										info.throwError( 'syntax_error' );
									info.setPeekNextWordEnd();
									while ( true )
									{
										info.extractNextWord( line );
										if ( info.type != 'variable' )
											info.throwError( 'syntax_error' );
										info.peekNextWord( line );
										if ( info.endOfInstruction )
											break;
										if ( info.text != ',' && info.textLower != 'to' )
											info.throwError( 'syntax_error' );
										info.setPeekNextWordEnd();
									}
								}
								break;
*/

							case 'end function':
								if ( info.section.type != 'function' )
									info.throwError( 'internal_error' );
								info.extractNextWord( line );
								if ( info.text != '(' )
									info.throwError( 'syntax_error' );
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONBRACKET );
								info.section.returnType = info.returnType;
								info.extractNextWord( line );
								if ( info.text != ')' )
									info.throwError( 'syntax_error' );
								info.peekNextWord( line );
								if ( !info.endOfInstruction )
									info.throwError( 'syntax_error' );
								quit = true;
								break;

							case 'procedure':
								if ( info.section.type != 'main' )
									info.throwError( 'procedure_not_closed' );

								info.position = 10;
								info.extractNextWord( line );
								if ( info.type != 'procedure' )
									info.throwError( 'procedure_not_defined' );
								var section = info.procedure;

								// New block (for return of procedure)
								//info.blockNumber++;

								// Get the parameters
								info.extractNextWord( line );
								if ( !info.eol && info.type != 'remark' )
								{
									if ( info.text != '[' )
										info.throwError( 'syntax_error' );

									while( true )
									{
										info.extractNextWord( line );
										if ( info.eol )
											info.throwError( 'syntax_error' );
										if ( info.type != 'variable' )
											info.throwError( 'syntax_error' );

										section.parameterNames.push( info.text );
										section.parameterTypes.push( info.returnType );
										section.variables[ info.text ] =
										{
											name: info.text,
											type: info.returnType,
											parameter: true
										}

										info.extractNextWord( line );
										if ( info.text == ']' )
											break
										if ( info.text != ',' )
											info.throwError( 'syntax_error' );
									}
								}

								// Recursively calls firstPass
								info.currentLine = firstPass( info.currentLine + 1, section.end, section, options );
								quit = true;
								continue;

							// The end of a procedure
							case 'end proc':
								if ( info.section.type != 'procedure' )
									info.throwError( 'procedure_not_opened' );
								info.section.endProc = info.currentLine;
								info.peekNextWord( line );
								if ( info.text == '[' )
								{
									info.position = info.peekNextWordEnd;
									info.waitingFunctionCount = 0;
									info.compileExpression( line, EXPFLAG_ENDONSQUAREBRACKET );
									info.section.endProcParamType = info.returnType;
									info.extractNextWord( line );
									if ( info.text != ']' )
										info.throwError( 'syntax_error' );
								}
								return info.currentLine;
							case 'pop proc':
								if ( info.section.type != 'procedure' )
									info.throwError( 'procedure_not_opened' );
								info.peekNextWord( line );
								if ( info.text == '[' )
								{
									info.position = info.peekNextWordEnd;
									info.waitingFunctionCount = 0;
									info.compileExpression( line, EXPFLAG_ENDONSQUAREBRACKET );
									info.extractNextWord( line );
									if ( info.text != ']' )
										info.throwError( 'syntax_error' );
								}
								break;

							// On XXX goto/gosub/proc
							case "on":
								var ons =
								{
									endBlock: ++info.blockNumber
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = ons;
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONGOTO | EXPFLAG_ENDONGOSUB | EXPFLAG_ENDONPROC );
								if ( info.returnType != '0' )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.textLower != 'goto' && info.textLower != 'gosub' && info.textLower != 'proc' )
									info.throwError( 'syntax_error' );

								// Skip labels
								while( !info.eol )
								{
									info.extractLabel( line );
									info.extractNextWord( line );
									if ( info.text != ',' )
										break;
								}
								break;

							// Every XXX gosub/proc
							case "every":
								info.section.anchors[ info.currentLine + '-' + info.position ] = ons;
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONGOTO | EXPFLAG_ENDONGOSUB | EXPFLAG_ENDONPROC );
								if ( info.returnType != '0' )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.textLower != 'gosub' && info.textLower != 'proc' )
									info.throwError( 'syntax_error' );

								// Skip labels
								while( !info.eol )
								{
									info.extractLabel( line );
									info.extractNextWord( line );
									if ( info.text != ',' )
										break;
								}
								break;

							// Gosub
							case 'gosub':
								var loop =
								{
									type: 'gosub',
									startBlock: ++info.blockNumber
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								info.extractLabel( line );
								break;

							// If/Else/End If
							case 'if':
								var ifs =
								{
									line: info.currentLine,
									column: info.column,
									endIfBlock: false,
									then: false,
									elseBlocks: [],
									elseCount: 0
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = ifs;
								info.section.pileIfs.push( ifs );
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONTHEN | EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
								if ( info.section.onError )
									info.blockNumber++;
								info.peekNextWord( line );
								if ( info.textLower == 'then' )
								{
									ifs.then = true;
									info.position = info.peekNextWordEnd;
								}
								break;
							case 'else':
								var ifs = info.section.pileIfs[ info.section.pileIfs.length - 1 ];
								if ( !ifs )
									info.throwError( 'syntax_error' );
								info.section.anchors[ info.currentLine  + '-' + info.position ] = ifs;
								ifs.elseBlocks.push( ++info.blockNumber );
								break;
							case 'else if':
								var ifs = info.section.pileIfs[ info.section.pileIfs.length - 1 ];
								if ( !ifs )
									info.throwError( 'syntax_error' );
								info.section.anchors[ info.currentLine  + '-' + info.position ] = ifs;
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_CONPARAISON | EXPFLAG_ENDONCOLON );
								if ( !checkTypes( '0', info.returnType ) )
									throw 'type_mismatch';
								ifs.elseBlocks.push( ++info.blockNumber );
								break;
							case 'end if':
								ifs = info.section.pileIfs.pop();
								if ( !ifs )
									info.throwError( 'syntax_error' );
								ifs.endIfBlock = ++info.blockNumber;
								break;

							// While / Wend
							case 'while':
								var loop =
								{
									line: info.currentLine,
									column: info.column,
									type: 'while',
									startBlock: ++info.blockNumber,
									endBlock: 0
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								loop.variable = info.text;
								info.section.pile.push( loop );
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_COMPARAISON | EXPFLAG_ENDONCOLON );
								break;
							case 'wend':
								var loop = info.section.pile.pop();
								if ( !loop )
									info.throwError( 'syntax_error' );
								if ( loop.type != 'while' )
									info.throwError( 'wend_without_while' );
								loop.endBlock = ++info.blockNumber;
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								break;


							// Repeat / Until
							case 'repeat':
								var loop =
								{
									line: info.currentLine,
									column: info.column,
									type: 'repeat',
									startBlock: ++info.blockNumber,
									endBlock: 0
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								info.section.pile.push( loop );
								break;
							case 'until':
								var loop = info.section.pile.pop();
								if ( loop.type != 'repeat' )
									info.throwError( 'until_without_repeat' );
								loop.endBlock = ++info.blockNumber;
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
								break;

							// Do / Loop
							case 'do':
								var loop =
								{
									line: info.currentLine,
									column: info.column,
									type: 'do',
									startBlock: ++info.blockNumber,
									endBlock: 0
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								info.section.pile.push( loop );
								break;
							case 'loop':
								var loop = info.section.pile.pop();
								if ( loop.type != 'do' )
									info.throwError( 'loop_without_do' );
								loop.endBlock = ++info.blockNumber;
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								break;

							// Exit
							case 'exit':
							case 'exit if':
								if ( info.section.pile.length < 2 )
									info.throwError( 'no_loop_to_exit' );
								var loop =
								{
									type: 'exit',
									loops: []
								};
								for ( var l = 0; l < info.section.pile.length; l++ )
									loop.loops.push( info.section.pile[ l ] );
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								if ( token.token == 'exit if' )
								{
									info.waitingFunctionCount = 0;
									info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
								}
								if ( info.section.onError )
									info.blockNumber++;
								break;

							// For/Next
							case 'for':
								var loop =
								{
									line: info.currentLine,
									column: info.column,
									type: 'for',
									startBlock: ++info.blockNumber,
									endBlock: 0
								};
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								info.extractNextWord( line );
								info.getVariable( false, true);
								if ( info.type != 'variable' )
									info.throwError( 'syntax_error' );
								loop.variable = info.text;
								loop.variableType = info.returnType;
								info.extractNextWord( line );
								if ( info.text != '=' )
									info.throwError( 'syntax_error' );
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONTO );
								info.peekNextWord( line );
								if ( info.textLower != 'to' )
									info.throwError( 'syntax_error' );
								info.position = info.peekNextWordEnd;
								info.compileExpression( line, EXPFLAG_ENDONSTEP | EXPFLAG_ENDONCOLON );
								info.peekNextWord( line );
								if ( info.textLower == 'step' )
								{
									info.setPeekNextWordEnd();
									info.compileExpression( line, EXPFLAG_ENDONCOLON );
								}
								info.section.pile.push( loop );
								break;
							case 'next':
								if ( info.section.pile.length == 0 )
									info.throwError( 'next_without_for' );
								var loop = info.section.pile.pop();
								if ( loop.type != 'for' )
									info.throwError( 'next_without_for' );
								info.section.anchors[ info.currentLine + '-' + info.position ] = loop;
								info.peekNextWord( line );
								if ( info.type == 'variable' )
								{
									if ( info.text != loop.variable )
										info.throwError( 'next_without_for' );
									info.position = info.peekNextWordEnd;
								}
								loop.endBlock = ++info.blockNumber;
								break;

							// Dim
							case 'dim':
								while(true)
								{
									info.extractNextWord( line );
									if ( info.type != 'variable' )
										break;

									var variable = info.section.variables[ info.text ];
									if ( variable )
										info.throwError( 'array_already_dimensionned' );

									variable =
									{
										name: info.text,
										type: info.returnType,
										isArray: true,
										global: false,
										numberOfDimensions: 0
									}
									info.section.variables[ info.text ] = variable;

									info.extractNextWord( line );
									if ( info.text != '(' )
										info.throwError( 'syntax_error' );

									info.waitingFunctionCount = 0;
									do
									{
										info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONBRACKET );
										if ( info.returnType != '0' )
											info.throwError( 'type_mismatch' );
										variable.numberOfDimensions++;
										info.peekNextWord( line );
										if ( info.text == ')' )
										{
											info.position = info.peekNextWordEnd;
											break;
										}
										if ( info.text != ',' )
											info.throwError( 'syntax_error' );
										info.setPeekNextWordEnd();
									} while( true )

									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								break;

							// On error
							case 'on error':
								info.extractNextWord( line );
								if ( info.type != 'token' || ( info.type == 'token' && info.token.token != 'goto' && info.token.token != 'proc' ) )
									info.throwError( 'syntax_error' );
								info.extractNextWord( line );
								if ( info.type != 'variable' && info.type != 'procedure' && info.type != 'number' )
									info.throwError( 'syntax_error' );
								if ( info.section.onError )
									info.blockNumber++;
								break;

							case 'goto':
								info.extractLabel( line );
								if ( info.section.onError )
									info.blockNumber++;
								break;

							case 'resume':
							case 'resume label':
								info.peekNextWord( line );
								if ( info.type == 'variable' || info.type == 'number' )
									info.setPeekNextWordEnd();
							case 'resume next':
								if ( info.section.onError )
									info.blockNumber++;
								break;

							case 'data':
								info.waitingFunctionCount = 0;
								while ( true )
								{
									info.section.dataPosition++;
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								break;
							case 'restore':
								if ( info.section.onError )
									info.blockNumber++;
								var labelType = info.getLabelType( line );
								if ( labelType == 'label' )
								{
									info.extractNextWord( line );
								}
								else if ( labelType = 'expression' )
								{
									info.section.saveLabels = true;
									info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOLON );
								}
								break;
							case 'read':
								while ( true )
								{
									info.extractNextWord( line );
									if ( info.type != 'variable' )
										info.throwError( 'syntax_error' );
									info.extractVariableAssignment( line, 'read' );
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								if ( info.section.onError )
									info.blockNumber++;
								break;

							case 'print':
								info.peekNextWord( line );
								info.waitingFunctionCount = 0;
								while ( !info.endOfInstruction )
								{
									info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONSEMICOLON | EXPFLAG_ENDONCOMMA );
									info.peekNextWord( line );
									if ( info.text != ';' && info.text != ',' )
										break;
									info.position = info.peekNextWordEnd;
									info.peekNextWord( line );
								}
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'print using':
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONSEMICOLON );
								if ( info.returnType != '2' )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.text != ';' )
									info.throwError( 'syntax_error' );
								while ( !info.endOfInstruction )
								{
									info.compileExpression( line, EXPFLAG_ENDONSEMICOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
									info.peekNextWord( line );
									if ( info.text != ';' && info.text != ',' )
										break;
									info.setPeekNextWordEnd();
									info.peekNextWord( line );
								}
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'print #':
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONCOMMA );
								if ( info.returnType != '0' )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.text != ',' )
									info.throwError( 'syntax_error' );
								while ( !info.endOfInstruction )
								{
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON | EXPFLAG_ENDONSEMICOLON );
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
									info.peekNextWord( line );
								}
								if ( info.text == ';' )
									info.setPeekNextWordEnd();
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'field':
								info.waitingFunctionCount = 0;
								info.compileExpression( line, EXPFLAG_ENDONCOMMA );
								if ( !checkTypes( '0', info.returnType ) )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.text != ',' )
									info.throwError( 'syntax_error' );
								while( true )
								{
									info.compileExpression( line, EXPFLAG_ENDONAS );
									if ( !checkTypes( '0', info.returnType ) )
										info.throwError( 'type_mismatch' );
									info.extractNextWord( line );
									if ( info.textLower != 'as' )
										info.throwError( 'syntax_error' );
									info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA );
									if ( !checkTypes( '2', info.returnType ) )
										throw 'type_mismatch';
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								break;

							case 'shared':
								if ( info.section.type == 'main' )
									info.throwError( 'syntax_error' );
								while(true)
								{
									info.extractNextWord( line );
									if ( info.type != 'variable' && info.type != 'string' )
										info.throwError( 'syntax_error' );
									variable = info.getVariable( true );
									variable.global = true;
									if ( !options.sections[ 0 ].globals[ info.text ] )
										options.sections[ 0 ].globals.push( info.text );
									info.extractNextWord( line );
									if ( info.text == '(' )
									{
										info.extractNextWord( line );
										if ( info.text != ')' )
											info.throwError( 'syntax_error' );
										info.extractNextWord( line );
									}
									if ( info.text != ',' )
										break;
								}
								break;

							case 'global':
								while(true)
								{
									info.extractNextWord( line );
									if ( info.type != 'variable' && info.type != 'string' )
										info.throwError( 'syntax_error' );
									variable = info.getVariable( true );
									variable.global = true;
									if ( !options.sections[ 0 ].globals[ info.text ] )
										options.sections[ 0 ].globals.push( info.text );
									info.extractNextWord( line );
									if ( info.text == '(' )
									{
										info.extractNextWord( line );
										if ( info.text != ')' )
											info.throwError( 'syntax_error' );
										info.extractNextWord( line );
									}
									if ( info.text != ',' )
										break;
								}
								break;

							case 'line input':
							case 'input':
								info.peekNextWord( line );
								if ( info.type == 'string' )
								{
									info.setPeekNextWordEnd();
									info.extractNextWord( line );
									if ( info.text != ';' )
										info.throwError( 'syntax_error' );
								}
								while ( true )
								{
									info.extractNextWord( line );
									if ( info.type != 'variable'  )
										info.throwError( 'syntax_error' );
									info.extractVariableAssignment( line, 'input' );
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								info.blockNumber++;
								break;

							case 'line input #':
							case 'input #':
								info.compileExpression( line, EXPFLAG_ENDONCOMMA );
								if ( !checkTypes( '0', info.returnType ) )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.text != ',' )
									info.throwError( 'syntax_error' );
								while ( true )
								{
									info.extractNextWord( line );
									if ( info.type != 'variable'  )
										info.throwError( 'syntax_error' );
									info.extractVariableAssignment( line, 'input' );
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								info.blockNumber++;
								break;

							case 'palette':
							case 'default palette':
								info.waitingFunctionCount = 0;
								while ( true )
								{
									if ( info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON | EXPFLAG_SKIPNEXTTOKEN ) & EXPFLAG_ENDONCOLON )
										break;
								}
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'set transparent':
							case 'stop transparent':
								info.waitingFunctionCount = 0;
								while ( true )
								{
									if ( info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_SKIPNEXTTOKEN ) & EXPFLAG_ENDONCOLON )
										break;
								}
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'set alpha':
								info.waitingFunctionCount = 0;
								while ( true )
								{
									info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONTO | EXPFLAG_SKIPNEXTTOKEN );
									if ( info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_SKIPNEXTTOKEN ) & EXPFLAG_ENDONCOLON )
										break;
								}
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'gamepad map buttons':
							case 'gamepad map axes':
							case 'gamepad map triggers':
								info.waitingFunctionCount = 0;
								info.compileExpression( lineEXPFLAG_ENDONCOMMA );
								if ( !checkTypes( '2', info.returnType ) )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.text != ',' )
									info.throwError( 'syntax_error' );
								while ( true )
								{
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
									if ( !checkTypes( '0', info.returnType ) )
										info.throwError( 'type_mismatch' );
									info.peekNextWord( line );
									if ( info.text != ',' )
										break;
									info.setPeekNextWordEnd();
								}
								if ( token.doError && info.section.onError )
									info.blockNumber++;
								break;

							case 'channel':
								info.compileExpression( lineEXPFLAG_ENDONTO );
								if ( !checkTypes( info.returnType, '0' ) )
									info.throwError( 'type_mismatch' );
								info.extractNextWord( line );
								if ( info.textLower != 'to' )
									info.throwError( 'syntax_error' );
								info.extractNextWord( line );
								if ( info.type != 'token' )
									info.throwError( 'syntax_error' );
								info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
								if ( !checkTypes( info.returnType, '0' ) )
									info.throwError( 'type_mismatch' );
								if ( ( token.doError && info.section.onError ) || token.newBlock )
									info.blockNumber++;
								break;

							case 'polyline':
							case 'polygon':
								info.peekNextWord( line );
								if ( info.textLower == 'to' )
									info.setPeekNextWordEnd();
							// Normal instruction. If On Error in the section, slow mode, one block per instruction.
							default:
								info.waitingFunctionCount = 0;
								// Reserved variable?
								if ( token.params[ 0 ].charAt( 0 ) == 'V' )
								{
									info.extractNextWord( line );
									if ( info.text == '(' )
									{
										while ( true )
										{
											info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONBRACKET | EXPFLAG_ENDONTO );
											info.extractNextWord( line );
											if ( info.text == ')' )
												break;
											if ( info.text != ',' && info.textLower != 'to' )
												info.throwError( 'syntax_error' );
											info.setPeekNextWordEnd();
										}
										info.extractNextWord( line );
									}
									if ( info.text != '=' )
										info.throwError( 'syntax_error' );
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
									break;
								}
								// Normal token
								info.peekNextWord( line );
								while ( !info.endOfInstruction )
								{
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONTO | EXPFLAG_ENDONCOLON );
									info.peekNextWord( line );
									if ( info.text != ',' && info.textLower != 'to' )
										break;
									info.setPeekNextWordEnd();
									info.peekNextWord( line );
								}
								if ( ( token.doError && info.section.onError ) || token.newBlock )
									info.blockNumber++;
								break;
						}
						break;

					case 'variable':
						if ( info.section.onError )
							info.blockNumber++;
						info.waitingFunctionCount = 0;
						info.extractVariableAssignment( line );
						break;

					case 'remark':
						continue;

					default:
						break;
				}
			}
		}
		catch( error )
		{
			//console.log( 'Crash first pass!' );
		}
	}
	if ( info.section.pile.length > 1 )
	{
		for ( var l = 1; l < info.section.pile.length; l++ )
		{
			var loop = info.section.pile[ l ];
			switch( loop.type )
			{
				case 'for':
					messages.pushError( { error: 'for_without_next', file: options.currentSourceFilename, line: loop.line, column: loop.column } );
					break;
				case 'while':
					messages.pushError( { error: 'while_without_wend', file: options.currentSourceFilename, line: loop.line, column: loop.column } );
					break;
				case 'do':
					messages.pushError( { error: 'do_without_loop', file: options.currentSourceFilename, line: loop.line, column: loop.column } );
					break;
				case 'repeat':
					messages.pushError( { error: 'repeat_without_until', file: options.currentSourceFilename, line: loop.line, column: loop.column } );
					break;
			}
		}
	}
	if ( info.section.pileIfs.length > 1 )
	{
		for ( var l = 1; l < info.section.pileIfs.length; l++ )
		{
			var line = info.section.pileIfs[ l ];
			messages.pushError( { error: 'if_without_endif', file: options.currentSourceFilename, line: line.line, column: line.column } );
		}
	}
	if ( javascript > 0 )
	{
		messages.pushError( { error: 'javascript_section_not_closed', file: options.currentSourceFilename, line: javascriptLine, column: javascriptColumn } );
	}
	if ( remarkCount > 0 )
	{
		messages.pushError( { error: 'remark_not_closed', file: options.currentSourceFilename, line: remarkLine, column: remarkColumn } );
	}
	return info.currentLine;
}

// Second pass!
function secondPass( start, end, section, options )
{
	var info = new information( options, { section: section, blockNumber: 0, sourceLine: '', currentBlock: '', pass: 2 } );

	// Generate code
	var endProc = false;
	var javascript = 0;
	var remarkCount = 0;
	for ( info.currentLine = start; info.currentLine < end && !endProc; info.currentLine++ )
	{
		// End of a if/then
		if ( info.flagThen )
		{
			info.flagThen = false;
			info.nextBlock( '' );
		}

		// Get next line
		var line = lines[ info.currentLine ];
		if ( line == '' )
			continue;

		// Insert Javascript code?
		info.position = 0;
		if ( remarkCount == 0 )
		{
			info.peekNextWord( line )
			if ( javascript == 0 )
			{
				if ( info.text == '{' )
				{
					var printLine = utilities.printLine( line );
					javascriptStart = printLine.indexOf( '{' );
					javascript++;
					info.addLine( '// Javascript' );
					info.unIndent();
					continue;
				}
			}
			else
			{
				if ( info.text == '}' )
					javascript--;
				else if ( info.text == '{' )
					javascript++;
				if ( javascript > 0 )
				{
					var printLine = utilities.printLine( line );
					info.position = 0;
					info.skipSpaces( printLine );
					var start = Math.min( javascriptStart, info.position );
					var code = printLine.substring( start );
					info.addLine( code );
					info.setPeekNextWordEnd();
				}
				else
				{
					info.indent();
					info.addLine( '// Javascript end' );
					info.setPeekNextWordEnd();
				}
				continue;
			}
		}

		// Include sourcecode
		if ( info.options.includeSource )
			info.sourceLine = '// ' + utilities.trimString( line );

		var checkLabel = true;
		var quit = false;
		try
		{
			while( !quit && !endProc )
			{
				info.extractNextWord( line );
				if ( info.eol )
					break;
				if ( info.remark )
				{
					info.addLine( '' );
					break;
				}

				// A block remark?
				if ( info.text.substring( 0, 2 ) == '/*' )
				{
					remarkCount++;
					if ( remarkCount == 1 )
					{
						remarkLine = info.currentLine;
						remarkColumn = info.column;
					}
				}
				else if ( info.text.length >= 2 && info.text.substring( info.text.length - 2 ) == '*/' )
				{
					remarkCount--;
					if ( remarkCount == 0 )
						info.addLine( '' );
					continue;
				}
				if ( remarkCount > 0 )
				{
					info.addLine( '' );
					continue;
				}

				// Look for a label
				if ( checkLabel && ( info.type == 'variable' || info.type == 'number' ) )
				{
					if ( ( info.type == 'variable' && line.charAt( info.position ) == ':' ) || info.type == 'number' )
					{
						info.position++;
						if ( !info.section.labels[ info.text ] )
							info.throwError( 'unknown_error' );
						info.options.label = true;
						if ( !info.section.onError )
							info.nextBlock( '' );
						checkLabel = false;
						continue;
					}
				}
				checkLabel = false;

				switch ( info.type )
				{
					// A procedure call?
					case 'procedure':
						specialCases.handleProc( line, info, true );
						break;

					// An instruction?
					case 'instruction':
						var instructionDefinition = info.instructionDefinition;
						code = 'return{type:10,instruction:"' + instructionDefinition.name + '",';
						if ( info.extension )
						{
							code += 'klass:"' + info.extension.name + '",';
							info.extension.inUse = true;
						}
						code += 'args:{';
						if ( instructionDefinition.parameterNames.length > 0 )
						{
							info.peekNextWord( line );
							if ( !info.endOfInstruction )
							{
								var param = 0;
								info.waitingFunctionCount = 0;
								while( true )
								{
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
									var returnType = info.returnType;
									if ( returnType == '?' )
										returnType = instructionDefinition.parameterTypes[ param ];
									if ( !checkTypes( instructionDefinition.parameterTypes[ param ], returnType ) )
										info.throwError( 'type_mismatch' );
									code += instructionDefinition.parameterNames[ param ] + ':' + info.result;
									info.peekNextWord( line );
									if ( info.endOfInstruction )
										break;
									if ( info.textLower != instructionDefinition.parameterSeparators[ param ] )
										info.throwError( 'syntax_error' );
									info.setPeekNextWordEnd();
									code += ',';
									param++;
								}
							}
						}
						code += '}};';
						info.nextBlock( code );
						break;

					// A function?
					case 'function':
						info.throwError( 'syntax_error' );

					// A variable?
					case 'variable':
						info.addSourceReference();
						info.waitingFunctionCount = 0;
						info.extractVariableAssignment( line );
						info.addLine( info.result );
						if ( info.section.onError && token.doError )
							info.nextBlock( '' );
						break;

					// An instruction?
					case 'token':
						var token = info.token;

						// Skip a procedure
						if ( token.token == 'procedure' )
						{
							info.extractNextWord( line );
							if ( info.type != 'procedure' )
								info.throwError( 'syntax_error' );
							info.currentLine = info.procedure.endProc;
							quit = true;
							break;
						}
						else if ( token.token == 'instruction' )
						{
							info.extractNextWord( line );
							var instructionDefinition = options.instructions[ utilities.replaceSpacesByUnderscores( info.textLower ) ];
							info.currentLine = instructionDefinition.end - 1;
							quit = true;
							break;
						}
						else if ( token.token == 'function' )
						{
							info.extractNextWord( line );
							var functionDefinition = options.functions[ utilities.replaceSpacesByUnderscores( info.textLower ) ];
							info.currentLine = functionDefinition.end - 1;
							quit = true;
							break;
						}

						// The end of a procedure
						info.waitingFunctionCount = 0;
						if ( token.token == 'end proc' )
						{
							info.peekNextWord( line );
							if ( info.text == '[' )
							{
								info.position = info.peekNextWordEnd;
								info.compileExpression( line, EXPFLAG_ENDONSQUAREBRACKET );
								var code;
								if ( info.returnType == '2' )
									code = 'this.parent.procParam$=';
								else
									code = 'this.parent.procParam=';
								info.addLine( code + info.result + ';' );
								info.peekNextWord( line );
								if ( info.text != ']' )
									info.throwError( 'syntax_error' );
								info.setPeekNextWordEnd();
							}
							quit = true;
							endProc = true;
							info.addLine( 'return{type:0};' );
							info.currentLine = lines.length;			// Force end of loop
							break;
						}
						else if ( token.token == 'end function' )
						{
							info.extractNextWord( line );			// Skip (
							info.compileExpression( line, EXPFLAG_ENDONBRACKET );
							info.addLine( 'this.amos.functionResult=' + info.result + ';' );
							info.extractNextWord( line );			// Skip )
							quit = true;
							break;
						}

						// End Function

						// A normal token
						if ( typeof token.compile == 'undefined' )
							info.throwError( 'instruction_not_implemented' );

						// Call the compilation
						if ( typeof token.compile == 'string' && token.compile.indexOf( '#function' ) == 0 )
						{
							var func = token.compile.substring( 10 );
							specialCases[ func ]( line, info );
						}
						else
						{
							// Must be a normal instruction
							var isInstruction = false;
							for ( var t = 0; t < token.params.length; t++ )
							{
								if ( token.params[ 0 ].charAt( 0 ) == 'I' || token.params[ 0 ].charAt( 0 ) == 'V' )
								{
									isInstruction = true;
									break;
								}
							}
							if ( !isInstruction )
								info.throwError( 'instruction_not_implemented' );

							// Add line numbers
							info.addSourceReference();

							// Extract the parameters
							var params = 0, numberOfParams = 0;
							var isVariable = ( token.params[ 0 ].charAt( 0 ) == 'V' );
							var isReservedVariable = 0;
							var foundSyntax = isVariable ? 'V' : 'I';
							var parameters = [];
							var positions = [];
							info.peekNextWord( line );
							if ( info.text == '(' )
							{
								if ( isVariable )
									info.setPeekNextWordEnd();
							}
							else if ( isVariable )
							{
								info.endOfInstruction = true;
							}
							if ( !info.endOfInstruction )
							{
								while( true )
								{
									positions[ foundSyntax.length ] = info.position;
									info.compileExpression( line, ( isVariable ? EXPFLAG_ENDONBRACKET : 0 ) | EXPFLAG_ENDONTO | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
									parameters[ params ] = info.result;
									switch( info.returnType )
									{
										case '0':
										case '1':
											foundSyntax += '0';
											break;
										case '2':
											foundSyntax += '2';
											break;
										case '?':
											foundSyntax += '?';
											break;
									}

									info.peekNextWord( line );
									if ( info.endOfInstruction )
									{
										numberOfParams = params + 1;
										break;
									}
									if ( info.text == ')' )
									{
										numberOfParams = params + 1;
										if ( !isVariable )
											info.throwError( 'syntax_error' );
										info.position = info.peekNextWordEnd;
										break;
									}
									if ( info.text.toLowerCase() == 'to' )
										foundSyntax += 't';
									else
										foundSyntax += ',';
									info.setPeekNextWordEnd();
									params++;
								}
							}
							var found = false;
							var paramNumber = 0;
							for ( var s = 0; s < token.params.length; s++ )
							{
								var syntax = token.params[ s ];
								if ( foundSyntax.length == syntax.length )
								{
									if ( foundSyntax == syntax )
									{
										found = true;
										break;
									}
									for ( var p = 1; p < foundSyntax.length; p++ )
									{
										if ( foundSyntax.charAt( p ) == '?' )
										{
											foundSyntax = syntax.substring( 0, p ) + syntax.charAt( p ) + syntax.substring( p + 1 );
										}
										else if ( syntax.charAt( p ) >= 'A' && syntax.charAt( p ) <= 'C' )
										{
											foundSyntax = foundSyntax.substring( 0, p ) + syntax.charAt( p ) + foundSyntax.substring( p + 1 );
											isReservedVariable = p;
										}
									}
									var paramNumber = 0;
									for ( var p = 1; p < syntax.length; p++ )
									{
										var foundChar = foundSyntax.charAt( p );
										var char = syntax.charAt( p );
										if ( foundChar != char )
										{
											if ( char == '3' )
											{
												parameters[ paramNumber ] = '(' + parameters[ paramNumber ] + ')/this.amos.degreeRadian';
											}
											else if ( char != '?' )
											{
												if ( getCharacterType( char ) == 'number')
													info.throwError( 'type_mismatch' );
												else
													info.throwError( 'syntax_error' );
											}
										}
										if ( char == ',' || char == 't' )
											paramNumber++;
									}
									if ( p >= syntax.length )
									{
										found = true;
										break;
									}
								}
							}
							if ( !found )
								info.throwError( 'syntax_error' );

							// Generates the code
							var code = token.compile[ s ];
							if ( code != '' )
							{
								if ( isVariable )
								{
									info.extractNextWord( line );
									if ( info.text != '=' )
										info.throwError( 'syntax_error' );
									var type = token.token.indexOf( '$' ) >= 0 ? '2' : '0';
									info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONBRACKET | EXPFLAG_ENDONCOLON );
									if ( !checkTypes( type, info.returnType ) )
										info.throwError( 'type_mismatch' );
									if ( ! isReservedVariable )
									{
										code = utilities.replaceStringInText( code, '%$PV', 'set' );
										code = utilities.replaceStringInText( code, '%$PP', numberOfParams == 0 ? info.result : info.result + ',' );
										code = utilities.replaceStringInText( code, '%$P;', ';' );
									}
									else
									{
										var save = info.position;
										var saveResult = info.result;
										info.position = positions[ isReservedVariable ];
										info.extractNextWord( line );
										info.extractVariableAssignment( line, 'input' );
										info.position = save;
										code = utilities.replaceStringInText( code, '%$PV', 'set' );
										code = utilities.replaceStringInText( code, '%$P1', info.result );
										code = utilities.replaceStringInText( code, '%$PP', numberOfParams == 0 ? saveResult : saveResult + ',' );
										code = utilities.replaceStringInText( code, '%$P;', ';' );
									}
								}
								for ( var p = numberOfParams - 1; p >= 0; p-- )
								{
									code = utilities.replaceStringInText( code, '%$P' + ( p + 1 ), parameters[ p ] );
								}
								info.addLine( code );
								if ( ( info.section.onError && token.doError ) || token.newBlock )
									info.nextBlock( '' );
							}
							break;
						}
						break;

					case 'other':
						info.throwError( 'syntax_error' );
						break;

					case 'remark':
						continue;
				}
			}
		}
		catch( error )
		{
			//console.log( 'Crash second pass!' );
		}
	}

	// Finish last block
	info.sourceLine = '';
	if ( info.currentBlock != '' )
		info.nextBlock( '' );
	if ( !endProc )
		info.nextBlock( 'return {type:0};' );
};

var specialCases = {};
specialCases.handleChannel = function( line, info )
{
	info.compileExpression( line, EXPFLAG_ENDONTO );
	if ( !checkTypes( info.returnType, '0' ) )
		info.throwError( 'type_mismatch' );
	var channel = info.result;

	info.extractNextWord( line );
	if ( info.textLower != 'to' )
		info.throwError( 'syntax_error' );
	info.extractNextWord( line );
	switch ( info.textLower )
	{
		case 'sprite':
			code = 'this.amos.amalChannelToSprite(';
			break;
		case 'bob':
			code = 'this.amos.amalChannelToBob(';
			break;
		case 'screen display':
			code = 'this.amos.amalChannelToScreenDisplay(';
			break;
		case 'screen size':
			code = 'this.amos.amalChannelToScreenSize(';
			break;
		case 'screen offset':
			code = 'this.amos.amalChannelToScreenOffset(';
			break;
		case 'rainbow':
			code = 'this.amos.amalChannelToRainbow(';
			break;
		default:
			info.throwError( 'syntax_error' );
	}
	info.compileExpression( line, EXPFLAG_ENDONCOLON );
	if ( !checkTypes( info.returnType, '0' ) )
		info.throwError( 'type_mismatch' );
	code += channel + ',' + info.result + ');';

	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleRolB = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.rolB(%$P1,%$P2);' );
};
specialCases.handleRolW = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.rolW(%$P1,%$P2);' );
};
specialCases.handleRolL = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.rolL(%$P1,%$P2);' );
};
specialCases.handleRorB = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.rorB(%$P1,%$P2);' );
};
specialCases.handleRorW = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.rorW(%$P1,%$P2);' );
};
specialCases.handleRorL = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.rorL(%$P1,%$P2);' );
};
specialCases.handleBset = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.bSet(%$P1,%$P2);' );
};
specialCases.handleBclr = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.bClr(%$P1,%$P2);' );
};
specialCases.handleBchg = function( line, info )
{
	specialCases.handleBitInstruction( line, info, 'this.amos.bChg(%$P1,%$P2);' );
};
specialCases.handleBitInstruction = function( line, info, source )
{
	info.addSourceReference();

	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	var shift = info.result;
	if ( !checkTypes( '0', info.returnType ) )
		info.throwError( 'type_mismatch' );
	info.extractNextWord( line );
	if ( info.text != ',' )
		info.throwError( 'syntax_error' );
	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );
	info.extractVariableAssignment( line, 'input' );
	if ( !checkTypes( '0', info.returnType ) )
		throw 'type_mismatch';
	source = utilities.replaceStringInText( source, '%$P1', info.result );
	source = utilities.replaceStringInText( source, '%$P2', shift );
	info.addLine( source );
};
specialCases.handleRotationInstruction = function( line, info, source )
{
	info.addSourceReference();

	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );
	info.extractVariableAssignment( line, 'input' );
	var variable = info.result;
	info.extractNextWord( line );
	if ( info.text != ',' )
		throw 'syntax_error';
	info.compileExpression( line, EXPFLAG_ENDONCOLON );
	if ( !checkTypes( '0', info.returnType ) )
		throw 'type_mismatch';
	source = utilities.replaceStringInText( source, '%$P1', variable );
	source = utilities.replaceStringInText( source, '%$P2', info.result );
	info.addLine( source );
};
specialCases.handleColourBack = function( line, info )
{
	info.addSourceReference();

	info.peekNextWord( line );
	if ( info.text == '(' )
	{
		info.setPeekNextWordEnd();
		info.compileExpression( line, EXPFLAG_ENDONBRACKET );
		info.addLine( 'this.amos.colourBack(' + info.result + ',true);' );
		info.extractNextWord( line );
	}
	else
	{
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		info.addLine( 'this.amos.colourBack(' + info.result + ',false);' );
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleLineInput = function( line, info )
{
	this.handleInput( line, info, true );
};
specialCases.handleInput = function( line, info, isLineInput )
{
	info.addSourceReference();

	var savePosition = info.position;
	var printText;
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONSEMICOLON );
	if ( info.constant )
	{
		if ( info.returnType != '2' )
			info.throwError( 'type_mismatch' );
		info.extractNextWord( line );
		if ( info.text != ';' )
			info.throwError( 'syntax_error' );
		printText = info.result;
	}
	else
	{
		printText ='""';
		info.position = savePosition;
	}
	var code = 'return{type:8,instruction:"input",args:{text:' + printText + ',variables:[';
	while( true )
	{
		info.extractNextWord( line );
		if ( info.type != 'variable' )
			info.throwError( 'syntax_error' );
		info.extractVariableAssignment( line, 'input' );
		code += info.result;
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
		code += ',';
	}
	code += ']';
	if ( info.text == ';' )
	{
		code += ',newLine:false'
		info.setPeekNextWordEnd();
	}
	else
	{
		code += ',newLine:true'
	}
	if ( isLineInput )
	{
		code += ',isLineInput:true'
	}
	code += '}};'
	info.nextBlock( code );
	return info;
};
specialCases.handleLineInputFrom = function( line, info )
{
	this.doInputFrom( line, info, 'this.amos.filesystem.input(%$P1,%$P2,false);' );
};
specialCases.handleInputFrom = function( line, info )
{
	this.doInputFrom( line, info, 'this.amos.filesystem.input(%$P1,%$P2,true);' );
};
specialCases.doInputFrom = function( line, info, code )
{
	info.addSourceReference();

	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	var port = info.result;
	var variables = '[';
	info.extractNextWord( line );
	while( true )
	{
		info.extractNextWord( line );
		if ( info.type != 'variable' )
			info.throwError( 'syntax_error' );
		info.extractVariableAssignment( line, 'input' );
		variables += info.result;
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
		variables += ',';
	}
	variables += ']';
	code = utilities.replaceStringInText( code, '%$P1', port + '' );
	code = utilities.replaceStringInText( code, '%$P2', variables );
	info.nextBlock( code );
};
specialCases.handleMapButtons = function( line, info )
{
	this.handleMap( line, info, 0 );
};
specialCases.handleMapAxes = function( line, info )
{
	this.handleMap( line, info, 16 );
};
specialCases.handleMapTriggers = function( line, info )
{
	this.handleMap( line, info, 32 );
};
specialCases.handleMap = function( line, info, delta )
{
	info.addSourceReference();

	var code = 'this.amos.gamepadMap(';
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	if ( !checkTypes( '2', info.returnType ) )
		info.throwError( 'type_mismatch' );
	code += info.result + ',[';
	info.extractNextWord( line );
	while ( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		code += info.result;
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
		code += ',';
	}
	code += '],' + delta + ');';
	info.addLine( code );
}

specialCases.handleSetTransparent = function( line, info )
{
	this.doTransparent( line, info, 'true' );
}
specialCases.handleStopTransparent = function( line, info )
{
	this.doTransparent( line, info, 'false' );
}
specialCases.doTransparent = function( line, info, startStop )
{
	info.addSourceReference();

	var args = '';
	info.waitingFunctionCount = 0;
	while ( true )
	{
		var flags = info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON | EXPFLAG_SKIPNEXTTOKEN );
		args += info.result;
		if ( !( flags & EXPFLAG_ENDONCOMMA ) )
			break;
		args += ',';
	}
	info.addLine( 'this.amos.currentScreen.setTransparent([' + args + '],' + startStop + ');' );
	return info;
}

specialCases.handleSetAlpha = function( line, info )
{
	info.addSourceReference();

	var colors = alphas = '';
	info.waitingFunctionCount = 0;
	while ( true )
	{
		info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONTO | EXPFLAG_SKIPNEXTTOKEN );
		colors += info.result;
		var flags = info.compileExpression( line, EXPFLAG_WANTNUMBER | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON | EXPFLAG_SKIPNEXTTOKEN );
		alphas += info.result;
		if ( !(flags & EXPFLAG_ENDONCOMMA ) )
			break;
		colors += ',';
		alphas += ',';
	}
	info.addLine( 'this.amos.currentScreen.setColorAlpha([' + colors + '],[' + alphas + ']);' );
	return info;
}

specialCases.handlePalette = function( line, info )
{
	info.addSourceReference();

	var args = '[';
	info.waitingFunctionCount = 0;
	while ( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		args += info.result;
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
		args += ',';
	}
	args += ']';
	info.addLine( 'this.amos.currentScreen.setPalette(' + args + ');' );
	return info;
}
specialCases.handleDefaultPalette = function( line, info )
{
	info.addSourceReference();

	var args = '[';
	info.waitingFunctionCount = 0;
	while ( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		args += info.result;
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
		args += ',';
	}
	args += ']';
	info.addLine( 'this.amos.setDefaultPalette(' + args + ');' );
	return info;
}
specialCases.handleMax = function( line, info )
{
	info.extractNextWord( line );
	if ( info.text != '(' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	var type = info.returnType;
	var result1 = info.result;
	info.extractNextWord( line );
	if ( info.text != ',' )
		info.throwError( 'syntax_error' );
	info.compileExpression( line, EXPFLAG_ENDONBRACKET );
	info.peekNextWord( line );
	if ( info.text != ')' )
		info.throwError( 'syntax_error' );
	info.setPeekNextWordEnd();
	if ( !checkTypes( type, info.returnType ) )
		info.throwError( 'type_mismatch' );
	var code = '';
	if ( type != '2' )
		code = 'Math.max(' + result1 + ',' + info.result + ')';
	else
		code = '(' + result1 + '>' + info.result + '?' + result1 + ':' + info.result + ')';
	info.result = code;
	info.returnType = '0';
	return info;
};
specialCases.handleMin = function( line, info )
{
	info.extractNextWord( line );
	if ( info.text != '(' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	var type = info.returnType;
	var result1 = info.result;
	info.extractNextWord( line );
	if ( info.text != ',' )
		info.throwError( 'syntax_error' );
	info.compileExpression( line, EXPFLAG_ENDONBRACKET );
	info.peekNextWord( line );
	if ( info.text != ')' )
		info.throwError( 'syntax_error' );
	info.setPeekNextWordEnd();
	if ( !checkTypes( type, info.returnType ) )
		info.throwError( 'type_mismatch' );
	var code = '';
	if ( type != '2' )
		code = 'Math.min(' + result1 + ',' + info.result + ')';
	else
		code = '(' + result1 + '<' + info.result + '?' + result1 + ':' + info.result + ')';
	info.result = code;
	info.returnType = '0';
	return info;
};
specialCases.handleAdd = function( line, info )
{
	info.addSourceReference();
	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.extractVariableAssignment( line, 'input' );
	if ( info.returnType == '2' )
		info.throwError( 'type_mismatch' );
	var variable = info.result;
	info.extractNextWord( line );
	if ( info.text != ',' )
		throw 'syntax_error';
	info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA );
	var plus = info.result;
	if ( !checkTypes( '0', info.returnType ) )
		throw 'type_mismatch';
	info.peekNextWord( line );
	var code;
	if ( info.text == ',' )
	{
		info.setPeekNextWordEnd();
		info.compileExpression( line, EXPFLAG_ENDONTO );
		if ( !checkTypes( '0', info.returnType ) )
			throw 'type_mismatch';
		var start = info.result;
		info.extractNextWord( line );
		if ( info.textLower != 'to' )
			throw 'syntax_error';
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		var end = info.result;
		code = 'this.amos.add(' + variable + ',' + plus + ',' + start + ',' + end + ')';
	}
	else
	{
		code = 'this.amos.add(' + variable + ',' + plus + ');';
	}
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleSort = function( line, info )
{
	info.addSourceReference();
	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.extractVariableAssignment( line, 'sort' );
	info.addLine( info.result );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleMatch = function( line, info )
{
	info.extractNextWord( line );
	if ( info.text != '(' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.extractNextWord( line );
	info.extractVariableAssignment( line, 'match' );
	var variable = info.result;
	var variableType = info.returnType;
	info.extractNextWord( line );
	if ( info.text != ',' )
		throw 'syntax_error';
	info.compileExpression( line, EXPFLAG_ENDONBRACKET );
	info.extractNextWord( line );
	if ( info.text != ')' )
		throw 'syntax_error';
	if ( !checkTypes( variableType, info.returnType ) )
		throw 'type_mismatch';
	info.result = variable + info.result + ')';
	info.returnType = '0';
};
specialCases.handleInc = function( line, info )
{
	info.addSourceReference();
	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.extractVariableAssignment( line, '++' );
	if ( info.returnType == '2' )
		info.throwError( 'type_mismatch' );
	info.addLine( info.result );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleDec = function( line, info )
{
	info.addSourceReference();
	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );
	info.waitingFunctionCount = 0;
	info.extractVariableAssignment( line, '--' );
	if ( info.returnType == '2' )
		info.throwError( 'type_mismatch' );
	info.addLine( info.result );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handlePolyline = function( line, info )
{
	info.addSourceReference();
	var code = '[';
	info.peekNextWord( line );
	if ( info.textLower == 'to' )
	{
		code += 'undefined,undefined,';
		info.setPeekNextWordEnd();
	}
	info.waitingFunctionCount = 0;
	while ( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOMMA );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		info.peekNextWord( line );
		if ( info.text != ',' )
			info.throwError( 'syntax_error' );
		info.setPeekNextWordEnd();
		code += info.result;
		info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONTO );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		code += ',' + info.result;
		info.peekNextWord( line );
		if ( info.textLower != 'to' )
			break;
		info.setPeekNextWordEnd();
		code += ',';
	}

	code = 'this.amos.currentScreen.polyline(' + code + ']);';
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handlePolygon = function( line, info )
{
	info.addSourceReference();

	var code = '[';
	info.peekNextWord( line );
	if ( info.textLower == 'to' )
	{
		code += 'undefined,undefined,';
		info.position = info.peekNextWordEnd;
	}
	info.waitingFunctionCount = 0;
	while ( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOMMA );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		info.peekNextWord( line );
		if ( info.text != ',' )
			info.throwError( 'syntax_error' );
		info.setPeekNextWordEnd();
		code += info.result;
		info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONTO );
		if ( info.returnType == '2' )
			info.throwError( 'type_mismatch' );
		code += ',' + info.result;
		info.peekNextWord( line );
		if ( info.textLower != 'to' )
			break;
		info.setPeekNextWordEnd();
		code += ',';
	}
	code = 'this.amos.currentScreen.polygon(' + code + ']);';
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
	return info;
};
specialCases.handlePopProc = function( line, info )
{
	if ( info.section.type != 'procedure' )
		info.throwError( 'procedure_not_opened' );

	info.addSourceReference();

	info.peekNextWord( line );
	if ( info.text == '[' )
	{
		info.setPeekNextWordEnd();
		info.waitingFunctionCount = 0;
		info.compileExpression( line, EXPFLAG_ENDONSQUAREBRACKET );
		var code;
		if ( info.returnType == '2' )
			code = 'this.parent.procParam$=';
		else
			code = 'this.parent.procParam=';
		info.addLine( code + info.result + ';' );
		info.extractNextWord( line );
		if ( info.text != ']' )
			info.throwError( 'syntax_error' );
	}
	info.addLine( 'return{type:0};' );
};
specialCases.handleSwap = function( line, info )
{
	info.addSourceReference();

	var code = 'var temp=';
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	info.extractNextWord( line );
	if ( info.text != ',' )
		info.throwError( 'syntax_error' );
	code += info.result + ';'
	var pos = info.result.indexOf( 'getValue' );
	if ( pos >= 0 )
	{
		code += info.result.substring( 0, pos ) + 'setValue(';
		pos = info.result.indexOf( '[' );
		var pos2 = info.result.indexOf( ']' );
		code += info.result.substring( pos, pos2 + 1 ) + ',';
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		code += info.result + ');'
	}
	else
	{
		code += info.result + '=';
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		code += info.result + ';'
	}
	var pos = info.result.indexOf( 'getValue' );
	if ( pos >= 0 )
	{
		code += info.result.substring( 0, pos ) + 'setValue(';
		pos = info.result.indexOf( '[' );
		var pos2 = info.result.indexOf( ']' );
		code += info.result.substring( pos, pos2 + 1 ) + ',';
		code += 'temp);'
	}
	else
	{
		code += info.result + '=temp;';
	}
	info.addLine( code );
};
specialCases.handleShared = function( line, info )
{
	while(true)
	{
		info.extractNextWord( line );
		if ( info.type != 'variable' && info.type != 'string' )
			info.throwError( 'syntax_error' );
		info.extractNextWord( line );
		if ( info.text == '(' )
		{
			info.extractNextWord( line );
			info.extractNextWord( line );
		}
		if ( info.text != ',' )
			break;
	}
};
specialCases.handleGlobal = function( line, info )
{
	while(true)
	{
		info.extractNextWord( line );
		if ( info.type != 'variable' && info.type != 'string' )
			info.throwError( 'syntax_error' );
		info.extractNextWord( line );
		if ( info.text == '(' )
		{
			info.extractNextWord( line );
			info.extractNextWord( line );
		}
		if ( info.text != ',' )
			break;
	}
};
specialCases.handleData = function( line, info )
{
	info.waitingFunctionCount = 0;
	while( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
		if ( info.constant )
			info.section.datas.push( info.result );
		else
			info.section.datas.push( 'function(){return ' + info.result + '}' );
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
	}
};
specialCases.handleRestore = function( line, info )
{
	info.addSourceReference();

	var code;
	var labelType = info.getLabelType( line );
	if ( labelType == 'none' )
	{
		code = 'this.dataPosition=0;';
	}
	else if ( labelType == 'label' )
	{
		info.extractNextWord( line );
		var label = info.section.labels[ info.text ];
		if ( !label )
			info.throwError( 'label_not_defined' );
		code = 'this.dataPosition=' + label.dataPosition + ';';
	}
	else
	{
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		code = 'var temp=this.labels[' + info.result + ']; if (temp) this.dataPosition=temp.dataPosition;  else throw "label_not_defined";';
	}
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleRead = function( line, info )
{
	info.addSourceReference();

	var code;
	while ( true )
	{
		info.extractNextWord( line );
		info.extractVariableAssignment( line, 'read' );
		code = info.result;
		if ( !info.variableDefinition.isArray )
		{
			 code += '=this.amos.read(' + info.variableDefinition.type + ');';
		}
		else
		{
			code = utilities.replaceStringInText( code, '%$PP', 'this.amos.read(' + info.variableDefinition.type + ')' );
			code += ';';
		}
		info.addLine( code );
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleOnError = function( line, info )
{
	info.addSourceReference();

	info.extractNextWord( line );
	if ( info.type != 'token' && info.token.token != 'goto' && info.token.token != 'proc' )
		info.throwError( 'syntax_error' );
	if ( info.token.token == 'goto' )
	{
		info.extractNextWord( line );
		if ( info.type != 'variable' && info.type != 'number' )
			info.throwError( 'syntax_error' );
		var label = info.section.labels[ info.text ];
		if ( !label )
			info.throwError( 'label_not_found' );
		info.addLine( 'this.amos.onError=' + label.labelBlock + ';' );
	}
	else
	{
		info.extractNextWord( line );
		if ( info.type != 'procedure' )
			info.throwError( 'syntax_error' );
		info.addLine( 'this.amos.onError=this.proc' + info.text + ';' );
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleResume = function( line, info )
{
	info.addSourceReference();

	info.peekNextWord( line );
	if ( info.type == 'variable' || info.type == 'number' )
	{
		var label = info.section.labels[ info.text ];
		if ( !label )
			info.throwError( 'label_not_found' );
		info.addLine( 'return{type:5,label:' + label.labelBlock + '};' );
		info.setPeekNextWordEnd();
	}
	else
	{
		info.addLine( 'return{type:5};' );
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleResumeLabel = function( line, info )
{
	info.addSourceReference();

	info.peekNextWord( line );
	if ( info.type == 'variable' || info.type == 'number' )
	{
		var label = info.section.labels[ info.text ];
		if ( !label )
			info.throwError( 'label_not_found' );
		info.addLine( 'this.amos.resumeLabel=' + label.labelBlock + ';' );
		info.setPeekNextWordEnd();
	}
	else
	{
		info.addLine( 'return{type:7};' );
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleDim = function( line, info )
{
	info.addSourceReference();

	while( true )
	{
		info.extractNextWord( line );
		if ( info.type != 'variable' )
			info.throwError( 'syntax_error' );
		var variable = info.section.variables[ info.text ];
		if ( typeof variable == 'undefined' )
			info.throwError( 'syntax_error' );

		if ( variable.global && info.section.type != 'main' )
			code ='this.amos.vars.' + info.text + '.dim([';
		else
			code ='this.vars.' + info.text + '.dim([';

		info.extractNextWord( line );
		if ( info.text != '(' )
			info.throwError( 'syntax_error' );

		info.waitingFunctionCount = 0;
		for ( var d = 0; d < variable.numberOfDimensions; d++ )
		{
			info.compileExpression( line, ( d == variable.numberOfDimensions - 1 ) ? EXPFLAG_ENDONBRACKET : EXPFLAG_ENDONCOMMA );
			info.peekNextWord( line );
			if ( info.text != ',' && info.text != ')' )
				info.throwError( 'syntax_error' );
			if ( d > 0 )
				code += ',';
			code += info.result;
			info.setPeekNextWordEnd();
		}
		code += '],0);';
		info.addLine( code );
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
	}
}
specialCases.handleResumeNext = function( line, info )
{
	info.addSourceReference();

	info.addLine( 'return{type:6};' );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleOn = function( line, info )
{
	info.addSourceReference();

	var ons = info.section.anchors[ info.currentLine + '-' + info.position ];
	var code = 'switch(';
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONGOTO | EXPFLAG_ENDONGOSUB | EXPFLAG_ENDONPROC );
	if ( info.returnType != 0 )
		info.throwError( 'type_mismatch' );
	code += info.result + ')';
	info.addLine( code );
	info.addLine( '{' );
	info.indent();

	info.extractNextWord( line );
	if ( info.type != 'token' )
		info.throwError( 'syntax_error' );
	var token = info.token.token;

	var count = 1;
	while( true )
	{
		info.addLine( 'case ' + count + ':' );
		info.indent();
		switch ( token )
		{
			case 'goto':
				if ( info.getLabelType( line ) == 'label' )
				{
					info.extractNextWord( line );
					var label = info.section.labels[ info.text ];
					if ( !label )
						info.throwError( 'label_not_defined' );
					info.addLine( 'return{type:1,label:' + label.labelBlock + '};' );
				}
				else
				{
					info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
					info.addLine( 'var temp=this.labels[' + info.result + ']; if(temp) return{type:1,label:temp.labelBlock}; else throw "label_not_defined";' );
				}
				break;
			case 'gosub':
				if ( info.getLabelType( line ) == 'label' )
				{
					info.extractNextWord( line );
					var label = info.section.labels[ info.text ];
					if ( !label )
						info.throwError( 'label_not_defined' );
					info.addLine( 'return{type:2,label:' + label.labelBlock + ',return:' + ons.endBlock + '};' );
				}
				else
				{
					info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
					info.addLine( 'var temp=this.labels[' + info.result + ']; if(temp) return{type:2,label:temp.labelBlock,return:' + ons.endBlock + '}; else throw "label_not_defined";' );
				}
				break;
			case 'proc':
				if ( info.type != 'procedure' )
					info.throwError( 'procedure_not_found' );
				info.addLine( 'return {type:4,procedure:proc' + info.text + ',args:{}}' );
				break;
			default:
				info.throwError( 'syntax_error' );
		}
		info.unIndent();

		info.extractNextWord( line );
		if ( info.text != ',' )
			break;
		count++;
	};
	info.unIndent();
	info.nextBlock( '}' );

	function getLabel()
	{
	}
};
specialCases.handleEvery = function( line, info )
{
	info.addSourceReference();

	var code = '';
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONGOSUB | EXPFLAG_ENDONPROC );
	if ( info.returnType != 0 )
		info.throwError( 'type_mismatch' );
	var interval = info.result;

	info.extractNextWord( line );
	if ( info.type != 'token' )
		info.throwError( 'syntax_error' );
	var token = info.token.token;

	switch ( token )
	{
		case 'gosub':
			if ( info.getLabelType( line ) == 'label' )
			{
				info.extractNextWord( line );
				var label = info.section.labels[ info.text ];
				if ( !label )
					info.throwError( 'label_not_defined' );
				info.addLine( 'var temp={type:2,label:' + label.labelBlock + '};' );
			}
			else
			{
				info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONCOLON );
				info.addLine( 'var temp=this.labels[' + info.result + ']; if(temp) temp={type:2,label:temp.labelBlock}; else throw "label_not_defined";' );
			}
			break;
		case 'proc':
			info.extractNextWord( line );
			if ( info.type != 'procedure' )
				info.throwError( 'procedure_not_found' );
			info.addLine( 'var temp={type:4,procedure:proc' + info.text + ',args:{}};' );
			break;
		default:
			info.throwError( 'syntax_error' );
	}
	info.addLine( 'this.amos.every(' + interval +',temp);' );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleReturn = function( line, info )
{
	info.addSourceReference();

	info.addLine( 'return{type:3};' );
};
specialCases.handlePop = function( line, info )
{
	info.addSourceReference();

	info.addLine( 'return{type:3};' );
};
specialCases.handleEnd = function( line, info )
{
	info.addSourceReference();

	info.addLine( 'return{type:0}' );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleExit = function( line, info )
{
	info.addSourceReference();

	var exit = info.section.anchors[ info.currentLine + '-' + info.position ];
	var nLoops = 1;
	info.peekNextWord( line );
	if ( info.type == 'number' )
	{
		nLoops = parseInt( info.text );
		if ( nLoops > exit.loops.length - 1 )
			info.throwError( 'not_enough_loops_to_exit' );
		info.setPeekNextWordEnd();
	}
	info.addLine( 'return{type:1,label:' + exit.loops[ exit.loops.length - nLoops ].endBlock + '};' );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleExitIf = function( line, info )
{
	info.addSourceReference();

	var exit = info.section.anchors[ info.currentLine + '-' + info.position ];
	var code = 'if(';
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
	code += info.result + ')';
	info.addLine( code );
	info.indent();

	var nLoops = 1;
	info.peekNextWord( line );
	if ( info.type == 'number' )
	{
		nLoops = parseInt( info.text );
		if ( nLoops > exit.loops.length - 1 )
			info.throwError( 'not_enough_loops_to_exit' );
		info.setPeekNextWordEnd();
	}
	info.addLine( 'return{type:1,label:' + exit.loops[ exit.loops.length - nLoops ].endBlock + '};' );
	info.unIndent();
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleDo = function( line, info )
{
	info.addSourceReference();

	info.nextBlock( '' );
};
specialCases.handleLoop = function( line, info )
{
	info.addSourceReference();

	var loop = info.section.anchors[ info.currentLine + '-' + info.position ];
	info.nextBlock( 'return{type:1,label:' + loop.startBlock + '};' );
};
specialCases.handleRepeat = function( line, info )
{
	info.addSourceReference();

	info.nextBlock( '' );
};
specialCases.handleUntil = function( line, info )
{
	info.addSourceReference();

	var loop = info.section.anchors[ info.currentLine + '-' + info.position ];
	var code = 'if(!(';
	info.waitingFunctionCount = 0;
	info.comparaison = true;
	info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
	info.comparaison = false;
	code += info.result + '))';
	info.addLine( code );
	info.indent();
	info.nextBlock( 'return{type:1,label:' + loop.startBlock + '};' );
	info.unIndent();
};
specialCases.handleWhile = function( line, info )
{
	info.addSourceReference();

	var loop = info.section.anchors[ info.currentLine + '-' + info.position ];
	info.waitingFunctionCount = 0;
	info.comparaison = true;
	info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
	info.comparaison = false;
	loop.codeTest = info.result;
	var code = 'if(!(';
	code += info.result + '))';
	info.addLine( code );
	info.indent();
	info.addLine( 'return{type:1,label:' + loop.endBlock + '};' );
	info.unIndent();
	info.nextBlock( '' );
};
specialCases.handleWend = function( line, info )
{
	info.addSourceReference();

	var loop = info.section.anchors[ info.currentLine + '-' + info.position ];
	info.addLine( 'if(' + loop.codeTest + ')' )
	info.indent();
	info.nextBlock( 'return{type:1,label:' + loop.startBlock + '};' );
	info.unIndent();
};
specialCases.handleFor = function( line, info )
{
	info.addSourceReference();

	var fr = info.section.anchors[ info.currentLine + '-' + info.position ];
	info.extractNextWord( line );
	if ( info.type != 'variable' )
		info.throwError( 'syntax_error' );

	info.waitingFunctionCount = 0;
	info.extractVariableAssignment( line );
	if ( info.text.toLowerCase() != 'to' )
		info.throwError( 'syntax_error' );
	info.addLine( info.result );
	fr.variable = info.variableSection;

	info.extractNextWord( line );
	if ( info.textLower != 'to' )
		info.throwError( 'syntax_error' );

	info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONSTEP );
	fr.end = info.result;

	info.peekNextWord( line );
	if ( info.type == 'token' && info.token.token == 'step' )
	{
		info.setPeekNextWordEnd();
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		fr.step = info.result;
	}
	else
		fr.step = '1';

	var code = 'if(' + fr.variable;
	if (fr.step.charAt(0)=='-')
		code += fr.variableType == '0' ? '<' : '<=';				// TODO: remove that for future mode!
	else if ( getCharacterType(fr.step.charAt(0)) == 'number' )
		code += fr.variableType == '0' ? '>' : '>=';
	else
		code += '>=';
	code += fr.end + ')';
	info.addLine( code );
	info.indent();
	info.addLine( 'return{type:1,label:' + fr.endBlock+ '};' );
	info.unIndent();
	info.nextBlock( '' );
};
specialCases.handleNext = function( line, info )
{
	info.addSourceReference();

	var fr = info.section.anchors[ info.currentLine + '-' + info.position ];
	info.peekNextWord( line );
	if ( info.type == 'variable' )
		info.setPeekNextWordEnd();
	info.addLine( fr.variable + '+=' + fr.step + ';' );
	var code = 'if(' + fr.variable;
	if ( fr.step.charAt( 0 ) == '-' )
		code += fr.variableType == '0' ? '>=' : '>';
	else if ( getCharacterType( fr.step ) == 'number' )
		code += fr.variableType == '0' ? '<=' : '<';
	else
		code += '<=';
	code += fr.end + ')';
	info.addLine( code );
	info.indent();
	info.addLine( 'return{type:1,label:' + fr.startBlock + '}' );
	info.unIndent();
	info.nextBlock( '' );
};
specialCases.handleIf = function( line, info )
{
	info.addSourceReference();

	var ifs = info.section.anchors[ '' + info.currentLine + '-' + info.position ];
	if ( !ifs )
		info.throwError( 'internal_error' );
	var code = 'if(!(';
	info.waitingFunctionCount = 0;
	info.comparaison = true;
	info.compileExpression( line, EXPFLAG_ENDONTHEN | EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
	info.comparaison = false;
	code += info.result;
	info.addLine( code += '))' );
	info.indent();
	if ( ifs.elseBlocks.length )
		info.addLine( 'return{type:1,label:' + ifs.elseBlocks[ ifs.elseCount ] + '};' );
	else
		info.addLine( 'return{type:1,label:' + ifs.endIfBlock + '};' );
	info.unIndent();
	info.peekNextWord( line );
	if ( info.text.toLowerCase() == 'then' )
	{
		info.flagThen = true;
		info.setPeekNextWordEnd();
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleElse = function( line, info )
{
	var ifs = info.section.anchors[ '' + info.currentLine + '-' + info.position ];
	if ( !ifs )
		info.throwError( 'internal_error' );
	info.nextBlock( 'return{type:1,label:' + ifs.endIfBlock + '};' );
};
specialCases.handleElseIf = function( line, info )
{
	info.addSourceReference();

	var ifs = info.section.anchors[ '' + info.currentLine + '-' + info.position ];
	if ( !ifs )
		info.throwError( 'internal_error' );
	info.nextBlock( 'return{type:1,label:' + ifs.endIfBlock + '};' );

	var code = 'if(!(';
	info.waitingFunctionCount = 0;
	info.comparaison = true;
	info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_COMPARAISON );
	info.comparison = false;
	code += info.result + '))';
	info.addLine( code );
	info.indent();
	if ( ++ifs.elseCount < ifs.elseBlocks.length )
		info.addLine( 'return{type:1,label:' + ( ifs.elseBlocks[ ifs.elseCount ] ) + '};' );
	else
		info.addLine( 'return{type:1,label:' + ifs.endIfBlock + '};' );
	info.unIndent();
};
specialCases.handleEndIf = function( line, info )
{
	info.addSourceReference();

	info.nextBlock( '' );
};
specialCases.handleProc = function( line, info, noSkip )
{
	if ( !noSkip )
	{
		info.extractNextWord( line );
		if ( info.type != 'procedure' )
			info.throwError( 'syntax_error' );
	}
	var procedure = info.procedure;
	info.addSourceReference();
	code = 'return {type:4,procedure:proc' + info.text + ',args:{';
	info.peekNextWord( line );
	if ( info.eol || info.text == ':' || info.textLower == 'else' || info.type == 'remark' )
	{
		info.nextBlock( code + '}};' );
	}
	else
	{
		info.setPeekNextWordEnd();
		if ( info.text == '[' )
		{
			var count = 0;

			// Get the parameters
			info.waitingFunctionCount = 0;
			for ( var count = 0; count < procedure.parameterNames.length; count++ )
			{
				code += procedure.parameterNames[ count ] + ':';
				info.compileExpression( line, EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONSQUAREBRACKET );
				if ( !checkTypes( procedure.parameterTypes[ count ], info.returnType ))
					info.throwError( 'type_mismatch' );
				code += info.result;
				info.peekNextWord( line );
				if ( info.text == ']' )
				{
					info.setPeekNextWordEnd();
					break;
				}
				if ( info.text != ',' )
					info.throwError( 'syntax_error' );
				code += ',';
				info.setPeekNextWordEnd();
			}
			info.nextBlock( code + '}};' );
		}
		else
			info.throwError( 'syntax_error' );
	}
};
specialCases.handleGoto = function( line, info )
{
	info.addSourceReference();

	if ( info.getLabelType( line ) == 'label' )
	{
		info.extractNextWord( line );
		var label = info.section.labels[ info.text ];
		if ( !label )
			info.throwError( 'label_not_defined' );
		info.addLine( 'return{type:1,label:' + label.labelBlock + '};' );
	}
	else
	{
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		info.addLine( 'var temp=this.labels[' + info.result + ']; if(temp) return{type:1,label:temp.labelBlock}; else throw "label_not_defined";' );
	}
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handleGosub = function( line, info )
{
	info.addSourceReference();

	var gosub = info.section.anchors[ info.currentLine + '-' + info.position ];
	if ( info.getLabelType( line ) == 'label' )
	{
		info.extractNextWord( line );
		var label = info.section.labels[ info.text ];
		if ( !label )
			info.throwError( 'label_not_defined' );
		info.nextBlock( 'return{type:2,label:' + label.labelBlock + ',return:' + gosub.startBlock + '};' );
	}
	else
	{
		info.compileExpression( line, EXPFLAG_ENDONCOLON );
		info.nextBlock( 'var temp=this.labels[' + info.result + ']; if(temp) return{type:2,label:temp.labelBlock,return:' + gosub.startBlock + '}; else throw "label_not_defined";' );
	}
};
specialCases.handleField = function( line, info )
{
	info.addSourceReference();
	info.waitingFunctionCount = 0;
	var code = 'this.amos.filesystem.field(';
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	code += info.result + ',';
	info.extractNextWord( line );
	var variables = '';
	var lengths = '';
	while( true )
	{
		info.compileExpression( line, EXPFLAG_ENDONAS );
		lengths += info.result;
		info.extractNextWord( line );		// As
		info.extractNextWord( line );		// Variable
		info.extractVariableAssignment( line, 'input' );
		variables += info.result;
		info.peekNextWord( line );
		if ( info.text != ',' )
			break;
		info.setPeekNextWordEnd();
		lengths += ',';
		variables += ',';
	}
	code += '[' + variables + '],[' + lengths + ']);';
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handlePrint = function( line, info )
{
	info.addSourceReference();
	info.waitingFunctionCount = 0;
	this.doHandlePrint( line, info, 'this.amos.currentScreen.currentWindow.print(%$P1,%$P2);' );
}
specialCases.handlePrintTo = function( line, info )
{
	info.addSourceReference();
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONCOMMA );
	info.extractNextWord( line );
	this.doHandlePrint( line, info, 'this.amos.filesystem.print('+ info.result + ',%$P1,%$P2);' );
}
specialCases.doHandlePrint = function( line, info, code )
{
	var newLine = true;
	var toPrint = '';
	info.peekNextWord( line );
	if ( !info.endOfInstruction )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONSEMICOLON );
		while( true )
		{
			if ( info.returnType != 2 )
				toPrint += 'this.amos.str$(' + info.result + ')';
			else
				toPrint += info.result;
			newLine = true;
			info.peekNextWord( line );
			if ( info.text == ',' )
				toPrint += '+"\\t"';
			else if ( info.text == ';' )
				newLine = false;
			else if ( info.endOfInstruction )
				break;
			info.setPeekNextWordEnd();
			info.peekNextWord( line );
			if ( info.endOfInstruction )
				break;
			toPrint += '+';
			info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONSEMICOLON );
		}
	}
	else
	{
		toPrint += '""';
	}
	code = utilities.replaceStringInText( code, '%$P1', toPrint );
	code = utilities.replaceStringInText( code, '%$P2', newLine ? 'true' : 'false' );
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
specialCases.handlePrintUsing = function( line, info )
{
	info.addSourceReference();

	var newLine = true;
	var code = 'this.amos.currentScreen.currentWindow.printUsing(';
	info.waitingFunctionCount = 0;
	info.compileExpression( line, EXPFLAG_ENDONSEMICOLON );
	code += info.result;
	info.extractNextWord( line );
	info.peekNextWord( line );
	code += ',[';
	if ( !info.endOfInstruction )
	{
		info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONSEMICOLON );
		while( true )
		{
			code += info.result;
			info.peekNextWord( line );
			if ( ( info.text != ',' && info.text != ';' ) || info.endOfInstruction )
				break;
			info.setPeekNextWordEnd();
			code += ',';
			info.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONSEMICOLON );
		}
	}
	code += ']';
	if ( newLine )
		code += ',true'
	code += ');';
	info.addLine( code );
	if ( info.section.onError )
		info.nextBlock( '' );
};
function checkTypes( typeDestination, typeSource )
{
	typeDestination = typeof typeDestination == 'number' ? '' + typeDestination : typeDestination;
	typeSource = typeof typeSource == 'number' ? '' + typeSource : typeSource;
	if ( typeDestination == '0' || typeDestination == '1' )
	{
		return ( typeSource == '0' || typeSource == '1' || typeSource == '?' );
	}
	else if ( typeDestination == '?' )
	{
		return true;
	}
	return typeSource == '2' || typeSource == '?';
}



//////////////////////////////////////////////////////////////////////////////////////////




function information( options, variables )
{
	this.options = options;
	this.instructions = options.instructions;
	this.functions = options.functions;
	this.sections = options.sections;
	this.resourcesSubpath = options.resourcesSubpath;
	if ( variables )
	{
		for ( var v in variables )
		{
			this[ v ] = variables[ v ];
		}
	}
}
information.prototype.extractLabel = function( line )
{
	var type = this.getLabelType( line );
	if ( type == 'label' )
	{
		this.extractNextWord( line );
	}
	else if ( type == 'expression' )
	{
		this.compileExpression( line, EXPFLAG_ENDONCOLON );
	}
	else
	{
		info.throwError( 'syntax_error' );
	}
};
information.prototype.getVariable = function( setGlobalIfNew, assignment )
{
	var variable = this.section.variables[ this.text ];
	if ( !variable )
	{
		if ( this.isGlobalVariable( this.text ) )
		{
			variable = this.sections[ 0 ].variables[ this.text ];
		}
		else
		{
			variable =
			{
				name: this.text,
				type: this.returnType,
				isArray: false,
				global: typeof setGlobalIfNew != 'undefined' ? setGlobalIfNew : false,
				numberOfDimensions: 0
			}
			if ( setGlobalIfNew )
			{
				this.sections[ 0 ].globals.push( this.text );
			}
			if ( !assignment && this.options.warnings[ 'variable_not_declared' ] )
				this.warning( 'warning_variable_not_declared' );
		}
		this.section.variables[ this.text ] = variable;
 	}
	return variable;
/*
	var variable = this.section.variables[ this.text ];
	if ( !variable )
	{
		variable =
		{
			name: this.text,
			type: this.returnType,
			isArray: false,
			numberOfDimensions: 0
		};
	}
	if ( setGlobal )
	{
		sections[ 0 ].globals.push( this.text );
		variable.global = true;
	}
	else
	{
		variable.global = this.isGlobalVariable( this.text );
	}
	if ( variable.global && sections[ 0 ].variables[ this.text ] )
	{
		variable.isArray = sections[ 0 ].variables[ this.text ].isArray;
		variable.numberOfDimensions = sections[ 0 ].variables[ this.text ].numberOfDimensions;
	}
	if ( !this.section.variables[ this.text ] )
		this.section.variables[ this.text ] = variable;

	if ( ( setGlobal || variable.global ) && this.section.type != 'main' && !sections[ 0 ].variables[ this.text ] )
	{
		sections[ 0 ].variables[ this.text ] = variable;
	}
*/
};
information.prototype.extractVariableAssignment = function( line, incOrDec )
{
	incOrDec = typeof incOrDec == 'undefined' ? '' : incOrDec;
	var variable = this.getVariable( false, true );
	this.variableDefinition = variable;

	var code;
	var end;
	if ( !variable.isArray )
	{
		if ( incOrDec == 'input' )
		{
			code = '{'
			if ( variable.global && this.section.type != 'main' )
				code += 'root:true,';
			code += 'name:"' + variable.name + '",type:' + variable.type + '}';
			this.result = code;
			this.returnType = variable.type;
			return;
		}
		if ( variable.global && this.section.type != 'main' )
			code = 'this.root.vars.' + variable.name;
		else
			code = 'this.vars.' + variable.name;
		this.variableSection = code;
		if ( incOrDec == '++' || incOrDec == '--' )
		{
			if ( variable.type == '2' )
				this.throwError( 'type_mismatch' );
			this.result = code + incOrDec + ';';
			this.returnType = variable.type;
			return;
		}
		if ( incOrDec == 'sort' || incOrDec == 'match' )
		{
			throw 'syntax_error';
		}
		if ( incOrDec == 'read' )
		{
			this.result = code;
			this.returnType = variable.type;
			return;
		}
		this.extractNextWord( line );
		if ( this.text != '=' )
			this.throwError( 'syntax_error' );
		code += '=';
		this.compileExpression( line, EXPFLAG_ENDONTO | EXPFLAG_ENDONCOLON );
		if ( !checkTypes( variable.type, this.returnType ) )
			this.throwError( 'type_mismatch' );
		if ( variable.type == '0' )
		{
			if ( this.constant )
			{
				if ( this.returnType == '0' )
					code += this.result;
				else
					code += '' + parseInt( this.result );
			}
			else
			{
				code += 'Math.floor(' + this.result + ')';
			}
		}
		else
		{
			code += this.result;
		}
		this.result = code + ';';
	}
	else
	{
		if ( incOrDec == 'input' )
		{
			code = '{';
			if ( variable.global && this.section.type != 'main' )
				code += 'root:true,';
			code += 'name:"' + variable.name + '",';
		}
		else
		{
			if ( variable.global && this.section.type != 'main' )
				code = 'this.root.vars.' + variable.name;
			else
				code = 'this.vars.' + variable.name;
			this.variableSection = code;
		}
		if ( incOrDec == '' || incOrDec == 'read' )
			code += '.setValue(';
		else if ( incOrDec == 'sort' )
			code += '.sort(';
		else if ( incOrDec == 'match' )
			code += '.match(';
		else if ( incOrDec == '++' )
			code += '.inc(';
		else if ( incOrDec == '--' )
			code += '.dec(';
		this.extractNextWord( line );
		if ( this.text != '(' )
			this.throwError( 'syntax_error' );
		var parameters = '[';
		for ( var d = 0; d < variable.numberOfDimensions; d++ )
		{
			this.compileExpression( line, ( d == variable.numberOfDimensions - 1 ) ? EXPFLAG_ENDONBRACKET : EXPFLAG_ENDONCOMMA );
			if ( this.returnType != '0' )
				this.throwError( 'type_mismatch' );
			this.peekNextWord( line );
			if ( this.text != ',' && this.text != ')' )
				this.throwError( 'syntax_error' );
			if ( d > 0 )
				parameters += ',';
			parameters += this.result;
			this.setPeekNextWordEnd();
		}
		parameters += ']';
		if ( incOrDec == '' )
		{
			code += parameters;
			code += ',';
			this.extractNextWord( line );
			if ( this.text != '=' )
				this.throwError( 'syntax_error' );
			this.compileExpression( line, EXPFLAG_ENDONCOLON | EXPFLAG_ENDONTO );
			if ( !checkTypes( variable.type, this.returnType ) )
				this.throwError( 'type_mismatch' );
			if ( variable.type == '0' )
			{
				if ( this.constant )
				{
					if ( this.returnType == '0' )
						code += this.result;
					else
						code += '' + parseInt( this.result );
				}
				else
				{
					code += 'Math.floor(' + this.result + ')';
				}
			}
			else
			{
				code += this.result;
			}
			this.result = code + ');';
		}
		else if ( incOrDec == 'input' )
		{
			code += 'dimensions:' + parameters + ',type:' + variable.type + '}';
			this.result = code;
		}
		else if ( incOrDec == 'read' )
		{
			code += parameters + ',%$PP)';
			this.result = code;
		}
		else if ( incOrDec == 'sort' )
		{
			code += parameters + ')';
			this.result = code;
		}
		else if ( incOrDec == 'match' )
		{
			code += parameters + ',';
			this.result = code;
		}
		else if ( incOrDec == '++' || incOrDec == '--' )
		{
			code += parameters + ')';
			this.result = code;
		}
	}
	this.returnType = variable.type;
};
information.prototype.getLabelType = function( line )
{
	var result = 'none';
	var savePosition = this.position;
	do
	{
		this.extractNextWord( line );
		if ( this.endOfInstruction )
			break;
		result = 'label';
		this.peekNextWord( line );
		if ( !this.endOfInstruction && this.text != ',' )
			result = 'expression';
	} while( false );
	this.position = savePosition;
	return result;
}
information.prototype.compileExpression = function( line, flags = 0 )
{
	var result = this.compileExp( line, flags );
	this.operator = false;
	return result;
};
information.prototype.compileExp = function( line, flags )
{
	var type;
	var quit = false;
	var code = '';
	this.constant = true;
	this.numberOfOperands = 0;
	var power = false;
	var powerPosition1, powerPosition2;
	var skipOperand = false;
	var result = '';
	var returnFlags = 0;

	while( !quit )
	{
		if ( !skipOperand )
		{
			// An operande first!
			var positionOperand = code.length;
			this.operator = false;
			skipOperator = false;
			this.extractNextWord( line );
			this.typeLastOperand = this.type;

			switch ( this.type )
			{
				case 'token':
					var token = this.token;

					// Special case for To for undefined parameters
					if ( this.token.token == 'to' )
					{
						code += 'undefined';
						if ( type )
							this.throwError( 'syntax_error' );
						type = '?';
						skipOperator = true;
						quit = true;
						break;
					}

					// Special case for NOT
					if ( this.token.token == 'not' )
					{
						code += '!(';
						this.compileExpression( line, flags );
						code += this.result + ')';
						if ( this.returnType != '0' )
							this.throwError( 'type_mismatch' );
						if ( !type )
							type = this.returnType
						break;
					}

					// Must be a function
					var foundSyntax = '';
					for ( var t = 0; t < token.params.length; t++ )
					{
						if ( token.params[ t ].charAt( 0 ) == 'V' || getCharacterType( token.params[ t ].charAt( 0 ) ) == 'number' )
						{
							foundSyntax = token.params[ t ].charAt( 0 );
							break;
						}
					}
					if ( foundSyntax == '' )
						this.throwError( 'syntax_error' );
					if ( typeof token.compile == 'undefined' )
						this.throwError( 'function_not_implemented' );

					if ( typeof token.compile == 'string' && token.compile.indexOf( '#function' ) == 0 )
					{
						var func = token.compile.substring( 10 );
						specialCases[ func ]( line, this );
						code += this.result;
						this.constant = false;
						type = this.returnType;
						break;
					}

					// Extract the parameters
					var numberOfParams = -1;
					var parameters = [];
					var positions = [];
					var isVariable = ( foundSyntax == 'V' );

					this.peekNextWord( line );
					if ( this.text == '(' )
					{
						this.setPeekNextWordEnd();

						var params = 0;
						while( true )
						{
							positions[ foundSyntax.length ] = this.position;
							this.acceptCommas = true;
							this.peekNextWord( line );
							if ( this.text == ')' )
							{
								// Missing last parameter?
								if ( foundSyntax.charAt( foundSyntax.length - 1 ) == ',' || foundSyntax.charAt( foundSyntax.length - 1 ) == 't')
								{
									foundSyntax += '?';
									parameters[ params++ ] = 'undefined';
								}
								numberOfParams = params;
								this.setPeekNextWordEnd();
								break;
							}

							if ( this.text != ')' )
							{
								this.compileExpression( line, EXPFLAG_ENDONBRACKET | EXPFLAG_ENDONCOMMA | EXPFLAG_ENDONTO );
								parameters[ params ] = this.result;
								switch( this.returnType )
								{
									case '0':
									case '1':
										foundSyntax += '0';
										break;
									case '2':
										foundSyntax += '2';
										break;
									case '?':
										foundSyntax += '?';
										break;
								}
							}
							this.peekNextWord( line );
							if ( this.text == ',' )
							{
								foundSyntax += ','
								this.setPeekNextWordEnd();
							}
							else if ( this.textLower == 'to' )
							{
								foundSyntax += 't'
								this.setPeekNextWordEnd();
							}
							else if ( this.text == ')' )
							{
								numberOfParams = params;
								this.setPeekNextWordEnd();
								break;
							}
							else
							{
								this.throwError( 'syntax_error' );
							}
							params++;
						}
					}

					var found = false;
					for ( var s = 0; s < token.params.length; s++ )
					{
						var syntax = token.params[ s ];

						if ( syntax.charAt( 0 ) == foundSyntax.charAt( 0 ) )
						{
							if ( foundSyntax == syntax )
							{
								found = true;
								break;
							}

							if ( foundSyntax.length == syntax.length )
							{
								var paramNumber = 0;
								for ( var p = 1; p < syntax.length; )
								{
									if ( foundSyntax.charAt( p ) != syntax.charAt( p ) )
									{
										if ( foundSyntax.charAt( p ) == '?' )
										{
											foundSyntax = foundSyntax.substring( 0, p ) + syntax.charAt( p ) + foundSyntax.substring( p + 1 );
										}
										if ( getCharacterType( syntax.charAt( p ) ) == 'letter' )
										{
											syntax = syntax.substring( 0, p ) + String.fromCharCode( syntax.charCodeAt( p ) - 65 + 48 ) + syntax.substring( p + 1 );
											if ( syntax.charAt( p ) != foundSyntax.charAt( p ) )
											{
												this.throwError( 'type_mismatch' );
											}
										}
										else if ( getCharacterType( syntax.charAt( p ) ) == 'number' )
										{
											if ( syntax.charAt( p ) !=  foundSyntax.charAt( p ) )
											{
												if ( syntax.charAt( p ) == '3' && foundSyntax.charAt( p ) == '0' )
												{
													parameters[ paramNumber ] = '(' + parameters[ paramNumber ] + ')/this.amos.degreeRadian';
												}
												else
												{
													this.throwError( 'type_mismatch' );
												}
											}
										}
										else
										{
											if ( syntax.charAt( p ) != '?' )
											{
												this.throwError( 'type_mismatch' );
											}
										}
									}
									if ( getCharacterType( syntax.charAt( p ) ) == 'number' || syntax.charAt( p ) == '?' )
										paramNumber++;
									if ( ++p >= syntax.length )
									{
										found = true;
										break;
									}
								}
								if ( found )
									break;
							}
						}
					}
					if ( !found )
						this.throwError( 'syntax_error' );

					// Check types
					var addBracket = '';
					var subCode = token.compile[ s ];
					var tokenType;
					if ( !isVariable )
					{
						tokenType = token.params[ s ].charAt( 0 );
					}
					else
					{
						tokenType = token.token.indexOf( '$' ) >= 0 ? '2' : '0';
						subCode = utilities.replaceStringInText( subCode, '%$PV', 'get' );
						subCode = utilities.replaceStringInText( subCode, '%$PP', '' );
						subCode = utilities.replaceStringInText( subCode, '%$P;', '' );
					}
					if ( !type )
						type = tokenType;
					var addBracket1 = '';
					var addBracket2 = '';
					if ( !checkTypes( tokenType, type ) )
					{
						if ( token.params[ s ].charAt( 0 ) == '3' )
						{
							addBracket1 = '(';
							addBracket2 = '*this.amos.degreeRadian)';
						}
						else
						{
							this.throwError( 'type_mismatch' );
						}
					}

					// A waiting function?
					if ( token.waiting )
					{
						for ( var p = numberOfParams; p >= 0; p-- )
							subCode = utilities.replaceStringInText( subCode, '%$P' + ( p + 1 ), parameters[ p ] );
						subCode = utilities.replaceStringInText( subCode, '%$PN', this.waitingFunctionCount );
						if ( this.pass == 2 )
							this.nextBlock( subCode );
						else
							this.blockNumber++;
						subCode = addBracket + 'this.amos.results[' + this.waitingFunctionCount++ + ']';
					}
					else
					{
						// Normal function
						subCode = addBracket1 + subCode + addBracket2;
						for ( var p = numberOfParams; p >= 0; p-- )
							subCode = utilities.replaceStringInText( subCode, '%$P' + ( p + 1 ), parameters[ p ] );
					}
					code += subCode;
					this.constant = false;
					break;

				case 'function':
					var functionDefinition = this.functionDefinition;
					var subCode = 'return{type:11,instruction:"' + functionDefinition.name + '",';
					if ( this.extension )
					{
						subCode += 'klass:"' + this.extension.name + '",';
						this.extension.inUse = true;
					}
					subCode += 'result:' + this.waitingFunctionCount + ',args:{';

					this.extractNextWord( line );
					if ( this.text == '(' )
					{
						var param = 0;
						this.peekNextWord( line )
						if ( this.text != ')' )
						{
							while( true )
							{
								this.compileExpression( line, EXPFLAG_ENDONBRACKET | EXPFLAG_ENDONCOMMA );
								subCode += functionDefinition.parameterNames[ param ] + ':' + this.result;
								var returnType = this.returnType;
								if ( type == '?' )
									type = functionDefinition.parameterTypes[ param ];
								if ( !checkTypes( functionDefinition.parameterTypes[ param ], returnType ) )
									this.throwError( 'type_mismatch' );
								this.extractNextWord( line );
								if ( this.text == ')' )
									break;
								if ( this.textLower != functionDefinition.parameterSeparators[ param ] )
									this.throwError( 'syntax_error' );
								subCode += ',';
								param++;
							}
						}
					}
					subCode += '}};';

					// Check types
					var returnType = functionDefinition.returnType;
					if ( !type )
						type = returnType;
					if ( !checkTypes( returnType, type ) )
						this.throwError( 'type_mismatch' );

					// Output code
					if ( this.pass == 2 )
						this.nextBlock( subCode );
					else
						this.blockNumber++;

					// Create code
					this.addLine( 'this.amos.results[' + this.waitingFunctionCount + ']=this.amos.functionResult;' );
					code += 'this.amos.results[' + this.waitingFunctionCount++ + ']';
					this.constant = false;
					break;

				case 'variable':
					// Look in section
					var variable = this.getVariable();
					if ( !type )
						type = this.returnType;
					if ( !checkTypes( type, this.returnType ) )
						this.throwError( 'type_mismatch' );
					if ( !variable.isArray )
					{
						code += 'this.';
						if ( variable.global && this.section.type != 'main' )
							code += 'root.vars.';
						else
							code += 'vars.';
						code += this.text;
					}
					else
					{
						if ( variable.global && this.section.type != 'main' )
							code += 'this.root.vars.' + this.text + '.getValue([';
						else
							code += 'this.vars.' + this.text + '.getValue([';
						this.extractNextWord( line );
						if ( this.text != '(' )
							this.throwError( 'syntax_error' );
						for ( var d = 0; d < variable.numberOfDimensions; d++ )
						{
							this.compileExpression( line, ( d == variable.numberOfDimensions - 1 ) ? EXPFLAG_ENDONBRACKET : EXPFLAG_ENDONCOMMA );
							if ( d > 0 )
								code += ',';
							code += this.result;
							this.peekNextWord( line );
							if ( this.text != ',' && this.text != ')' )
								this.throwError( 'syntax_error' );
							this.setPeekNextWordEnd();
						}
						code += '])';
					}
					this.constant = false;
					break;

				case 'number':
					if ( !type )
						type = this.returnType;
					if ( !checkTypes( type, this.returnType ) )
						this.throwError( 'type_mismatch' );
					code += this.text;
					break;
				case 'string':
					if ( !type )
						type = this.returnType;
					if ( !checkTypes( type, this.returnType ) )
						this.throwError( 'type_mismatch' );
					code += '"' + this.text + '"';
					break;
				default:
					if ( this.text == '-' )
					{
						code += '-';
						skipOperator = true;
						break;
					}
					if ( this.text == '(' )
					{
						code += '(';
						this.compileExpression( line, EXPFLAG_ENDONBRACKET );
						code += this.result + ')';
						if ( !type )
							type = this.returnType;
						if ( !checkTypes( type, this.returnType ) )
							this.throwError( 'type_mismatch' );
						this.peekNextWord( line );
						if ( this.text != ')' )
							this.throwError( 'syntax_error' );
						this.setPeekNextWordEnd();
						this.constant = false;
						break;
					}
					if ( this.text == '$' )
					{
						var text = '';
						var savePosition = this.position;
						for ( ; this.position < line.length; this.position++ )
						{
							c = line.charAt( this.position ).toUpperCase();
							var ascii = c.charCodeAt( 0 );
							if ( !( ( ascii >= 48 && ascii <= 57 ) || ( ascii >= 65 && ascii <= 70 ) ) )
								break;
							text += c;
						}
						if ( text.length > 0 )
						{
							if ( !type )
								type = '0';
							if ( !checkTypes( type, '0' ) )
								this.throwError( 'type_mismatch' );
							code += '0x' + text;
							break;
						}
						this.position = savePosition;
					}
					if ( this.text == '%' )
					{
						var text = '';
						var savePosition = this.position;
						for ( ; this.position < line.length; this.position++ )
						{
							c = line.charAt( this.position );
							if ( c != '0' && c != '1' )
								break;
							text += c;
						}
						if ( text.length > 0 )
						{
							if ( !type )
								type = '0';
							if ( !checkTypes( type, '0' ) )
								this.throwError( 'type_mismatch' );
							code += '0b' + text;
							break;
						}
						this.position = savePosition;
					}
					if ( this.text == ',' || this.text == ':' || this.remark )
					{
						code += 'undefined';
						if ( type )
							this.throwError( 'syntax_error' );
						type = '?';
						this.position = this.column;
						skipOperator = true;
						quit = true;
					}
					else if ( this.eol  )
					{
						code += 'undefined';
						if ( type )
							this.throwError( 'syntax_error' );
						type = '?';
						skipOperator = true;
						quit = true;
					}
					else
					{
						this.throwError( 'syntax_error' );
					}
					break;
			}
			this.numberOfOperands++;

			// ^ operator?
			if ( power )
			{
				var first = code.substring( powerPosition1, powerPosition2 );
				var second = code.substring( positionOperand );
				code = code.substring( 0, powerPosition1 ) + 'Math.pow(' + first + ',' + second + ')';
				power = false;
			}
		}
		skipOperand = false;

		// An operator?
		if ( skipOperator )
			continue;

		this.operator = true;
		this.peekNextWord( line );
		if ( ( flags & EXPFLAG_ENDONCOLON ) && this.eol )
		{
			returnFlags |= ( EXPFLAG_ENDONCOLON | EXPFLAG_ENDOFINSTRUCTION );
			this.endOfInstruction = true;
			break;
		}
		else if ( ( flags & EXPFLAG_ENDONCOLON ) && this.remark )
		{
			returnFlags |= ( EXPFLAG_ENDONCOLON | EXPFLAG_ENDOFINSTRUCTION );
			quit = true;
			break;
		}
		else if ( this.token )
		{
			switch ( this.token.token )
			{
				case 'as':
					quit = ( flags & EXPFLAG_ENDONAS ) != 0;
					returnFlags |= EXPFLAG_ENDONAS;
					break;
				case 'then':
					quit = ( flags & EXPFLAG_ENDONTHEN ) != 0 ;
					returnFlags |= EXPFLAG_ENDONTHEN;
					break;
				case 'else':
				case 'else if':
					quit = ( flags & EXPFLAG_ENDONCOLON ) != 0 ;
					returnFlags |= EXPFLAG_ENDONCOLON;
					break;
				case 'to':
					quit = ( flags & EXPFLAG_ENDONTO ) != 0 ;
					returnFlags |= EXPFLAG_ENDONTO;
					break;
				case 'step':
					quit = ( flags & EXPFLAG_ENDONSTEP ) != 0 ;
					returnFlags |= EXPFLAG_ENDONSTEP;
					break;
				case 'goto':
					quit = ( flags & EXPFLAG_ENDONGOTO ) != 0 ;
					returnFlags |= EXPFLAG_ENDONGOTO;
					break;
				case 'gosub':
					quit = ( flags & EXPFLAG_ENDONGOSUB ) != 0 ;
					returnFlags |= EXPFLAG_ENDONGOSUB;
					break;
				case 'proc':
					quit = ( flags & EXPFLAG_ENDONPROC ) != 0 ;
					returnFlags |= EXPFLAG_ENDONPROC;
					break;
				default:
					this.throwError( 'syntax_error' );
			}
		}
		else
		{
			switch ( this.textLower )
			{
				case '_and_':
					if ( flags & EXPFLAG_COMPARAISON )
					{
						type = undefined;
						code += '&&';
					}
					else
					{
						code += '&';
					}
					this.setPeekNextWordEnd();
					break;
				case '_or_':
					if ( flags & EXPFLAG_COMPARAISON )
					{
						type = undefined;
						code += '||';
					}
					else
					{
						code += '|';
					}
					this.setPeekNextWordEnd();
					break;
				case '=':
					code += '==';
					this.setPeekNextWordEnd();
					break;

				case '^':
					powerPosition1 = positionOperand;
					powerPosition2 = code.length;
					power = true;
					this.constant = false;
					this.setPeekNextWordEnd();
					break;
				case 'mod':
					code += '%';
					this.constant = false;
					this.setPeekNextWordEnd();
					break;

				case '-':
					if ( type == 2 )
					{
						this.setPeekNextWordEnd();
						code = 'this.amos.subtractString(' + code + ',';
						this.compileExpression( line, flags );		// WEAK!
						code += this.result + ')';
						skipOperand = true;
					}
					break;

				case '+':
				case '*':
				case '/':
				case '&':
				case '&&':
				case '|':
				case '||':
				case '<<':
				case '>>':
					code += this.text;
					this.constant = false;
					this.setPeekNextWordEnd();
					break;

				case '<':
				case '<=':
				case '>':
				case '>=':
				case '!=':
					code += this.text;
					this.constant = false;
					this.setPeekNextWordEnd();
					comparaison = true;
					break;
				case '<>':
					code += '!=';
					this.setPeekNextWordEnd();
					this.constant = false;
					comparaison = true;
					break;

				case ')':
					if ( !( flags & EXPFLAG_ENDONBRACKET ) )
						this.throwError( 'syntax_error' );
					returnFlags |= EXPFLAG_ENDONBRACKET;
					quit = true;
					break;
				case ',':
					if ( !( flags & EXPFLAG_ENDONCOMMA ) )
						this.throwError( 'syntax_error' );
					returnFlags |= EXPFLAG_ENDONCOMMA;
					quit = true;
					break;
				case ':':
					this.endOfInstruction = true;
					if ( !( flags & EXPFLAG_ENDONCOLON ) )
						this.throwError( 'syntax_error' );
					returnFlags |= EXPFLAG_ENDONCOLON | EXPFLAG_ENDOFINSTRUCTION;
					quit = true;
					break;
				case ';':
					if ( !( flags & EXPFLAG_ENDONSEMICOLON ) )
						this.throwError( 'syntax_error' );
					returnFlags |= EXPFLAG_ENDONSEMICOLON;
					quit = true;
					break;
				case ']':
					if ( !( flags & EXPFLAG_ENDONSQUAREBRACKET ) )
						this.throwError( 'syntax_error' );
					quit = true;
					break;

				default:
					this.throwError( 'syntax_error' );
			}
		}
	}
	this.returnType = type;
	this.result = result + code;
	if ( flags & EXPFLAG_SKIPNEXTTOKEN )
		this.skipNextWord( line );
	return returnFlags;
};

information.prototype.isGlobalVariable = function( name )
{
	if ( this.section.type != 'main' )
	{
		for ( var v = 0; v < this.section.globals.length; v++ )
		{
			if ( isFiltered( name, this.section.globals[ v ] ) )
				return true;
		}
	}
	for ( var v = 0; v < this.sections[ 0 ].globals.length; v++ )
	{
		if ( isFiltered( name, this.sections[ 0 ].globals[ v ] ) )
			return true;
	}
	return false;

	function isFiltered( name, filter )
	{
		if ( filter.indexOf( '*' ) >= 0 || filter.indexOf( '?' ) >= 0 )
		{
			var pName = 0;
			var pFilter = 0;
			while( pName < name.length )
			{
				var c = filter.charAt( pFilter++ );
				if ( c == '*' )
					break;
				else if ( c != '?' && c != name.charAt( pName ) )
					return false;
				pName++;
			}
		}
		else
		{
			if ( filter != name )
				return false;
		}
		return true;
	}
};

information.prototype.skipNextWord = function( line )
{
	// TODO: UGLY! Correct...
	var result = this.result;
	var token = this.token;
	var variable = this.variable;
	var procedure = this.procedure;
	var remark = this.remark;
	var text = this.text;
	var textLower = this.textLower;
	var type = this.type;
	var endOfInstruction = this.endOfInstruction;
	this.extractNextWord( line );
	this.result = result;
	this.token = token;
	this.variable = variable;
	this.procedure = procedure;
	this.remark = remark;
	this.text = text;
	this.textLower = textLower;
	this.type = type;
	this.endOfInstruction = endOfInstruction;
};
information.prototype.peekNextWord = function( line )
{
	var position = this.position;
	this.extractNextWord( line );
	this.peekNextWordEnd = this.position;
	this.position = position;
};
information.prototype.extractNextWord = function( line )
{
	this.token = false;
	this.variable = false;
	this.procedure = false;
	this.remark = false;
	this.text = '';
	this.textLower = '';
	this.type = '';
	this.endOfInstruction = false;
	var noArray = this.noArray;
	this.noArray = false;
	this.skipSpaces( line );
	if ( this.eol )
	{
		this.endOfInstruction = true;
		return;
	}

	this.column = this.position;
	var start = this.position;
	this.debugString = line.substring( this.position );
	var c = line.charAt( this.position );
	var type = getCharacterType( c );
	if ( type == 'quote' )
	{
		this.extractString( line );
		this.textLower = this.text.toLowerCase();
		return;
	}
	else if ( type == 'number' || ( !this.operator && type == 'minus' ) )
	{
		this.extractNumber( line );
		this.textLower = this.text.toLowerCase();
		return;
	}
	else if ( type == 'letter' )
	{
		var lineTokens = [];
		var lineText = '';
		var linePlus = '';
		this.extension = null;
		do
		{
			var pp;
			for ( pp = this.position; pp < line.length; pp++ )
			{
				c = line.charAt( pp ).toLowerCase();
				if ( ( c >= 'a' && c <= 'z' ) || c == ' ' || c == '_' || c == '$' || c == '#' )
				{
					if ( c == ' ' )
					{
						lineTokens.push( lineText );
						linePlus += '_';
					}
					else
					{
						lineText += linePlus + c;
						linePlus = '';
					}
				}
				else
				{
					pp = line.length;
					break;
				}
			}
			if ( linePlus == '' )
				lineTokens.push( lineText );
		} while ( pp < line.length)

		// AND or OR?
		if ( lineTokens[ 0 ] == 'and' )
		{
			this.text = '_and_';
			this.textLower = this.text;
			this.type = 'other';
			this.position += 3;
			return;
		}
		if ( lineTokens[ 0 ] == 'or' )
		{
			this.text = '_or_';
			this.textLower = this.text;
			this.type = 'other';
			this.position += 2;
			return;
		}

		// Find in normal tokens
		for ( lt = lineTokens.length - 1; lt >= 0; lt-- )
		{
			var lineTk = lineTokens[ lt ];

			// One of the main tokens?
			if ( tokenList[ lineTk ] )
			{
				this.text = line.substr( start, lineTk.length );
				this.textLower = this.text.toLowerCase();
				this.position = start + lineTk.length;
				this.type = 'token';
				this.token = tokenList[ lineTk ];
				if ( this.textLower == 'rem' )
				{
					this.remark = true;
					this.endOfInstruction = true;
				}
				if ( this.textLower == 'else' )
				{
					this.endOfInstruction = true;
				}
				return;
			}

			// Instructions or function within program itself?
			if ( this.instructions[ lineTk ] )
			{
				this.text = line.substr( start, lineTk.length );
				this.textLower = this.text.toLowerCase();
				this.position = start + lineTk.length;
				this.type = 'instruction';
				this.instructionDefinition = this.instructions[ lineTk ];
				return;
			}
			if ( this.functions[ lineTk ] )
			{
				this.text = line.substr( start, lineTk.length );
				this.textLower = this.text.toLowerCase();
				this.position = start + lineTk.length;
				this.type = 'function';
				this.functionDefinition = this.functions[ lineTk ];
				return;
			}

			// An instruction or function from an extension?
			if ( !this.options.isExtension )
			{
				for ( var e in this.options.extensions )
				{
					var extension = this.options.extensions[ e ];
					if ( extension.instructions[ lineTk ] )
					{
						this.text = line.substr( start, lineTk.length );
						this.textLower = this.text.toLowerCase();
						this.position = start + lineTk.length;
						this.type = 'instruction';
						this.extension = extension;
						this.instructionDefinition = extension.instructions[ lineTk ];
						return;
					}
					if ( extension.functions[ lineTk ] )
					{
						this.text = line.substr( start, lineTk.length );
						this.textLower = this.text.toLowerCase();
						this.position = start + lineTk.length;
						this.type = 'function';
						this.extension = extension;
						this.functionDefinition = extension.functions[ lineTk ];
						return;
					}
				}
			}
		}

		// Get the next word, adding numbers
		this.text = '';
		for ( ; this.position < line.length; this.position++ )
		{
			c = line.charAt( this.position );
			var t = getCharacterType( c );
			if (  t != 'letter' && t != 'number' )
				break;
			this.text += c;
		}
		this.textLower = this.text.toLowerCase();

		// A procedure?
		for ( var p = 1; p < this.sections.length; p++ )
		{
			if ( this.sections[ p ].name == this.text )
			{
				this.type = 'procedure';
				this.procedure = this.sections[ p ];
				return;
			}
		}

		// A label?
		if ( this.checkLabels && line.charAt( this.position ) == ':' )		// Want a column right after no space
		{
			this.type = 'label';
			this.position++;
			return;
		}

		// Should be a variable!
		this.type = 'variable';
		this.returnType = '0';
		if ( this.position < line.length )
		{
			this.skipSpaces( line );
			if ( !this.eol )
			{
				c = line.charAt( this.position );
				if ( c == '#' )
				{
					this.text += '_';
					this.returnType = '1';
					this.position++;
					this.skipSpaces( line );
					if ( !this.eol && line.charAt( this.position ) == '(' && !noArray )
					{
						this.text += '_array';
					}
				}
				else if ( c == '$' )
				{
					this.text += '$';
					this.returnType = '2';
					this.position++;
					this.skipSpaces( line );
					if ( !this.eol && line.charAt( this.position ) == '(' && !noArray )
					{
						this.text += '_array';
					}
				}
				else if ( c == '(' && !noArray )
				{
					this.text += '_array';
				}
			}
		}
		this.noArray = false;
		return;
	}

	// Any other type
	this.text = c;
	this.type = type;
	this.position++;
	if ( c == "'" )
	{
		this.remark = true;
		this.endOfInstruction = true;
	}
	else if ( c != ')' && c != ']' && c != '(' && c != ',' && c != '=' )
	{
		for ( ; this.position < line.length; this.position++ )
		{
			c = line.charAt( this.position );
			if ( getCharacterType( c ) != type )
				break;
			this.text += c;
		}
	}
	if ( this.text == ':' )
		this.endOfInstruction = true;
	if ( this.text == '//' )
	{
		this.remark = true;
		this.endOfInstruction = true;
	}
	this.textLower = this.text.toLowerCase();
};
information.prototype.extractNextChar = function( line )
{
	this.skipSpaces( line );
	if ( this.eol )
		return;

	this.text = line.charAt( this.position++ );
	this.type = getCharacterType( this.text );
};
information.prototype.extractString = function( line )
{
	this.skipSpaces( line );
	if ( this.eol )
		return;

	this.text = '';
	var quote = line.charAt( this.position++ );
	if ( getCharacterType( quote ) == 'quote' )
	{
		while( this.position < line.length )
		{
			var c = line.charAt( this.position++ );
			if ( c == '\\' )
			{
				this.text += '\\\\';
				continue;
			}
			else if ( c == quote )
			{
				this.type = 'string';
				this.returnType = '2';
				return;
			}
			this.text += c;
		}
		this.throwError( 'string_not_closed' );
	}
	else
	{
		this.type = 'empty';
	}
};
information.prototype.extractNumber = function( line )
{
	this.skipSpaces( line );
	if ( this.eol )
		return;

	this.text = '';
	this.type = 'empty';
	var c = line.charAt( this.position );
	if ( c == '-' )
	{
		this.position++;
		this.skipSpaces( line );
		if ( this.eol )
			return;
		this.text += '-';
		c = line.charAt( this.position );
	}
	if ( getCharacterType( c ) == 'number' )
	{
		this.text += c;
		this.position++;
		this.returnType = '0'
		while( this.position < line.length )
		{
			c = line.charAt( this.position );
			if ( !( ( c >= '0' && c <= '9' ) || c == '.' ) )
				break;
			this.text += c;
			if ( c == '.' )
				this.returnType = '1';
			this.position++;
		}
		this.type = 'number';
	}
};
information.prototype.skipSpaces = function( line )
{
	this.eol = false;
	while( this.position < line.length && ( line.charCodeAt( this.position ) == 32 || line.charCodeAt( this.position ) == 9 ) )
		this.position++;
	if ( this.position >= line.length )
		this.eol = true;
};
information.prototype.setPeekNextWordEnd = function()
{
	this.position = this.peekNextWordEnd;
};
information.prototype.addLine = function( code )
{
	if ( typeof this.sourceLine != 'undefined' && this.sourceLine != '' )
	{
		var sourceLine = utilities.trimString( this.sourceLine );
		this.currentBlock += this.options.tabs + sourceLine + '\n';
		this.sourceLine = '';
	}
	if ( code != '' )
		this.currentBlock += this.options.tabs + code + '\n';
};
information.prototype.nextBlock = function( code )
{
	if ( code != '' && this.sourceLine != '' )
	{
		this.currentBlock += this.options.tabs + this.sourceLine + '\n';
		this.sourceLine = '';
	}
	if ( code != '' )
		this.currentBlock += this.options.tabs + code + '\n';
	this.section.blocks[ this.blockNumber++ ] = this.currentBlock;
	this.currentBlock = '';
	if ( this.sourceLine != '' )
	{
		this.currentBlock += this.options.tabs + this.sourceLine + '\n';
		this.sourceLine = '';
	}
	this.options.label = false;
};
information.prototype.indent = function()
{
	this.options.tabs += '\t';
};
information.prototype.unIndent = function()
{
	this.options.tabs = this.options.tabs.substring( 0, this.options.tabs.length - 1 );
};
information.prototype.addSourceReference = function()
{
	if ( this.options.addSourceReference )
		this.addLine( 'this.amos.sourcePos="' + this.currentLine + ':' + this.column + '";' );
};
information.prototype.throwError = function( error )
{
	messages.pushError( { error: error, file: this.options.currentSourceFilename, line: this.currentLine + 1, column: this.column } );
	//throw {};
};
information.prototype.warning = function( error )
{
	messages.pushError( { warning: error, file: this.options.currentSourceFilename, line: this.currentLine + 1, column: this.column } );
};


function getCharacterType( c )
{
	if ( c >= '0' && c <= '9' )
		type = 'number';
	else if ( c == ' ' || c == "\t" )
		type = 'space';
	else if ( ( c >= 'a' && c <= 'z') || ( c >= 'A' && c <= 'Z' ) || c == '_' )
		type = 'letter';
	else if ( c == '"' )
		type = 'quote';
	else if ( c == "'")
		type = 'remark';
	else if ( c == ':' )
		type = 'column';
	else if ( c == ';' )
		type = 'semicolumn';
	else if ( c == '-' )
		type = 'minus';
	else if ( c == '(' || c == ')' )
		type = 'bracket';
	else
		type = 'other';
	return type;
};
