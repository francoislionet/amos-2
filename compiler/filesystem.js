/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * File system compiler
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 22/12/2018
 */
const fs = require( 'fs' );
const utilities = require( './utilities' );
const tokens = require( './tokens' );
const messages = require( './messages' );
const mkdirp = require( 'mkdirp' );

var errors;

function init( options )
{
	return true;
};
module.exports.init = init;

function compileFilesystem( options )
{
	var drives = [];
	var fileNumber = 0;

	// Default filesystem folder
	compileRoot( options.defaultFileSystemPath );

	// Application's filesystem folder
	compileRoot( options.sourcePath + '/filesystem/' );

	var code = '';
	var applicationFound = false;
	for ( var d in drives )
	{
		var drive = drives[ d ];
		first = false;
		code += '\t"' + drive.name + '":\n\t{\n\t\t__size__:' + drive.size + ',\n';
		createDrive( drive, '' );
		code += '\n\t},\n';
		if ( drive.name == options.contextName )
			applicationFound = true;
	}
	if ( !applicationFound )
	{
		code += '\t"' + options.contextName + '":\n\t{\n\t\tsize:1048576,\n\t},';
	}

	// Insert code into source, load the fonts.js runtime library file
	options.fileSystemTree += code;
	return true;

	function createDrive( drive, path, first, tabs )
	{
		tabs = typeof tabs == 'undefined' ? '' : tabs;
		for ( var f in drive.files )
		{
			var file = drive.files[ f ];
			if ( file.isDirectory )
			{
				first = false;
				code += '\t\t' + tabs + '"' + file.path.substring( path.length ) + '":\n' + '\t\t' + tabs + '{\n';
				tabs += '\t';
				createDrive( file, file.path + '/', true, tabs );
				tabs = tabs.substring( 0, tabs.length - 1 );
				code += '\n\t\t' + tabs + '},';
			}
		}
		if ( options.isExtension || ( !options.isExtension && options.externalFiles ) )
			utilities.createDirectories( options.destinationPath + '/resources/filesystem/' );
		for ( var f in drive.files )
		{
			var file = drive.files[ f ];
			if ( !file.isDirectory )
			{
				if ( options.isExtension || ( !options.isExtension && !options.externalFiles ) )
					code += '\t\t' + tabs + '"' + options.contextName + '_' + file.number + '":{length:' + file.size + ',base64:"' + file.base64 + '"},\n';
				else if ( options.isExtension || ( !options.isExtension && options.externalFiles ) )
				{
					code += '\t\t' + tabs + '"' + options.contextName + '_' + file.number + '":{length:' + file.size + '}';
					var fileCode = 'Filesdata[' + options.contextName + '_' + file.number + ']="' + file.base64 + '";\n';
					var filePath = options.destinationPath + '/resources/filesystem/' + options.contextName + '_' + file.number + '.js';
					options.fileSaver.saveUTF8( filePath, fileCode, { encoding: 'utf8' } );
				}
			}
		}
	};
	function compileRoot( path )
	{
		if ( !fs.existsSync( path ) )
			return;

		var files = utilities.getDirectory( path, { recursive: true } );

		// Lower level-> drives!
		for ( var f = 0; f < files.length; f++ )
		{
			var file = files[ f ];
			if ( !file.isDirectory )
			{
				messages.pushError( { compilerWarning: 'file_at_root_filesystem', file: file.name }, false );
			}
			else
			{
				var content = compileDrive( file.files, file.path + '/' );
				drives[ file.name ] =
				{
					name: file.name,
					files: content.content,
					size: content.size
				};
			}
		}
	};
	function compileDrive( drive, basePath, content )
	{
		var size = 1024 * 1024 * 1024;
		var content = {};
		for ( var f = 0; f < drive.length; f++ )
		{
			var file = drive[ f ];
			var path = file.path.substring( basePath.length );
			if ( !file.isDirectory )
			{
				var pos = file.name.indexOf( '.drivesize' )
				if ( pos >= 0 )
				{
					var s = parseInt( file.name.substring( 0, pos ) );
					if ( !isNaN( size ) )
						size = s;
				}
				else
				{
					var base64 = compileFile( file.path );
					if ( base64 )
					{
						content[ file.name ] =
						{
							name: file.name,
							path: path,
							isDirectory: false,
							length: file.size,
							base64: base64,
							number: fileNumber++
						};
					}
				}
			}
			else
			{
				var newContent = compileDrive( file.files, basePath );
				content[ file.name ] =
				{
					name: file.name,
					path: path,
					isDirectory: true,
					files: newContent.content
				};
			}
		}
		return { content: content, size: size };
	};
	function compileFile( path )
	{
		var buffer = utilities.loadFile( path, { encoding: 'arraybuffer' } );
		return utilities.convertArrayBufferToString( buffer );
	};
};
module.exports.compileFilesystem = compileFilesystem;
