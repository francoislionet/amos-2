/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * The return of AMOS!
 *
 * @author FL (Francois Lionet)
 * @date first push on 22/12/2018
 */
const version = '0.7 - 07/08/2019';
const manifestVersion = 7;
const manifestVersionExtension = 1;

const fs = require( 'fs' );
const mkdirp = require( 'mkdirp' );
const HJSON = require( 'hjson' );
const PATH = require("path");
const messages = require( './messages' );
const utilities = require( './utilities' );
const compiler = require( './compiler' );
const banks = require( './banks' );
const filesystem = require( './filesystem' );
const us = require( './us' ) ;
var timeStartOfCompilation;
var indexFiles = '';

// Default options
var baseOptions =
{
	currentPath: utilities.cleanPath( process.cwd() ),
	fullScreen: false,
	fullPage: false,
	version: version,
	externalFiles: true,
	includeSource: true,
	addSourceReference: false,
	executable: false,
	logOn: false,
	doCompile: true,
	doWatch: false,
	doExtract: false,
	extensions: {},
	clean: true,
	cleanExtensions: false,
	verbosity: messages.VERBOSE_MESSAGES,
	fonts:
	{
		amigaList: '',
		googleList: '',
		google: '',
		amiga: '',
		families: '',
		faces: '',
	},
	fileSystemTree: '',
	warnings:
	{
		'variable_not_declared': true
	}
};
baseOptions.version = version;
baseOptions.runtimePath = baseOptions.currentPath + '/runtime';
baseOptions.templatesPath = baseOptions.currentPath + '/templates';
baseOptions.libraryPath = baseOptions.runtimePath + '/run';
baseOptions.defaultPath = baseOptions.currentPath + '/default';
baseOptions.defaultFileSystemPath = baseOptions.defaultPath + '/filesystem';
baseOptions.fontPath = baseOptions.currentPath + '/fonts';
baseOptions.logPath = baseOptions.currentPath + '/compilation.log';
baseOptions.extensionsPath = baseOptions.currentPath + '/extensions';

// Title
messages.setVerbosity( baseOptions.verbosity );
messages.print_always( 'title0', version );
messages.print_always( 'title1', version );
messages.print_always( 'title2', version );
messages.print_always( 'title3', version );
messages.print_always( 'title4', version );
messages.print_always( 'title5', version );

// Get command line arguments
var error = false;
var quit = false;
var count = 0;
for ( var a = 2; ( a < process.argv.length ) && !quit && !error; a++ )
{
    switch ( command = process.argv[ a ].toLowerCase() )
    {
        case '-fullscreen':
			baseOptions.fullScreen = true;
            break;
		case 'clean':
			baseOptions.clean = true;
			break;
		case 'cleanExtensions':
			baseOptions.cleanExtensions = true;
			break;
        case '-fullpage':
			baseOptions.fullPage = true;
            break;
		case '-speed':
			command = process.argv[ ++a ].toLowerCase();
			if ( command != 'safe' && command != 'fair' && command != 'graphics' && command != 'fast' )
				error = true;
			baseOptions.speed = command;
			break;
		case '-exe':
			baseOptions.executable = true;
            break;
        case '-saveto':
			baseOptions.destinationPath = utilities.cleanPath( utilities.getString( process.argv[ ++a ] ) );
            break;
        case '-log':
			command = process.argv[ ++a ].toLowerCase();
			if ( command == 'on' )
				baseOptions.logOn = true;
			else if ( command == 'off' )
				baseOptions.logOn = false;
			else
				baseOptions.logPath = utilities.cleanPath( utilities.getString( command ) );
			break;
        case '-h':
        case '-help':
        case '--help':
            messages.print_always( 'help', version );
            exit_app();
            break;

		case '--extract':
			error = ( count++ != 0 );
			baseOptions.doExtract = true;
			count++;
			break;

		case '--compile':
			error = ( count++ != 0 );
			baseOptions.doCompile = true;
			count++;
			break;

		case '--watch':
			error = ( count++ != 0 );
			baseOptions.doWatch = true;
			count++;
			break;

		case '':
        case ' ':
			break;

		default:
            if ( command.charAt( 0 ) == '-' || baseOptions.sourcePath )
            {
                messages.error( 'command_not_supported', process.argv[ a ] );
                error = true;
            }
            else
            {
				baseOptions.sourcePath = utilities.cleanPath( utilities.getString( command ) );
				quit = true;
            }
            break;
    }
}

// Check that the source directory exists
if ( !baseOptions.sourcePath || ( baseOptions.sourcePath && !fs.existsSync( baseOptions.sourcePath ) ) )
{
	messages.pushError( { compilerError: 'application_does_not_exist', file: baseOptions.sourcePath } );
	theEnd( false );
}

// Destination path
if ( !baseOptions.destinationPath )
	baseOptions.destinationPath = baseOptions.sourcePath + '/html';

// Clean the folders


// Load the manifest
baseOptions.manifest = loadManifest( baseOptions );
if ( !baseOptions.manifest )
	theEnd( false );

// Check fundamental values-> warnings.
if ( ( baseOptions.manifest.default.screen.width / baseOptions.manifest.default.screen.window.fontWidth ) % 1 )
{
	messages.pushError( { compilerWarning: 'screen_not_multiple_of_font_size', file: path }, false );
}

baseOptions.fullPage |= ( typeof baseOptions.manifest.display.fullPage != 'undefined' ? baseOptions.manifest.display.fullPage : false );
baseOptions.fullScreen |= ( typeof baseOptions.manifest.display.fullScreen != 'undefined' ? baseOptions.manifest.display.fullScreen : false );
baseOptions.manifest.display.fullPage = baseOptions.fullPage;
baseOptions.manifest.display.fullScreen = baseOptions.fullScreen;

// Check the presence of main.amos
if ( baseOptions.manifest.infos.start )
	baseOptions.currentSourceFilename = baseOptions.manifest.infos.start;
else
	baseOptions.currentSourceFilename = 'main.amos';
baseOptions.currentSourcePath = baseOptions.sourcePath + '/' + baseOptions.currentSourceFilename;
if ( !fs.existsSync( baseOptions.currentSourcePath ) )
{
	messages.pushError( { compilerError: 'cannot_load_file', file: baseOptions.currentSourcePath }, false );
	theEnd( false );
}

// Initialization of compiler
var error = !compiler.init( baseOptions );
error |= !banks.init( baseOptions );
error |= !filesystem.init( baseOptions );
if ( error )
	theEnd( false );

// Compile the extensions that need to be compiled
if ( !compileExtensions() )
	theEnd( false );


// Compilation!
timeStartOfCompilation = new Date().getTime();
if ( baseOptions.doCompile )
{
	if ( !compile() )
		theEnd( false );
}

// Complete the code and generate the index
theEnd( true );

// Security to avoid Visual Studio Code hanging
setInterval( function()
{
	messages.print_always( messages.Title );
}, 10000 );

// Compilation!
function compile()
{
	try
	{
		messages.print_info( 'skip_line' );

		// Clean directory?
		if ( baseOptions.clean )
		{
			messages.print_info( 'removing_directory', baseOptions.destinationPath );
			utilities.deleteDirectory( baseOptions.destinationPath, { recursive: true, catch: false } );
		}

		// Create the FileSaver/Loader objects
		baseOptions.fileSaver = new utilities.FileSaver( baseOptions.destinationPath, { version: version } );
		baseOptions.fileLoader = new utilities.FileLoader( baseOptions.sourcePath, { version: version, recursive: true } );
		baseOptions.baseOptions = baseOptions;
		baseOptions.contextName = 'application';
		baseOptions.isExtension = false;

		messages.print_info( 'compiling', baseOptions.sourcePath, baseOptions.manifest.compilation.emulation, baseOptions.manifest.compilation.speed );

		utilities.createDirectories( baseOptions.destinationPath );
		utilities.createDirectories( baseOptions.destinationPath + '/run' );
		utilities.createDirectories( baseOptions.destinationPath + '/resources' );
		baseOptions.fileSaver.saveUTF8( baseOptions.destinationPath + '/run/application.js', compiler.compile( baseOptions ) );

		messages.print_info( 'compiling_images' );
		banks.compileImages( baseOptions );

		messages.print_info( 'compiling_sounds' );
		banks.compileSounds( baseOptions );

		messages.print_info( 'compiling_fonts' );
		banks.compileFonts( baseOptions, baseOptions.manifest.default.screen.window.font );

		messages.print_info( 'compiling_filesystem' );
		filesystem.compileFilesystem( baseOptions );

		copyResources( baseOptions );
		generateIndex( baseOptions );

		baseOptions.fileSaver.close();
		baseOptions.fileLoader.close();
	}
	catch( e )
	{
		if ( e.stack )
			theEnd( false, e.message, e.stack );
		return false;
	}
	return true;
}

// Compilation of extensions...
function compileExtensions()
{
	try
	{
		// Get the directories of the extensions
		var extensionsDirectories = utilities.getDirectory( baseOptions.extensionsPath, { recursive: false, onlyDirectories: true } );
		for ( var e in extensionsDirectories )
		{
			var extensionDir = extensionsDirectories[ e ];
			var newOptions = utilities.copyObject( baseOptions );
			newOptions.sourcePath = extensionDir.path;
			newOptions.destinationPath = extensionDir.path + '/html';
			newOptions.destinationPathLocal = './' + extensionDir.name;
			newOptions.extensionName = utilities.replaceSpacesByUnderscores( extensionDir.name ).toLowerCase();
			newOptions.contextName = newOptions.extensionName;

			// Clean directory?
			if ( baseOptions.cleanExtensions )
			{
				utilities.deleteDirectory( newOptions.destinationPath, { recursive: true, catch: false } );
			}

			// Handle this extension?
			newOptions.fileLoader = new utilities.FileLoader( newOptions.sourcePath, { version: version, recursive: true } );
			var htmlExists = fs.existsSync( newOptions.destinationPath );
			if ( !htmlExists || newOptions.fileLoader.isDirectoryDifferent( newOptions.sourcePath, { excludes: [ '*[/html/]*', '*[files_load.hjson]*', '*[extension.hjson]*' ], recursive: true } ) )
			{
				newOptions.isExtension = true;
				newOptions.fileSaver = new utilities.FileSaver( newOptions.destinationPath, { version: version } );
				messages.print_info( 'skip_line' );
				messages.print_info( 'compiling_extension', newOptions.sourcePath );
				if ( baseOptions.cleanExtensions )
					messages.print_info( 'removing_directory', newOptions.destinationPath );

				newOptions.manifest = loadManifest( newOptions );
				if ( !newOptions.manifest )
					theEnd( false );
				if ( !newOptions.manifest.versionExtension || newOptions.manifest.versionExtension < manifestVersionExtension )
				{
					messages.pushError( { compilerError: "illegal_extension_manifest", path: newOptions.sourcePath }, false );
					theEnd( false );
				}

				// Get the entry point
				if ( newOptions.manifest.infos.start )
					newOptions.currentSourceFilename = newOptions.manifest.infos.start;
				else
					newOptions.currentSourceFilename = 'main.amos';
				newOptions.currentSourcePath = newOptions.sourcePath + '/' + newOptions.currentSourceFilename;

				utilities.createDirectories( newOptions.destinationPath );
				utilities.createDirectories( newOptions.destinationPath + '/run' );
				utilities.createDirectories( newOptions.destinationPath + '/resources' )
				newOptions.fileSaver.saveUTF8( newOptions.destinationPath + '/run/object.js', compiler.compileExtension( newOptions ) );

				// Save instructions and functions for extension
				var instructions = {};
				for ( var i in newOptions.instructions )
				{
					var instruction = newOptions.instructions[ i ];
					instructions[ i ] =
					{
						name: instruction.name,
						type: instruction.type,
						parameterTypes: instruction.parameterTypes,
						parameterNames: instruction.parameterNames,
						parameterSeparators: instruction.parameterSeparators,
					}
				}
				var functions = {};
				for ( var f in newOptions.functions )
				{
					var func = newOptions.functions[ f ];
					functions[ f ] =
					{
						name: func.name,
						type: func.type,
						returnType: func.returnType,
						parameterTypes: func.parameterTypes,
						parameterNames: func.parameterNames,
						parameterSeparators: func.parameterSeparators,
					}
				}

				banks.compileImages( newOptions, baseOptions );
				banks.compileSounds( newOptions, baseOptions );
				var windowFont = utilities.getManifestProperty( 'default.screen.window.font', newOptions );
				banks.compileFonts( newOptions, windowFont );
				filesystem.compileFilesystem( newOptions, baseOptions );

				var extensionDefinition =
				{
					name: newOptions.extensionName,
					instructions: instructions,
					functions: functions ,
					imageList: newOptions.imageList,
					imagesPalette: newOptions.imagesPalette,
					soundList: newOptions.soundList,
					fileSystemTree: newOptions.fileSystemTree,
					fonts: utilities.copyObject( newOptions.fonts ),
					windowFont: utilities.copyObject( utilities.getManifestProperty( 'default.screen.window.font', newOptions, baseOptions ) )
				};
				var extensionJson = HJSON.stringify( extensionDefinition );
				utilities.saveUTF8( newOptions.sourcePath + '/extension.hjson', extensionJson );
				baseOptions.extensions[ newOptions.extensionName ] = extensionDefinition;

				newOptions.fileLoader.close();
				newOptions.fileSaver.close();

				// Print warnings
				messages.printWarnings();
				messages.clearWarnings();
			}
			else
			{
				baseOptions.extensions[ newOptions.extensionName ] = utilities.loadHJSON( newOptions.sourcePath + '/extension.hjson' );
			}
		}
	}
	catch( e )
	{
		if ( e.stack )
			theEnd( false, e.message, e.stack );
		return false;
	}
	return true;
}

// End of compilation
function theEnd( response, message, stack )
{
	// Wait for all files to be closed
	var handle = setInterval( function()
	{
		if ( utilities.numberOfFilesOpen == 0 )
		{
			clearInterval( handle );
			if ( !response )
			{
				if ( stack )
				{
					console.log( message );
					console.log( stack );
				}
				else
				{
					messages.printErrors();
					messages.printWarnings();
				}
				messages.print_always( 'compilation_failed' );
				process.exit( 1 );
			}
			messages.printWarnings();
			var deltaTime = ( new Date().getTime() - timeStartOfCompilation ) / 1000;
			messages.print_always( 'compilation_success', deltaTime.toString() );
			process.exit( 0 );
		}
	}, 10 );
}

//
// Copy various resources
//
function copyResources( options )
{
	var path = options.destinationPath + '/run/resources';
	utilities.createDirectories( path );

	if ( !options.isExtension )
	{
		// The cursor
		var cursorPath = options.manifest.default.screen.window.cursorImage ? options.manifest.default.screen.window.cursorImage : 'resources/cursor.png';
		if ( !utilities.isAbsolutePath( cursorPath ) )
			cursorPath = options.currentPath + '/' + cursorPath;
		cursorPath = PATH.resolve( cursorPath );

		options.fileSaver.copyAndUnlockImage( path + '/cursor.js', cursorPath, options );

		// The images for the full-screen button
		if ( options.manifest.display.fullScreenIcon )
		{
			var fullScreenPath = options.manifest.display.fullScreenIconImage;
			if ( !utilities.isAbsolutePath( fullScreenPath ) )
				fullScreenPath = options.currentPath + '/' + fullScreenPath;
			fullScreenPath = PATH.resolve( fullScreenPath );

			var smallScreenPath = options.manifest.display.smallScreenIconImage;
			if ( !utilities.isAbsolutePath( smallScreenPath ) )
				smallScreenPath = options.currentPath + '/' + smallScreenPath;
			smallScreenPath = PATH.resolve( smallScreenPath );

			options.fileSaver.copyFile( path + '/full_screen.png', fullScreenPath, options );
			options.fileSaver.copyFile( path + '/small_screen.png', smallScreenPath, options );
		}

		// Copy the filesystem files of the extensions if external
		if ( baseOptions.externalFiles )
		{
			var destinationDirectory = baseOptions.destinationPath + '/resources/filesystem';
			utilities.createDirectories( destinationDirectory );
			for ( var e in baseOptions.extensions )
			{
				var extension = baseOptions.extensions[ e ];
				if ( extension.inUse )
				{
					var sourceDirectory = extension.sourcePath + '/html/resources/filesystem';
					baseOptions.fileSaver.copyDirectory( destinationDirectory, sourceDirectory, { recursive: false } );
				}
			}
		}

		// Copy extension resources
		for ( var e in baseOptions.extensions )
		{
			var extension = baseOptions.extensions[ e ];
			if ( extension.inUse )
			{
				// Images
				var destinationDirectory = baseOptions.destinationPath + '/resources/images/' + extension.name;
				var sourceDirectory = baseOptions.extensionsPath + '/' + extension.name + '/html/resources/images';
				utilities.createDirectories( destinationDirectory );
				baseOptions.fileSaver.copyDirectory( destinationDirectory, sourceDirectory, { recursive: false } );

				// Sounds
				var destinationDirectory = baseOptions.destinationPath + '/html/resources/sounds/' + extension.name;
				var sourceDirectory = extension.destinationPath + '/' + extension.name+ '/html/resources/sounds';
				utilities.createDirectories( destinationDirectory );
				baseOptions.fileSaver.copyDirectory( destinationDirectory, sourceDirectory, { recursive: false } );
			}
		}
	}
};

//
// Load the manifest
//
function loadManifest( options )
{
	var path = options.sourcePath + '/manifest.hjson';
	var defaultJson = utilities.loadIfExist( options.defaultPath + '/manifest.hjson', { encoding: 'utf8' } );
	var json = utilities.loadIfExist( path, { encoding: 'utf8' } );
	if ( !json && !defaultJson )
	{
		messages.pushError( { compilerError: 'manifest_not_found', file: options.sourcePath + '/manifest.hjson' } );
		return false;
	}

	var manifest, defaultManifest;
	try
	{
		if ( defaultJson )
		{
			defaultJson = utilities.fixJson( defaultJson );
			defaultManifest = HJSON.parse( defaultJson );
		}
		if ( json )
		{
			json = utilities.fixJson( json );
			manifest = HJSON.parse( json );
		}
		if ( manifest && defaultManifest )
		{
			manifest = utilities.replaceMissingProperties( manifest, defaultManifest );
		}
		else if ( !manifest )
		{
			manifest = defaultManifest;
		}
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'illegal_manifest', file: path }, false );
		return false;
	}

	// Compatible version of the manifest?
	if ( !manifest.version || manifest.version < manifestVersion )
	{
		messages.pushError( { compilerError: 'bad_version_of_manifest', file: path }, false );
		return false;
	}
	return manifest;
};

//
// Generate index.html file
//
function generateIndex()
{
	// Load the proper index.html file
	var indexPath = baseOptions.runtimePath + '/index';
	if ( baseOptions.fullScreen || baseOptions.fullPage )
		indexPath += '_fullscreen';
	indexPath += '.html';
	var index = utilities.loadIfExist( indexPath, { encoding: 'utf8' } );
	if ( !index )
	{
		messages.pushError( { error: 'cannot_load_runtime_index', parameter: indexPath } );
		return false;
	}

	// Replace width and height
	index = utilities.replaceStringInText( index, 'CANVASWIDTH', '' + baseOptions.manifest.display.width );
	index = utilities.replaceStringInText( index, 'CANVASHEIGHT', '' + baseOptions.manifest.display.height );
	index = utilities.replaceStringInText( index, 'APPLICATIONNAME', '' + baseOptions.manifest.infos.applicationName );

	// Color of body
	var bodyBackgroundColor = baseOptions.manifest.display.bodyBackgroundColor;
	if ( !bodyBackgroundColor )
		bodyBackgroundColor = baseOptions.manifest.display.backgroundColor
	index = utilities.replaceStringInText( index, 'BODYCOLOR', '' + bodyBackgroundColor );

	// Image in the back
	var bodyBackgroundImage = '';
	var bodyImagePath = baseOptions.manifest.display.bodyBackgroundImage;
	if ( bodyImagePath )
	{
		if ( !utilities.isAbsolutePath( bodyImagePath ) )
			bodyImagePath = baseOptions.currentPath + '/' + bodyImagePath;
		bodyImagePath = PATH.resolve( bodyImagePath );
		var imageName = utilities.getFilename( bodyImagePath ) + '.' + utilities.getFileExtension( bodyImagePath );
		var dPath = baseOptions.destinationPath + '/run/resources/' + imageName;
		baseOptions.fileSaver.copyFile( dPath, bodyImagePath, baseOptions );
		bodyBackgroundImage = '\t\t<div id="bg">\n\t\t\t<img src="./run/resources/' + imageName + '" alt="">\n\t\t</div>\n';
	}
	index = utilities.replaceStringInText( index, 'BODYIMAGE', bodyBackgroundImage );

	// Copy runtime files and insert them into index.html
	var opts =
	{
		filters: [ '*.js' ],
		toCallback:
		[
			{ filter: 'errors.js', callback: cbErrors, encoding: 'utf8' },
			{ filter: 'amos.js', callback: cbAMOS, encoding: 'utf8'  },
			{ filter: 'filesystem.js', callback: cbFileSystem, encoding: 'utf8' },
			{ filter: 'banks.js', callback: cbBanks, encoding: 'utf8' },
			{ filter: 'fonts.js', callback: cbFonts, encoding: 'utf8' },
			{ filter: 'screens.js', callback: cbScreens, encoding: 'utf8' },
			{ filter: '*.js', callback: cbFile, encoding: 'utf8' }
		],
	}

	var fileLast = '';
	baseOptions.fileSaver.copyDirectory( baseOptions.destinationPath + '/run', baseOptions.libraryPath, opts );

	// Insert extension code
	for ( var e in baseOptions.extensions )
	{
		var extension = baseOptions.extensions[ e ];
		if ( extension.inUse )
		{
			var extensionPath = baseOptions.extensionsPath + '/' + extension.name + '/html/run/object.js';
			baseOptions.fileSaver.copyFile( baseOptions.destinationPath + '/run/' + extension.name + '.js', extensionPath );
			indexFiles += '\t\t<script src="./run/' + extension.name + '.js"></script>\n';
		}
	}

	// Put the list of Javascript files
	indexFiles += fileLast;
	indexFiles += '\t\t<script src="./run/application.js"></script>\n';
	index = utilities.replaceStringInText( index, 'INSERTFILES', indexFiles );

	// Put Google Fonts
	index = utilities.replaceStringInText( index, 'INSERT_GOOGLEFONTS_FACES', baseOptions.fonts.faces  );
	var css = baseOptions.fonts.families;
	css += ( css.length > 0 ? ',' : '' ) + 'sans-serif;';
	index = utilities.replaceStringInText( index, 'INSERT_GOOGLEFONTS_CSS', 'font-family: ' + css );

	// Save index.html
	baseOptions.fileSaver.saveUTF8( baseOptions.destinationPath + '/index.html', index, baseOptions );

	// No error
	return true;

	function cbErrors( response, data, extra )
	{
		var errors = data.source;
		var list = messages.getErrorList();
		var code = 'List of errors\n\tthis.errors=\n\t[\n';
		for ( var l = 0; l < list.length; l++ )
		{
			var pos = list[ l ].indexOf( ':' );
			if ( pos >= 0 )
			{
				var start = pos + 1;
				while( list[ l ].charCodeAt( start ) <= 32 )
					start++;
				code += '\t\t"' + list[ l ].substring( 0, pos + 1 ) + list[ l ].substring( start ) + '"';
			}
			else
			{
				code += '\t\t""';
			}
			if ( l < list.length - 1 )
				code += ',';
			code += '\n';
		}
		code += '\t];'
		errors = utilities.replaceStringInText( errors, '<ERRORS-INSERT>', code );
		indexFiles += '\t\t<script src="' + './run/errors.js"></script>\n';
		return errors;
	};
	function cbAMOS( response, data, extra )
	{
		var code = data.source;
		var mode = typeof baseOptions.manifest.compilation.speed != 'undefined' ? baseOptions.manifest.compilation.speed : 'safe';
		var maxLoopTime = 5;
		var timeCheckCounter = 100;
		switch ( mode )
		{
			case 'fair':
				maxLoopTime = 10;
				timeCheckCounter = 1000;
				break;
			case 'graphics':
				maxLoopTime = 15;
				timeCheckCounter = 100;
				break;
			case 'fast':
				maxLoopTime = 18;
				timeCheckCounter = 10000;
				break;
		}
		code = utilities.replaceStringInText( code, 'MAX_LOOP_TIME', '' + maxLoopTime );
		code = utilities.replaceStringInText( code, 'TIME_CHECK_COUNTER', '' + timeCheckCounter );
		fileLast += '\t\t<script src="' + './run/amos.js"></script>\n';
		return code;
	};
	function cbScreens( response, data, extra )
	{
		var insert = '';
		for ( var e in baseOptions.extensions )
		{
			var extension = baseOptions.extensions[ e ];
			if ( extension.inUse )
			{
				insert += 'this.bobContext.addContext("' + extension.name + '");\n';
			}
		}
		code = utilities.replaceStringInText( data.source, 'INSERT_EXTENSIONCONTEXT', insert );
		indexFiles += '\t\t<script src="' + './run/' + utilities.getFilename( data.path ) + '.js"></script>\n';
		return code;
	};
	function cbBanks( response, data, extra )
	{
		var code = 'this.imageBank.setElement( "application", new ImageBank( this.amos, [' + baseOptions.imageList + '], [ ' + baseOptions.imagesPalette +  ' ], {} ), 1 );\n';
		for ( var e in baseOptions.extensions )
		{
			var extension = baseOptions.extensions[ e ];
			if ( extension.inUse )
				code += '\tthis.imageBank.setElement( "' + e + '", new ImageBank( this.amos, [' + extension.imageList + '], [ ' + extension.imagesPalette +  ' ], {} ), 1 );\n';
		}
		code = utilities.replaceStringInText( data.source, 'INSERT_CODE', code );
		indexFiles += '\t\t<script src="' + './run/' + utilities.getFilename( data.path ) + '.js"></script>\n';
		return code;
	};
	function cbFileSystem( response, data, extra )
	{
		var code = baseOptions.fileSystemTree;
		for ( var e in baseOptions.extensions )
		{
			var extension = baseOptions.extensions[ e ];
			if ( extension.inUse )
				code += extension.fileSystemTree;
		}
		code = utilities.removeLastComma( code );
		source = utilities.replaceStringInText( data.source, 'INSERT_EXTERNALFILES', baseOptions.externalFiles );
		source = utilities.replaceStringInText( source, 'INSERT_TREE', code );
		indexFiles += '\t\t<script src="' + './run/' + utilities.getFilename( data.path ) + '.js"></script>\n';
		return source;
	};
	function cbFonts( response, data, extra )
	{
		var windowFont = baseOptions.manifest.default.screen.window.font;
 		var code = '\tthis.getFonts([' + utilities.removeLastComma( baseOptions.fonts.googleList ) + '],[' + utilities.removeLastComma( baseOptions.fonts.amigaList ) + '],"application");\n';
		code += '\tthis.setWindowFont( "' + windowFont.name + '","' + windowFont.type + '",'  + windowFont.height + ',"' + ( windowFont.weight ? windowFont.weight : 'normal' ) + '","' + ( windowFont.italic ? windowFont.italic : 'normal' ) + '","' + ( windowFont.stretch ? windowFont.stretch : 'normal' ) + '","' + ( windowFont.tags ? + windowFont.tags : '' ) + '","application");\n';
		for ( var e in baseOptions.extensions )
		{
			var extension = baseOptions.extensions[ e ];
			if ( extension.inUse )
			{
				code += '\tthis.getFonts([' + utilities.removeLastComma( extension.fonts.googleList ) + '],[' + utilities.removeLastComma( extension.fonts.amigaList ) + '],"' + e + '")\n';
				code += '\tthis.setWindowFont( "' + extension.windowFont.name +'","' + extension.windowFont.type +'",' + extension.windowFont.height + ',"' + ( extension.windowFont.weight ? extension.windowFont.weight : 'normal' ) + '","' + ( extension.windowFont.italic ? extension.windowFont.italic : 'normal' ) + '","' + ( extension.windowFont.stretch ? extension.windowFont.stretch : 'normal' ) + '","' + ( extension.windowFont.tags ? + extension.windowFont.tags : '' ) + '","' + e + '");\n';
			}
		}
		var source = utilities.replaceStringInText( data.source, 'INSERT_CODE', code );
		source = utilities.replaceStringInText( source, 'INSERT_GOOGLEFONTS', baseOptions.fonts.google );
		source = utilities.replaceStringInText( source, 'INSERT_AMIGAFONTS', baseOptions.fonts.amiga );
		indexFiles += '\t\t<script src="' + './run/' + utilities.getFilename( data.path ) + '.js"></script>\n';
		return source;
	};
	function cbFile( response, data, extra )
	{
		indexFiles += '\t\t<script src="' + './run/' + utilities.getFilename( data.path ) + '.js"></script>\n';
		return data.source;
	};
	function cbNoCopy( response, data, extra )
	{
		indexFiles += '\t\t<script src="' + './run/' + utilities.getFilename( data.path ) + '.js"></script>\n';
		return null;
	};
};
