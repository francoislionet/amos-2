/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * Banks compiler
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 22/12/2018
 */
const fs = require( 'fs' );
const utilities = require( './utilities' );
const tokens = require( './tokens' );
const messages = require( './messages' );
const mkdirp = require( 'mkdirp' );
const HJSON = require( 'hjson' );

var googleFontsDone;
var amigaFontsDone;

function init( options )
{
	googleFontsDone = {};
	amigaFontsDone = {};
	return true;
};
module.exports.init = init;

function compileImages( options, baseOptions )
{
	var copyList = [];
	var code = '';
	var codeDone = false;
	var masksCode = '';
	options.imageList = '';
	options.imagesPalette = '';

	// Default sprites
	var defaultFiles;
	//defaultFiles = utilities.getDirectory( options.defaultPath + '/resources/images', { filters: [ '*.png', '*.jpg' ], recursive: false } );
	//if ( defaultFiles )
	//{
	//	utilities.createDirectories( options.destinationPath + '/resources/images' );
	//	compileBank( defaultFiles, null, options.	defaultPath );
	//}

	// Application or extension images
	var files = utilities.getDirectory( options.sourcePath + '/resources/images', { filters: [ '*.png', '*.jpg' ], recursive: false } );
	if ( files )
	{
		utilities.createDirectories( options.destinationPath + '/resources/images' );
		compileBank( files, defaultFiles, options.sourcePath );
	}
	options.imageList = code;

	// If main code or an extension WITH images...
	if ( !options.isExtension || code != '' )
	{
		// Palette
		var palette = utilities.getManifestProperty( "default.screen.palette", options, baseOptions );
		var code = '';
		for ( var p = 0; p < palette.length; p++ )
		{
			var color = palette[ p ].toString( 16 );
			code += '"' + color + '"';
			if ( p < palette.length - 1 )
				code += ',';
		}
		options.imagesPalette = code;

		// Save in destination, unlocking the images...
		if ( copyList.length > 0 )
			options.fileSaver.equateFolderToList( options.destinationPath + '/resources/images', copyList, { recursive: false, unlockImages: true } );
	}
	return;

	function compileBank( files, defaultFiles, path )
	{
		for ( var f = 0; f < files.length; f++ )
		{
			if ( !files[ f ].isDirectory )
			{
				var done = false;
				if ( defaultFiles )
				{
					for ( var df = 0; df < defaultFiles.length; df++ )
					{
						if ( files[ f ].path == defaultFiles[ df ].path )
						{
							done = true;
							break;
						}
					}
				}

				if ( !done )
				{
					// Add image to copy list
					copyList.push( { source: files[ f ].path, destination: options.destinationPath + '/resources/images/' + files[ f ].name } );

					// Generate code
					if ( codeDone )
						code += ',';
					else
						codeDone = true;
					var subPath = '';
					if ( options.isExtension )
						subPath = options.extensionName + '/';
					code += '"' + subPath + utilities.getFilename( files[ f ].name ) + '.js"';
				}
			}
		}
	}
};
module.exports.compileImages = compileImages;

function compileSounds( options, baseOptions )
{
	var copyList = [];
	var code = '';
	var codeDone = false;
	var masksCode = '';
	options.soundList = '';

	// Application sounds
	var files = utilities.getDirectory( options.sourcePath + '/resources/sounds', { filters: [ '*.mp3', '*.wav' ], recursive: false } );
	if ( files )
	{
		utilities.createDirectories( options.destinationPath + '/resources/sounds' );
		compileBank( files, undefined, options.sourcePath );
	}
	options.soundList = code;

	// Save in destination, unlocking the images...
	if ( copyList.length > 0)
		options.fileSaver.equateFolderToList( options.destinationPath + '/resources/sounds', copyList, { recursive: false, unlockSounds: true } );

	return;

	function compileBank( files, defaultFiles, path )
	{
		for ( var f = 0; f < files.length; f++ )
		{
			if ( !files[ f ].isDirectory )
			{
				var done = false;
				if ( defaultFiles )
				{
					for ( var df = 0; df < defaultFiles.length; df++ )
					{
						if ( files[ f ].path == defaultFiles[ df ].path )
						{
							done = true;
							break;
						}
					}
				}

				if ( !done )
				{
					// Add image to copy list
					copyList.push( { source: files[ f ].path, destination: options.destinationPath + '/resources/sounds/' + files[ f ].name } );

					// Generate code
					if ( codeDone )
						code += ',';
					else
						codeDone = true;
					var subPath = '';
					if ( options.isExtension )
						subPath = options.extensionName + '/';
					code += '"' + subPath + utilities.getFilename( files[ f ].name ) + '.js"';
				}
			}
		}
	}
};
module.exports.compileSounds = compileSounds;

function compileFonts( options, textWindowFont, baseOptions )
{
	var googleDone = false;
	var googleFontsCode = '';
	var googleFontsFamilies = '';
	var googleFontsFaces = '';
	var amigaFontsCode = '';
	var amigaFontsFolder = [];

	// Google fonts
	var name;
	var fontList = '';
	utilities.createDirectories( options.destinationPath + '/resources/fonts/google' );
	if ( options.manifest.fonts && options.manifest.fonts.google )
	{
		for ( var f = 0; f < options.manifest.fonts.google.length; f++ )
		{
			name = options.manifest.fonts.google[ f ];
			addGoogleFont( name );
		}
	}
	options.fonts.googleList += fontList;

	// Amiga fonts
	var fontList = '';
	utilities.createDirectories( options.destinationPath + '/resources/fonts/amiga' );
	if ( options.manifest.fonts && options.manifest.fonts.amiga )
	{
		for ( var f = 0; f < options.manifest.fonts.amiga.length; f++ )
		{
			name = options.manifest.fonts.amiga[ f ];
			addAmigaFont( name );
		}
	}
	options.fonts.amigaList += fontList;

	// Text window font
	if ( textWindowFont )
	{
		if ( textWindowFont.type == 'google' )
		{
			if ( !googleFontsDone[ textWindowFont.name ] )
			{
				addGoogleFont( textWindowFont.name );
			}
		}
		else if ( textWindowFont.type == 'amiga' )
		{
			if ( !amigaFontsDone[ textWindowFont.name ] )
			{
				addAmigaFont( textWindowFont.name );
			}
		}
	}
	options.fonts.google += googleFontsCode;
	options.fonts.amiga += amigaFontsCode;
	options.fonts.families += googleFontsFamilies;
	options.fonts.faces += googleFontsFaces;

	options.fileSaver.cleanDirectory( options.destinationPath + '/resources/fonts/amiga', amigaFontsFolder, {} );
	return;

	function addGoogleFont( fontName, fontType )
	{
		var fontPath = options.fontPath + '/google/' + fontName;
		if ( !fs.existsSync( fontPath ) )
			messages.pushError( { compilerWarning: 'font_not_found', file: fontName }, false );
		else
		{
			if ( !googleFontsDone[ fontName ] )
			{
				var definition = utilities.loadFile( fontPath + '/font.definition', { encoding: 'utf8' } );
				googleFontsCode += 'GoogleFonts["' + fontName.toLowerCase() + '"]=\n{\n\t';
				if ( googleDone )
					googleFontsFamilies += '|';
				else
					googleDone = true;
				googleFontsFamilies += fontName;

				var def = utilities.extractCode( definition, 'FONT_FACE' );
				var done = false;
				var start = def.indexOf( '@font-face' );
				var family = false;
				while ( start >= 0 )
				{
					var end = def.indexOf( '}', start );
					if ( !family )
					{
						family = getCSSValue( 'font-family:', def, start, end );
						googleFontsCode += 'name:"' + family.substring( 1, family.length - 1 ) + '",sizes:[';
					}
					var style = getCSSValue( 'font-style:', def, start, end );
					var weight = getCSSValue( 'font-weight:', def, start, end );
					if ( done )
						googleFontsCode += ',';
					else
						done = true;
					googleFontsCode += '{weight:"' + weight + '",style:"' + style + '"}';
					start = def.indexOf( '@font-face', end );
				};
				googleFontsCode += ']\n};\n';
				def = utilities.replaceStringInText( def, '../fonts/', './resources/fonts/google/' );
				googleFontsFaces += def;

				// Clean the Google font folder
				var opts = { filters: [ '*.eot', '*.svg', '*.ttf', '*.woff', '*.woff2' ] };
				options.fileSaver.copyDirectory( options.destinationPath + '/resources/fonts/google', fontPath, opts );

				// One more font!
				googleFontsDone[ fontName ] = true;
			}
			fontList += '"' + fontName + '",';
		}
	}
	function addAmigaFont( fontName, fontType )
	{
		var fontPath = options.fontPath + '/amiga/' + fontName;
		if ( !fs.existsSync( fontPath ) )
			messages.pushError( { compilerWarning: 'font_not_found', file: fontName }, false );
		else
		{
			var sizes = utilities.getDirectory( fontPath, { } );
			if ( sizes )
			{
				if ( !amigaFontsDone[ fontName ] )
				{
					amigaFontsCode += 'AmigaFonts["' + fontName + '"]=\n{\n';
					var charactersCode = 'AmigaCharacters["' + fontName + '"]=\n{\n';
					for ( var s = 0; s < sizes.length; s++ )
					{
						if ( utilities.getFileExtension( sizes[ s ].path ) == '' )
						{
							var fontSize = parseInt( sizes[ s ].name );
							if ( !isNaN( fontSize ) && fontSize > 0 )
							{
								var result = convertAmigaFont( fontName, fontSize );
								if ( result )
								{
									amigaFontsCode += result.main;
									charactersCode += result.characters;
									if ( s < sizes.length - 1 )
									{
										charactersCode += ',';
										amigaFontsCode += ',';
									}
									charactersCode += '\n';
									amigaFontsCode += '\n';
								}
							}
						}
						else
						{
							messages.pushError( { compilerWarning: 'garbage_found_in_folder', file: fontPath }, false );
						}
					}
					amigaFontsCode += '};\n';
					charactersCode += '};\n';

					// Save the file
					var jsCode = utilities.loadFile( options.templatesPath + '/amiga font/amigaFont.js', { encoding: 'utf8' } );
					jsCode = utilities.replaceStringInText( jsCode, 'INSERT_CHARACTERS', charactersCode );
					var savePath = options.destinationPath + '/resources/fonts/amiga/' + fontName + '.js';
					options.fileSaver.saveUTF8( savePath, jsCode, options );
					amigaFontsFolder.push( savePath );

					// One more font!
					amigaFontsDone[ fontName ] = true;
				}
				fontList += '"' + fontName + '",';
			}
		}
	}
	function getCSSValue( tag, text, start, end )
	{
		position = text.indexOf( tag, start );
		if ( position >= 0 )
		{
			position += tag.length;
			var semiColon = text.indexOf( ';', position );
			if ( semiColon >= 0 && semiColon < end )
			{
				var result = text.substring( position, semiColon );
				return utilities.trimString( result );
			}
		}
		return false;
	};

	function convertAmigaFont( fontName, fontSize )
	{
		// Load the font
		var buffer = utilities.loadFile( options.fontPath + '/amiga/' + fontName + '/' + fontSize, { encoding: 'arraybuffer' } );
		var memoryBlock = new MemoryBlock( buffer, 'big' );

		var font = 36;

		/* Skip the whole DiskFontHeader */
		font += 4;	// dfh_TF.ln_Succ
		font += 4;	// dfh_TF.ln_Pred
		font += 1;	// dfh_TF.ln_Type
		font += 1;	// dfh_TF.ln_Pri
		font += 4;	// dfh_TF.ln_Name
		font += 2;	// dfh_FileID
		font += 2;	// dfh_Revision
		font += 4;	// dfh_Segment
		font += 32;	// MAXFONTNAME;

		/* dfh_TF starts */
		font += 8;

		/* skip type and pri */
		var type = memoryBlock.peek( font ); font += 1;
		if ( type != 12 )
		{
			messages.pushError( { compilerWarning: 'font_not_supported', file: fontName } );
			return '';
		}
		font += 1;

		/* Skip name pointer, replyport and msg length */
		font += 10;

		var height = memoryBlock.deek( font ); font += 2;			// tf_YSize
		var style = memoryBlock.peek( font ); font += 1;			// tf_Style
    	var flags = memoryBlock.peek( font ); font += 1;			// tf_Flags
    	var widthFont = memoryBlock.deek( font ); font += 2;		// tf_XSize
    	var baseLine = memoryBlock.deek( font ) + 1; font += 2;			// tf_Baseline
    	var boldSmear = memoryBlock.deek( font ); font += 2;		// tf_BoldSmear
    	var accessors = memoryBlock.deek( font ); font += 2;		// tf_Accessors
    	var loChar = memoryBlock.peek( font ); font += 1;			// tf_LoChar
    	var hiChar = memoryBlock.peek( font ); font += 1;			// tf_HiChar
    	var charData = memoryBlock.leek( font ) + 32; font += 4;	// tf_CharData
    	var modulo = memoryBlock.deek( font ); font += 2;			// tf_Modulo
		var charLoc = memoryBlock.leek( font ) + 32; font += 4;		// tf_CharLoc
		var charSpace = memoryBlock.leek( font ) == 0 ? 0 : memoryBlock.leek( font ) + 32;
		font += 4;
		var charKern = memoryBlock.leek( font ) == 0 ? 0 : memoryBlock.leek( font ) + 32;
		font += 4;
		if ( ( style & ( 1 << 6 ) ) != 0 )
		{
			messages.pushError( { compilerWarning: 'font_not_supported', file: fontName } );
			return '';
		}
		var mainCode = '\t' + height + ':{\n';
		mainCode += '\t\twidth:' + widthFont + ',\n';
		mainCode += '\t\theight:' + height + ',\n';
		mainCode += '\t\tstyle:' + style + ',\n';
		mainCode += '\t\tflags:' + flags + ',\n';
		mainCode += '\t\tbaseLine:' + baseLine + ',\n';
		mainCode += '\t\tboldSmear:' + boldSmear + ',\n';
		mainCode += '\t\taccessors:' + accessors + ',\n';
		mainCode += '\t\tloChar:' + loChar + ',\n';
		mainCode += '\t\thiChar:' + hiChar + '\n';
		mainCode += '\t}';

		// The bitmaps
		var code = '\t' + height + ':\n\t[\n';
		for ( var character = 0; character < hiChar - loChar; character++ )
		{
			code += '\t\t{' + '\t\t\t\t\t\t\t\t\t\t\t\t\t\t// "';
			if ( character + loChar >= 32)
				code += String.fromCharCode( character + loChar );
			else
				code += 'ascii:' + character + loChar;
			code += '"\n';
			var bitStart = memoryBlock.deek( charLoc + character * 4 );
			var width = memoryBlock.deek( charLoc + character * 4 + 2 );
			var space = charSpace == 0 ? width : memoryBlock.deek( charSpace + character * 2, true );
			var kern = charKern == 0 ? 0 : memoryBlock.deek( charKern + character * 2, true );
			if ( width > 256 || space > 256 || space < -256 || kern > 256 || kern < -256 )
			{
				messages.pushError( { compilerWarning: 'font_not_supported', file: fontName } );
				return;
			}
			code += '\t\t\twidth:' + width + ',\n';
			code += '\t\t\tspace:' + space + ',\n';
			code += '\t\t\tkern:' + kern + ',\n';
			code += '\t\t\tcode:' + ( character + loChar ) + ',\n';
			code += '\t\t\tbitmap:\n\t\t\t[\n'
			if ( width == 0 )
			{
				for ( var line = 0; line < height; line++ )
				{
					code += '\t\t\t\t0';
					if ( line < height - 1 )
						code += ',';
					code += '\n'
				}
			}
			else
			{
				var address1 = charData + Math.floor( bitStart / 8 );
				for ( var line = 0; line < height; line++ )
				{
					code += '\t\t\t\t';

					var address2 = address1;
					var mask = 0x80 >> ( bitStart & 7 );
					var byte = memoryBlock.peek( address2++ );
					var destination = '';
					destinationCount = 0;
					code += '0b';
					for ( var w = 0; w < width; w++ )
					{
						if ( ( byte & mask ) != 0 )
							destination += '1';
						else
							destination += '0';
						if ( mask == 1 )
						{
							byte = memoryBlock.peek( address2++ );
							mask = 0x80;
						}
						else
						{
							mask >>= 1;
						}
						destinationCount++;
						if ( destinationCount == 8 )
						{
							code += destination;
							destination = '';
							destinationCount = 0;
							if ( w < width - 1 )
								code += ',0b';
						}
					}
					if ( destinationCount )
					{
						while ( destination.length < 8 )
							destination += '0';
						code += destination;
					}
					if ( line < height - 1 )
						code += ',';
					code += '\n';
					address1 += modulo;
				}
			}
			code += '\t\t\t]\n';
			code += '\t\t}';
			if ( character < hiChar - loChar - 1 )
				code += ',';
			code += '\n';
		}
		code += '\t]';

		// Finished!
		return { main: mainCode, characters: code };
	};
};
module.exports.compileFonts = compileFonts;

// Finish the fonts code
function finishFonts( options )
{
};
module.exports.finishFonts = finishFonts;

// Memory block class
function MemoryBlock( buffer, endian )
{
	this.buffer = buffer;
	this.bufferView = new Uint8Array( buffer );
	this.endian = typeof endian != 'undefined' ? endian : 'big';
};
module.exports.MemoryBlock = MemoryBlock;
MemoryBlock.prototype.extractString = function( address, length )
{
	address &= 0x00FFFFFF;
	if ( length < 0 )
		throw 'illegal_function"call';
	if ( address + length > this.bufferView.length )
		throw 'illegal_function_call';
	var result = '';
	for ( var l = 0; l < length; l++ )
	{
		var c = this.bufferView[ address + l ];
		if ( c == 0 )
			break;
		if ( c < 32 )
			c = ' ';
		result += String.fromCharCode( c );
	}
	return result;
};
MemoryBlock.prototype.peek = function( address, signed )
{
	address &= 0x00FFFFFF;
	if ( address >= this.bufferView.length )
		throw 'illegal_function_call';
	if ( signed && v >= 0x80 )
		return -( 0x100 - v );
	return this.bufferView[ address ];
};
MemoryBlock.prototype.deek = function( address, signed )
{
	address &= 0x00FFFFFF;
	if ( address + 2 >= this.bufferView.length )
		throw 'illegal_function_call';
	var v;
	if ( this.endian == 'big' )
	{
		v = ( this.bufferView[ address ] & 0xFF ) << 8 | this.bufferView[ address + 1 ] & 0xFF;
	}
	else
	{
		v = ( this.bufferView[ address + 1 ] & 0xFF ) << 8 + this.bufferView[ address ] & 0xFF;
	}
	if ( signed && v >= 0x8000 )
		return -( 0x10000 - v );
	return v;
};
MemoryBlock.prototype.leek = function( address, signed )
{
	address &= 0x00FFFFFF;
	if ( address + 4 >= this.bufferView.length )
		throw 'illegal_function_call';
	var v;
	if ( this.endian == 'big' )
	{
		v = ( this.bufferView[ address ] & 0xFF ) << 24 | ( this.bufferView[ address + 1 ] & 0xFF ) << 16 | ( this.bufferView[ address + 2 ] & 0xFF ) << 8 | this.bufferView[ address + 3 ] & 0xFf;
	}
	else
	{
		v = ( this.bufferView[ address + 3 ] & 0xFF ) << 24 | ( this.bufferView[ address + 2 ] & 0xFF ) << 16 | ( this.bufferView[ address + 1 ] & 0xFF ) << 8 | this.bufferView[ address ] & 0xFF;
	}
	if ( signed && v >= 0x80000000 )
		return -( 0x100000000 - v );
	return v;
};
MemoryBlock.prototype.poke = function( address, value )
{
	address &= 0x00FFFFFF;
	if ( address >= this.bufferView.length )
		throw 'illegal_function_call';
	this.bufferView[ address ] = value;
};
MemoryBlock.prototype.doke = function( address, value )
{
	address &= 0x00FFFFFF;
	if ( address + 2 >= this.bufferView.length )
		throw 'illegal_function_call';
	if ( this.endian == 'big' )
	{
		this.bufferView[ address ] = ( value & 0xFF ) >> 8;
		this.bufferView[ address + 1 ] = value & 0xFF;
	}
	else
	{
		this.bufferView[ address ] = value & 0xFF;
		this.bufferView[ address + 1 ] = ( value & 0xFF ) >> 8;
	}
};
MemoryBlock.prototype.loke = function( address, value )
{
	address &= 0x00FFFFFF;
	if ( address + 4 >= this.bufferView.length )
		throw 'illegal_function_call';
	if ( this.endian == 'big' )
	{
		this.bufferView[ address ] = ( value & 0xFF ) >> 24;
		this.bufferView[ address + 1 ] = ( value & 0xFF ) >> 16;
		this.bufferView[ address + 2 ] = ( value & 0xFF ) >> 8;
		this.bufferView[ address + 3 ] = value & 0xFF;
	}
	else
	{
		this.bufferView[ address ] = value & 0xFF;
		this.bufferView[ address + 1 ] = ( value & 0xFF ) >> 8;
		this.bufferView[ address + 2 ] = ( value & 0xFF ) >> 16;
		this.bufferView[ address + 3 ] = ( value & 0xFF ) >> 24;
	}
};
