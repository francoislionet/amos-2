/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Compiler
 *
 * Utilities
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/12/2018
 */
const fs = require( 'fs' );
const mkdirp = require( 'mkdirp' );
const HJSON = require( 'hjson' );
const messages = require( './messages' );
PNG = require('pngjs').PNG;

var numberOfFilesOpen = 0;
module.exports.numberOfFilesOpen = numberOfFilesOpen;

//
// File utilities
//
function filterFilename( name, wildcards )
{
	name = name.toLowerCase();
	if ( typeof wildcards == 'string' )
		wildcards = [ wildcards ];

	for ( w = 0; w < wildcards.length; w++ )
	{
		var wildcard = wildcards[ w ].toLowerCase();

		// Look for *[ and ]*
		if ( ( start = wildcard.indexOf( '*[' ) ) >= 0 )
		{
			var end = wildcard.indexOf( ']*', start );
			if ( end >= start )
			{
				start += 2;
				var filter = wildcard.substring( start, end );
				if ( name.indexOf( filter ) >= 0 )
					return true;
				if ( start - 2 == 0 && end + 2 == wildcard.length )
					continue;
				var newFilter = '';
				for ( var f = 0; f < end - start; f++ )
					newFilter += '?';
				wildcard = wildcard.substring( 0, start - 2 ) + newFilter + wildcard.substring( end + 2 );
			}
		}

		var pName = 0;
		var pWild = 0;
		var afterDot = false;
		var bad = false;
		do
		{
			var cName = name.charAt( pName );
			var cWild = wildcard.charAt( pWild );
			switch ( cWild )
			{
				case '*':
					if ( afterDot )
						return true;
					pName = name.lastIndexOf( '.' );
					pWild = wildcard.indexOf( '.' );
					if ( pName < 0 && pWild < 0 )
						return true;
					afterDot = true;
					break;
				case '.':
					afterDot = true;
					if ( cName != '.' )
						bad = true;
					break;
				case '?':
					break;
				default:
					if ( cName != cWild )
						bad = true;
					break;
			}
			pName++;
			pWild++;
		} while( !bad && pName < name.length )
		if( !bad && pWild < wildcard.length )
			bad = true;
		if ( !bad )
			return true;
	}
	return false;
};
module.exports.filterFilename = filterFilename;

var numberOfDirectories = 0;
var numberOfFiles = 0;
function getDirectory( path, options )
{
	numberOfDirectories = 0;
	numberOfFiles = 0;
	return getDir( path, options );
};
function getDir( path, options )
{
	var result = [];
	path = cleanPath( path );

	var files;
	try
	{
		files = fs.readdirSync( path );
	}
	catch( err )
	{
		return null;
	}
	if ( files )
	{
		for ( var f = 0; f < files.length; f++ )
		{
			var sPath = path + '/' + files[ f ];
			var stats = fs.statSync( sPath );
			if ( !stats.isDirectory() )
			{
				if ( !options.excludes || ( options.excludes && !filterFilename( sPath, options.excludes ) ) )
				{
					if ( !options.filters || ( options.filters && filterFilename( sPath, options.filters ) ) )
					{
						numberOfFiles++;
						result.push(
						{
							name: files[ f ],
							path: sPath,
							isDirectory: false,
							size: stats.size
						} );
					}
				}
			}
			else
			{
				numberOfDirectories++;
				if ( options.recursive )
				{
					var newResult = getDir( sPath + '/', options );
					if ( ( options.onlyDirectories && !options.onlyFiles ) || ( ( !options.onlyDirectories || options.onlyFiles ) && newResult.length > 0 ) )
					{
						if ( options.log )
							messages.print_verbose( 'scanning', numberOfDirectories, numberOfFiles, sPath );
						result.push(
						{
							name: files[ f ],
							path: sPath,
							isDirectory: true,
							files: newResult
						} );
					}
				}
				else
				{
					if ( !options.onlyFiles || options.onlyDirectories )
					{
						result.push(
						{
							name: files[ f ],
							path: sPath,
							isDirectory: true
						} );
					}
				}
			}
		}
	}
	return result;
};
module.exports.getDirectory = getDirectory;

function getFilesFromTree( tree, result )
{
	if ( !result )
		result = {};
	for ( var d = 0; d < tree.length; d++ )
	{
		var entry = tree[ d ];
		if ( !entry.isDirectory )
		{
			result[ '"' + entry.path + '"' ] = entry;
		}
		else if ( entry.files )
		{
			getFilesFromTree( entry.files, result );
		}
	}
	return result;
}
module.exports.getFilesFromTree = getFilesFromTree;

function getDirectoryArrayFromTree( tree, options )
{
	var result = [];
	getDirArrayFromTree( tree, result );
	if ( options.sort )
	{
		result.sort( function( a, b )
		{
			if ( a.path == b.path )
				return 0;
			if ( a.path.indexOf( b.path ) == 0 )
				return a.path.length < b.path.length ? -1 : 1;
			if ( b.path.indexOf( a.path ) == 0 )
				return b.path.length < a.path.length ? -1 : 1;
		} );
	}
	return result;
}
function getDirArrayFromTree( tree, result )
{
	for ( var d = 0; d < tree.length; d++ )
	{
		var entry = tree[ d ];
		if ( entry.isDirectory )
		{
			result.push( entry );
			getDirArrayFromTree( entry.files, result );
		}
	}
	return result;
}
module.exports.getDirectoryArrayFromTree = getDirectoryArrayFromTree;


function deleteDirectory( destinationPath, options )
{
	if ( fs.existsSync( destinationPath ) )
	{
		var file;
		try
		{
			var opts = copyObject( options );
			opts.onlyFiles = true;
			var tree = getDirectory( destinationPath, opts );
			if ( !tree )
			{
				if ( options.catch )
					messages.pushError( { compilerWarning: 'missing_folder', file: destinationPath } ) ;
				return;
			}
			var files = getFilesFromTree( tree );
			for ( var f in files )
			{
				file = files[ f ];
				fs.unlinkSync( file.path );
			}

			opts.onlyFiles = false;
			opts.onlyDirectories = true;
			tree = getDirectory( destinationPath, opts );
			var folders = getDirectoryArrayFromTree( tree, { sort: true } );
			for ( var f = 0; f < folders.length; f++ )
			{
				file = folders[ f ];
				fs.rmdirSync( file.path );
			}

			fs.rmdirSync( destinationPath );
		}
		catch( error )
		{
			if ( options.catch )
			{
				if ( !file.isDirectory )
					messages.pushError( { compilerWarning: 'cannot_delete_file', file: file.path } );
				else
					messages.pushError( { compilerWarning: 'cannot_delete_directoty', file: file.path } );
			}
		}
	}
};
module.exports.deleteDirectory = deleteDirectory;

function isAbsolutePath( path )
{
	if ( path.charAt( 0 ) == '/' || path.charAt( 0 ) == '\\' )
		return true;
	if ( path.indexOf( ':' ) >= 0 )
		return true;
	return false;
}
module.exports.isAbsolutePath = isAbsolutePath;

function cleanPath( path )
{
	var pos = 0;
	while( ( pos = path.indexOf( '\\', pos ) )  >= 0 )
		path = path.substring( 0, pos ) + '/' + path.substring( pos + 1 );
	if ( path.charAt( path.length - 1 ) == '/' )
		path = path.substring( path, path.length - 1 );
	return path;
};
module.exports.cleanPath = cleanPath;

function getDirectoriesFromPath( path )
{
	var filename = getFilename( path );
	filename = path.substring( 0, path.length - filename.length );
	if ( filename.charAt( filename.length - 1 ) == '/' )
		filename = filename.substring( 0, filename.length - 1 );
	var pos =  filename.indexOf( ':' );
	if ( pos >= 0 )
		filename = filename.substring( pos + 1 );
	if ( filename.charAt( 0 ) == '/' )
		filename = filename.substring( 1 );
	return filename;
};
module.exports.getDirectoriesFromPath = getDirectoriesFromPath;

function getFilename( path )
{
	var posPoint = path.lastIndexOf( '.' );
	if ( posPoint < 0 )
		posPoint = path.length;

	var posSlash1 = path.lastIndexOf( '/' );
	var posSlash2 = path.lastIndexOf( '\\' );
	if ( posSlash1 >= 0 && posSlash2 >= 0 )
		posSlash1 = Math.max( posSlash1, posSlash2 ) + 1;
	else if ( posSlash1 < 0 && posSlash2 < 0 )
		posSlash1 = 0;
	else if ( posSlash1 < 0 )
		posSlash1 = posSlash2 + 1;
	else
		posSlash1++;

	return path.substring( posSlash1, posPoint );
};
module.exports.getFilename = getFilename;

function getFileExtension( path )
{
	var posPoint = path.lastIndexOf( '.' );
	if ( posPoint < 0 )
		return '';
	return path.substring( posPoint + 1 );
};
module.exports.getFileExtension = getFileExtension;

function loadFile( path, options )
{
	var result = loadIfExist( path, options );
	if ( !result )
	{
		messages.pushError( { compilerError: 'cannot_load_file', file: path }, true );
	}
	return result;
}
module.exports.loadFile = loadFile;

function loadIfExist( path, options )
{
	if ( fs.existsSync( path ) )
	{
		if ( options.encoding == 'utf8' )
		{
			try
			{
				result = fs.readFileSync( path, { encoding: 'utf8' } );
				return result;
			}
			catch( err )
			{
				return null;
			}
		}
		else if ( options.encoding == 'arraybuffer' )
		{
			try
			{
				result = fs.readFileSync( path );
				return result;
			}
			catch( err )
			{
				return null;
			}
		}
	}
	return null;
}
module.exports.loadIfExist = loadIfExist;

function createDirectories( path )
{
	try
	{
		mkdirp.sync( path );
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_create_directory', file: path }, true );
	}
}
module.exports.createDirectories = createDirectories;

function getStringHash( text )
{
	var hash = 0, i, chr;
	if ( text.length === 0 )
		return hash;
	for ( i = 0; i < text.length; i++ )
	{
	  	chr = text.charCodeAt( i );
	  	hash  = ( (hash << 5) - hash ) + chr;
	  	hash |= 0;
	}
	return hash;
};
module.exports.getStringHash = getStringHash;





//
// String utilities
//
module.exports.extractCode = function( source, mark )
{
	var start = source.indexOf( '<' + mark + '-START>' );
	var end = source.indexOf( '<' + mark + '-END>' );
	if ( start >= 0 && end >= start )
	{
		var text = source.substring( start, end );
		var firstLine = findNextLine( text, 0 );
		var lastLine = findStartOfLine( text, text.length - 1 );
		return text.substring( firstLine, lastLine );
	}
	return '';
};
function findNextLine( text, pos )
{
	if ( pos >= text.length )
		return -1;

	while( text.charCodeAt( pos ) != 13 && text.charCodeAt( pos ) != 10 && pos < text.length )
		pos++;
	if ( text.charCodeAt( pos ) == 13 )
	{
		if ( pos + 1 < text.length && text.charCodeAt( pos + 1 ) == 10 )
			return pos + 2;
		return pos + 1;
	}
	return pos + 1;
};
module.exports.findNextLine = findNextLine;

function findStartOfLine( text, pos )
{
	while( text.charCodeAt( pos ) != 10 && pos > 0 )
		pos--;
	if ( pos > 0 )
	{
		pos++;
		return pos;
	}
	return -1;
};
module.exports.findStartOfLine = findStartOfLine;

function trimString( str, position )
{
	var start = 0;
	position = typeof position == 'undefined' ? { left: true, right: true } : position;
	if ( position.left )
	{
		while( start < str.length && ( str.charCodeAt( start ) == 32 || str.charCodeAt( start ) == 9 || str.charCodeAt( start ) == 10 || str.charCodeAt( start ) == 13 ) )
			start++;
	}
	var end = str.length;
	if ( position.right )
	{
		while( end > 0 && ( str.charCodeAt( end - 1 ) == 32 || str.charCodeAt( end - 1 ) == 9 || str.charCodeAt( end - 1 ) == 10 || str.charCodeAt( end - 1 ) == 13 ) )
			end--;
	}
	if ( end > start )
		return str.substring( start, end );
	return '';
}
module.exports.trimString = trimString;

function printLine( str )
{
	var result = '';
	for ( var p = 0; p < str.length; p++ )
	{
		if ( str.charAt( p ) == '\t' )
			result += '    ';
		else
			result += str.charAt( p );
	}
	return result;
}
module.exports.printLine = printLine;

function replaceSpacesByUnderscores( str )
{
	return replaceSpacesByChar( str, '_' );
};
module.exports.replaceSpacesByUnderscores = replaceSpacesByUnderscores;

function replaceSpacesByChar( str, char )
{
	var result = '';
	for ( var p = 0; p < str.length; p++ )
	{
		var c = str.charAt( p );
		if ( c == ' ' )
			result += char;
		else
			result += c;
	}
	return result;
}
module.exports.replaceSpacesByChar = replaceSpacesByChar;

function replaceStringInText( text, mark, replacement )
{
	var pos = text.indexOf( mark );
	while( pos >= 0 )
	{
		text = text.substring( 0, pos ) + replacement + text.substring( pos + mark.length );
		pos = text.indexOf( mark );
	}
	return text;
};
module.exports.replaceStringInText = replaceStringInText;






//
// Javascript utilities
//
function isObject( item )
{
    return typeof item != 'undefined' ? (typeof item === "object" && !Array.isArray( item ) && item !== null) : false;
};
module.exports.isObject = isObject;

function isArray( item )
{
    return typeof item != 'undefined' ? Array.isArray( item ) : false;
};
module.exports.isArray = isArray;

function convertArrayBufferToString( arraybuffer )
{
	var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	var bytes = new Uint8Array( arraybuffer ),
	i, len = bytes.length, base64 = "";

	for (i = 0; i < len; i+=3)
	{
		base64 += chars[bytes[i] >> 2];
		base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
		base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
		base64 += chars[bytes[i + 2] & 63];
	}

	if ((len % 3) === 2)
	{
		base64 = base64.substring(0, base64.length - 1) + "=";
	}
	else if (len % 3 === 1)
	{
		base64 = base64.substring(0, base64.length - 2) + "==";
	}
	return base64;
}
module.exports.convertArrayBufferToString = convertArrayBufferToString;

function replaceMissingProperties( destination, source )
{
	for ( var p in source )
	{
		if ( typeof destination[ p ] == 'undefined' )
		{
			destination[ p ] = source[ p ];
		}
		if ( isObject( source[ p ] ) )
		{
			replaceMissingProperties( destination[ p ], source[ p ] );
		}
	}
	return destination;
};
module.exports.replaceMissingProperties = replaceMissingProperties;

function getFormattedDate()
{
    var date = new Date();

    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var str = day + '/' + month + '/' + date.getFullYear() + "-" + hour + ":" + min + ":" + sec;

    return str;
};
module.exports.getFormattedDate = getFormattedDate;

function fixJson( json )
{
	var jsonFixed = '';
	for ( var p = 0; p < json.length; p++ )
	{
		jsonFixed += json.charAt( p );
		if ( json.charCodeAt( p ) == 13 && p + 1 < json.length && json.charCodeAt( p + 1 ) != 10 )
			jsonFixed += String.fromCharCode( 10 );
	}
	return jsonFixed;
}
module.exports.fixJson = fixJson;

function getString( str )
{
	str = trimString( str );
	var start = 0;
	var end = str.length;
	var quote = str.charAt( 0 );
	if ( quote == '"' || quote == "'" )
	{
		for ( end = start + 1; start < str.length && str.charAt( end ) != quote; end++ ) {}
	}
	return str.substring( start, end );
}
module.exports.getString = getString;

function cleanObject( arr, exclude )
{
	var temp = {};
	for ( var key in arr )
	{
		if ( arr[ key ] && arr[ key ] != exclude )
			temp[ key ] = arr[ key ];
	}
	return temp;
};
module.exports.cleanObject = cleanObject;

function removeLastComma( str )
{
	if ( str && str.length > 0 && str.charAt( str.length - 1 ) == ',' )
		return str.substring( 0, str.length - 1 );
	return str;
};
module.exports.removeLastComma = removeLastComma;

function copyObject( obj )
{
	var result = {};
	for ( var key in obj )
	{
		if ( isObject( obj[ key ] ) )
			result[ key ] = copyObject( obj[ key ] );
		else
			result[ key ] = obj[ key ];
	}
	return result;
};
module.exports.copyObject = copyObject;

function getManifestProperty( address, options1, options2 )
{
	var prop = getProp( address, options1.manifest );
	if ( typeof prop == 'undefined' && options2 )
		prop = getProp( address, options2.manifest );
	return prop;

	function getProp( addr, obj )
	{
		var dot = addr.indexOf( '.' );
		while ( dot >= 0 )
		{
			obj = obj[ addr.substring( 0, dot ) ];
			addr = addr.substring( dot + 1 );
			if ( typeof obj == 'undefined' )
				return obj;
			dot = addr.indexOf( '.' );
		}
		return obj[ addr ];
	}
}
module.exports.getManifestProperty = getManifestProperty;



//
// Image manipulation
//
function getCollisionMask( image, threeshold = 10 )
{
	var arrayBuffer = new ArrayBuffer( image.width * image.height );
	var dataView = new Uint8Array( arrayBuffer );
	for ( var y = 0; y < image.height; y++ )
	{
		for ( var x = 0; x < image.width; x++ )
		{
			if ( image.data[ ( y * image.width + x ) * 4 + 3 ] >= threeshold )
			{
				dataView[ y * image.width + x ] = 0xFF;
			}
		}
	}
	return { buffer: arrayBuffer, view: dataView, width: image.width, height: image.height };
};
module.exports.getCollisionMask = getCollisionMask;

function loadImage( path )
{
	try
	{
		var data = fs.readFileSync( path );
		return PNG.sync.read( data );
	}
	catch( error )
	{
	}
	return null;
};
module.exports.loadImage = loadImage;


function saveUTF8( destinationPath, source )
{
	destinationPath = cleanPath( destinationPath );
	try
	{
		fs.writeFileSync( destinationPath, source, { encoding: 'utf8' } );
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_save_file', file: destinationPath }, true );
	}
};
module.exports.saveUTF8 = saveUTF8;

function loadHJSON( path, options )
{
	path = cleanPath( path );
	var json = loadFile( path, { encoding: 'utf8' } );
	try
	{
		var json = loadFile( path, { encoding: 'utf8' } );
		json = fixJson( json );
		return HJSON.parse( json );
	}
	catch( e )
	{
	}
	messages.pushError( { compilerError: 'illegal_json_file', file: path }, false );
	return false;
}
module.exports.loadHJSON = loadHJSON;

function getHash( rootPath, path, suffix )
{
	rootPath = cleanPath( rootPath ).toLowerCase();
	path = cleanPath( path ).toLowerCase();
	if ( path.substring( 0, rootPath.length ) != rootPath )
		throw { error: 'internal_error', parameter: 'FileSave.getHash' };
	var hash = path.substring( rootPath.length );
	if ( hash.charAt( 0 ) == '/' )
		hash = hash.substring( 1 );
	return '"' + hash + suffix + '"';
};
module.exports.getHash = getHash;

function loadFileIfExists( path, options )
{
	var result;
	path = cleanPath( path );
	if ( fs.existsSync( path ) )
	{
		try
		{
			if ( options.encoding == 'utf8' )
			{
				result = fs.readFileSync( path, { encoding: 'utf8' } );
			}
			else if ( options.encoding == 'arraybuffer' )
			{
				result = fs.readFileSync( path );
			}
		}
		catch ( error )
		{
			messages.pushError( { compilerError: 'cannot_load_file', file: path } , true );
		}
	}
	return result;
};
module.exports.loadFileIfExists = loadFileIfExists;




//
// File loader object
//
// Only load the files that have been modified
// since the last compilation...
//
function FileLoader( path, options )
{
	this.files = null;
	this.rootPath = cleanPath( path ).toLowerCase();
	this.waitingCount = 0;
	this.options = options;

	// Load the files_load.hjson file
	var json;
	this.jsonPath = cleanPath( this.rootPath + '/files_load.hjson' );
	try
	{
		json = fs.readFileSync( this.jsonPath, { encoding: 'utf8' } );
		json = fixJson( json );
		this.files = HJSON.parse( json );
	}
	catch( err ) {}
	if ( !this.files || ( this.files && ( this.files.version != options.version || this.files.rootPath != this.rootPath ) ) )
	{
		this.files =
		{
			version: options.version,
			rootPath: this.rootPath,
			infos: {}
		};
	}
	numberOfFilesOpen++;
}
module.exports.FileLoader = FileLoader;

FileLoader.prototype.close = function()
{
	// Get the current state of the directory
	var files =
	{
		version: this.options.version,
		rootPath: this.rootPath,
		infos: {}
	};
	try
	{
		var tree = getDirectory( this.rootPath, this.options );
		if ( tree )
		{
			var flat = getFilesFromTree( tree );
			for ( var f in flat )
			{
				var file = flat[ f ];
				var fileStats = fs.statSync( file.path );
				var hash = getHash( this.rootPath, file.path, '_loader' );
				files.infos[ hash ] = { size: fileStats.size, date: fileStats.mtime.toISOString() };
			}
		}

		// Save the data
		var json = HJSON.stringify( files );
		fs.writeFileSync( this.jsonPath, json, { encoding: 'utf8' } );
	}
	catch( err )
	{
		messages.pushError( { compilerError: 'cannot_write_file', file: this.jsonPath } );
	}
	numberOfFilesOpen--;
};

FileLoader.prototype.isDifferent = function( path )
{
	path = cleanPath( path );
	var different = true;
	try
	{
		var hash = getHash( this.rootPath, path, '_loader' );
		if ( fs.existsSync( path ) )
		{
			var fileStats = fs.statSync( path );
			var stats = this.files.infos[ hash ];
			if ( stats )
			{
				if( stats.size == fileStats.size )
				{
					if ( stats.date == fileStats.mtime.toISOString() )
					{
						different = false;
					}
				}
			}
		}
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_load_file', file: file.path }, true );
	}
	return different;
};

FileLoader.prototype.isDirectoryDifferent = function( path, options )
{
	var differentFiles = [];
	try
	{
		var tree = getDirectory( path, options );
		if ( tree )
		{
			var flat = getFilesFromTree( tree );
			for ( var f in flat )
			{
				var file = flat[ f ];
				if ( this.isDifferent( file.path, options ) )
				{
					differentFiles.push( file );
				}
			}
		}
		else
		{
			messages.pushError( { compilerError: 'cannot_read_directory', file: path }, true );
		}
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_load_file', file: file.path }, true );
	}
	return differentFiles.length > 0 ? differentFiles : null;
};


//
// File saver object
//
// Copy and save only the files that have not been modified
// since their creation...
//
function FileSaver( path, options )
{
	this.files = null;
	this.rootPath = cleanPath( path ).toLowerCase();
	this.waitingCount = 0;
	this.options = options;

	// Load the files.hjson file
	var json;
	this.jsonPath = cleanPath( this.rootPath + '/files.hjson' );
	try
	{
		json = fs.readFileSync( this.jsonPath, { encoding: 'utf8' } );
		json = fixJson( json );
		this.files = HJSON.parse( json );
	}
	catch( err ) {}
	if ( !this.files || ( this.files && ( this.files.version != options.version || this.files.rootPath != this.rootPath ) ) )
	{
		this.files =
		{
			version: options.version,
			rootPath: this.rootPath,
			infos: {}
		};
	}
	numberOfFilesOpen++;
}
module.exports.FileSaver = FileSaver;

FileSaver.prototype.close = function()
{
	var self = this;
	var handle = setInterval( function()
	{
		if ( self.waitingCount == 0 )
		{
			clearInterval( handle );
			var json = HJSON.stringify( self.files );
			try
			{
				fs.writeFileSync( self.jsonPath, json, { encoding: 'utf8' } );
			}
			catch( err )
			{
				messages.pushError( { compilerError: 'cannot_write_file', file: self.jsonPath } );
			}
			numberOfFilesOpen--;
		}
	}, 20 );
};

FileSaver.prototype.copyFile = function( destinationPath, sourcePath )
{
	sourcePath = cleanPath( sourcePath );
	destinationPath = cleanPath( destinationPath );
	var hash = getHash( this.rootPath, destinationPath, '_saver' );
	try
	{
		var doCopy = true;
		var destinationStats = this.files.infos[ hash ];
		if ( destinationStats )
		{
			var sourceStats = fs.statSync( sourcePath );
			if ( fs.existsSync( destinationPath ) && sourceStats.size == destinationStats.size )
			{
				var dateSrce = sourceStats.mtime.toISOString();
				if ( dateSrce == destinationStats.date )
				{
					doCopy = false;
				}
			}
		}
		if ( doCopy )
		{
			var self = this;
			messages.print_verbose( 'filesaver_copying', destinationPath );
			fs.copyFileSync( sourcePath, destinationPath );
			this.waitingCount++;
			numberOfFilesOpen++;
			var handle = setInterval( function()
			{
				try
				{
					var stats = fs.statSync( destinationPath );
					self.files.infos[ hash ] = { size: stats.size, date: stats.mtime.toISOString() };
					clearInterval( handle );
					self.waitingCount--;
					numberOfFilesOpen--;
				}
				catch( err ){}
			}, 10 );
		}
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_copy_file', file: sourcePath }, true );
	}
};

FileSaver.prototype.saveUTF8 = function( destinationPath, source )
{
	destinationPath = cleanPath( destinationPath );
	var hash = getHash( this.rootPath, destinationPath, '_saver' );
	var sourceHash;
	try
	{
		var doSave = true;
		var destinationHash = this.files.infos[ hash ];
		if ( fs.existsSync( destinationPath ) && destinationHash )
		{
			sourceHash = getStringHash( source );
			doSave = !( sourceHash == destinationHash );
		}
		if ( doSave )
		{
			messages.print_verbose( 'filesaver_saving', destinationPath );
			fs.writeFileSync( destinationPath, source, { encoding: 'utf8' } );
			if ( !sourceHash )
				sourceHash = getStringHash( source );
			this.files.infos[ hash ] = sourceHash;
		}
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_copy_file', file: file.path }, true );
	}
};
FileSaver.prototype.copyAndUnlockImage = function( destinationPath, sourcePath )
{
	try
	{
		var data = fs.readFileSync( sourcePath );
	}
	catch( error )
	{
		messages.pushError( { compilerError: 'cannot_copy_file', file: sourcePath }, true );
	}
	var text = convertArrayBufferToString( data );
	var source = 'window["image_' + getFilename( destinationPath ) + '"]="' + text + '";';
	this.saveUTF8( destinationPath, source, { encoding: 'utf8' } );
};

FileSaver.prototype.deleteFile = function( path )
{
	path = cleanPath( path );
	var hash = getHash( this.rootPath, path, '_saver' );
	try
	{
		fs.unlinkSync( path );
		if ( this.files.infos[ hash ] )
		{
			this.files = cleanObject( this.files, this.files.infos[ hash ] );
		}
	}
	catch( e )
	{
		messages.pushError( { compilerError: 'cannot_delete_file', file: path }, true );
	}
};

FileSaver.prototype.copyList = function( list )
{
	for ( var f = 0; f < list.length; f++ )
	{
		this.copyFile( list.destination, list.source );
	}
};
FileSaver.prototype.equateFolderToList = function( destinationPath, list, options )
{
	var tree = getDirectory( destinationPath, options );
	if ( !tree )
	{
		messages.pushError( { compilerWarning: 'missing_folder', file: destinationPath } ) ;
		return;
	}
	var folder = getFilesFromTree( tree );

	// Copy the files
	for ( var l = 0; l < list.length; l++ )
	{
		var ext = getFileExtension( list[ l ].source ).toLowerCase();
		if ( options.unlockImages && ( ext == 'png' || ext == 'jpg' || ext == 'jpeg' ) )
		{
			list[ l ].destination = list[ l ].destination.substring( 0, list[ l ].destination.length - ext.length ) + 'js';
			this.copyAndUnlockImage( list[ l ].destination, list[ l ].source, options );
		}
		else
		{
			this.copyFile( list[ l ].destination, list[ l ].source, options );
		}
		folder[ '"' + list[ l ].destination + '"' ] = null;
	}

	// Delete the extra files
	for ( var f in folder )
	{
		if ( folder[ f ] )
		{
			this.deleteFile( folder[ f ].path );
			folder[ f ] = null;
		}
	}

	// Delete empty directories
	// TODO! When resources structure is tree.
};
FileSaver.prototype.cleanDirectory = function( destinationPath, list, options )
{
	var tree = getDirectory( destinationPath, options );
	if ( !tree )
	{
		messages.pushError( { compilerWarning: 'missing_folder', file: destinationPath } ) ;
		return;
	}
	var folder = getFilesFromTree( tree );

	for ( var f in folder )
	{
		for ( var l = 0; l < list.length; l++ )
		{
			if ( folder[ f ].path == list[ l ] )
			{
				folder[ f ] = null;
				break;
			}
		}
	}
	for ( var f in folder )
	{
		if ( folder[ f ] )
		{
			this.deleteFile( folder[ f ].path );
		}
	}
};

FileSaver.prototype.copyDirectory = function( destination, source, options )
{
	try
	{
		// Clean source path
		source = cleanPath( source );
		destination = cleanPath( destination );

		// Create destination directory
		mkdirp.sync( destination );

		// Copy files
		var files = getDirectory( source, options );
		if ( files )
		{
			for ( var f = 0; f < files.length; f++ )
			{
				var file = files[ f ];
				var subPath = file.path.substring( source.length + 1 );
				var dPath = destination + '/' + subPath;
				if ( !file.isDirectory )
				{
					var copied = false;
					if ( options.toCallback )
					{
						for ( var cb = 0; cb < options.toCallback.length; cb++ )
						{
							var opt = options.toCallback[ cb ];
							if ( filterFilename( file.name, opt.filter ) )
							{
								if ( opt.encoding.toLowerCase() == 'utf8' )
								{
									var src;
									if ( !opt.noLoad )
									{
										src = loadFile( file.path, { encoding: 'utf8' } );
									}
									src = opt.callback( true, { source: src, path: file.path } );
									if ( src )
									{
										this.saveUTF8( dPath, src, options );
									}
									copied = true;
									break;
								}
								else
								{
									throw { error: 'internal_error', parameter: 'FileSaver option type' }
								}
							}
						}
					}
					if ( !copied )
					{
						this.copyFile( dPath, file.path, options );
						if ( options.permissions )
						{
							try
							{
								fs.chmodSync( dPath, options.permissions )
							}
							catch( e )
							{
								messages.pushError( { compilerWarning: 'cannot_set_permissions', file: file.path } );
							}
						}
					}
				}
				else if ( options.recursive )
				{
					this.copyDirectory( file.path, dPath, options );
				}
			}
		}
	}
	catch( err )
	{
		messages.pushError( { compilerError: 'cannot_copy_directory', file: source }, true );
	}
}