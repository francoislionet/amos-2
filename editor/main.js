// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')

let mainWindow
function createWindow ()
{
	// Create the browser window.
	mainWindow = new BrowserWindow( { width: 1700, height: 900 } );
	mainWindow.setMenu( null );

	// Open the DevTools.
	mainWindow.webContents.openDevTools();

	// And load the index.html of the app.
	mainWindow.loadFile( 'index.html' );

	// Emitted when the window is closed.
	mainWindow.on( 'closed', function ()
	{
		mainWindow = null
	} );
}
app.on( 'ready', createWindow );

// Quit when all windows are closed.
app.on( 'window-all-closed', function ()
{
	if (process.platform !== 'darwin')
	{
		app.quit()
	}
} );

app.on( 'activate', function ()
{
	if (mainWindow === null)
	{
		createWindow();
	}
} )
