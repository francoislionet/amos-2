/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * AMOS Editor entry point
 * Build with the Tree Engine
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 17/10/2018
 */
function Editor()
{
	var self = this;
	this.width = document.body.clientWidth;
	this.height = document.body.clientHeight;

	// Initialise the UI
	Tree.init( {}, function( response, data, extra )
	{
		// Loaded OK?
		if ( !response )
		{
			this.quit();
			return;
		}

		// Creates a new instance of the Tree engine
		self.tree = new Tree( self,
		{
			title: 'AMOS Editor',
			renderers: [ { name: 'Renderer_HTML' } ],
			renderingId: 'Application',
			width: self.width,
			height: self.height
		} );

		// Creates the root object of the tree
		// When no 'root' or 'parent' options are indicated, this creates the root item...
		self.root = new Root( self.tree, 'Root',
		{
			x: 0,
			y: 0,
			z: 0,
			zoomX: 1,
			zoomY: 1,
			width: self.width,
			height: self.height
		} );
	}, 'This should come back.' );
};
Editor.prototype.quit = function()
{

};
