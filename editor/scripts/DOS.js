/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * AMOS2 DOS interface
 *
 * Makes the interface between the application and the local file-system
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 30/10/2018
 */
var app = require( 'electron' ).remote;
var dialog = app.dialog;
var fs = require( 'fs' );
var pathSeparator = require( 'path' ).sep;


window.OneOS = window.OneOS || {};
window.OneOS.DOS = {};

OneOS.DOS.showOpenDialog = function( window, options, callback, extra )
{
	dialog.showOpenDialog( window, options, function( filenames, bookmarks )
	{
		if ( filenames )
			callback( true, { filenames: filenames, bookmarks: bookmarks }, extra );
		else
			callback( false, { error: 'Cancelled' }, extra );
	} );
};
OneOS.DOS.showSaveDialog = function( window, options, callback, extra )
{
	dialog.showSaveDialog( window, options, function( filename, bookmark )
	{
		if ( filename )
			callback( true, { filename: filename, bookmark: bookmark }, extra );
		else
			callback( false, { error: 'Cancelled' }, extra );
	} );
};
OneOS.DOS.getDirectory = function( path, options, callback, extra )
{
	// Star recursion
	var response = true;
	var list = [];
	var depth = 0;
	getDir( list, path, options );

	// Watchdog for the end of recursion
	var handle = setInterval( function()
	{
		if ( depth <= 0 )
		{
			clearInterval( handle );
			callback( response, response ? list : [], extra );
		}
	}, 20 );

	// Recursive function
	function getDir( listDir, path, options )
	{
		depth++;
		fs.readdir( path, function( err, files )
		{
			if ( !err )
			{
				var stats = [];
				for ( var f = 0; f < files.length; f++ )
				{
					var fullPath = path + pathSeparator + files[ f ];
					stats.push( fs.statSync( fullPath ) );
				}
				if ( !options.noDirectories )
				{
					// Look for directories
					var file, stat;
					for ( var i = 0; i < files.length; i++ )
					{
						stat = stats[ i ];
						file = files[ i ];
						if ( stat.isDirectory() )
						{
							var value =
							{
								filename: file,
								path: path + pathSeparator + file,
								isDirectory: true,
								children: []
							};
							if ( options.recursive )
							{
								getDir( value.children, value.path, options );
							}
							listDir.push( value );
						}
					}

					// Sort?
					if ( options.sort )
						listDir.sort( compare );
				}

				// Look for files
				var listTemp = [];
				if ( !options.noFiles )
				{
					for ( var i = 0; i < files.length; i++ )
					{
						stat = stats[ i ];
						file = files[ i ];
						if ( stat.isFile() )
						{
							var value =
							{
								filename: file,
								path: path + pathSeparator + file,
								isFile: true,
								size: stat.size
							}
							listTemp.push( value );
						}
					}

					// Sort?
					if ( options.sort )
						listTemp.sort( compare );
				}

				// Adds to the main array
				if ( !options.filesFirst )
				{
					for ( i = 0; i < listTemp.length; i++ )
						listDir.push( listTemp[ i ] );
				}
				else
				{
					for ( i = 0; i < listTemp.length; i++ )
						listDir.unshift( listTemp[ i ] );
				}
			}
			else
			{
				response = false;
			}
			depth--;
		} );
	}

	// Comparaison function
	function compare( fileA, fileB )
	{
		if ( fileA.filename < fileB.filename )
			return -1;
		if ( fileA.filename > fileB.filename )
			return 1;
		return 0;
	}
};
OneOS.DOS.getDisks = function( options, callback, extra )
{
};
OneOS.DOS.loadFile = function( path, options, callback, extra )
{
	fs.readFile( path, options, function( err, file )
	{
		if ( !err )
			callback( true, file, extra );
		else
			callback( false, err, extra );
	} );
};
OneOS.DOS.getDriveInfo = function( path, options, callback, extra )
{
};
OneOS.DOS.getFileAccess = function( path, options, callback, extra )
{
};
OneOS.DOS.getFileInfo = function( path, options, callback, extra )
{
};
OneOS.DOS.rename = function( newPath, oldPath, options, extra )
{
	fs.rename( oldPath, newPath, function( err )
	{
		if ( !err )
			callback( true, {}, extra );
		else
			callback( false, err, extra );
	} );
};
OneOS.DOS.delete = function( path, options, extra )
{

};
OneOS.DOS.open = function( path, options, extra )
{

};
OneOS.DOS.close = function( path, options, extra )
{

};
OneOS.DOS.write = function( path, options, extra )
{

};
OneOS.DOS.read = function( path, options, extra )
{

};
