/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * AMOS2 - Config saving and loading interface
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 30/10/2018
 */
const Store = require( 'electron-store' );
window.OneOS = window.OneOS || {};

OneOS.Config = function( options )
{
	this.store = new Store();
};
OneOS.Config.prototype.get = function( options, callback, extra )
{
	if ( callback )
		callback( true, this.store.store, extra );
	return this.store.store;
};
OneOS.Config.prototype.set = function( data, options, callback, extra )
{
	this.store.store = data;
	if ( callback )
		callback( true, {}, extra );
	return true;
};
OneOS.Config.prototype.getKey = function( key, options, callback, extra )
{
	var value = this.store.get( key, options.defaultValue );
	if ( callback )
		callback( true, value, extra );
	return value;
};
OneOS.Config.prototype.setKey = function( key, data, options, callback, extra )
{
	this.store.set( key, data );
	if ( callback )
		callback( true, {}, extra );
	return true;
};
OneOS.Config.prototype.setKeys = function( data, options, callback, extra )
{
	this.store.set( data );
	if ( callback )
		callback( true, {}, extra );
	return true;
};
OneOS.Config.prototype.exists = function( key, options, callback, extra )
{
	var value = this.store.has( key );
	if ( callback )
		callback( true, {}, extra );
	return value;
};
OneOS.Config.prototype.deleteKey = function( key, options, callback, extra )
{
	this.store.delete( key );
	if ( callback )
		callback( true, {}, extra );
	return true;
};
OneOS.Config.prototype.deleteAll = function( options, callback, extra )
{
	this.store.clear();
	if ( callback )
		callback( true, {}, extra );
	return true;
};
OneOS.Config.prototype.getSize = function( options, callback, extra )
{
	var value = this.store.size;
	if ( callback )
		callback( true, value, extra );
	return value;
};
