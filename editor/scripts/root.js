/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Main game
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 18/08/2017
 */


/**
 * Game root
 * The root of the game tree
 */
window.Root = function( tree, name, options )
{
	// Initialize the root item
    this.renderItemName = 'Tree.RenderItems.Empty';
	Tree.Items.init( this, tree, 'AMOS 2', 'Root', options );

	// Load the extra sources
	var self = this;
	tree.include

	// Loads the images
	tree.resources.loadImages(
	[
		{ name: 'arrowRight', path: 'resources/arrowRight.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'arrowDown', path: 'resources/arrowDown.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_unknown', path: 'resources/unknown.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_source', path: 'resources/source.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_html', path: 'resources/html.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_image', path: 'resources/picture.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_sound', path: 'resources/sound.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_css', path: 'resources/css.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'file_text', path: 'resources/text.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'tab_close', path: 'resources/close.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolBookmark', path: 'resources/toolBookmark.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolBuild', path: 'resources/toolBuild.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolEditor', path: 'resources/toolEditor.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolFind', path: 'resources/toolFind.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolFile', path: 'resources/toolFile.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolFindInFiles', path: 'resources/toolFindInFiles.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolFolder', path: 'resources/toolFolder.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolKeyboard', path: 'resources/toolKeyboard.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolNew', path: 'resources/toolNew.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolNextBookmark', path: 'resources/toolNextBookmark.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolOpen', path: 'resources/toolOpen.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolPreferences', path: 'resources/toolPreferences.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolPreviousBookmark', path: 'resources/toolPreviousBookmark.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolProject', path: 'resources/toolProject.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolReplace', path: 'resources/toolReplace.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolRun', path: 'resources/toolRun.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolSave', path: 'resources/toolSave.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolSaveAs', path: 'resources/toolSaveAs.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolToolbar', path: 'resources/toolToolbar.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolArrow', path: 'resources/toolArrow.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
		{ name: 'toolUpdate', path: 'resources/toolUpdate.png', hotSpot: Tree.HOTSPOT_TOPLEFT },
	],
	{},
	function ( response, data, extra )
	{
		if ( response )
		{
			// Adds the main
			new Main( self.tree, 'main',
			{
				root: self,
				parent: self,
				x: 0,
				y: 0,
				z: 0,
				widthCalc: 'width( root )',
				heightCalc: 'height( root )'
			} );
		}
	}, 'This should come back.' );
};

// Message Up
Root.prototype.messageUp = function ( message )
{
	// Nothing to process
	return false;
};
Root.prototype.messageDown = function ( message )
{
	return false;
};
