/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * AMOS2 IDE
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 05/03/2018
 */
var pathSeparator = require( 'path' ).sep;

window.Main = function( tree, name, options )
{
	this.renderItemName = 'Tree.RenderItems.Empty';
	Tree.Items.init( this, tree, name, 'Main', options );

	this.projectToolbarTools =
	[
		{
			name: 'fileNewProject',
			text: 'New Project',
			nameImage: 'toolNew'
		},
		{
			name: 'open',
			text: 'Open',
			nameImage: 'toolOpen',
			children:
			[
				{
					name: 'fileOpenDirectory',
					text: 'Open Directory',
					nameImage: 'toolFolder',
					displayText: true,
					imageWidth: 24,
					imageHeight: 24
				},
				{
					name: 'fileOpenProject',
					text: 'Open Project',
					nameImage: 'toolProject',
					displayText: true,
					imageWidth: 24,
					imageHeight: 24
				}
			]
		},
		{
			name: 'fileSaveProject',
			text: 'Save Project',
			nameImage: 'toolSave'
		},
		{
			name: 'fileUpdateProject',
			text: 'Update Project',
			nameImage: 'toolUpdate'
		},
		{
			name: 'editFindInFiles',
			text: 'Find in Files',
			nameImage: 'toolFind'
		},
		{
			name: 'editReplaceInFiles',
			text: 'Replace in Files',
			nameImage: 'toolReplace'
		},
		{
			name: 'runBuild',
			text: 'Build Project',
			nameImage: 'toolBuild'
		},
		{
			name: 'runRun',
			text: 'Run Project',
			nameImage: 'toolRun'
		},
		{
			name: 'filePreferences',
			text: 'Preferences',
			nameImage: 'toolPreferences'
		}
	];
	this.editorToolbarTools =
	[
		{
			name: 'fileNew',
			text: 'New File',
			nameImage: 'toolNew'
		},
		{
			name: 'fileOpen',
			text: 'Open File',
			nameImage: 'toolOpen'
		},
		{
			name: 'fileSave',
			text: 'Save Current File',
			nameImage: 'toolSave'
		},
		{
			name: 'fileSaveAs',
			text: 'Save Current File As...',
			nameImage: 'toolSaveAs'
		},
		{
			name: 'editFind',
			text: 'Find',
			nameImage: 'toolFind'
		},
		{
			name: 'editReplace',
			text: 'Replace',
			nameImage: 'toolReplace'
		},
		{
			name: 'editBookmark',
			text: 'Set Bookmark',
			nameImage: 'toolBookmark'
		},
		{
			name: 'editPreviousBookark',
			text: 'Goto Previous Bookmark',
			nameImage: 'toolPreviousBookmark'
		},
		{
			name: 'editNextBookmark',
			text: 'Goto Next Bookmark',
			nameImage: 'toolNextBookmark'
		}
	];
	// The main menu
	this.menu =
	[
		{
			name: 'file',
			text: 'File',
			children:
			[
				{
					name: 'fileNew',
					text: 'New File',
					shortcut: 'ctrl-N'
				},
				{
					name: 'fileNewProject',
					text: 'New Project',
					shortcut: 'ctrl-shift-N'
				},
				{
					name: 'separator'
				},
				{
					name: 'fileOpen',
					text: 'Open File',
					shortcut: 'ctrl-O'
				},
				{
					name: 'fileOpenDirectory',
					text: 'Open Directory'
				},
				{
					name: 'fileOpenProject',
					text: 'Open Project'
				},
				{
					name: 'separator'
				},
				{
					name: 'fileSave',
					text: 'Save File',
					shortcut: 'ctrl-S'
				},
				{
					name: 'fileSaveAs',
					text: 'Save File As',
					shortcut: 'ctrl-shift-S'
				},
				{
					name: 'fileSaveAll',
					text: 'Save All',
					shortcut: 'ctrl-shift-A'
				},
				{
					name: 'fileSaveProject',
					text: 'Save Project'
				},
				{
					name: 'separator'
				},
				{
					name: 'fileClose',
					text: 'Close File'
				},
				{
					name: 'fileCloseProject',
					text: 'Close Project'
				},
				{
					name: 'separator'
				},
				{
					name: 'fileUpdateProject',
					text: 'Update Project'
				},
				{
					name: 'separator'
				},
				{
					name: 'filePreferences',
					text: 'Preferences'
				},
				{
					name: 'separator'
				},
				{
					name: 'fileQuit',
					text: 'Quit'
				},
			]
		},
		{
			name: 'edit',
			text: 'Edit',
			children:
			[
				{
					name: 'editUndo',
					text: 'Undo',
					shortcut: 'ctrl-Z'
				},
				{
					name: 'editRedo',
					text: 'Redo',
					shortcut: 'ctrl-Y'
				},
				{
					name: 'separator'
				},
				{
					name: 'editCopy',
					text: 'Copy',
					shortcut: 'ctrl-C'
				},
				{
					name: 'editPaste',
					text: 'Paste',
					shortcut: 'ctrl-V'
				},
				{
					name: 'editCut',
					text: 'Cut',
					shortcut: 'ctrl-X'
				},
				{
					name: 'separator'
				},
				{
					name: 'editFind',
					text: 'Find',
					shortcut: 'ctrl-F'
				},
				{
					name: 'editReplace',
					text: 'Replace',
					shortcut: 'ctrl-H'
				},
				{
					name: 'editFindInFiles',
					text: 'Find in Files',
					shortcut: 'ctrl-shift-F'
				},
				{
					name: 'editReplaceInFiles',
					text: 'Replace in Files',
					shortcut: 'ctrl-shift-H'
				},
				{
					name: 'separator'
				},
				{
					name: 'editBookmark',
					text: 'Toggle Bookmark',
					shortcut: 'ctrl-B'
				},
				{
					name: 'editPreviousBookmark',
					text: 'Goto Previous Bookmark'
				},
				{
					name: 'editNextBookmark',
					text: 'Goto Next Bookmark'
				}
			]
		},
		{
			name: 'run',
			text: 'Run',
			children:
			[
				{
					name: 'runBuild',
					text: 'Build Project'
				},
				{
					name: 'separator'
				},
				{
					name: 'runRun',
					text: 'Run Project',
					shortcut: 'ctrl-shift-R'
				}
			]
		}
	];

	// Default theme
	this.theme =
	{
		'Tree.UI.Toolbar':
		{
			color: '#272822',
			colorPadding: '#272822',
			font: '12px sans serif'
		},
		'Tree.UI.Tool':
		{
			font: '12px sans serif',
			color: '#272822',
			colorDown: '',
			colorMouseOver: '',
			sizeBorder: 0,
			colorPadding: '#272822'
		},
		'Tree.UI.TreeBox':
		{
			font: '12px sans serif',
			colorBack: '#2F3129',
			colorBackDown: '#272822',
			colorBackMouseOver: '#3F4139',
			colorText: '#C0C0C0',
			colorTextMouseOver: '#F0F0F0',
			colorTextDown: '#FFFFFF',
			colorPadding: '#272822'
		},
		'Tree.UI.Tabs':
		{
			font: '12px sans serif',
			colorBack: '#272822',
			colorText: '#C0C0C0',
			colorTextMouseOver: '#F0F0F0',
			colorTextDown: '#FFFFFF',
			colorTab: '#272822',
			colorTabDown: '#2F3129',
			colorTabMouseOver: '#2D2D2D',
			colorPadding: '#272822'
		},
		'Tree.UI.MenuBar':
		{
			color: '#E0E0E0',
			colorPadding: '#E0E0E0',
			font: '12px sans serif'
		},
		'Tree.UI.MenuPopup':
		{
			color: '#E0E0E0',
			colorPadding: '#E0E0E0',
			font: '12px sans serif'
		},
		'Tree.UI.MenuItem':
		{
			font: '12px sans serif',
			colorText: '#000000',
			colorTextMouseOver: '#000000',
			colorTextDown: '#FFFFFF',
			colorTextInactive: '#808080',
			colorBack: '#E0E0E0',
			colorBackMouseOver: '#A0A0A0',
			colorbackDown: '#202020',
		},
		'Tree.UI.Text':
		{
			font: '12px sans serif'
		}
	};

	// Main display
	this.menu = new Tree.UI.MenuBar( this.tree, 'menuBar',
	{
		root: this.root,
		parent: this,
		x: 0,
		y: 0,
		widthCalc: 'width( parent )',
		height: 24,
		theme: this.theme,
		list: this.menu,
		caller: this,
		onOptionSelected: this.onMenu
	} );
	this.information = new Tree.UI.ColorBox( this.tree, 'information',
	{
		root: this.root,
		parent: this,
		x: 0,
		yCalc: 'height( parent ) - height( this )',
		widthCalc: 'width( parent )',
		height: 24,
		color: '#FF0000',
		theme: this.theme,
		paddingH: 2,
		paddingV: 2,
		colorPadding: '#123475'
	} );
	this.projectTools = new Tree.UI.Toolbar( this.tree, 'projectTools',
	{
		root: this.root,
		parent: this,
		x: 0,
		yCalc: 'height( menuBar )',
		width: 40,
		heightCalc: 'height( parent ) - height( menuBar ) - height( information )',
		theme: this.theme,
		list: this.projectToolbarTools,
		openOnMouseOver: true,
		paddingH: 2,
		paddingV: 2,
		widthTools: 32,
		heightTools: 32,
		caller: this,
		onClickProc: this.onMenu
	} );
	this.projectTabs = new Tree.UI.Tabs( this.tree, 'projectTabs',
	{
		root: this.root,
		parent: this,
		xCalc: 'right( projectTools )',
		yCalc: 'bottom( menuBar )',
		width: 256,
		height: 32,
		theme: this.theme,
		caller: this,
		onClick: this.clickOnProjectTab,
		tabs:
		[
			{
				text: 'Project'
			},
			{
				text: 'Find in Files'
			}
		]
	} );
	this.projectTree = new Tree.UI.TreeBox( this.tree, 'project',
	{
		root: this.root,
		parent: this,
		xCalc: 'right( projectTools )',
		yCalc: 'bottom( projectTabs )',
		widthCalc: 'width( projectTabs )',
		heightCalc: 'height( projectTools ) - height( projectTabs )',
		theme: this.theme,
		lines: false,
		caller: this,
		onClick: this.clickOnProjectLine,
		onDblClick: this.dblClickOnProjectLine,
		onRightClick: this.rightClickOnProjectLine
	} );
	this.editorTools = new Tree.UI.Toolbar( this.tree, 'editorTools',
	{
		root: this.root,
		parent: this,
		xCalc: 'right( project )',
		yCalc: 'bottom( menuBar )',
		width: 28,
		heightCalc: 'height( projectTools )',
		widthTools: 20,
		heightTools: 20,
		theme: this.theme,
		list: this.editorToolbarTools,
		paddingH: 2,
		paddingV: 2,
		caller: this,
		onClickProc: this.onMenu
	} );
	this.tabs = new Tree.UI.Tabs( this.tree, 'editorTabs',
	{
		root: this.root,
		parent: this,
		xCalc: 'right( editorTools )',
		yCalc: 'bottom( menuBar )',
		widthCalc: 'width( parent ) - right( editorTools )',
		height: 32,
		button: 'tab_close',
		theme: this.theme,
		caller: this,
		onClick: this.clickOnTab,
		onClose: this.closeTab,
	} );
	this.editor = new Tree.Misc.Ace( this.tree, 'editor',
	{
		root: this.root,
		parent: this,
		xCalc: 'right( editorTools )',
		yCalc: 'bottom( editorTabs )',
		widthCalc: 'width( parent ) - right( editorTools )',
		heightCalc: 'height( projectTools ) - height( editorTabs )',
		paddingH: 0,
		paddingV: 0,
	} );
	this.tree.start();

	var self = this;
	if ( true )
	{
		this.loadConfig( { history: true, openProject: 0 }, function( ok )
		{
		} );
	}
	else
	{
		debugger;
		this.initConfig();
		this.setDirectory( '' );
	}
};
Main.getIconFromPath = function( path )
{
	var extension = this.utilities.getFileExtension( path );
	switch( extension )
	{
		case 'php':
		case 'pl':
		case 'sql':
		case 'sh':
		case 'as':
		case 'sol':
		case 'info':
		case 'json':
		case 'js':
		case 'url':
		case 'jsx':
		case 'xml':
		case 'c':
		case 'h':
		case 'cpp':
		case 'd':
		case 'ini':
		case 'java':
		case 'run':
			icon = 'file_source';
			break;
		case 'css':
			icon = 'file_css';
			break;
		case 'txt':
		case 'apf':
		case 'conf':
		case 'lang':
		case 'md':
			icon = 'file_text';
			break;
		case 'tpl':
		case 'ptpl':
		case 'html':
		case 'htm':
			icon = 'file_html';
			break;
		case 'png':
		case 'jpg':
		case 'jpeg':
		case 'gif':
			icon = 'file_image';
			break;
		case 'wav':
		case 'mp3':
			icon = 'file_sound';
			break;
		default:
			icon = 'file_unknown';
			break;
	}
	return icon;
};
// Message Up
Main.messageUp = function ( message )
{
	if ( message.command == 'quit' )
	{
		if ( this.projectPath != '' )
		{
			this.saveProjectConfig()
			this.saveConfig( { history: true } );
		}
	}
	return false;
};
Main.messageDown = function ( message )
{
	return false;
};

Main.rightClickOnProjectLine = function( line )
{
	if ( !this.projectPopup )
	{
		// Define the popup menu
		var popup =
		[
			{
				name: 'treeEdit',
				text: 'Edit'
			},
			{
				name: 'separator'
			},
			{
				name: 'treeCopy',
				text: 'Copy'
			},
			{
				name: 'treePaste',
				text: 'Paste'
			},
			{
				name: 'treeCut',
				text: 'Cut'
			},
			{
				name: 'separator'
			},
			{
				name: 'treeRename',
				text: 'Rename'
			},
			{
				name: 'treeDelete',
				text: 'Delete'
			}
		];
		this.projectPopup = new Tree.UI.MenuPopup( this.tree, 'projectPopup',
		{
			root: this.root,
			parent: this,
			x: this.root.mouseX,
			y: this.root.mouseY,
			z: this.z + 10,
			theme: this.theme,
			list: popup,
			caller: this,
			onDestroyed: onDestroyed
		} );
		function onDestroyed()
		{
			this.projectPopup = false;
		}
	}
};
Main.onMenu = function( optionName, menuItem )
{
	var self = this;
	var mainWindow = require('electron').remote.getCurrentWindow();
	switch( optionName )
	{
		case 'fileNew':
			break;
		case 'fileNewProject':
			break;
		case 'fileOpen':
			break;
		case 'fileOpenDirectory':
			OneOS.DOS.showOpenDialog( mainWindow,
			{
				title: 'Please choose a directory...',
				buttonLabel: 'Open directory',
				properties: [ 'openDirectory' ]
			}, function( response, data, extra )
			{
				if ( response )
				{
					self.setDirectory( data.filenames[ 0 ] );
				}
			} );
			break;
		case 'fileOpenProject':
			break;
		case 'fileSave':
			break;
		case 'fileSaveAs':
			break;
		case 'fileSaveAll':
			break;
		case 'fileSaveProject':
			break;
		case 'fileClose':
			break;
		case 'fileCloseProject':
			break;
		case 'fileUpdateProject':
			break;
		case 'filePreferences':
			break;
		case 'fileQuit':
			break;
		case 'editUndo':
			break;
		case 'editRedo':
			break;
		case 'editCopy':
			break;
		case 'editPaste':
			break;
		case 'editCut':
			break;
		case 'editFind':
			break;
		case 'editReplace':
			break;
		case 'editFindInFiles':
			break;
		case 'editReplaceInFiles':
			break;
		case 'editBookmark':
			break;
		case 'editPreviousBookmark':
			break;
		case 'editNextBookmark':
			break;
		case 'runBuild':
			break;
		case 'runRun':
			break;
		default:
			console.log( 'AMOS: unimplemented option!' );
			break;
	}
};

Main.clickOnTab = function( tab )
{
	var line = this.findLineFromPath( tab.path );
	if ( line && line.edited )
	{
		this.projectTree.activateLine( line );
		if ( !line.edited )
			return;			// Should never happen

		this.editor.setSession( line.path );
		this.projectTree.doRefresh();
	}
};
Main.closeTab = function( tab )
{
	// Find the corresponding line
	var line = this.findLineFromPath( tab.path );
	if ( line )
	{
		// Line no longer edited
		line.edited = false;
		this.projectTree.deactivateLine( line );

		// No more active session
		this.editor.killSession( line.path );

		// Remove the tab / activate previous in history
		var tab = this.tabs.deleteTab( tab );
		if ( tab )
		{
			line = this.findLineFromPath( tab.identifier );
			this.clickOnProjectLine( line );
		}
	}
};
Main.clickOnProjectLine = function( line )
{
	// Is the line opened?
	if ( line.type == 'file' )
	{
		if ( !line.edited )
		{
			line.edited = true;
			line.loaded = false;

			// Add a new tab if not already created
			var found = false;
			var tab = this.tabs.getFirstTab();
			while( tab )
			{
				if ( tab.identifier == line.path )
				{
					found = true;
					break;
				}
				tab = this.tabs.getNextTab();
			}
			if ( !found )
			{
				this.tabs.insertTab(
				{
					text: line.text,
					path: line.path,
					hint: this.projectPath + line.path,
					identifier: line.path
				}, 'afterCurrent' );
			}

			// Do we need to load the source?
			var self = this;
			if ( self.editor.getSession( line.path ) )
			{
				self.editor.setSession( line.path );
				line.loaded = true;
			}
			else
			{
				// Load the source
				debugger;
				OneOS.DOS.loadFile( self.projectPath + pathSeparator + line.path, {}, function( response, source, extra )
				{
					if ( response )
					{
						line.loaded = true;
						line.sessionIdentifier = self.editor.openSession( line.path, source );
					}
				} );
			}
		}
		else
		{
			// Activates the tab
			this.tabs.activateTab( line.path );
			this.projectTree.activateLine( line );
			this.editor.setSession( line.path );
		}
	}
};
Main.dblClickOnProjectLine = function( line )
{
	debugger;
};
Main.initConfig = function()
{
	// Creates the configuration if it does not exist
	if ( !this.config )
	{
		this.configStore = new OneOS.Config();
		this.config = { history: [] };
	}
}
Main.loadConfig = function( options, callback, extra )
{
	var self = this;

	self.initConfig();
	self.configStore.getKey( 'history', { defaultValue: [] }, function( response, data )
	{
		if ( response )
		{
			self.config.history = data;

			// Load the last open project
			self.configStore.getKey( 'openProject', { defaultValue: -1 }, function( response, num, extra )
			{
				if ( response && num >= 0 )
				{
					if ( num < config.history.length )
					{
						self.loadProjectConfig( self.config.history[ num ], function()
						{
							if ( callback )
								callback( true, {}, extra );
						} );
					}
				}
				else
				{
					if ( callback )
						callback( true, {}, extra );
				}
			}, extra );
		}
		else
		{
			callback( false, {}, extra );
		}
	}, extra );
},
Main.saveConfig = function( options )
{
	if ( options[ 'history' ] )
	{
		// Current project first, then the others
		var history = [ this.projectPath ];
		for ( var h = 0; h < this.config.history.length; h++ )
		{
			if ( this.config.history[ h ] != this.projectPath )
				history.push( this.config.history[ h ] );
		}

		// Limit the size of history to 8
		history.length = Math.min( 8, history.length );

		// Save!
		self.configStore.setKey( 'history', history );
	}
},
Main.loadProjectConfig = function( path, callback, extra )
{
	var self = this;

	// Read the config
	OneOS.DOS.loadFile( path + 'editor.config', {}, function( response, data, extra )
	{
		if ( response )
		{
			var config = false;
			try
			{
				config = JSON.parse( data );
			}
			catch( e )
			{
				if ( callback )
					callback( false, { error: 'ERROR - Cannot load editor configuration.'} );
				return;
			}
			if( config  )
			{
				self.setDirectory( config.projectPath, function( response )
				{
					// Which files are open?
					for ( var path in config.tree )
					{
						var line = self.findLineFromPath( path );
						if ( line )
						{
							line.edited = config.tree[ path ].edited;
							line.open = config.tree[ path ].open;
						}
					}
					self.projectTree.doRefresh();

					// Recreates the tabs
					for ( var t = 0; t < config.tabs.length; t++ )
					{
						var tab = config.tabs[ t ];
						var line = self.findLineFromPath( tab.identifier );
						if ( line )
						{
							var tab =
							{
								text: line.text,
								path: line.path,
								hint: self.projectPath + line.path,
								identifier: tab.identifier
							}
							self.tabs.insertTab( tab, 'end' );
						}
					}

					// Sets the history back
					if ( config.tabHistory )
						self.tabs.setHistory( config.tabHistory );

					// Restore editor sessions and tabs
					var count = 0;
					for ( var s in config.sessions )
					{
						count++;
						self.editor.setSessionInformation( config.sessions[ s ], config.projectPath, function()
						{
							count--;

							if ( count == 0 )
							{
								// Open all editors
								for ( var t = 0; t < config.tabs.length; t++ )
								{
									var tab = config.tabs[ t ];
									var line = self.findLineFromPath( tab.identifier );
									if ( line )
									{
										line.edited = false;
										self.clickOnProjectLine( line, function( path )
										{
										} );
									}
								}

								// Wait for all to be loaded, and activates first in history
								var handleInterval = setInterval( function()
								{
									var count = 0;
									for ( var tt = 0; tt < config.tabs.length; tt++ )
									{
										var tab = config.tabs[ tt ];
										var line = self.findLineFromPath( tab.identifier );
										if ( line.loaded )
											count++;
										if ( count == config.tabs.length )
										{
											// Activates first in history
											if ( config.tabHistory && config.tabHistory.length )
											{
												var line = self.findLineFromPath( config.tabHistory[ 0 ] );
												if ( line )
													self.clickOnProjectLine( line );
											}
											clearInterval( handleInterval );
											if ( callback )
												callback( true );
											return;
										}
									}
								}, 20 );
							}
						} );
					}
					if ( count == 0 )
					{
						if ( callback )
							callback( true );
					}
				} );
			}
		}
	}, extra );
};
Main.saveProjectConfig = function()
{
	// Build the config object
	var config = {};
	config.projectPath = this.projectPath;

	// Stores the open/close state of the tree
	config.tree = {};
	var line = this.projectTree.getFirstLine();
	while( line )
	{
		config.tree[ line.identifier ] =
		{
			open: line.open ? true : false,
			edited: line.edited ? true : false
		};
		line = this.projectTree.getNextLine();
	}

	// Stores the tabs
	config.tabs = [];
	var tab = this.tabs.getFirstTab();
	while( tab )
	{
		config.tabs.push(
		{
			identifier: tab.identifier
		} );
		tab = this.tabs.getNextTab();
	}

	// History of the tabs
	config.tabHistory = this.tabs.getHistory();

	// Editor sessions
	config.sessions = this.editor.getSessionsInformation();

	// Save config
	var json = JSON.stringify( config );
	OneOS.DOS.writeFile( this.projectPath + 'editor.config', json, {}, function( response, data )
	{
	} );
};

Main.findTabFromPath = function( path )
{
	var tab = this.tabs.getFirstTab();
	while( tab )
	{
		if ( tab.path == line.path )
			return tab;
		tab = this.tabs.getNextTab();
	}
	return null;
}
Main.findLineFromPath = function( path )
{
	var line = this.projectTree.getFirstLine();
	while( line )
	{
		if ( line.path == path )
			return line;
		line = this.projectTree.getNextLine();
	}
	return false;
};
Main.setDirectory = function( path, callback )
{
	var self = this;
	self.projectPath = path;
	self.treeContent = false;

	// Get project directory, folders first
	OneOS.DOS.getDirectory( path, { recursive: true, sort: true }, function( response, list, extra )
	{
		if ( response )
		{
			// Setup root
			var p = path;
			var fileName;
			if ( p.substring( path.length -1 ) == pathSeparator )
				p = p.substring( 0, p.length -1 );
			if ( p.lastIndexOf( pathSeparator ) >= 0 )
				fileName = p.substring( p.lastIndexOf( pathSeparator ) + 1 );
			else
				fileName = p.split( ':' )[ 1 ];
			var def =
			{
				text: fileName,
				path: '<---root--->',
				type: 'directory',
				children: [],
				identifier: '<---root--->',
				open: 1
			}
			self.treeContent = def;

			// Directories to explore?
			if ( list.length )
			{
				doDirectory( def.children, list );
			}
			self.projectTree.setTreeDefinition( self.treeContent );
		}
		if ( callback )
			callback( true, self.treeContent, extra );

		function doDirectory( destination, list )
		{
			for ( var l = 0; l < list.length; l++ )
			{
				var item = list[ l ];
				var subPath = item.path.substring( path.length );
				if ( subPath.charAt( 0 ) == pathSeparator )
					subPath = subPath.substring( 1 ) ;
				if ( item.isDirectory )
				{
					var def =
					{
						text: item.filename,
						path: subPath,
						type: 'directory',
						children: [],
						identifier: subPath,
						open: 0
					}
					destination.push( def );
					doDirectory( def.children, item.children );
				}
				else if ( item.isFile )
				{
					var def =
					{
						text: item.filename,
						path: subPath,
						type: 'file',
						identifier: subPath
					}
					// Finds the icon
					def.icon = self.getIconFromPath( item.filename );
					destination.push ( def );
				}
			}
		}
	} );
}
