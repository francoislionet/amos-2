
Welcome to the development of AMOS 2
------------------------------------

This project is open source, contributors welcomed when the project has a little matured.

GOAL: re-create AMOS Basic on modern machines, with the power of today's computers and a modern editor.

STEP 1: the editor
A modern source editor to edit and modify the code. Based on the Tree engine, the Javascript engine 'that mimics nature'.
The Tree engine is a all-purpose engine based on nature's parent/children relationships and the moto: what is simple is powerful.
A brief description of the fundamentals of the Tree engine can be found here: https://docs.google.com/document/d/1l1pf5hwTvY7yfrUQoMv1ANEKbL_XEs53_iYLOuNN7Gs/edit?usp=sharing

CONTACT:
Feel free to message me at justefrancoissansceedille@gmail.com
