/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine Controller - joystick / mouse / accelerometers / keyboard input
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 21/08/2017
 */
Tree = Tree || {};
Tree.Controller = Tree.Controller || {};

/**
 * Controller object, manages keyboard, mouse, joystick and inputs
 *
 * @param (object) tree the Tree object
 */
Tree.Controller = function( tree, name, options )
{
	if ( !options.parent || ( options.parent && options.parent.parent != null ) )
	{
		Tree.log( this, { level: Tree.ERRORLEVEL_BREAK, error: 'A controller item can only be inserted on the root of a tree.' } )
	}
	Tree.Items.init( this, tree, name, 'Tree.Controller', options );

	this.map =
	[
		{ keyCode: 38, joyCode: Tree.Controller.UP },
		{ keyCode: 40, joyCode: Tree.Controller.DOWN },
		{ keyCode: 39, joyCode: Tree.Controller.RIGHT },
		{ keyCode: 37, joyCode: Tree.Controller.LEFT },
		{ keyCode: 32, joyCode: Tree.Controller.FIRE1 },
		{ keyCode: 13, joyCode: Tree.Controller.FIRE2 }
	];
	this.input = 0;
	this.previousInput = 0;
	var self = this;
	return this;
};
Tree.Controller.LEFT = 0x00000001;
Tree.Controller.RIGHT = 0x00000002;
Tree.Controller.UP = 0x00000004;
Tree.Controller.DOWN = 0x00000008;
Tree.Controller.FIRE1 = 0x00000010;
Tree.Controller.FIRE2 = 0x00000020;

Tree.Controller.messageUp = function( message )
{
	var flag;
	switch ( message.command )
	{
		case 'refresh':
			this.joystickPrevious = this.joystick;
			flag = true;
			break;

		case 'keydown':
			for ( var key in this.map )
			{
				if ( message.event.keyCode == this.map[ key ].keyCode )
				{
					message.code = this.map[ key ].joyCode;
					this.joystick |= message.code;
					this.joystickPrevious &= ~message.code;
					message.command = 'joystickdown';
					message.type = 'controller';
					flag = true;
					break;
				}
			}
			break;

		case 'keyup':
			for ( var key in this.map )
			{
				if ( message.event.keyCode == this.map[ key ].keyCode )
				{
					message.code = this.map[ key ].joyCode;
					this.joystick &= ~message.code;
					this.joystickPrevious &= ~message.code;
					message.command = 'joystickup';
					message.type = 'controller';
					flag = true;
					break;
				}
			}
			break;
	}
	// If something happened, send messages to process (delays, combo keys etc.)
	if ( flag )
	{
		return this.startProcess( message, [ 'joystick', 'joystickPrevious' ] );
	}
	return false;
};
Tree.Controller.messageDown = function( message )
{
	// Gather new data
	this.endProcess( message, [ 'joystick', 'joystickPrevious' ] );

	// Still a message to send?
	if ( message.command == 'joystickup' || message.command == 'joystickdown' )
	{
		message.joystick = this.joystick;		// Restores value containing 'UPDATED'
		for ( var t = 0; t < this.tree.trees.length; t++ )
		{
			var root = this.tree.trees[ t ];

			// For each item that has registered
			var list = root.events[ 'controller' ];
			if ( list )
			{
				for ( identifier in list )
				{
					var item = root.allItems[ identifier ];
					if ( item && ( list[ identifier ] & message.options ) != 0 )
					{
						message.type = 'controller';
						Tree.sendMessageToItem( root, item, message );
					}
				}
			}
		}
	}
};

/**
 * isDown
 *
 * Returns true if the joystick is pressed
 *
 * @param (number) key definition
 * @return true if the key is down, false if not
 */
Tree.Controller.isDown = function ( key )
{
	return ( this.joystick & key ) != 0;
};

/**
 * isUp
 *
 * Returns true if the joystick is not pressed
 *
 * @param (number) key definition
 * @return true if the key is down, false if not
 */
Tree.Controller.isUp = function ( key )
{
	return ( this.joystick & key ) == 0;
};

/**
 * isPressed
 *
 * Returns true if the joystick has just been pressed. (no repeat)
 *
 * @param (number) key definition
 * @return true if the key has been pressed, false if up
 */
Tree.Controller.isPressed = function ( key )
{
	if ( ( this.joystick & key ) != 0 )
	{
		if ( ( this.joystickPrevious & key ) == 0 )
		{
			this.joystickPrevious &= key;
			return true;
		}
	}
	return false;
};

/**
 * isReleased
 *
 * Returns true if the joystick has just been released.
 *
 * @param (number) key definition
 * @return true if the key has been pressed, false if up
 */
Tree.Controller.isReleased = function ( key )
{
	if ( ( this.joystick & key ) == 0 )
	{
		if ( ( this.joystickPrevious & key ) == 0 )
		{
			this.joystickPrevious &= key;
			return true;
		}
	}
	return false;
};
