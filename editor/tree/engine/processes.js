/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine main processes
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 18/09/2017
 */
Tree = Tree || {};

/**
 * Common functions
 */
Tree.Processes =
{
	/**
	 * init
	 *
	 * Process initialisation, must be called from the process constructor
	 *
	 * @param (object) self the process itself
	 * @param (object) treethe Tree engine
	 * @param (object) object the item handled by the process
	 * @param (string) className the name of the process class
	 * @param (object) options process creation options
	 */
	init: function ( tree, self, item, className, options )
	{
		self.tree = tree;
		self.isProcess = true;
		self.utilities = tree.utilities;
		self.item = item;
		self.root = item.root;
		self.className = className;
		self.utilities.setOptions( self, options );
		self.resources = tree.resources;
		self.addProcess = Tree.Items.addProcess;
		self.removeProcess = Tree.Items.removeProcess;
		self.getTemporaryFunctions = Tree.Items.getTemporaryFunctions;
		self.getTemporaryFunctionsCount = Tree.Items.getTemporaryFunctionsCount;
		self.setTemporaryProperty = Tree.Items.setTemporaryProperty;
		self.setAfter = Tree.Items.setAfter;
		self.callAfter = Tree.Items.callAfter;
		self.temporaryFunctions = [];

        // Assign the functions of the class
        Tree.Utilities.assignToObject( self, className );
	}
};
