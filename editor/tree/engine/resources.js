/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine resources management
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 18/10/2018
 */
Tree.Resources = Tree.Resources || {};

/**
 * Resources constructor
 *
 * @param (object) options creation options
 *
 * Options
 * tree: (object) the Tree object
 */
Tree.Resources.Manager = function ( options )
{
	this.tree = options.tree;
	this.tree.utilities.setOptions( this, options );
	this.images = { };
	this.sounds = { };
	this.toLoad = 0;
	this.loaded = 0;
	Object.assign( this, Tree.Resources.Manager );

	// Try to load the icon of the application
	/*
	var self = this;
	this.loadSingleImage( 'resources/icon.png', { hotSpot: Tree.HOTSOPT_LEFTTOP, name: 'applicationIcon' }, function( image )
	{
		self.applicationIcon = image;
	}, 'This should come back.' );
	*/
	return this;
};
Tree.Resources.Manager.isLoaded = function ()
{
	return this.loaded == this.toLoad;
},
Tree.Resources.Manager.getLoadPercent = function ()
{
	return ( this.loaded / this.toLoad ) * 100;
},
Tree.Resources.Manager.checkCompletion = function ()
{
	if ( this.caller )
	{
		if ( this.loaded == this.toLoad )
		{
			if ( this.loadCompleted )
			{
				this.loadCompleted.apply( this.caller, [ this.loaded ] );
			}
		}
		else
		{
			if ( this.loadProgress )
				this.loadProgress.apply( this.caller, [ ( this.loaded / this.toLoad ) * 100 ] );
		}
	}
	return true;
},
/**
 * loadImages
 *
 * Loads an image or a list of images
 * Calls the Tree loadProgress function after loading each image
 * Calls the Tree loadCompleted function when finished
 *
 * @param (string or array of objects) imageList the images to load
 *        imageList.name: name of the image
 *        imageList.path: the path to the image file
 *        imageList.hotSpot: (optional) the Tree hotSpot definitiion
 *        imageList.hotSpotX: (optional) the horizontal hotSpot
 *        imageList.hotSpotY: (optional) the vbertical hotSpot
 */
 Tree.Resources.Manager.loadImages = function ( imageList, options, callback, extra )
 {
	this.loadError = false;
	Tree.Utilities.setOptions( this, options );

	var self = this;
	this.imageDefinitions = {};

	 // Calculate total number of images to load
	 for ( var i = 0; i < imageList.length; i ++ )
	 {
		 if ( typeof imageList[ i ].start != 'undefined' )
			 this.toLoad += imageList[ i ].end - imageList[ i ].start + 1;
		 else
			 this.toLoad ++;
	 }

	 // Load all the images
	 var image;
	 var comboImage;
	 for ( i = 0; i < imageList.length; i ++ )
	 {
		 var imageDef = imageList[ i ];
		 if ( !imageDef.name )
		 {
			var name = Tree.Utilities.getFileName( imageDef.path );
			imageDef.name = name.split( '.' )[ 0 ]
		 }

		 // An list of numbered images?
		 if ( imageDef.start )
		 {
			 comboImage = [];
			 self.images[ imageDef.name ] =
			 {
				 name: imageDef.name,
				 images: comboImage
			 };
			 var dot = imageDef.path.lastIndexOf( '.' );
			 for ( var z = imageDef.start; z <= imageDef.end; z++ )
			 {
				var transmit =
				{
					imageDef: imageDef,
					comboImage: comboImage,
					z: z
				}
				var image = new Image();
				image.transmit = transmit;
				image.onload = function()
				{
					var def = this.transmit.imageDef;
					var combo = this.transmit.comboImage;
					var z = this.transmit.z;
					var name = def.name + z;
					this.treeName = name;
					this.hotSpot = def.hotSpot;
					this.hotSpotX = 0;
					this.hotSpotY = 0;
					if ( typeof imageDef.hotSpotX != 'undefined' )
						this.hotSpotX = def.hotSpotX;
					if ( typeof imageDef.hotSpotY != 'undefined' )
						this.hotSpotY = def.hotSpotY;

					combo.push(
					{
						name: name,
						image: image,
						z: z - def.start
					} );
					self.images[ name ] =
					{
						name: name,
						image: image
					};
					self.finishImage( image, {}, callback, extra );
				}
				image.src = imageDef.path.substring( 0, dot ) + z + imageDef.path.substring( dot );
			}
		}
		else
		{
			// A single image
			var image = new Image();
			image.transmit = imageDef;
			image.onload = function( data )
			{
				var def = this.transmit;
				this.treeName = def.name;
				this.treeWidth = def.width;
				this.treeHeight = def.height;
				this.hotSpot = def.hotSpot;
				this.hotSpotX = 0;
				this.hotSpotY = 0;
				if ( typeof def.hotSpotX != 'undefined' )
					this.hotSpotX = def.hotSpotX;
				if ( typeof def.hotSpotY != 'undefined' )
					this.hotSpotY = def.hotSpotY;

				self.images[ def.name ] =
				{
					name: def.name,
					image: this
				};
				self.finishImage( image, {}, callback, extra );
			};
			image.src = imageDef.path;
		}
	}
	return true;
};
Tree.Resources.Manager.finishImage = function ( image, options, callback, extra )
{
	var self = this;
	if ( typeof image.treeWidth != 'undefined' && typeof image.treeHeight != 'undefined' )
	{
		if ( image.treeWidth != image.width || image.treeHeight != image.height )
		{
			var canvas = document.createElement( 'canvas' );
			canvas.width = image.treeWidth;
			canvas.height = image.treeHeight;
			var context = canvas.getContext( '2d' );
			context.drawImage( image, 0, 0, image.treeWidth, image.treeHeight );
			newImage = new Image();
			newImage.treeName = image.treeName;
			newImage.hotSpot = image.hotSpot;
			newImage.hotSpotX = image.hotSpotX;
			newImage.hotSpotY = image.hotSpotY;
			self.images[ image.treeName ].image = newImage;
			newImage.onload = function()
			{
				finishIt( this, options, callback, extra );
			}
			newImage.src = canvas.toDataURL( "image/png" );
			return;
		}
	}
	finishIt( image, options, callback, extra );

	// When loaded
	function finishIt( image, options, callback, extra )
	{
		if ( typeof image.hotSpot != 'undefined' )
		{
			self.setImageHotSpot( image, image.hotSpot );
		}

		// Adapt the image to the renderer
		var count = 0;
		for ( var r = 0; r < self.tree.renderers.length; r++ )
		{
			var renderer = self.tree.renderers[ r ];
			count++;
			renderer.setImage( image, {}, function( image )
			{
				Tree.log( self, { infos: 'Loaded image: ' + image.treeName, level: Tree.ERRORLEVEL_LOW } );
				count--;
				if ( count == 0 )
				{
					if ( options.immediateCallback )
					{
						callback( true, image, extra );
					}
					else
					{
						self.loaded++;
						if ( self.loaded == self.toLoad )
						{
							callback( true, self.loaded, extra );
						}
					}
				}
			}, extra );
		}
	}
};
Tree.Resources.Manager.loadSingleImage = function( path, options, callback, extra )
{
	var self = this;

	// Name defined
	var name = options.name;
	if ( typeof name == 'undefined' )
	{
		name = Tree.Utilities.getFileName( path );
		name = name.split( '.' )[ 0 ];
	}

	// Image already loaded?
	if ( self.images[ name ] )
	{
		callback( true, self.images[ name ], extra );
		return;
	}

	image = new Image();
	self.images[ name ] =
	{
		name: name,
		image: image
	};
	image.treeName = name;
	if ( typeof options.width != 'undefined' )
		image.treeWidth = options.width;
	if ( typeof options.height != 'undefined' )
		image.treeHeight = options.height;
	image.hotSpot = options.hotSpot;
	image.onload = onLoaded;
	image.src = this.tree.utilities.getPath( path );

	function onLoaded()
	{
		self.finishImage( this, { immediateCallback: true }, callback, extra );
	}
};
Tree.Resources.Manager.getApplicationIcon = function ()
{
	return this.applicationIcon; 	// Can be undefined at start, or if icon is not present!
};

Tree.Resources.Manager.setImageHotSpot = function ( image, hotSpot )
{
	switch ( hotSpot )
	{
		case Tree.HOTSPOT_LEFTTOP:
			image.hotSpotX = 0;
			image.hotSpotY = 0;
			break;
		case Tree.HOTSPOT_CENTERTOP:
			image.hotSpotX = image.width / 2;
			image.hotSpotY = 0;
			break;
		case Tree.HOTSPOT_RIGHTTOP:
			image.hotSpotX = image.width;
			image.hotSpotY = 0;
			break;
		case Tree.HOTSPOT_LEFTCENTER:
			image.hotSpotX = 0;
			image.hotSpotY = image.height / 2;
			break;
		case Tree.HOTSPOT_CENTER:
			image.hotSpotX = image.width / 2;
			image.hotSpotY = image.height / 2;
			break;
		case Tree.HOTSPOT_RIGHTCENTER:
			image.hotSpotX = image.width;
			image.hotSpotY = image.height / 2;
			break;
		case Tree.HOTSPOT_LEFTBOTTOM:
			image.hotSpotX = 0;
			image.hotSpotY = image.height;
			break;
		case Tree.HOTSPOT_CENTERBOTTOM:
			image.hotSpotX = image.width / 2;
			image.hotSpotY = image.height;
			break;
		case Tree.HOTSPOT_RIGHTBOTTOM:
			image.hotSpotX = image.width;
			image.hotSpotY = image.height;
			break;
	}
	// Default hotSpot is in the center
	if ( typeof image.hotSpotX == 'undefined' )
		image.hotSpotX = 0;
	if ( typeof image.hotSpotY == 'undefined' )
		image.hotSpotY = 0;
	image.hotSpot = hotSpot;
};

// Returns the path of all the images
Tree.Resources.Manager.getImagesPaths = function ( )
{
	var result = [];
	for ( var i in this.images )
	{
		if ( this.images[ i ].image )
			result.push( this.images[ i ].image.src );
		else
		{
			for ( var ii = 0; ii < this.image[ i ].images.length; ii++ )
			{
				result.push( this.images[ i ].images[ ii ].image.src );
			}
		}
	}
	return result;
};

/**
 * loadSounds
 *
 * Loads a sound or a list of sounds
 * Calls the Tree loadProgress function after loading each sound
 * Calls the Tree loadCompleted function when finished
 *
 * @param (string or array of objects) soundList the images to load
 *        soundList.name: name of the sound
 *        sounhdList.path: the path to the sound file
 *        soundList.volume (optional) default volume of this sound (from 0 to 1)
 *        soundList.loops: (optional) loop the sound at the end ( <=0: infinite, >0: number of loops, default = 1)
 */
Tree.Resources.Manager.loadSounds = function ( soundList, options, callback, extra )
{
	Tree.Utilities.setOptions( this, options );

	// Calculate total number of sounds to load
	for ( var i = 0; i < soundList.length; i ++ )
	{
		if ( typeof soundList[ i ].start != 'undefined' )
			this.toLoad += soundList[ i ].end - soundList[ i ].start;
		else
			this.toLoad ++;
	}

	// Load all the sounds
	var sound;
	var self = this;
	for ( i = 0; i < soundList.length; i ++ )
	{
		var soundDef = soundList[ i ];

		var name = options.name;
		if ( name == 'undefined' )
		{
			name = Tree.Utilities.getFileName( soundDef.path );
			name = name.split( '.' )[ 0 ];
		}
/*
		var path = soundDef.path;
		sound = new SoundObject( path, loaded );
		sound.name = name;
		sound.volume = 1;
		if ( typeof soundDef.volume != 'undefined' )
			sound.volume = soundDef.volume;
		sound.loops = 1;
		if ( typeof soundDef.loops != 'undefined' )
			sound.loops = soundDef.loops;
		this.sounds[ soundDef.name ] = sound;
*/
	}

	// When a sound is loaded
	function loaded()
	{
		// One more loaded, check for completion
		self.loaded ++;
		if ( self.loaded == self.toLoad )
		{
			callback( true, this, extra );
		}
	}
	return true;
};

/**
 * getSound
 *
 * Finds a sound from its name
 *
 * @param (string) name of the sound to find
 * @return (object) this sound found or null if not found
 */
Tree.Resources.Manager.getSound = function ( name )
{
	if ( this.sounds[ name ] )
		return this.sounds[ name ];
	return false;
};

/**
 * addImage
 *
 * Adds a new image to the list
 *
 * @param (string) name of the image to find
 * @return (object) this image found or null if not found
 */
Tree.Resources.Manager.addImage = function ( name, image, hotSpot, callback )
{
	if ( !this.images[ name ] )
	{
		// Adds the image to the list
		image.treeName = name;
		this.setImageHotSpot( image, hotSpot );
		this.images[ name ] =
		{
			name: name,
			image: image
		};
/*
		// Adapt the image to the renderer
		this.tree.renderer.setImage( image, function( image )
		{
			Tree.log( self, { infos: 'Tree.Resources.Manager.addImage: added image ' + image.treeName, level: Tree.ERRORLEVEL_LOW } );
			if ( callback )
				callback( image );
		} );
*/
		return true;
	}
	else
	{
		Tree.log( self, { infos: 'Tree.Resources.Manager.addImage: image already exists ' + name, level: Tree.ERRORLEVEL_HIGH } );
	}
	return false;
};

/**
 * getImage
 *
 * Finds an image from its name
 *
 * @param (string) name of the image to find
 * @return (object) this image found or null if not found
 */
Tree.Resources.Manager.getImage = function ( name )
{
	var image = this.images[ name ];
	if ( image )
	{
		if ( image.images )
		{
			image.width = image.images[ 0 ].image.width;
			image.height = image.images[ 0 ].image.height;
			image.hotSpotX = image.images[ 0 ].image.hotSpotX;
			image.hotSpotY = image.images[ 0 ].image.hotSpotY;
			return image;
		}
		else
			return image.image;
	}
	console.log( 'Tree.Resources.Manager, image ' + name + ' not found...')
	return false;
};

/**
 * deleteSound
 *
 * Deletes a sound from its name
 *
 * @param (string) name name of the sound to remove
 */
Tree.Resources.Manager.deleteSound = function ( name )
{
	var temp = { };
	for ( var i in this.sounds )
	{
		if ( i != name )
			temp[ name ] = this.sounds[ i ];
	}
	this.sounds = temp;
	return false;
};

/**
 * deleteSound
 *
 * Deletes an image from its name
 *
 * @param (string) name name of the image to remove
 */
Tree.Resources.Manager.deleteImage = function ( name )
{
	var temp = { };
	for ( var i in this.images )
	{
		if ( i != name )
			temp[ name ] = this.images[ i ];
	}
	this.images = temp;
	return false;
};
