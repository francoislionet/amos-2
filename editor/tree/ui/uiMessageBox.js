/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.MessageBox = function ( tree, name, options )
{
	this.title = 'My title';
	this.text = 'My message box text';
	this.cancel = 'Cancel';
	this.font = '16px sans serif';
	this.colorBack = '#C0C0C0';
	this.colorBright = '#E0E0E0';
	this.colorDark = '#808080';
	this.colorText = '#000000';
	this.colorTitle = '#FF0000';
	this.widthButton = 80;
	this.heightButton = 32;

	this.renderItemName = 'Tree.UI.RenderItems.MessageBox';
	Tree.Items.init( this, tree, name, 'Tree.UI.MessageBox', options );
};
Tree.UI.MessageBox.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height' ] );
};
Tree.UI.MessageBox.messageDown = function ( message )
{
	return this.endProcess( message, [ 'x', 'y', 'z', 'width', 'height' ] );
};
Tree.UI.MessageBox.click = function ( )
{
	if ( this.caller && this.onCancel )
		this.onCancel.apply( this.caller, [ ] );
	this.destroy();
};


Tree.UI.RenderItems.MessageBox_HTML = function ( tree, name, options )
{
	this.font = false;
	this.text = false;
	this.title = false;
	this.colorBack = false;
	this.colorBright = false;
	this.colorDark = false;
	this.colorText = false;
	this.colorTitle = false;
	this.widthButton = false;
	this.heightButton = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_HTML';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.MessageBox_HTML', options );

	this.width = 320;
	this.height = 200;
	this.item.width = this.width;
	this.item.height = this.height;

	this.item.startInsertItems();
	var button = new Tree.UI.Button( this.tree, 'cancel',
	{
		root: this.item.root,
		parent: this.item,
		font: this.utilities.setFontSize( this.item.font, 14 ),
		text: this.item.cancel,
		caller: this,
		onClick: onClick,
		theme: this.item.theme
	} );
	button.x = this.width - button.width - 8;
	button.y = this.height - button.height - 8;
	this.item.addItem( button );
	this.item.endInsertItems();

	function onClick()
	{
		this.item.destroy();
		if ( this.item.caller && this.item.onCancel )
			this.item.onCancel.apply( this.item.caller, [] );
	}
};
Tree.UI.RenderItems.MessageBox_HTML.render = function ( options )
{
	this.thisRect.drawHilightedBox( options, this.item.colorBack, this.item.colorBright, this.item.colorDark );
	options.rendererItem.drawText( options, this.rect.width / 2, this.rect.height / 12, this.item.title, this.item.font, this.item.colorTitle, 'center', 'middle', 20 );
	options.rendererItem.drawText( options, this.rect.width / 2, this.rect.height / 2, this.item.text, this.item.font, this.item.colorText, 'center', 'middle', 20 );
	return options;
};


Tree.UI.RenderItems.MessageBox_Canvas2D = function ( tree, name, options )
{
	this.font = false;
	this.text = false;
	this.title = false;
	this.colorBack = false;
	this.colorBright = false;
	this.colorDark = false;
	this.colorText = false;
	this.colorTitle = false;
	this.widthButton = false;
	this.heightButton = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_Canvas2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.MessageBox_Canvas2D', options );

	this.width = 320;
	this.height = 200;
	this.item.width = this.width;
	this.item.height = this.height;

	this.item.startInsertItems();
	var button = new Tree.UI.Button( this.tree, 'cancel',
	{
		root: this.item.root,
		parent: this.item,
		text: this.item.cancel,
		font: this.utilities.setFontSize( this.item.font, 14 ),
		caller: this,
		onClick: onClick
	} );
	button.x = this.width - button.width - 8;
	button.y = this.height - button.height - 8;
	this.item.addItem( button );
	this.item.endInsertItems();

	function onClick()
	{
		this.item.destroy();
		if ( this.item.caller && this.item.onCancel )
			this.item.onCancel.apply( this.item.caller, [] );
	}
};
Tree.UI.RenderItems.MessageBox_Canvas2D.render = Tree.UI.RenderItems.MessageBox_HTML.render;
