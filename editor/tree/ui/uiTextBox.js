/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.TextBox = function ( tree, name, options )
{
    this.font = '12px sans serif';
	this.text = 'My textbox text';
	this.colorBack = '#C0C0C0';
	this.colorBright = '#E0E0E0';
	this.colorDark = '#808080';
	this.colorText = '#000000';

	this.renderItemName = 'Tree.UI.RenderItems.TextBox';
	Tree.Items.init( this, tree, name, 'Tree.UI.TextBox', options );
};
Tree.UI.TextBox.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z', 'font', 'text' ] );
};
Tree.UI.TextBox.messageDown = function ( message )
{
	return this.endProcess( message, [ 'x', 'y', 'z', 'font', 'text' ] );
};



Tree.UI.RenderItems.TextBox_HTML = function ( tree, name, options )
{
	this.text = false;
	this.font = false;
	this.colorBack = false;
	this.colorBright = false;
	this.colorDark = false;
	this.colorText = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_HTML';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.TextBox_HTML', options );
};
Tree.UI.RenderItems.TextBox_HTML.render = function ( options )
{
	// Draw box
	this.thisRect.drawHilightedBox( options, this.item.colorBack, this.item.colorBright, this.item.colorDark );

	// Draw text
	options.context.drawText( options, this.rect.width / 2, this.rect.height / 2, this.item.text, this.item.font, this.item.textColor );

	return options;
};


Tree.UI.RenderItems.TextBox_Canvas2D = function ( tree, name, options )
{
	this.text = false;
	this.font = false;
	this.colorBack = false;
	this.colorBright = false;
	this.colorDark = false;
	this.colorText = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_Canvas2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.TextBox_Canvas2D', options );
};
Tree.UI.RenderItems.TextBox_Canvas2D.render = Tree.UI.RenderItems.TextBox_HTML.render;
