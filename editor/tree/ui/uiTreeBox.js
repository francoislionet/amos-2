/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.TreeBox = function ( tree, name, options )
{
    this.font = '12px sans serif';
	this.colorText = '#FFFFFF';
	this.colorTextMouseOver = '#808080';
	this.colorTextDown = '#202020';
	this.colorBack = '#404040';
	this.colorBackMouseOver = '#000080';
	this.colorBackDown = '#0000C0';
	this.colorLines = '#FFFFFF';
	this.widthTab = 16;
	this.widthIcon = 16;
	this.heightIcon = 16;
	this.widthArrow = 8;
	this.heightArrow = 8;
	this.alphaIcon = 0.5;
	this.icons = true;
	this.lines = true;
	this.nameArrowRight = false;
	this.nameArrowDown = false;
	this.paddingLine = 1;
	this.paddingIcon = 2;

	this.caller = false;
	this.onClick = false;
	this.onDoubleClick = false;
	this.onRightClick = false;

	this.wheelDelta = 32;
	this.position = 0;
	this.treeDefinition = false;
	this.renderItemName = 'Tree.UI.RenderItems.TreeBox';
	Tree.Items.init( this, tree, name, 'Tree.UI.TreeBox', options );

	this.yMaximum = 0;
	this.identifierCount = 0;
	this.treeContent = [];

	if ( this.treeDefinition )
	{
		this.setTreeDefinition( this.treeDefinition );
	}
};
Tree.UI.TreeBox.messageUp = function ( message )
{
	if ( message.type == 'mouse' )
		this.callRenderItem( 'onMouse', [ message ] );
	return this.startProcess( message, [ 'x', 'y', 'z', 'font', 'text' ] );
};
Tree.UI.TreeBox.messageDown = function ( message )
{
	return this.endProcess( message, [ 'x', 'y', 'z', 'font', 'text' ] );
};
Tree.UI.TreeBox.setTreeDefinition = function ( definition )
{
	var self = this;
	checkLine( definition, 0 );
	this.doRefresh();
	function checkLine( definition, rank )
	{
		if ( !definition.identifier )
			definition.identifier = '<T' + this.identifierCount++ + '>';
		if ( typeof definition.open == 'undefined' )
			definition.open = 1;
		definition.visible = true;
		definition.inside = false;
		definition.down = 0;
		definition.mouseOver = 0;
		definition.parent = false;
		definition.previousSibbling = false;
		definition.nextSibbling = false;
		definition.rank = rank;
		definition.rect = new Tree.Utilities.Rect();
		definition.rectIcon = new Tree.Utilities.Rect();
		self.treeContent.push( definition );

		var previous = false;
		if ( definition.children )
		{
			for ( var c = 0; c < definition.children.length; c++ )
			{
				checkLine( definition.children[ c ], rank + 1 );
				definition.children[ c ].parent = definition;
				definition.children[ c ].previousSibbling = previous;
				if ( c < definition.children.length - 1 )
					definition.children[ c ].nextSibbling = definition.children[ c + 1 ];
				else
					definition.children[ c ].nextSibbling = false;
				previous = definition.children[ c ];
			}
		}
	}
};
Tree.UI.TreeBox.getFirstLine = function ()
{
	this.currentGetLine = 0;
	return this.getNextLine();
};
Tree.UI.TreeBox.getNextLine = function ()
{
	if ( this.currentGetLine < this.treeContent.length )
		return this.treeContent[ this.currentGetLine++ ];
	return false;
};
Tree.UI.TreeBox.activateLine = function ( line )
{
	for ( var l = 0; l < this.treeContent.length; l++ )
		this.treeContent[ l ].down = false;
	line.down = true;
	this.doRefresh();
};
Tree.UI.TreeBox.deactivateLine = function ( line )
{
	line.down = false;
	this.doRefresh();
};


Tree.UI.RenderItems.TreeBox_HTML = function ( tree, name, options )
{
	this.icons = false;
	this.lines = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_HTML';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.TreeBox_HTML', options );
	this.position = 0;
	this.item.registerEvents( 'mouse' );
};
Tree.UI.RenderItems.TreeBox_HTML.render = function ( options )
{
	// Draw background box
	this.thisRect.drawBox( options, this.item.colorBack );

	var rect = new Tree.Utilities.Rect();
	rect.x = 0;
	rect.y = - this.item.position;
	this.item.yMaximum = 0;
	if ( this.item.treeContent.length )
		this.drawLine( options, this.item.treeContent[ 0 ], rect );
	return options;
};
Tree.UI.RenderItems.TreeBox_HTML.drawLine = function ( options, lineDefinition, rect )
{
	var measure = this.renderer.measureText( lineDefinition.text, this.item.font );
	var height = measure.height + this.item.paddingLine;
	rect.height = height;

	// Displayed or not?
	var inside = false;
	if ( rect.y + rect.height > 0 && rect.y <= this.height )
		inside = true;

	lineDefinition.inside = inside;

	// Erase background
	rect.width = options.width;
	if ( inside )
	{
		var color = this.item.colorBack;
		if ( lineDefinition.mouseOver )
			color = this.item.colorBackMouseOver;
		if ( lineDefinition.down )
			color = this.item.colorBackDown;
		rect.drawBox( options, color );
	}
	lineDefinition.rect = Object.assign( {}, rect );

	// Draws the lines on the left
	if ( this.item.lines )
	{
		// Find previous sibbling
		var parent = lineDefinition.parent;
		if ( parent )
		{
			options.rendererItem.setLineWidth( options, 1 );
			options.rendererItem.setStrokeStyle( options, this.item.colorLines );
			if ( parent.children )
			{
				for ( var s = 0; s < parent.children.length; s++ )
				{
					if ( parent.children[ s ] == lineDefinition )
					{
						// Draw horizontal line to parent
						var x1 = parent.rank * this.item.widthTab + this.item.widthArrow / 2;
						var x2 = rect.x;
						var y1 = rect.y + rect.height / 2;
						if ( inside )
						{
							options.rendererItem.beginPath( options );
							options.rendererItem.moveTo( options, x1, y1 );
							options.rendererItem.lineTo( options, x2, y1 );
							options.rendererItem.stroke( options );
							options.rendererItem.closePath();
						}

						// Draw vertical line to parent (even if out of view)
						// Only the last sibbling in a group
						if ( !lineDefinition.nextSibbling )
						{
							var y1 = parent.rect.y + parent.rect.height;
							var y2 = rect.y + rect.height / 2;
							if ( y1 < this.height && y2 > 0 )
							{
								options.rendererItem.beginPath( options );
								options.rendererItem.moveTo( options, x1, y1 );
								options.rendererItem.lineTo( options, x1, y2 );
								options.rendererItem.stroke( options );
								options.rendererItem.closePath();
							}
						}
						break;
					}
					previous = parent.children[ s ];
				}
			}
		}
	}

	// Color of the text
	var color = this.item.colorText;
	if ( lineDefinition.mouseOver )
		color = this.item.colorTextMouseOver;
	if ( lineDefinition.down )
		color = this.item.colorTextDown;

	// Draws the image on the left
	var xSave = rect.x;
	if ( lineDefinition.children && lineDefinition.children.length )
	{
		if ( this.item.icons )
		{
			var image, direction;
			if ( lineDefinition.open )
			{
				if ( this.item.nameArrowDown )
					image = this.resources.getImage( this.nameArrowDown );
				direction = 'bottom';
			}
			else
			{
				if ( this.nameArrowRight )
					image = this.resources.getImage( this.nameArrowRight );
				direction = 'right';
			}
			if ( image )
				lineDefinition.rectIcon = new Tree.Utilities.Rect( rect.x, rect.y + rect.height / 2 - this.item.heightIcon / 2, this.item.widthIcon, this.item.heightIcon );
			else
				lineDefinition.rectIcon = new Tree.Utilities.Rect( rect.x, rect.y + rect.height / 2 - this.item.heightArrow / 2, this.item.widthArrow, this.item.heightArrow );

			rect.x += lineDefinition.rectIcon.width + this.item.paddingIcon;
			if ( inside )
			{
				if ( image )
				{
					options.rendererItem.setAlpha( options, this.item.alphaIcon );
					lineDefinition.rectIcon.drawImage( options, image );
					options.rendererItem.setAlpha( options, options.alpha );
				}
				else
					lineDefinition.rectIcon.drawFilledTriangle( options, direction, color );
			}
		}
	}
	else
	{
		if ( this.item.icons && lineDefinition.icon )
		{
			lineDefinition.rectIcon = new Tree.Utilities.Rect( rect.x, rect.y + rect.height / 2 - this.item.heightIcon / 2, this.item.widthIcon, this.item.heightIcon );
			rect.x += this.item.widthIcon + this.item.paddingIcon;
			image = this.resources.getImage( lineDefinition.icon );
			if ( inside && image )
			{
				options.rendererItem.setAlpha( options, this.item.alphaIcon );
				lineDefinition.rectIcon.drawImage( options, image );
				options.rendererItem.setAlpha( options, options.alpha );
			}
		}
	}

	// Draws the text
	rect.width = options.width - rect.x;		// Clip!
	rect.drawText( options, lineDefinition.text, this.item.font, color, 'left', 'center' );
	rect.x = xSave;

	// Next!
	rect.y += height + this.item.paddingLine;
	this.item.yMaximum += height + this.item.paddingLine;

	// Display children?
	var l = 0;
	if ( lineDefinition.open && lineDefinition.children && lineDefinition.children.length )
	{
		l = lineDefinition.children.length;
		var xSave = rect.x;
		rect.x += this.item.widthTab;
		for ( var childNumber = 0; childNumber < lineDefinition.children.length; childNumber++ )
			this.drawLine( options, lineDefinition.children[ childNumber ], rect );
		rect.x = xSave;
	}
	return l;
};
Tree.UI.RenderItems.TreeBox_HTML.onMouse = function ( message )
{
	var refresh = false;
	switch( message.command )
	{
		case 'mousewheel':
			var delta = 16;
			if ( this.item.wheelDelta )
				delta = this.item.wheelDelta;
			this.item.position -= message.delta * delta;

			// Calculates the maximum in height
			if ( this.item.yMaximum - this.item.position < this.item.height )
				this.item.position = this.item.yMaximum - this.item.height;

			// Too much on the top
			if ( this.item.position < 0 )
				this.item.position = 0;

			refresh = true;
			break;
		case 'mousemove':
			this.lineUnderMouse = false;
			for ( var l = 0; l < this.item.treeContent.length; l++ )
			{
				var definition = this.item.treeContent[ l ];
				if ( definition.rect.isPointIn( this.item.mouse.x, this.item.mouse.y ) )
				{
					this.lineUnderMouse = definition;
					if ( !definition.mouseOver )
					{
						definition.mouseOver = true;
						refresh = true;
					}
				}
				else
				{
					if ( definition.mouseOver )
					{
						definition.mouseOver = false;
						refresh = true;
					}
				}
			}
			break;
		case 'mouseleave':
			for ( var l = 0; l < this.item.treeContent.length; l++ )
			{
				var definition = this.item.treeContent[ l ];
				if ( definition.mouseOver )
				{
					definition.mouseOver = false;
					this.lineUnderMouse = false;
					refresh = true;
				}
			}
			this.item.doRefresh();
			break;
		case 'click':
			if ( this.lineUnderMouse )
			{
				// Erases down in all the others
				for ( var l = 0; l < this.item.treeContent.length; l++ )
					this.item.treeContent[ l ].down = 0;

				refresh = true;
				if ( this.lineUnderMouse.children && this.lineUnderMouse.children.length != 0 )
					this.lineUnderMouse.open = 1 - this.lineUnderMouse.open;
				this.lineUnderMouse.down = 1;
				if ( this.item.caller && this.item.onClick )
					this.item.onClick.apply( this.item.caller, [ this.lineUnderMouse ] );
			}
			break;
		case 'dblclick':
			if ( this.lineUnderMouse )
			{
				if ( !this.lineUnderMouse.down )
				{
					// Erases down in all the others
					for ( var l = 0; l < this.item.treeContent.length; l++ )
						this.item.treeContent[ l ].down = 0;
					refresh = true;
					this.lineUnderMouse.down = true;
					if ( this.item.caller && this.item.onDoubleClick )
						this.item.onDoubleClick.apply( this.item.caller, [ this.lineUnderMouse ] );
				}
			}
			break;
		case 'contextmenu':
			if ( this.lineUnderMouse && this.lineUnderMouse.mouseOver )
			{
				if ( this.item.caller && this.item.onRightClick )
					this.item.onRightClick.apply( this.item.caller, [ this.lineUnderMouse ] );
			}
			break;
	}
	if ( refresh )
		this.item.doRefresh();
};
Tree.UI.RenderItems.TreeBox_HTML.message = function ( message )
{
	switch ( message.command )
	{
		case 'resize':
			if ( message.width )
			{
				this.width = message.width;
				this.item.width = message.width;
			}
			if ( message.height )
			{
				this.height = message.height;
				this.item.height = message.height;
			}
			this.item.doRefresh();
			break;
		default:
			break;
	}
}
