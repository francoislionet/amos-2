/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 06/05/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.Tool = function ( tree, name, options )
{
    this.text = 'Tool description';
    this.shortcut = '';
    this.nameImage = false;
    this.children = [];
    this.color = '#808080';
    this.colorDown = '#C0C0C0';
    this.colorMouseOver = '#A0A0A0';
    this.colorBorder = '#FFFFFF';
    this.sizeBorder = 0;
    this.font = '12px sans serif';
    this.colorFont = '#FFFFFF';
    this.alphaDown = 1;
    this.alphaMouseOver = 1;
    this.alpha = 0.5;
    this.displayText = false;
    this.openOnMouseOver = false;
    this.caller = false;
    this.onClick = false;
	this.onClickProc = false;
    this.hint = true;
	this.textHint = false;
	this.spaceAfterImage = 16;

	this.renderItemName = 'Tree.UI.RenderItems.Tool';
    Tree.Items.init( this, tree, name, 'Tree.UI.Tool', options );

    if ( this.children.length )
    {
        this.caller = this;
        this.onClick = this.openToolBox;
    }
	else
	{
		this.doCaller = this.caller;
		this.doClick = this.onClick;
        this.caller = this;
		this.onClick = function()
		{
			if ( this.onClickProc )
			{
				this.onClickProc.apply( this.doCaller, [ this.name ] );
			}
			else if ( this.doClick )
			{
				this.onClick.apply( this.doCaller, [] );
			}
		}
	}
    this.addProcess( new Tree.UI.GestureButton( this.tree, this, options ) );
    if ( this.hint )
    {
        if ( !this.textHint )
            this.textHint = this.text;
        this.addProcess( new Tree.UI.GestureHint( this.tree, this, options ) );
    }
};
Tree.UI.Tool.messageUp = function ( message )
{
    // Open / Close the tool box if it exists
	if ( message.type == 'mouse' )
	{
		if ( message.command == 'mouseenter' && this.openOnMouseOver )
		{
            this.openToolBox();
        }
    }
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'down', 'mouseOver', 'caller', 'onClick' ] );
};
Tree.UI.Tool.onDestroy = function ( message )
{
	if ( this.handleInterval )
	{
		clearInterval( this.handleInterval );
		this.handleInterval = null;
	}
};
Tree.UI.Tool.messageDown = function ( message )
{
	return this.endProcess( message );
};
Tree.UI.Tool.openToolBox = function ( message )
{
    if ( !this.toolBox && this.children.length )
    {
        // Create a box
        this.startInsertItems();
        var color = '#000000';

        this.toolBox = new Tree.UI.ColorBox( this.tree, this.name + '-box',
        {
            root: this.root,
            parent: this,
            x: this.rect.x + this.width,
            y: this.rect.y + this.height / 5,
            z: this.z + 1,
            width: 500,
            height: 500,
            theme: this.theme,
            color: this.color,
            sizeBorder: this.sizeBorder,
            colorBorder: this.colorBorder,
			paddingH: this.paddingH + 8,
			paddingV: this.paddingV + 8,
			colorPadding: this.colorPadding,
            renderSubItems: true
        } );
        this.addItem( this.toolBox );
        this.toolBox.registerEvents( 'mouse' );     // To check if the mouse is inside!
        this.endInsertItems();

        // Create the sub tools
        var count = 0;
        this.toolBox.startInsertItems();
        var y = 0;
        var maxWidth = 0;
        for ( var t = 0; t < this.children.length; t++ )
        {
            var tool = this.children[ t ];
            var toolProperties =
            {
                root: this.root,
                parent: this.toolBox,
                x: 0,
                y: y,
                widthCalc: 'native',
                height: this.height,
				paddingH: 0,
				paddingV: 0,
                color: this.color,
                colorDown: this.colorDown,
                colorMouseOver: this.colorMouseOver,
                theme: this.theme,
                hint: false
            };

            // Copy tool options
            for ( var p in tool )
                toolProperties[ p ] = tool[ p ];

            // Create the tool
            var newTool = new Tree.UI.Tool( this.tree, this.name + '-subtool#' + count++, toolProperties );
            maxWidth = Math.max( maxWidth, newTool.widthPadded );
            this.toolBox.addItem( newTool );

            // Next!
            y += newTool.heightPadded;
        }
        this.toolBox.resize( maxWidth, y );
        this.toolBox.endInsertItems();

        // Message only for the tool and its children
        this.tree.setModal( this, true );
        this.registerEvents( 'refresh' );
    }

	if ( !this.handleInterval )
	{
		var self = this;
		this.handleInterval = setInterval( function()
		{
			if ( self.toolBox )
			{
				var inside = self.mouse.inside || self.toolBox.mouse.inside;
				if ( !inside )
				{
					self.tree.setModal( self, false );
					self.toolBox.destroy();             // Will destroy all children
					self.toolBox = false;
					self.cancelEvent( 'refresh' );
				}
			}
			if ( !self.toolBox )
			{
				clearInterval( self.handleInterval );
				self.handleInterval = null;
			}
		}, 500 );
	}
};

Tree.UI.RenderItems.Tool_HTML = function ( tree, name, options )
{
    this.rendererName = 'Renderer_HTML';
    this.rendererType = 'Canvas';
    this.renderInParent = true;
    Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.Tool_HTML', options );
    this.setImage();

    // Calculates size
    var o = this.renderer.measureText( this.item.text, this.item.font );
    var width = this.item.height + this.item.spaceAfterImage + ( this.item.displayText ? o.width : 0 );
    var height = Math.max( this.item.height, o.height );
    this.setDefaultSize( width, height );
};
Tree.UI.RenderItems.Tool_HTML.render = function ( options )
{
    // Draw background box
    var alpha = this.item.alpha;
    var color = this.item.color;
    if ( this.item.mouseOver )
    {
        color = this.item.colorMouseOver;
        alpha = this.item.alphaMouseOver;
    }
    if ( this.item.down )
    {
        color = this.item.colorDown;
        alpha = this.item.alphaDown;
    }
    if ( color == '' && this.item.parent )
        color = this.item.parent.color;
    if ( color && color != '' )
	    this.thisRect.drawBox( options, color );

    // Draw image
    var rect = new Tree.Utilities.Rect( this.thisRect );
    options.context.setAlpha( options, alpha );
    options.context.drawImage( options, this.image, rect.x, rect.y, options.height, options.height );

    // Draw arrow if children and mouseover
    if ( this.item.children.length > 0 )
    {
        var arrow = this.resources.getImage( 'toolArrow' );
        if ( arrow )
            options.context.drawImage( options, arrow, rect.x + options.height - 8, rect.y + this.height - 8, 8, 8 );
    }

    // Draw text
    if ( this.item.displayText )
    {
        rect.x += options.height + this.item.spaceAfterImage;
        rect.drawText( options, this.item.text, this.item.font, this.item.colorFont, 'left', 'middle' );
    }
	return options;
};
Tree.UI.RenderItems.Tool_HTML.setImage = function()
{
	if ( this.nameImage != this.item.nameImage )
	{
		var image = this.resources.getImage( this.item.nameImage );
		if ( image )
		{
            this.nameImage = this.item.nameImage;
            this.image = image;
			this.hotSpotX = image.hotSpotX;
			this.hotSpotY = image.hotSpotY;
			return true;
		}
		else
		{
		    Tree.log( this, { level: Tree.ERRORLEVEL_HIGH, error: 'non existant image: ' + this.image + '.' })
		}
	}
    return false;
};
