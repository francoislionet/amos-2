/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 21/05/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

//
// MenuBar
///////////////////////////////////////////////////////////////////////////////
Tree.UI.MenuBar = function ( tree, name, options )
{
    this.list = [];
    this.color = '#FFFFFF';
    this.font = '12px sans serif';
    this.type = 'bar';
    this.openOnMouseOver = true;
    this.theme = false;
    this.tools = false;
    this.caller = false;
    this.onOptionSelected = false;
	this.paddingItemH = 8;
	this.paddingItemV = 2;
	this.renderItemName = 'Tree.UI.RenderItems.MenuBar';
    Tree.Items.init( this, tree, name, 'Tree.UI.MenuBar', options );

    this.setContent( this.list );
};
Tree.UI.MenuBar.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height' ] );
};
Tree.UI.MenuBar.messageDown = function ( message )
{
	return this.endProcess( message );
};
Tree.UI.MenuBar.setContent = function ( list )
{
    this.startInsertItems();

    // Create the sub tools
    var count = 0;
    var x = 0;
    var y = 0;
    var maxWidth = 0;
    for ( var l = 0; l < this.list.length; l++ )
    {
        var item = this.list[ l ];
        var itemProperties =
        {
            root: this.root,
            parent: this,
            openOnMouseOver: this.openOnMouseOver,
            displayArrows: false,
            theme: this.theme,
            font: this.font,
            x: x,
            y: y,
            widthCalc: 'native',
			height: this.height,
			paddingH: this.paddingItemH,
			paddingV: this.paddingItemV,
			colorPadding: this.colorPadding,
            level: 1,
            rootMenu: this
        };

        // Copy item options
        for ( var p in item )
            itemProperties[ p ] = item[ p ];

        // Create the menu entry
        var newItem = new Tree.UI.MenuItem( this.tree, this.name + '-menu#' + count++, itemProperties );
        maxWidth = Math.max( maxWidth, newItem.widthPadded );
        this.addItem( newItem );

		x += newItem.widthPadded;
    }
    this.endInsertItems();
};
Tree.UI.MenuBar.clickOnItem = function ( item )
{
    // If this function is called, we are root! Call main item with the name of the option
    if ( this.caller && this.onOptionSelected )
        this.onOptionSelected.apply( this.caller, [ item.name, item ] );
};
Tree.UI.RenderItems.MenuBar_HTML = function ( tree, name, options )
{
	this.color = false;

    this.rendererName = 'Renderer_HTML';
    this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.MenuBar_HTML', options );
};
Tree.UI.RenderItems.MenuBar_HTML.render = function ( options )
{
    // Draw background box
    this.thisRect.drawBox( options, this.item.color );

    // Continue drawing in this item
    options.renderInParent = options.rendererItem;

    return options;
};

//
// MenuBar
///////////////////////////////////////////////////////////////////////////////
Tree.UI.MenuPopup = function ( tree, name, options )
{
    this.list = [];
    this.color = '#FFFFFF';
    this.font = '14px sans serif';
    this.sizeBorder = 0;
    this.colorBorder = '#FFFFFF';
    this.openOnMouseOver = true;
    this.separatorH = 12;
    this.theme = false;
    this.level = 0;
    this.type = 'popup';
    this.theme = false;
    this.fromInside = false;
    this.rootMenu = false;
    this.caller = false;
    this.onOptionSelected = false;
	this.paddingItemH = 8;
	this.paddingItemV = 2;
	this.renderItemName = 'Tree.UI.RenderItems.MenuPopup';
    Tree.Items.init( this, tree, name, 'Tree.UI.MenuPopup', options );

    this.setContent( this.list );
    this.registerEvents( 'mouse' );
};
Tree.UI.MenuPopup.messageUp = function ( message )
{
    var changed;
    if ( message.command == 'mouseenter' )
        this.setModal( true );
    else if ( message.command == 'mouseleave' )
    {
        this.setModal( false );
        if ( !this.fromInside )
        {
            this.destroy();
        }
    }
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height' ] );
};
Tree.UI.MenuPopup.messageDown = function ( message )
{
	return this.endProcess( message );
};
Tree.UI.MenuPopup.setContent = function ( list )
{
    this.startInsertItems();

    // Create the sub tools
    var x = 0;
    var y = 0;
    var maxWidth = 0;
    for ( var l = 0; l < this.list.length; l++ )
    {
        var item = this.list[ l ];
        var itemProperties =
        {
            root: this.root,
            parent: this,
            openOnMouseOver: this.openOnMouseOver,
            displayArrows: this.displayArrows,
            theme: this.theme,
            font: this.font,
            x: x,
            y: y,
            widthCalc: 'native',
			heightCalc: 'native',
			paddingH: this.paddingItemH,
			paddingV: this.paddingItemV,
			colorPadding: this.colorPadding,
            level: this.level + 1,
            renderInParent: true,
            rootMenu: this.fromInside ? this.rootMenu : this
        };

        // Copy item options
        for ( var p in item )
            itemProperties[ p ] = item[ p ];

        // Create the menu entry
        var newItem = new Tree.UI.MenuItem( this.tree, '', itemProperties );         // Name in options
        maxWidth = Math.max( maxWidth, newItem.widthPadded + this.paddingItemH * 2 );
        this.addItem( newItem );

        // Next!
        y += newItem.heightPadded;
    }
    this.endInsertItems();

    // Resize all menuitems to the largest width
    for ( var i = 0; i < this.items.length; i++ )
    {
        var item = this.items[ i ];
        item.resize( maxWidth );
    }

    // Resize itself to match the width
    this.resize( maxWidth + this.paddingItemH * 2 + 1, y + 20 );			// TODO: Find why!
};
Tree.UI.MenuPopup.clickOnItem = function ( item )
{
    // If this function is called, we are root! Call main item with the name of the option
    if ( this.caller && this.onOptionSelected )
        this.onOptionSelected.apply( this.caller, [ item.name, item ] );
};

Tree.UI.RenderItems.MenuPopup_HTML = function ( tree, name, options )
{
	this.color = false;
	this.sizeBorder = false;
	this.colorBorder = false;

    this.rendererName = 'Renderer_HTML';
    this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.MenuPopup_HTML', options );
};
Tree.UI.RenderItems.MenuPopup_HTML.render = function ( options )
{
    // Draw background box
    this.thisRect.drawBox( options, this.item.color, this.item.sizeBorder, this.item.colorBorder );

    // Continue drawing in this item
    options.renderInParent = options.rendererItem;

    return options;
};

//
// Menu Item
///////////////////////////////////////////////////////////////////////////////
Tree.UI.MenuItem = function( tree, name, options )
{
    this.rootMenu = this;
    this.text = 'Text';
    this.shortcut = '';
    this.caller = false;
    this.onClick = false;
	this.colorText = '#000000';
    this.colorTextMouseOver = '#000000';
    this.colorTextDown = '#FFFFFF';
    this.colorTextInactive = '#808080';
    this.colorBack = '#FFFFFF';
    this.colorBackMouseOver = '#C0C0C0';
    this.colorBackDown = '#404040';
    this.colorBackInactive = '#FFFFFF';
    this.colorHint = '#000000';
    this.colorHintBack = '#AAAA00';
    this.sizeHintBorder = 1;
    this.colorHintBorder = '#000000';
    this.alphaShortcut = 0.5;
    this.spaceAfterImage = 8;
    this.font = '12px Sans Serif';
    this.nameImage = '';
    this.children = false;
    this.renderInParent = true;
    this.displayArrows = true;
    this.activated = true;
    this.openOnMouseOver = true;
    this.level = 0;
    this.theme = false;
    this.hint = true;
    this.textHint = false;
    this.rootMenu = false;
    this.spaceBeforeShortcut = 64;
	this.spaceBeforeArrow = 32;
    this.heightSeparator = 12;
	this.renderItemName = 'Tree.UI.RenderItems.MenuItem';
    Tree.Items.init( this, tree, name, 'Tree.UI.MenuItem', options );
    this.mouseOver = false;
    this.down = false;
    if ( this.name != 'separator' )
    {
        this.parentCaller = this.caller;
        this.parentOnClick = this.onClick;
        this.onClick = this.clickOnItem;
        this.caller = this;
        this.addProcess( new Tree.UI.GestureButton( this.tree, this, options ) );
    }
};
Tree.UI.MenuItem.messageUp = function( message )
{
    if ( message.command == 'destroy' && message.itemEvent == this )
    {
        if ( this.popup )
        {
            this.popup = false;
            clearInterval( this.popupHandle );
        }
    }
    return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'down', 'mouseOver', 'caller', 'onClick' ] );
};
Tree.UI.MenuItem.messageDown = function( message )
{
    this.endProcess( message );

    if ( message.mouseOver == Tree.UPDATED )
    {
        var changed;
        if ( this.mouseOver && this.children && !this.popup && this.openOnMouseOver )
        {
            this.clickOnItem();
            changed = true;
        }
        if ( changed )
        {
            changed =
            {
                command: 'selectionChanged',
                type: 'toParent',
                sender: this
            }
            this.tree.sendMessageToItem( this.tree, this.parent, changed, true );       // Recursive: will do all sibblings
        }
    }
    if ( message.command == 'selectionChanged' )
    {
        if ( this.popup && message.sender != this )
        {
            this.popup.destroy();
            this.popup = false;
            clearInterval( this.popupHandle );
        }
    }
};
Tree.UI.MenuItem.clickOnItem = function()
{
    // Childrens? Open a sub menu
    if ( this.children && !this.popup )
    {
        var x = 0;
        var y = this.height - 1;
        if ( this.parent.type == 'popup' )
        {
            x = this.width;
            y = 0;
        }
        this.startInsertItems();
        this.popup = new Tree.UI.MenuPopup( this.tree, this.name + '-popup',
        {
            root: this.root,
            parent: this,
            x: x,
            y: y,
            z: this.z + 1,
            width: 0,
            height: 0,
            theme: this.theme,
            level: this.level + 1,
            list: this.children,
            fromInside: true,
            rootMenu: this.rootMenu,
			paddingH: 1,
			paddingV: 12
        } );
        this.addItem( this.popup );
        this.endInsertItems();
        var self = this;
        this.popupHandle = setInterval( function()
        {
            if ( !self.mouse.inside && !self.popup.mouse.inside )
            {
                self.popup.destroy();
                clearInterval( self.popupHandle );
                self.popup = false;
            }
        }, 1000 );
    }
    else
    {
        // Simple menu option: close all popups above
        var quit = false;
        var parent = this;
        while( parent != this.rootMenu )
        {
            parent = parent.parent;

            // Destroy popups and not items
            if ( parent.className == 'Tree.UI.MenuPopup' )
                parent.destroy();
        }

        // No click on a menu bar option
        if ( this.parent.className != 'Tree.UI.MenuBar' )
        {
            // If the caller was defined in the option itself, direct call
            if ( this.parentCaller && this.parentOnClick )
            {
                this.parentCaller.apply( this.parentOnClick, [ this ] );
            }
            else
            {
                // If not, call the root of the menu
                this.rootMenu.clickOnItem( this );
            }
        }
    }
};



Tree.UI.RenderItems.MenuItem_HTML = function( tree, name, options )
{
	this.rendererName = 'Renderer_HTML';
	this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.MenuItem_HTML', options );

    // Calculates size
    var width = 0;
    var height = 0;
    if ( this.item.name == 'separator' )
    {
        width += 32;
        height += this.item.heightSeparator;
    }
    else
    {
        if ( this.item.nameImage )
        {
            width += this.height + this.item.spaceRightImage;
            height += this.height;
        }
        var o = this.renderer.measureText( this.item.text, this.item.font );
        width += o.width;
        height = Math.max( height, o.height );
        if ( !this.item.children && this.item.shortcut )
        {
            width += this.item.spaceBeforeShortcut;
            o = this.renderer.measureText( this.item.shortcut, this.item.font );
            width += o.width;
            height = Math.max( height, o.height );
        }
        else
		{
			if ( this.item.displayArrows )
			{
				width += this.item.spaceBeforeArrow;
				this.widthArrow = o.height * 0.5;
				this.heightArrow = o.height * 0.75;
				width += this. widthArrow;
			}
		}
    }
    this.setDefaultSize( width, height );
};
Tree.UI.RenderItems.MenuItem_HTML.render = function( options )
{
    // The background
    var color;
    if ( this.item.activated )
    {
        color = this.item.colorBack;
        if ( this.item.mouseOver )
            color = this.item.colorBackMouseOver;
        if ( this.item.down )          // I need to change this!
        {
            // No down aspect if it is a menubar entry
            if ( this.item.parent.className != 'Tree.UI.MenuBar' )
            {
                color = this.item.colorBackDown;
            }
        }
    }
    else
    {
        color = this.item.colorBackInactive;
    }
    this.thisRect.fillRectangle( options, color );

    // Color of text
    if ( this.item.activated )
    {
        color = this.item.colorText;
        if ( this.item.mouseOver )
            color = this.item.colorTextMouseOver;
        if ( this.item.down )
        {
            if ( this.item.parent.className != 'Tree.UI.MenuBar' )
            {
                color = this.item.colorTextDown;
            }
        }
    }
    else
    {
        color = this.item.colorTextInactive;
    }

    // A separator?
	var name = this.item.name;
    if ( this.item.name == 'separator' )
    {
        var x1 = this.thisRect.x;
        var x2 = x1 + this.thisRect.width;
        var y = this.thisRect.y + this.thisRect.height / 2;
        this.thisRect.drawLine( options, x1, y, x2, y, color, 0.5 );
    }
    else
    {
        // The icon?
        var x = 0;
        var yCenter = this.thisRect.y + this.thisRect.height / 2;
        if ( this.item.nameImage )
        {
            var image = this.resources.getImage( this.item.nameImage );
            if ( image )
            {
                options.rendererItem.drawImage( options, image, x, yCenter, options.height, options.height );
                x += options.height + this.spaceAfterImage;
            }
        }

        // The text
        options.rendererItem.drawText( options, x, yCenter, this.item.text, this.item.font, color, 'left', 'middle' );

        // A shortcut?
        if ( !this.item.children && this.shortcut )
        {
            options.rendererItem.setAlpha( options, this.item.alphaShortcut );
            options.rendererItem.drawText( options, this.thisRect.width, yCenter, this.item.shortcut, this.item.font, color, 'right', 'middle' );
            options.rendererItem.setAlpha( options, options.alpha );
        }
        else if ( this.item.children && this.item.displayArrows )
        {
            // An arrow?
            var rect = new Tree.Utilities.Rect( this.thisRect.width - this.widthArrow, yCenter - this.heightArrow / 2, this.widthArrow, this.heightArrow );
            rect.drawFilledTriangle( options, 'right', color );
        }
    }
	return options;
};
