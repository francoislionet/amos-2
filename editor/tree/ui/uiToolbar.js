/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 06/05/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.Toolbar = function ( tree, name, options )
{
    this.color = '#808080';
    this.colorBorder = '#FFFFFF';
    this.sizeBorder = 0;
    this.direction = 'horizontal';
    this.theme = false;
	this.widthTools = 32;
	this.heightTools = 32;
	this.openOnMouseOver = false;
	this.list = [];
	this.renderItemName = 'Tree.UI.RenderItems.Toolbar';
    Tree.Items.init( this, tree, name, 'Tree.UI.Toolbar', options );

	// Creates the tools
    this.startInsertItems();
    var x = 0;
    var y = 0;
    var count = 0;
    this.tools = [];
    for ( var t = 0; t < this.list.length; t++ )
    {
        var tool = this.list[ t ];
        var toolProperties = {};

        // New options
        toolProperties.root = this.root;
        toolProperties.parent = this;
        toolProperties.width = this.widthTools;
        toolProperties.height = this.heightTools;
		toolProperties.colorPadding = this.colorPadding;
		toolProperties.openOnMouseOver = this.openOnMouseOver;
        toolProperties.theme = this.theme;

        // Copy tool options
        for ( var p in tool )
            toolProperties[ p ] = tool[ p ];

        if ( this.direction = 'vertical' )
        {
            // New options
            toolProperties.x = this.paddingH;
            toolProperties.y = y;
            var tool = new Tree.UI.Tool( this.tree, this.name + '-tool#' + count++, toolProperties );
            this.addItem( tool );

            // Next!
            y += tool.heightPadded;
        }
        else
        {
            toolProperties.x = x;
            toolProperties.y = 0;
            var tool = new Tree.UI.Tool( this.tree, this.name + '-tool#' + count++, toolProperties );
            this.addItem( tool );

            // Next!
            x += tool.widthPadded;
        }
    }
	this.endInsertItems();
};
Tree.UI.Toolbar.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height' ] );
};
Tree.UI.Toolbar.messageDown = function ( message )
{
	return this.endProcess( message );
};

Tree.UI.RenderItems.Toolbar_HTML = function ( tree, name, options )
{
	this.color = false;
    this.colorBorder = false;
    this.sizeBorder = false;

    this.rendererName = 'Renderer_HTML';
    this.rendererType = 'Canvas';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.Toolbar_HTML', options );

	this.setDefaultSize( this.width, this.height );
};
Tree.UI.RenderItems.Toolbar_HTML.render = function ( options )
{
    // Draw background box
 	this.thisRect.drawBox( options, this.item.color, this.item.colorBorder, this.item.sizeBorder );

    // Continue drawing in this item
    options.renderInParent = options.rendererItem;

	return options;
};
