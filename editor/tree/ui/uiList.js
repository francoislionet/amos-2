/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

/**
 * List object
 */
Tree.UI.List = function ( tree, name, options )
{
	this.font = '14px sans serif';
	this.colorBack = '#FFFFFF';
	this.color = '#000000';

	this.multipleSelections = false;
	this.defaultSelected = -1;
	this.lines = [];
	this.options = [];
	this.caller = false;
	this.onClick = false;
	this.renderItemName = 'Tree.UI.RenderItems.List';
	Tree.Items.init( this, tree, name, 'Tree.UI.List', options );
	this.identifierCount = 1;
	this.addLines( this.lines );
};
Tree.UI.List.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'caller', 'onClick' ] );
};
Tree.UI.List.messageDown = function ( message )
{
	return this.endProcess( message, [ 'x', 'y', 'z', 'width', 'height' ] );
};
Tree.UI.List.addLines = function ( lines )
{
	for ( var l = 0; l < lines.length; l++ )
		this.addLine( lines[ l ] );
};
Tree.UI.List.addLine = function ( line, value )
{
	var option;
	if ( typeof value == 'undefined' )
		value = 0;
	var identifier = 'ID' + this.identifierCount++;
	if ( typeof line == 'string' )
		option = { text: line, identifier: identifier, value: value, down: false, mouseOver: false };
	else
	{
		option = line;
		option.identifier = identifier;
		option.down = false;
		option.mouseOver = false;
		option.value = value;
	}
	this.options.push( option );
	this.callAllRenderItems( 'addLine', [ option ] );
	this.doRefresh();
	return option.identifier;
};
Tree.UI.List.reset = function ( lines )
{
	this.options = [];
	this.callAllRenderItems( 'reset', [ lines ] );
	this.doRefresh();
};
Tree.UI.List.removeLine = function ( identifier )
{
	for ( var o = 0; o < this.options.length; o++ )
	{
		if ( this.options[ o ].identifier == identifier )
		{
			this.options.splice( o, 1 );
			this.callAllRenderItems( 'removeLine', [ o ] );
			this.doRefresh();
		}
	}
};
Tree.UI.List.removeLineFromText = function ( text )
{
	for ( var o = 0; o < this.options.length; o++ )
	{
		if ( this.options[ o ].text == text )
		{
			this.options.splice( o, 1 );
			this.callAllRenderItems( 'removeLine', [ o ] );
			this.doRefresh();
		}
	}
};
Tree.UI.List.removeLineFromValue = function ( value )
{
	for ( var o = 0; o < this.options.length; o++ )
	{
		if ( this.options[ o ].value == value )
		{
			this.options.splice( o, 1 );
			this.callAllRenderItems( 'removeLine', [ o ] );
			this.doRefresh();
		}
	}
};
Tree.UI.List.getItemFromIdentifier = function ( identifier )
{
	for ( var o = 0; o < this.options.length; o++ )
	{
		if ( this.options[ o ].identifier == identifier )
		{
			return this.options[ o ];
		}
	}
	return null;
};
Tree.UI.List.getNumberOfLines = function()
{
	return this.options.length;
};


Tree.UI.RenderItems.List_HTML = function ( tree, name, options )
{
	this.font = false;
	this.colorBack = false;
	this.color = false;
	this.options = false;

	this.rendererName = 'Renderer_HTML';
	this.rendererType = 'Element';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.List_HTML', options );
	this.prepareElement( options );
};
Tree.UI.RenderItems.List_HTML.prepareElement = function ( options )
{
	this.element = document.createElement( 'select' );
	this.element.style.position = 'absolute';
	this.element.style.visibility = 'hidden';
	this.element.style.fontSize = this.utilities.getFontSize( this.item.font ) + 'px';
	this.element.style.fontFamily = this.utilities.getFontFamily( this.item.font );
	this.element.style.backgroundColor = this.item.colorBack;

	var self = this;
	this.element.onclick = function()
	{
		if ( self.item.caller && self.item.onClick )
			self.item.onClick.apply( self.item.caller, [ self.item.options[ this.selectedIndex ] ] );
	};
	this.element.ondblclick = function()
	{
		if ( self.item.caller && self.item.onDoubleClick )
			self.item.onDoubleClick.apply( self.item.caller, [ self.item.options[ this.selectedIndex ] ] );
	};
};
Tree.UI.RenderItems.List_HTML.render = function ( options )
{
	this.element.size = Math.max( this.element.length, 3 );		// Keep the list open
	return options;
};
Tree.UI.RenderItems.List_HTML.reset = function (  )
{
	while( this.element.length )
		this.element.remove( 0 );
};
Tree.UI.RenderItems.List_HTML.addLine = function ( source )
{
	var option = new Option( source.text, source.value, false, false );
	option.style.color = this.item.color;
	option.style.fontSize = this.utilities.getFontSize( this.item.font ) + 'px';
	option.style.fontFamily = this.utilities.getFontFamily( this.item.font );
	this.element.add( option );
};
Tree.UI.RenderItems.List_HTML.removeLine = function ( number )
{
	this.element.remove( number );
};



Tree.UI.RenderItems.List_Canvas2D = function ( tree, name, options )
{
	this.font = false;
	this.colorBack = false;
	this.color = false;

	this.rendererName = 'Renderer_Canvas2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.List_Canvas2D', options );
	Tree.UI.RenderItems.List_HTML.prepareElement.apply( this, [ options ] );
	document.body.appendChild( this.element );
};
Tree.UI.RenderItems.List_Canvas2D.render = function ( options )
{
	this.element.size = Math.max( this.element.length, 3 );		// Keep the list open
	this.element.style.zIndex = options.z;
	this.element.style.left = options.xReal + 'px';
	this.element.style.top = options.yReal + 'px';
	this.element.style.width = this.width + 'px';
	this.element.style.height = this.height + 'px';
	this.element.style.opacity = options.alpha.toString();
	this.element.style.visibility = this.visible ? 'visible' : 'hidden';
	return options;
};
Tree.UI.RenderItems.List_Canvas2D.onDestroy = function ( options )
{
	document.body.removeChild( this.element );
};
Tree.UI.RenderItems.List_Canvas2D.reset = Tree.UI.RenderItems.List_HTML.reset;
Tree.UI.RenderItems.List_Canvas2D.addLine = Tree.UI.RenderItems.List_HTML.addLine;
Tree.UI.RenderItems.List_Canvas2D.removeLine = Tree.UI.RenderItems.List_HTML.removeLine;

/*

Tree.UI.RenderItems.List_Three2D = function ( tree, name, options )
{
	this.font = '12px Arial';
	this.colorBack = '#FFFFFF';
	this.brightColor = '#000000';
	this.darkColor = '#000000';
	this.color = '#000000';
	this.mouseOverColor = '#C0C0C0';
	this.downColor = '#808080';
    this.borderSize = 1;
	this.sliderWidth = 20;
	this.caller = false;
	this.onMouseOver = false;
	this.onClick = false;
	this.onDoubleClick = false;
	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_Three2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.List_Three2D', options );
	this.parent.addProcess( new Tree.UI.GestureList( this.tree, this.parent, options ) );
	this.lineHeight = this.renderer.getFontSize( this.font ) + 2;
	this.nLines = Math.floor( this.height / this.lineHeight );
	this.firstLine = 0;
	this.rects = [];

	this.startInsertItems();
	this.slider = this.addItem ( new Tree.UI.Slider( this.tree, 'slider',
	{
		root: this.root,
		parent: this,
		x: this.width - this.sliderWidth,
		y: 0,
		width: this.sliderWidth,
		height: this.height,
		position: this.position,
		range: this.nLines,
		size: this.items.length,
		caller: this,
		onChange: this.sliderChange
	} ) );
	this.endInsertItems();

};
Tree.UI.RenderItems.List_Three2D.render = function ( options )
{
	// Draw box
	this.thisRect.drawHilightedBox( options, this.colorBack, this.brightColor, this.darkColor, this.borderSize );
	var rect = new Tree.Utilities.Rect( this.rect );
	rect.shrink( this.borderSize, this.borderSize );
	rect.save( options );
	rect.clip( options );

	// Draw the lines
	var count = 0;
	this.rects = [];
	for ( var o = this.position; o < this.item.options.length && o < this.nLines; o++ )
	{
		var option = this.item.options[ o ];
		var color = this.colorBack;
		if ( option.mouseOver )
			color = this.mouseOverColor;
		if ( option.down )
			color = this.downColor;
		rect.drawBox( options, color );
		rect.drawText( options, this.item.options[ o ].text, this.font, textColor, 'left', 'center' );
		this.rects.push( rect );
		rect.y += this.lineHeight;
	}

	// Restores the context
	rect.restore( options );
	return options;
};

Tree.UI.RenderItems.List_Three2D.reset = function (  )
{
	this.position = 0;
	this.items.splice( this.firstLine, this.items.length );
};
Tree.UI.RenderItems.List_Three2D.addLine = function ( option )
{
	this.startInsertItems();
	var text = new Tree.UI.Text( this.tree, this.name + '|text',
	{
		root: this.root,
		parent: this,
		x: this.borderSize,
		y: this.borderSize + this.lineHeight * ( this.items.length - this.firstLine ),
		width: this.width - this.sliderWidth - 2,
		height: this.lineHeight,
		text: option.text,
		color: this.textColor,
		colorBack: this.colorBack,
        colorBackMouseOver: this.renderer.addColor( this.colorBack, '#181818', -1 ),
        colorBackDown: this.renderer.addColor( this.colorBack, '#303030', -1 ),
		font: this.font,
        multipleSelections: false,
		forceWidth: true,
		forceHeight: true,
		hAlign: 'left',
		vAlign: 'center',
		value: option.value,
		caller: this,
		onClick: this.click,
		onDoubleClick: this.doubleClick,
		optionIdentifier: option.identifier
	} );
	this.addItem( text );
	this.endInsertItems();
};
Tree.UI.RenderItems.List_Three2D.sliderChange = function ( position )
{
	this.setPosition( position );
};
Tree.UI.RenderItems.List_Three2D.removeLine = function ( number )
{
	this.items.splice( number + this.firstLine, 1 );
};
Tree.UI.RenderItems.List_Three2D.setPosition = function ( position )
{
	this.position = position;
	for ( var i = 0; i < this.items.length - 1; i ++ )
	{
		var y = - this.position * this.lineHeight + i * this.lineHeight;
		if ( y + this.lineHeight > 0 && y < this.height + this.lineHeight )
		{
			this.items[ i + 1 ].y = y;
			this.items[ i + 1 ].active = true;
			this.items[ i + 1 ].visible = true;
		}
		else
		{
			this.items[ i + 1 ].active = false;
			this.items[ i + 1 ].visible = false;
		}
	}
	this.doRefresh();
};
*/

/**
 * List
 *
 * A scrollable list of items - hand-drawn
 *
 */
/*
Tree.UI.D2List = function ( tree, name, options )
{
	this.tree = tree;

	this.font = '16px Arial';
	this.colorBack = '#FFFFFF';
	this.brightColor = '#000000';
	this.darkColor = '#000000';
	this.textColor = '#000000';
    this.borderSize = 1;
	this.mouseOverHilight = '#202020';
	this.clickHilight = '#404040';
	this.sliderWidth = 20;
	this.caller = false;
	this.onMouseOver = false;
	this.onClick = false;
	this.onDoubleClick = false;
	this.rendererType = 'Canvas';
	this.rendererClip = true;
	Tree.Items.init( this, tree, name, 'Tree.UI.D2List', options );

	this.lineHeight = this.renderer.getFontSize( this.font ) + 2;
	this.position = 0;
	this.nLines = Math.floor( this.height / this.lineHeight );
	this.startInsertItems();
	this.slider = this.addItem ( new Tree.UI.Slider( this.tree, 'slider',
	{
		root: this.root,
		parent: this,
		x: this.width - this.sliderWidth,
		y: 0,
		width: this.sliderWidth,
		height: this.height,
		position: this.position,
		range: this.nLines,
		size: this.items.length,
		caller: this,
		onChange: this.sliderChange
	} ) );
	this.endInsertItems();
};
Tree.UI.D2List.render = function ( options )
{
	// Draw box
	this.thisRect.drawHilightedBox( options, this.colorBack, this.brightColor, this.darkColor, this.borderSize );
	var rect = new Tree.Utilities.Rect( this.rect );
	rect.shrink( 3, 3 );
	rect.clip( options );
	return options;
};
Tree.UI.D2List.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z' ] );
};
Tree.UI.D2List.messageDown = function ( message )
{
	return this.endProcess( message, [ 'x', 'y', 'z' ] );
};
Tree.UI.D2List.reset = function (  )
{
	this.position = 0;
	this.dragPosition = 0;
	this.size = 0;
	this.items.splice( 1, this.items.length - 1 );
	this.doRefresh();
};
Tree.UI.D2List.addLine = function ( text, data )
{
	this.startInsertItems();
	var text = new Tree.UI.Text( this.tree, this.name + '|text',
	{
		root: this.root,
		parent: this,
		x: this.borderSize,
		y: this.borderSize + this.lineHeight * ( this.items.length - 1 ),
		width: this.width - this.sliderWidth - 2,
		height: this.lineHeight,
		text: text,
		color: this.textColor,
		colorBack: this.colorBack,
        colorBackMouseOver: this.renderer.addColor( this.colorBack, '#181818', -1 ),
        colorBackDown: this.renderer.addColor( this.colorBack, '#303030', -1 ),
		font: this.font,
        multipleSelections: false,
		forceSx: true,
		forceSy: true,
		hAlign: 'left',
		vAlign: 'center',
		data: data,
		caller: this,
		onClick: this.click,
		onDoubleClick: this.doubleClick
	} );
	this.addItem( text );
	text.addProcess( new Tree.UI.GestureButton( this.tree, text, { } ) );
	this.endInsertItems();

	this.doRefresh();
	return text.identifier;
};
Tree.UI.D2List.click = function ( item )
{
    if ( !this.multipleSelections )
    {
        for ( var i = 1; i < this.items.length; i++ )
            this.items[ i ].activated = false;
    }
	item.activated = true;
	if ( this.caller && this.onClick )
		this.onClick.apply( this.caller, [ item ] );
};
Tree.UI.D2List.doubleClick = function ( item )
{
	item.activated = true;
	if ( this.caller && this.onDoubleClick )
		this.onDoubleClick.apply( this.caller, [ item ] );
};
Tree.UI.D2List.sliderChange = function ( position )
{
	this.setPosition( position );
};
Tree.UI.D2List.removeLine = function ( identifier )
{
	for ( var i = 1; i < this.items.length; i ++ )
	{
		if ( this.items[ i ].identifier == identifier )
		{
			this.items.splice( i, 1 );
			this.doRefresh();
			i --;
		}
	}
};
Tree.UI.D2List.removeLineFromText = function ( text )
{
	for ( var i = 1; i < this.items.length; i ++ )
	{
		if ( this.items[ i ].text == text )
		{
			this.items.splice( i, 1 );
			this.doRefresh();
			i --;
		}
	}
};
Tree.UI.D2List.removeLineFromData = function ( data )
{
	for ( var i = 1; i < this.items.length; i ++ )
	{
		if ( this.items[ i ].data == data )
		{
			this.items.splice( i, 1 );
			this.doRefresh();
			i --;
		}
	}
};
Tree.UI.D2List.getItemFromIdentifier = function ( identifier )
{
	for ( var i = 1; i < this.items.length; i ++ )
	{
		if ( this.items[ i ].identifier = identifier )
			return this.items[ i ];
	}
	return false;
};
Tree.UI.D2List.setPosition = function ( position )
{
	this.position = position;
	for ( var i = 0; i < this.items.length - 1; i ++ )
	{
		var y = - this.position * this.lineHeight + i * this.lineHeight;
		if ( y + this.lineHeight > 0 && y < this.height + this.lineHeight )
		{
			this.items[ i + 1 ].y = y;
			this.items[ i + 1 ].active = true;
			this.items[ i + 1 ].visible = true;
		}
		else
		{
			this.items[ i + 1 ].active = false;
			this.items[ i + 1 ].visible = false;
		}
	}
	this.doRefresh();
};
*/
