/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.CheckBox = function ( tree, name, options )
{
	this.text = 'CheckBox';
	this.font = '16px sans serif';
	this.color = '#000000';
	this.colorBack = '#FFFFFF';
	this.colorMouseOver = '#C0C0C0';
	this.colorDown = '#000000';
	this.colorBorder = '#000000';
	this.sizeBorder = 1;
	this.sizeCheckmark = 12;

	this.caller = false;
	this.onChange = false;
	this.renderItemName = 'Tree.UI.RenderItems.CheckBox';
	this.mouseOver = false;
	this.down = false;
	this.value = false;
	Tree.Items.init( this, tree, name, 'Tree.UI.CheckBox', options );
};
Tree.UI.CheckBox.messageUp = function ( message )
{
	this.changed = false;
	if ( message.command == 'setValue' )
	{
		this.value = message.value;
		this.changed = true;
	}
	return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'text', 'font', 'value', 'down', 'mouseOver' ] );
};
Tree.UI.CheckBox.messageDown = function ( message )
{
	this.endProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'text', 'font', 'value', 'down', 'mouseOver' ] );
	if ( this.changed || message.value == Tree.UPDATED )
	{
		if ( this.onChange && this.caller )
			this.onChange.apply( this.caller, [ this.value ] );
		this.doRefresh();
	}
};
Tree.UI.CheckBox.getValue = function ( message )
{
	return this.state;
};



Tree.UI.RenderItems.CheckBox_HTML = function ( tree, name, options )
{
	this.text = false;
	this.font = false;
	this.color = false;
	this.colorBack = false;
	this.colorMouseOver = false;
	this.colorDown = false;
	this.colorBorder = false;
	this.sizeBorder = false;
	this.sizeCheckmark = false;
	this.value = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_HTML';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.CheckBox_HTML', options );
	this.item.addProcess( new Tree.UI.GestureCheckBox( this.tree, this.item, { } ) );

	this.width = 200;
	this.height = 32;
	this.item.width = this.width;
	this.item.height = this.height;
};
Tree.UI.RenderItems.CheckBox_HTML.render = function ( options )
{
	// Clears the canvas
	this.thisRect.clear( options );

	// Checkbox
	var rect = new Tree.Utilities.Rect( 0, this.rect.height / 2 - this.item.sizeCheckmark / 2, this.item.sizeCheckmark, this.item.sizeCheckmark );
	var colorBack = this.item.colorBack;
	if ( this.item.mouseOver )
	{
		colorBack = this.item.colorMouseOver;
	}
	rect.drawBox( options, colorBack, this.item.colorBorder, this.item.sizeBorder );

	// Checkmark
	if ( this.item.value )
	{
		rect.drawDiagonal( options, this.item.colorDown, 1, Tree.DIAGONAL_TOPLEFT_BOTTOMRIGHT | Tree.DIAGONAL_TOPRIGHT_BOTTOMLEFT );
	}

	// Text
	rect = new Tree.Utilities.Rect( this.thisRect );
	rect.x += this.item.sizeCheckmark + 4;
	rect.drawText( options, this.item.text, this.item.font, this.item.color, 'left', 'middle' );

	// Allow rendering in parent
	if ( this.item.renderSubItems )
        options.renderInParent = options.rendererItem;

	return options;
};

Tree.UI.RenderItems.CheckBox_Canvas2D = function ( tree, name, options )
{
	this.text = false;
	this.font = false;
	this.color = false;
	this.colorBack = false;
	this.colorMouseOver = false;
	this.colorDown = false;
	this.colorBorder = false;
	this.sizeBorder = false;
	this.sizeCheckmark = false;
	this.value = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_Canvas2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.CheckBox_Canvas2D', options );
	this.item.addProcess( new Tree.UI.GestureCheckBox( this.tree, this.item, { } ) );
	this.render = Tree.UI.RenderItems.CheckBox_Three2D.render;

	this.width = 200;
	this.height = 32;
	this.item.width = this.width;
	this.item.height = this.height;
};
Tree.UI.RenderItems.CheckBox_Canvas2D.render = Tree.UI.RenderItems.CheckBox_HTML.render;
