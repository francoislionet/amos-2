/*©agpl*************************************************************************
*                                                                              *
* This file is part of FRIEND UNIFYING PLATFORM.                               *
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.init = function( options, callback, extra )
{
	var scriptList =
	[
        "tree/ui/uiArrow.js",
        "tree/ui/uiButton.js",
        "tree/ui/uiCheckBox.js",
        "tree/ui/uiColorBox.js",
        "tree/ui/uiComboBox.js",
        "tree/ui/uiDialog.js",
        "tree/ui/uiList.js",
        "tree/ui/uiMessageBox.js",
        "tree/ui/uiProcesses.js",
        "tree/ui/uiProgressBar.js",
        "tree/ui/uiRadioButton.js",
        "tree/ui/uiSlider.js",
        "tree/ui/uiText.js",
        "tree/ui/uiTextBox.js",
        "tree/ui/uiTreeBox.js",
        "tree/ui/uiGroup.js",
        "tree/ui/uiEdit.js",
        "tree/ui/uiTabs.js",
        "tree/ui/uiTool.js",
        "tree/ui/uiToolbar.js",
        "tree/ui/uiResizeBar.js",
        "tree/ui/uiMenu.js",
        "tree/ui/uiHint.js",
    ];
	Tree.include( scriptList, {}, function( response, data, extra )
	{
		callback( response, data, extra );
	}, extra );
};

