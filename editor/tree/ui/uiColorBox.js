/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.ColorBox = function( tree, name, options )
{
    this.color = '#808080';
    this.sizeBorder = 0;
    this.colorBorder = '#FFFFFF';
    this.renderSubItems = false;
	this.renderItemName = 'Tree.UI.RenderItems.ColorBox';
    Tree.Items.init( this, tree, name, 'Tree.UI.ColorBox', options );
};
Tree.UI.ColorBox.messageUp = function( message )
{
    return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'rotation', 'color' ] );
};
Tree.UI.ColorBox.messageDown = function( message )
{
    return this.endProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'rotation', 'color' ] );
};


Tree.UI.RenderItems.ColorBox_HTML = function( tree, name, options )
{
	this.color = false;
	this.sizeBorder = false;
	this.colorBorder = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_HTML';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.ColorBox_HTML', options );

	this.setDefaultSize( 32, 32 );
};
Tree.UI.RenderItems.ColorBox_HTML.render = function( options )
{
	this.thisRect.drawBox( options, this.item.color, this.item.colorBorder, this.item.sizeBorder );

	// Allow rendering in parent
	if ( this.item.renderSubItems )
        options.renderInParent = options.rendererItem;

	return options;
};

Tree.UI.RenderItems.ColorBox_Canvas2D = function( tree, name, options )
{
	this.color = false;
    this.sizeBorder = false;
	this.colorBorder = false;

    this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_Canvas2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.ColorBox_Canvas2D', options );
	this.render = Tree.UI.RenderItems.ColorBox_Three2D.render;

	this.setDefaultSize( 32, 32 );
};
Tree.UI.RenderItems.ColorBox_Canvas2D.render = Tree.UI.RenderItems.ColorBox_HTML.render;
