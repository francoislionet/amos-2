/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.ProgressBar = function ( tree, name, options )
{
	this.color = '#FFFF00';
	this.colorBack = '#FF0000';
	this.colorBorder = '#000000';
	this.sizeBorder = 1;
	this.position = 50;
	this.size = 100;
	this.direction = Tree.DIRECTION_RIGHT;

	this.renderItemName = 'Tree.UI.RenderItems.ProgressBar';
	Tree.Items.init( this, tree, name, 'Tree.UI.ProgressBar', options );
};
Tree.UI.ProgressBar.messageUp = function ( message )
{
	return this.startProcess( message, [ 'x', 'y', 'z', 'color', 'colorBack', 'colorBorder', 'sizeBorder', 'rotation', 'image' ] );
};
Tree.UI.ProgressBar.messageDown = function ( message )
{
	return this.endProcess( message, [ 'x', 'y', 'z', 'rotation', 'color', 'colorBack', 'colorBorder', 'sizeBorder' ] );
};
Tree.UI.ProgressBar.setPosition = function ( position )
{
	if ( position != this.position )
	{
		if ( position < 0 )
			position = 0;
		if ( position > this.size )
			position = this.size;
		this.position = position;
		this.doRefresh();
	}
};
Tree.UI.ProgressBar.getPosition = function ()
{
	return this.position;
};
Tree.UI.ProgressBar.setSize = function ( size )
{
	this.size = size;
	this.doRefresh();
}
Tree.UI.ProgressBar.getSize = function ()
{
	return this.size;
}


Tree.UI.RenderItems.ProgressBar_HTML = function ( tree, name, options )
{
	this.color = false;
	this.colorBack = false;
	this.colorBorder = false;
	this.sizeBorder = false;
	this.position = false;
	this.size = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_HTML';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.ProgressBar_HTML', options );

	this.width = 320;
	this.height = 200;
	this.item.width = this.width;
	this.item.height = this.height;
};
Tree.UI.RenderItems.ProgressBar_HTML.render = function ( options )
{
	var rect = new Tree.Utilities.Rect( this.thisRect );
	if ( this.item.sizeBorder && typeof this.item.colorBorder != 'undefined' )
	{
		rect.drawRectangle( options, this.item.colorBorder, this.item.sizeBorder );
		rect.shrink( - this.item.sizeBorder, - this.item.sizeBorder )
	}
	if ( typeof this.item.colorBack != 'undefined' )
		rect.drawBox( options, this.item.colorBack );

	switch ( this.item.direction )
	{
		case Tree.DIRECTION_RIGHT:
			rect.width = this.item.position / this.item.size * rect.width;
			break;
		case Tree.DIRECTION_LEFT:
			rect.x = rect.x + rect.width - ( this.item.position / this.item.size * rect.width );
			rect.width = this.item.position / this.item.size * rect.width;
			break;
		case Tree.DIRECTION_DOWN:
			rect.height = this.item.position / this.item.size * rect.height;
			break;
		case Tree.DIRECTION_UP:
			rect.y = rect.y + rect.height - ( this.item.position / this.item.size * rect.height );
			rect.height = this.item.position / this.item.size * rect.height;
			break;
	}
	rect.drawBox( options, this.item.color );

	return options;
};

Tree.UI.RenderItems.ProgressBar_Canvas2D = function ( tree, name, options )
{
	this.color = false;
	this.colorBack = false;
	this.colorBorder = false;
	this.sizeBorder = false;
	this.position = false;
	this.size = false;

	this.rendererType = 'Canvas';
	this.rendererName = 'Renderer_Canvas2D';
	Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.ProgressBar_Canvas2D', options );
};
Tree.UI.RenderItems.ProgressBar_Canvas2D.render = Tree.UI.RenderItems.ProgressBar_HTML.render;
