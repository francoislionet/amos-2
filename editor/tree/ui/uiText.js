/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine interface elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.UI = Tree.UI || {};
Tree.UI.RenderItems = Tree.UI.RenderItems || {};

Tree.UI.Text = function( tree, name, options )
{
	this.hAlign = 'center';
    this.vAlign = 'middle';
	this.color = '#000000';
    this.colorMouseOver = 'none';
    this.colorDown = 'none';
    this.colorBack = 'none';
    this.colorBackMouseOver = 'none';
    this.colorBackDown = 'none';
    this.font = '12px sans serif';
    this.text = 'My text';

	this.text = 'Text';
    this.forceWidth = false;
    this.forceHeight = false;
    this.value = false;
    this.caller = false;
    this.onClick = false;
	this.onDoubleClick = false;

	this.renderItemName = 'Tree.UI.RenderItems.Text';
    Tree.Items.init( this, tree, name, 'Tree.UI.Text', options );
    this.mouseOver = false;
    this.down = false;
};
Tree.UI.Text.messageUp = function( message )
{
    return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'rotation', 'text', 'font', 'down', 'mouseOver', 'caller', 'onClick', 'onDoubleClick'] );
};
Tree.UI.Text.messageDown = function( message )
{
    this.endProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'rotation', 'text', 'mouseOver', 'down', 'font' ] );
};
Tree.UI.Text.setValue = function( text )
{
	this.updateProperty( 'text', text );
    this.doRefresh();
};
Tree.UI.Text.getValue = function()
{
    return this.text;
};




Tree.UI.RenderItems.Text_HTML = function( tree, name, options )
{
    this.hAlign = false;
    this.vAlign = false;
	this.color = false;
    this.colorMouseOver = false;
    this.colorDown = false;
    this.colorBack = false;
    this.colorBackMouseOver = false;
    this.colorBackDown = false;
    this.font = false;
    this.text = false;

	this.rendererName = 'Renderer_HTML';
	this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.Text_HTML', options );

	// Add default Gesture process
    if ( this.item.caller && ( this.item.onClick || this.item.onDoubleClick ) )
    	this.item.addProcess( new Tree.UI.GestureButton( this.tree, this.item, options ) );
    this.setFont();
};
Tree.UI.RenderItems.Text_HTML.render = function( options )
{
    if ( this.item.colorBack != 'none' )
    {
        var colorBack = this.item.colorBack;
        if ( this.item.mouseOver && this.item.colorBackMouseOver != 'none' )
            colorBack = this.item.colorBackMouseOver;
        if ( ( this.item.down || this.item.activated ) && this.item.colorBackDown != 'none' )
            colorBack = this.item.backColorDown;
        this.thisRect.fillRectangle( options, colorBack );
	}

    var textColor = this.item.color;
    if ( this.item.mouseOver && this.item.colorMouseOver != 'none' )
        textColor = this.item.colorMouseOver;
    if ( ( this.item.down || this.item.activated ) && this.item.colorDown != 'none' )
        textColor = this.item.colorDown;
    this.thisRect.drawText( options, this.item.text, this.item.font, textColor, this.item.hAlign, this.item.vAlign );
	return options;
};


Tree.UI.RenderItems.Text_Canvas2D = function( tree, name, options )
{
    this.hAlign = false;
    this.vAlign = false;
	this.color = false;
    this.colorMouseOver = false;
    this.colorDown = false;
    this.colorBack = false;
    this.colorBackMouseOver = false;
    this.colorBackDown = false;
    this.font = false;
    this.text = false;

	this.rendererName = 'Renderer_Canvas2D';
	this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.UI.RenderItems.Text_Canvas2D', options );

	// Add default Gesture process
    if ( this.item.caller && this.item.onClick )
	    this.item.addProcess( new Tree.UI.GestureButton( this.tree, this.item, options ) );
};
Tree.UI.RenderItems.Text_Canvas2D.render = Tree.UI.RenderItems.Text_HTML.render;
