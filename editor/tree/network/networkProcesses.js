/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine network elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.Network = Tree.Network || {};
Tree.Network.RenderItems = Tree.Network.RenderItems || {};

/**
* Process: TreeShareEmitter
*
* Append this process to an object and it will be transmitted through the network
*
* @param tree (object) The Tree engine
* @param name (string) The name of the object
* @param options (object) Creation options
*
* Options
*/
Tree.TreeShareEmitter = function( tree, object, options )
{
    this.treeShare = 0;
    this.created = 0;
   Tree.Processes.init( this, tree, object, 'Tree.TreeShareEmitter', options )
}
Tree.TreeShareEmitter.processUp = function ( message )
{
    if ( message.itemEvent == this.object )
    {
        switch ( message.command )
        {
            case 'create':
                var response =
                {
                    userName: Application.username,
                    creationFlags: this.utilities.replaceObjectsByNames( this.object.root, options.creationFlags ), // Transmits the whole object creation options
                    name: message.name,
                    identifier: this.object.identifier,
                    parentName: this.object.parent.name,
                    className: this.object.className
                };
                this.treeShare.send( 'create', response );
                break;
            case 'destroy':
                var response =
                {
                    userName: Application.username,
                    identifier: this.object.identifier
                };
                this.network.send( 'destroy', response );
                break;
        }
    }
   return true;
};
Tree.TreeShareEmitter.processDown = function ( message )
{
   var flag = false;
   if ( message.refresh )
   {
       // Copy only the modified options
       var toSend = {};
       for ( var p in message )
       {
           if ( options[ p ] != Tree.NOTINITIALIZED )
               toSend[ p ] = options[ p ];
       }
       var response =
       {
           identifier: this.object.identifier,
           options: this.network.replaceObjectsByNames( this.object.root, toSend )
       };
       var time = new Date().getTime();
       this.network.send( 'update', response );
   }
   return true;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/**
* Process: TreeShareReceiver
*
* Append this process to an object and it will be controller by the emitter object from the other player's machine
*
* @param tree (object) The Tree engine
* @param name (string) The name of the object
* @param options (object) Creation options
*
* Options
*/
Tree.TreeShareReceiver = function( tree, object, options )
{
   this.network = 0;
   Tree.Processes.init( this, tree, object, 'Tree.TreeShareReceiver', options )
}
Tree.TreeShareReceiver.processUp = function ( message )
{
    return true;
};
Tree.TreeShareReceiver.processDown = function ( message )
{
    return true;
};
