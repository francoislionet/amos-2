/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine network elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.Network = Tree.Network || {};
Tree.Network.RenderItems = Tree.Network.RenderItems || {};

Tree.Network.init = function( options, callback, extra )
{
	var scriptList =
	[
        "tree/network/networkChooseTree.js",
        "tree/network/networkManager.js",
        "tree/network/networkProcesses.js",
        "tree/network/networkDormant.js",
    ];
	Tree.include( scriptList, {}, function( response, data, extra )
	{
		callback( response, data, extra );
	}, extra );
};
