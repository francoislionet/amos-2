/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine HTML Renderer
 * Renders all items in the document
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 02/03/2018
 */
Tree.Renderer = Tree.Renderer || {};

Tree.Renderer.Renderer_HTML = function( localProperties, globalProperties )
{
	this.tree = globalProperties.tree;
	this.name = 'Renderer_HTML';
	this.className = 'Tree.Renderer.Renderer_HTML';
	this.utilities = globalProperties.utilities;
	this.resources = globalProperties.resources;
	this.utilities.setOptions( this, globalProperties );

	// Direct copy of options, they are all set!
	Object.assign( this, localProperties );

	// Makes sure all functions are defined (?)
	Object.assign( this, Tree.Renderer.Renderer_HTML );

	// Renderer variables
    this.pile = [];
    this.items = {};
    this.images = {};
	this.renderFlags = {};
	this.canvasses = {};
	this.pile = [];
    if ( this.width == Tree.NOTDEFINED )
        this.width = this.tree.width;
    if ( this.height == Tree.NOTDEFINED )
        this.height = this.tree.height;
	this.refresh = true;
};

// Default options. Will be send on renderer creation
Tree.Renderer.Renderer_HTML.defaultProperties =
{
	x: 0,
	y: 0,
	z: 0,
	width: Tree.NOTDEFINED,
	height: Tree.NOTDEFINED,
	defaultFont: '12px Verdana',
	antialias: true,
	resize:
	{
		mode: 'keepProportions',
		borderColor: 0xFF0000
	}
};

Tree.Renderer.Renderer_HTML.resize = function( width, height )
{
	this.width = width;
	this.height = height;
};

Tree.Renderer.Renderer_HTML.changeExposed = function( info )
{
	switch ( info.id )
	{
        default:
            break;
	}
};
Tree.Renderer.Renderer_HTML.getScreenCoordinates = function( renderItem )
{
    return rendererItem.getVector();
}
Tree.Renderer.Renderer_HTML.startRenderTo = function( name, options )
{
};
Tree.Renderer.Renderer_HTML.stopRenderTo = function( id )
{
};
Tree.Renderer.Renderer_HTML.add = function( renderItem, rendererItem )
{
	this.items[ renderItem.identifier ] = rendererItem;
};
Tree.Renderer.Renderer_HTML.clear = function()
{
	this.canvasses = {};
    this.renderFlags = {};
	this.images = {};
	this.pile = [];
	for ( var i in this.items )
		this.items[ i ].destroy();
	this.items = {};
};
Tree.Renderer.Renderer_HTML.refreshItem = function( renderItem )
{
	if ( this.items[ renderItem.identifier ] )
		this.items[ renderItem.identifier ].doRefresh();
};
Tree.Renderer.Renderer_HTML.startDestroy = function()
{
};
Tree.Renderer.Renderer_HTML.destroy = function( renderItem )
{
	if ( this.items[ renderItem.identifier ] )
	{
        this.deleteItemImages( renderItem );
		this.items[ renderItem.identifier ].destroy();
		if ( renderItem.canvasPadding )
		{
			this.destroyPadding( renderItem );
		}
		this.items[ renderItem.identifier ] = false;
	}
};
Tree.Renderer.Renderer_HTML.endDestroy = function()
{
	this.items = this.utilities.cleanArray( this.items );
};
Tree.Renderer.Renderer_HTML.setImage = function( srcImage, options, callback, extra )
{
	callback( true, srcImage, extra );
};
Tree.Renderer.Renderer_HTML.getCanvasFromImage = function( name, renderItem, qualifier )
{
	// Get the id of image
	var id = renderItem.identifier;
	if ( qualifier )
		id = name + qualifier;

    // Already defined?
    if ( !this.images[ id ] )
        this.images[ id ] = {};
    if ( this.images[ id ][ name ] )
        return this.images[ id ][ name ];

    // Creates a local copy of the image
    var image = this.resources.getImage( name );
    if ( image )
    {
        var canvas = document.createElement( 'canvas' );
        canvas.width = image.width;
        canvas.height = image.height;
        var context = canvas.getContext( '2d' );
        context.drawImage( image, 0, 0 );
        this.images[ renderItem.identifier ][ name ] = canvas;
        return canvas;
    }
    return null;
};
Tree.Renderer.Renderer_HTML.deleteItemImages = function( renderItem )
{
    if ( this.images[ renderItem.identifier ] )
    {
        this.images[ renderItem.identifier ] = false;
        this.images = this.utilities.cleanArray( this.images );
    }
};
Tree.Renderer.Renderer_HTML.getCanvas = function( id )
{
	var canvas = this.canvasses[ id ];
	if ( canvas )
		return canvas;
	return false;
};
Tree.Renderer.Renderer_HTML.createCanvas = function( width, height, name, renderItem, rendererItem, force )
{
	var id = renderItem.identifier + '<>' + name;
	var canvas = this.canvasses[ id ];
	if ( !canvas || force  )
	{
		canvas = document.createElement( 'canvas' );
		canvas.treeName = name;
		canvas.treeRendererId = id;
		canvas.width = width;
		canvas.height = height;
		rendererItem.canvas = canvas;
		rendererItem.context = canvas.getContext( '2d' );
		this.canvasses[ id ] = canvas;
	}
	return canvas;
};

Tree.Renderer.Renderer_HTML.setFontSize = function( font, size )
{
	if ( typeof font == 'undefined' )
		font = this.defaultFont;

	var pos = font.indexOf( 'px' );
	if ( pos >= 0 )
	{
		font = size + font.substring( pos );
	}
	else
	{
		font = size + 'px ' + font;
	}
	return font;
};

Tree.Renderer.Renderer_HTML.getFontSize = function( font )
{
 	if ( typeof font == 'undefined' )
		font = this.defaultFont;
	var end = font.indexOf( 'px' );
	if ( end > 0 )
	{
		var start = end;
		while ( start >= 0 && font.charAt( start ) != ' ' )
			start--;
		return parseInt( font.substring( start + 1, end ), 10 );
	}
	return 12;
};
Tree.Renderer.Renderer_HTML.addColor = function( color, modification, direction )
{
	if ( typeof direction === 'undefined' )
		direction = 1;
	for ( var c = 1; c < 7; c += 2 )
	{
		var mod = parseInt( modification.substr( c, 2 ), 16 ) * direction;
		var col = parseInt( color.substr( c, 2 ), 16 );
		var temp = col + mod;
		if ( temp > 255 )
			temp = 255;
		if ( temp < 0 )
			temp = 0;
		temp = temp.toString( 16 );
		if ( temp.length < 2 )
			temp = '0' + temp;
		color = color.substring( 0, c ) + temp + color.substring( c + 2 )
	}
	return color;
};

Tree.Renderer.Renderer_HTML.measureText = function( text, font )
{
	var canvas = document.createElement( 'canvas' );
	var context = canvas.getContext( '2d' );
	if ( typeof font == 'undefined' )
		font = this.defaultFont;
	context.font = font;
	var coords = context.measureText( text );
	coords.height = this.getFontSize( font ) * 1.33;
	return coords;
}


Tree.Renderer.Renderer_HTML.updateItem = function( renderItem )
{
	if ( renderItem.rendererItem )
	{
		if ( renderItem.rendererItem )
			renderItem.rendererItem.needsUpdate = true;
		if ( renderItem.rendererItemPadding )
			renderItem.rendererItemPadding.needsUpdate = true;
	}
};

Tree.Renderer.Renderer_HTML.resizeRenderItem = function( renderItem, width, height )
{
	if ( renderItem.rendererItem )
	{
		renderItem.rendererItem.resize( width, height );
		if ( renderItem.canvasPadding )
			this.resizePadding( renderItem );
	}
};

///////////////////////////////////////////////////////////////////////////////
//
// Rendering process
//
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_HTML.getRenderFlags = function( extraFlags )
{
	var options =
	{
		x: 0,
		y: 0,
		z: 0,
		offsetX: 0,
		offsetY: 0,
		renderer: this,
		zoomX: 1,
		zoomY: 1,
		rotation: 0,
		alpha: 1,
		xCenter: this.width / 2,
		yCenter: this.height / 2,
		perspective: 0
	};
	this.utilities.setOptions( options, extraFlags );
	return options;
};

Tree.Renderer.Renderer_HTML.renderStart = function( options )
{
};

Tree.Renderer.Renderer_HTML.renderUp = function( options, renderItem )
{
	options.renderItem = renderItem;

	// Nothing to draw (security)
	if ( !renderItem.rendererType )
	{
        options.rendererItem = false;
		this.pile.push( Object.assign( {}, options ) );
        this.renderFlags[ renderItem.identifier ] = Object.assign( {}, options );
		options = this.renderPrepare( options, renderItem );
		return options;
	}

	// A refresh is needed
	options.renderer.refresh = true;

	// Creates the renderingItem if it does not exist
	if ( renderItem.renderInParent && options.renderInParent )
	{
		if ( !options.renderingInParent )
		{
			options.rendererItem = options.renderInParent;

			options.renderInParentX = 0;
			options.renderInParentY = 0;
			options.renderingInParent = true;
		}
	}
	else
	{
		options.renderingInParent = false;
		if ( !renderItem.rendererItem )
		{
			// Padding?
			if ( renderItem.paddingH || renderItem.paddingV )
			{
				this.createPadding( renderItem, options );
			}

			// Render item
			renderItem.rendererItem = new Tree.Renderer.Renderer_HTML[ 'RendererItem' + renderItem.rendererType ]( this, renderItem, options );
			renderItem.rendererItem.renderItem = renderItem;
			this.add( renderItem, renderItem.rendererItem );
			renderItem.rendererItem.visible = true;
		}
		options.rendererItem = renderItem.rendererItem;
	}

	// Context = rendererItem
	options.context = options.rendererItem;
	this.pile.push( Object.assign( {}, options ) );
	this.renderFlags[ renderItem.identifier ] = Object.assign( {}, options );

	// Calculates coordinates
	options = this.renderPrepare( options, renderItem );

	return options;
};
Tree.Renderer.Renderer_HTML.renderPrepare = function( options, renderItem )
{
	var xx = renderItem.x;
	var yy = renderItem.y;
	if ( !renderItem.noOffsets )
	{
		xx += options.offsetX;
		yy += options.offsetY;
	}
	options.offsetX = 0;
	options.offsetY = 0;

	if ( !options.noPerspective )
	{
		// Calculates the x and y shift
		xx += ( xx - options.xCenter ) * options.perspective;
		yy += ( yy - options.yCenter ) * options.perspective;

		// Specific perspective for the children of the renderItem?
		if ( renderItem.perspective  )
		{
			options.perspective = renderItem.perspective;
			if ( typeof renderItem.xCenter != 'undefined' )
				options.xCenter = renderItem.xCenter;
			if ( typeof renderItem.yCenter != 'undefined' )
				options.yCenter = renderItem.yCenter;
		}
		if ( typeof renderItem.noPerspective != 'undefined' )
		options.noPerspective = renderItem.noPerspective;
	}

	options.x += Math.floor( xx );
	options.y += Math.floor( yy );
	options.z = renderItem.z;
	options.width = renderItem.width;
	options.height = renderItem.height;
	renderItem.rect.x = options.x;
	renderItem.rect.y = options.y;
	renderItem.rect.width = options.width;
	renderItem.rect.height = options.height;
	renderItem.thisRect.x = 0;
	renderItem.thisRect.y = 0;
	renderItem.thisRect.width = options.width;
	renderItem.thisRect.height = options.height;
	if ( options.renderingInParent )
	{
		options.renderInParentX += Math.floor( xx );
		options.renderInParentY += Math.floor( yy );
		renderItem.thisRect.x = options.renderInParentX;
		renderItem.thisRect.y = options.renderInParentY;
	}
	if ( !renderItem.noRotation )
        options.rotation += renderItem.rotation;
	else
        options.rotation = 0;
	options.zoomX *= renderItem.zoomX;
    options.zoomY *= renderItem.zoomY;
	options.alpha *= renderItem.alpha;

	// Render padding
	if ( renderItem.canvasPadding )
	{
		this.renderPadding( renderItem, options );
	}
	return options;
};
Tree.Renderer.Renderer_HTML.renderIt = function( options, renderItem )
{
	if ( options.rendererItem && !options.renderingInParent )
	{
		// Visible or not?
		if ( renderItem.visible != options.rendererItem.visible )
        	options.rendererItem.setVisible( renderItem.visible );

		// Refreshes renderItem if it has changed
		options.rendererItem.update( options );
	}
	return options;
};
Tree.Renderer.Renderer_HTML.renderDown = function( options, renderItem )
{
	renderItem.refresh = false;
	return this.pile.pop();
};
Tree.Renderer.Renderer_HTML.renderUpFast = function( options, renderItem )
{
	if ( this.renderFlags[ renderItem.identifier ] )
	{
		options = Object.assign( {}, this.renderFlags[ renderItem.identifier ] );
		options = this.renderPrepare( options, renderItem );
		return options;
	}
	return false;
};
Tree.Renderer.Renderer_HTML.renderDownFast = function( options, renderItem )
{
	renderItem.refresh = false;
	return options;
};
Tree.Renderer.Renderer_HTML.renderEnd = function ()
{
};
Tree.Renderer.Renderer_HTML.postProcess = function( imageOrCanvas, renderItem )
{
};

Tree.Renderer.Renderer_HTML.createPadding = function( renderItem, options )
{
	renderItem.canvasPadding = document.createElement( 'canvas' );
	renderItem.canvasPadding.width = renderItem.widthPadded;
	renderItem.canvasPadding.height = renderItem.heightPadded;
	renderItem.contextPadding = renderItem.canvasPadding.getContext( '2d' );
	this.drawPadding( renderItem );

	renderItem.canvasPadding.style.position = 'absolute';
	renderItem.canvasPadding.style.visibility = 'hidden';
	document.body.appendChild( renderItem.canvasPadding );
};
Tree.Renderer.Renderer_HTML.resizePadding = function( renderItem )
{
	renderItem.canvasPadding.width = renderItem.widthPadded;
	renderItem.canvasPadding.height = renderItem.heightPadded;
	this.drawPadding( renderItem );
};
Tree.Renderer.Renderer_HTML.drawPadding = function( renderItem, options )
{
	renderItem.contextPadding.fillStyle = renderItem.colorPadding;
	renderItem.contextPadding.fillRect( 0, 0, renderItem.widthPadded, renderItem.heightPadded );
};
Tree.Renderer.Renderer_HTML.renderPadding = function( renderItem, options )
{
	renderItem.canvasPadding.style.zIndex = options.z;
    renderItem.canvasPadding.style.left = Math.floor( options.x - renderItem.paddingH ) + 'px';
    renderItem.canvasPadding.style.top = Math.floor( options.y - renderItem.paddingV ) + 'px';
    renderItem.canvasPadding.style.width = Math.floor( options.width + renderItem.paddingH * 2 ) + 'px';
    renderItem.canvasPadding.style.height = Math.floor( options.height + renderItem.paddingV * 2 ) + 'px';
    renderItem.canvasPadding.style.opacity = options.alpha.toString();
	renderItem.canvasPadding.style.visibility = 'visible';
};
Tree.Renderer.Renderer_HTML.destroyPadding = function( renderItem )
{
    document.body.removeChild( renderItem.canvasPadding );
	renderItem.canvasPadding = null;
	renderItem.contextPadding = null;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_HTML.RendererItemDiv = function( renderer, renderItem, options )
{
	Object.assign( this, Tree.Renderer.Renderer_HTML.RendererItemDiv );
	this.renderer = renderer;
	this.renderItem = renderItem;
	this.name = renderItem.name;
	this.className = 'Tree.Renderer.Renderer_HTML.RendererItemDiv';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.width = false;
	this.height = false;
	this.utilities.setOptions( this, options );
	this.div = document.createElement( 'div' );
	this.div.style.position = 'absolute';
	this.div.style.zIndex = this.renderItem.z;
	this.div.style.visibility = 'hidden';
	this.checkHTML();
	document.body.appendChild( this.div );
	this.toRefresh = true;
}
Tree.Renderer.Renderer_HTML.RendererItemDiv.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_HTML.RendererItemDiv.checkHTML = function( z )
{
	if ( this.renderItem.HTML && ( this.HTML != this.renderItem.HTML || this.needsUpdate ) || this.forceReset )
	{
		this.HTML = this.renderItem.HTML;
		this.div.innerHTML = this.renderItem.HTML;
	}
};
Tree.Renderer.Renderer_HTML.RendererItemDiv.update = function( options )
{
	this.checkHTML();
	this.div.style.zIndex = options.z;
    this.div.style.left = Math.floor( options.x ) + 'px';
    this.div.style.top = Math.floor( options.y ) + 'px';
    this.div.style.width = Math.floor( this.renderItem.width ) + 'px';
    this.div.style.height = Math.floor( this.renderItem.height ) + 'px';
	this.div.style.opacity = options.alpha.toString();
    this.div.style.visibility = this.visible ? 'visible' : 'hidden';
};
Tree.Renderer.Renderer_HTML.RendererItemDiv.destroy = function( options )
{
    document.body.removeChild( this.div );
};
Tree.Renderer.Renderer_HTML.RendererItemDiv.setVisible = function( flag )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_HTML.RendererItemDiv.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemDiv.getVector = function()
{
    return { x: 0, y: 0 };
};

Tree.Renderer.Renderer_HTML.RendererItemElement = function( renderer, renderItem, options )
{
	Object.assign( this, Tree.Renderer.Renderer_HTML.RendererItemElement );
	this.renderer = renderer;
	this.renderItem = renderItem;
	this.name = renderItem.name;
	this.className = 'Tree.Renderer.Renderer_HTML.RendererItemElement';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.width = false;
	this.height = false;
	this.utilities.setOptions( this, options );
	this.element = renderItem.element;
	document.body.appendChild( this.element );
    this.toRefresh = true;
}
Tree.Renderer.Renderer_HTML.RendererItemElement.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_HTML.RendererItemElement.update = function( options )
{
	this.element.style.zIndex = options.z;
    this.element.style.left = Math.floor( options.x ) + 'px';
    this.element.style.top = Math.floor( options.y ) + 'px';
    this.element.style.width = Math.floor( this.renderItem.width ) + 'px';
    this.element.style.height = Math.floor( this.renderItem.height ) + 'px';
    this.element.style.opacity = options.alpha.toString();
    this.element.style.visibility = this.visible ? 'visible' : 'hidden';
};
Tree.Renderer.Renderer_HTML.RendererItemElement.destroy = function( options )
{
    document.body.removeChild( this.element );
};
Tree.Renderer.Renderer_HTML.RendererItemElement.setVisible = function( flag )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_HTML.RendererItemElement.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemElement.getVector = function()
{
    return { x: 0, y: 0 };
};



Tree.Renderer.Renderer_HTML.RendererItemSprite = function( renderer, renderItem, options )
{
	Object.assign( this, Tree.Renderer.Renderer_HTML.RendererItemSprite );
	this.renderer = renderer;
	this.renderItem = renderItem;
	this.name = renderItem.name;
	this.className = 'Tree.Renderer.Renderer_HTML.RendererItemSprite';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.width = false;
	this.height = false;
	this.utilities.setOptions( this, options );
    this.checkImage();
    this.toRefresh = true;
}
Tree.Renderer.Renderer_HTML.RendererItemSprite.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite.checkImage = function( z )
{
	if ( this.renderItem.nameImage && ( this.nameImage != this.renderItem.nameImage || this.needsUpdate ) || this.forceReset )
	{
        var canvas = this.renderer.getCanvasFromImage( this.renderItem.nameImage, this.renderItem );
        if ( canvas )
        {
            // Removes from document
            if ( this.canvas )
                document.body.removeChild( this.canvas );

            // Change image
            this.canvas = canvas;
            this.nameImage = this.renderItem.nameImage;
            this.width = this.canvas.width;
            this.height = this.canvas.height;

            // Put new one in document
            this.canvas.style.position = 'absolute';
            this.canvas.style.visibility = 'hidden';
            document.body.appendChild( this.canvas );
       }
	}
};
Tree.Renderer.Renderer_HTML.RendererItemSprite.update = function( options )
{
	this.canvas.style.zIndex = options.z;
    this.canvas.style.left = Math.floor( options.x ) + 'px';
    this.canvas.style.top = Math.floor( options.y ) + 'px';
    this.canvas.style.width = Math.floor( this.renderItem.width ) + 'px';
    this.canvas.style.height = Math.floor( this.renderItem.height ) + 'px';
    this.canvas.style.opacity = options.alpha.toString();
    this.canvas.style.visibility = this.visible ? 'visible' : 'hidden';
};
Tree.Renderer.Renderer_HTML.RendererItemSprite.destroy = function( options )
{
    document.body.removeChild( this.canvas );
};
Tree.Renderer.Renderer_HTML.RendererItemSprite.setVisible = function( flag )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite.getVector = function()
{
    return { x: 0, y: 0 };
};






Tree.Renderer.Renderer_HTML.RendererItemSprite3D = function( renderer, renderItem, options )
{
	Object.assign( this, Tree.Renderer.Renderer_HTML.RendererItemSprite3D );
	this.renderer = renderer;
	this.renderItem = renderItem;
	this.name = renderItem.name;
	this.className = 'Tree.Renderer.Renderer_HTML.RendererItemSprite3D';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.widthItem = false;
	this.heightItem = false;
	this.utilities.setOptions( this, options );
	this.layerList = {};
	this.maxLayers = 0;
    this.checkImages();
    this.toRefresh = true;
}
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.checkImages = function( zBase )
{
	var maxLayers = this.maxLayers;
	if ( !this.layerList[ this.renderItem.nameImage ] || this.nameImage != this.renderItem.nameImage || this.forceReset )
	{
		this.forceReset = false;
		this.nameImage = this.renderItem.nameImage;
		this.layerList[ this.renderItem.nameImage ] = [];

		// Removes the previous images from the document
		for ( var l = 0; l < this.layerList.length; l++ )
		{
			var layer = this.layerList[ l ];
			if ( layer && layer.element )
			{
				layer.element = false;
				document.body.removeChild( layer.element );
			}
		}

		// Creates the new ones
		var imageList = this.resources.getImage( this.renderItem.nameImage, this.renderItem );
		for ( var z = 0; z < imageList.images.length; z ++ )
		{
			var image = this.renderer.getCanvasFromImage( imageList.images[ z ].name, this.renderItem );
			var layer =
			{
				element: image,
				width: image.width,
				height: image.height,
				z: z
			};
			if ( z == 0 )
			{
				this.width = image.width;
				this.height = image.height;
			}

			// Adds to document
			image.style.position = 'absolute';
			image.style.visibility = 'hidden';
			document.body.appendChild( image );

			//Add to table
			this.layerList[ this.renderItem.nameImage ].push( layer );
			this.maxLayers = Math.max( this.maxLayers, z );
		}
	}
	if ( this.maxLayers < maxLayers )
	{
		for ( z = this.maxLayers; z < this.maxLayers; z++ )
		{
			var layer = this.layerList[ z ];
			if ( layer.element )
			{
				document.body.removeChild( layer.element );
				layer.element = false;
			}
		}
	}
	return this.layerList[ this.renderItem.nameImage ];
}
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.update = function( options )
{
	this.checkImages();
	var z = 0;
	var layerList = this.layerList[ this.renderItem.nameImage ];
	if ( layerList )
	{
		// All the layers one above the other by changing the Z order up, and adding perspective
		for ( z = 0; z < layerList.length && z < this.maxLayers; z++ )
		{
			var layer = layerList[ z ];
			var deltaX = ( ( options.x - options.xCenter ) * z * options.perspective ) * options.zoomX;
			var deltaY = ( ( options.y - options.yCenter ) * z * options.perspective ) * options.zoomY;
			layer.element.style.left = options.x + deltaX + 'px';
			layer.element.style.top = options.y + deltaY + 'px';
			layer.element.style.zIndex = options.z + z;
			layer.element.style.width = Math.floor( layer.width * options.zoomX ) + 'px';
			layer.element.style.height = Math.floor( layer.height * options.zoomY ) + 'px';
			layer.element.style.opacity = options.alpha.toString();
			layer.element.style.visibility = this.visible ? 'visible' : 'hidden';
		}
	}
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.destroy = function( options )
{
	// All the layers one above the other by changing the Z order up, and adding perspective
	for ( image in this.layerList )
	{
		var layerList = this.layerList[ image ];
		for ( z = 0; z < layerList.length; z++ )
		{
			if ( layerList[ z ].element )
				document.body.removeChild( layerList[ z ].element );
		}
	}
};
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.setVisible = function( options )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.getVector = function()
{
    return { x: 0, y: 0 };
};
Tree.Renderer.Renderer_HTML.RendererItemSprite3D.resize = function( width, height )
{
};



Tree.Renderer.Renderer_HTML.RendererItemMap = function( renderer, renderItem, options )
{
	Object.assign( this, Tree.Renderer.Renderer_HTML.RendererItemMap );
	this.renderer = renderer;
	this.renderItem = renderItem;
	this.name = renderItem.name;
	this.className = 'Tree.Renderer.Renderer_HTML.RendererItemMap';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.utilities.setOptions( this, options );

	this.tileWidth = this.renderItem.tileWidth;
	this.tileHeight = this.renderItem.tileHeight;
	this.mapWidth = this.renderItem.mapWidth;
	this.mapHeight = this.renderItem.mapHeight;
	this.tileCount = 0;
	this.reset();
	return this;
}
Tree.Renderer.Renderer_HTML.RendererItemMap.update = function( options )
{
	var offsetX = this.renderItem.offsetX;
	var offsetY = this.renderItem.offsetY;

	var xx = options.x - offsetX * options.zoomX;
	var yy = options.y - offsetY * options.zoomY;

	// Position the background
	this.backgroundCanvas.style.zIndex = '' + options.z;
    this.backgroundCanvas.style.left = Math.floor( xx ) + 'px';
    this.backgroundCanvas.style.top = Math.floor( yy ) + 'px';
    this.backgroundCanvas.style.width = Math.floor( this.backgroundCanvas.width * options.zoomX ) + 'px';
    this.backgroundCanvas.style.height = Math.floor( this.backgroundCanvas.height * options.zoomY ) + 'px';
    this.backgroundCanvas.style.opacity = options.alpha.toString();
    this.backgroundCanvas.style.visibility = this.visible ? 'visible' : 'hidden';

	// Position the animated sprites. TODO: clipping and culling!
	for ( var y in this.sprites )
	{
		for ( var x in this.sprites[ y ] )
		{
			var spriteDefinition = this.sprites[ y ][ x ];
			for ( var z = 0; z < spriteDefinition.length; z++ )
			{
				var pDefinition = spriteDefinition[ z ];
				var element = spriteDefinition[ z ].element;

				var x1 = x * this.tileWidth;
				var y1 = y * this.tileHeight;
				x1 += ( x1 - options.xCenter ) * options.perspective * z;
				y1 += ( y1 - options.yCenter ) * options.perspective * z;
				//var x2 = x1 + this.tileWidth;
				//var y2 = y1 + this.tileHeight;

				element.style.zIndex = '' + options.z + z;
				element.style.left = Math.floor( xx + x1  ) + 'px';
				element.style.top = Math.floor( yy + y1 ) + 'px';
				element.style.width = Math.floor( this.tileWidth * options.zoomX ) + 'px';
				element.style.height = Math.floor( this.tileHeight * options.zoomY ) + 'px';
				element.style.opacity = options.alpha.toString();
				element.style.visibility = this.visible ? 'visible' : 'hidden';

				// Culling
				/*if ( x2 >= this.renderItem.x && x1 < this.renderItem.width && y2 >= this.renderItem.y && y1 < this.renderItem.height )
				{
					if ( !pDefinition.inScene )
					{
						pDefinition.inScene = true;
						this.renderer.scene.add( pDefinition.sprite );
					}
				}
				else
				{
					if ( pDefinition.inScene )
					{
						pDefinition.inScene = false;
						this.renderer.scene.remove( pDefinition.sprite );
					}
				}
				*/
			}
		}
	}
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemMap.destroy = function( options )
{
	document.body.removeChild( this.backgroundCanvas );
	for ( var s = 0; s < this.spriteList.length; s++ )
	{
		if ( this.spriteList[ s ].element )
			document.body.removeChild( this.spriteList[ s ].element );
	}
};
Tree.Renderer.Renderer_HTML.RendererItemMap.setVisible = function( flag )
{
	if ( this.visible != flag )
	{
		this.visible = flag;
	}
};
Tree.Renderer.Renderer_HTML.RendererItemMap.doRefresh = function()
{
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemMap.getVector = function()
{
	var position = new THREE.Vector3();
	return position.getPositionFromMatrix( this.backgroundSprite.matrixWorld );
};
Tree.Renderer.Renderer_HTML.RendererItemMap.reset = function()
{
	// Computes the tiles definition
	this.tiles = [];
    for ( var i = 0; i < this.renderItem.tiles.length; i ++ )
    {
		var tile = this.renderItem.tiles[ i ];
		var tileDefinition = {};
		if ( tile.nameImage )
		{
			var image = this.resources.getImage( tile.nameImage );
			tileDefinition.width = image.width;
			tileDefinition.height = image.height;
			tileDefinition.nameImage = tile.nameImage;
		}
		else if ( tile.images )
		{
			tileDefinition.images = [];
			for ( var ii = 0; ii < tile.images.length; ii++ )
			{
				var image = this.resources.getImage( tile.images[ ii ].nameImage );
				var def =
				{
					nameImage: tile.images[ ii ].nameImage,
					width: image.width,
					height: image.height,
					hotSpotX: image.hotSpotX,
					hotSpotY: image.hotSpotY,
					z: ii
				};
				tileDefinition.images.push( def );
			}
		}
		this.tiles.push( tileDefinition );
	}

	// Creates the static ground layer canvas
	this.width = this.mapWidth * this.tileWidth;
	this.height = this.mapHeight * this.tileHeight;
	this.backgroundCanvas = this.renderer.createCanvas( this.width, this.height, 'mapBackground', this.renderItem, this );

	// Draw the background if defined
	if ( this.renderItem.background )
	{
		var backImage = this.resources.getImage( this.renderItem.background, this.renderItem );
		var repeatX = Math.floor( ( this.mapWidth * this.tileWidth ) / backImage.width ) + 1;
		var repeatY = Math.floor( ( this.mapHeight * this.tileHeight ) / backImage.height ) + 1;
		for ( var x = 0; x < repeatX; x++ )
		{
			for ( var y = 0; y < repeatY; y++ )
			{
				this.context.drawImage( backImage, x * backImage.width, y * backImage.height, backImage.width, backImage.height );
			}
		}
	}

	// Creates the background
	this.backgroundCanvas.style.position = 'absolute';
	this.backgroundCanvas.style.visibility = 'hidden';
	document.body.appendChild( this.backgroundCanvas );

	// Draw the tiles, and creates the animated sprites columns
	this.map = this.renderItem.map;
	this.sprites = [];
	this.spriteList = [];
	var x = 0;
	var y = 0;
    for ( var y = 0; y < this.map.length; y++ )
    {
		for ( var x = 0; x < this.map[ y ].length; x++ )
		{
			var tileNumber = this.map[ y ][ x ];
			if ( tileNumber >= 0 )
			{
				if ( this.tiles[ tileNumber ] )
				{
					var tile = this.tiles[ tileNumber ];
					if ( tile.nameImage )
					{
						var image = this.resources.getImage( tile.nameImage );

						// A single image, draw into background
						this.context.drawImage( image, x * this.tileWidth, y * this.tileHeight );
					}
					else
					{
						// Animated tile, creates the column of sprites
						for ( var m = 0; m < tile.images.length; m++ )
						{
							if ( !this.sprites[ y ] )
								this.sprites[ y ] = {};
							if ( !this.sprites[ y ][ x ] )
								this.sprites[ y ][ x ] = [];

							var element = this.renderer.getCanvasFromImage( tile.images[ m ].nameImage, this.renderItem, this.tileCount++ );
							var spriteDefinition =
							{
								element: element,
 								width: tile.images[ m ].width,
								height: tile.images[ m ].height,
								hotSpotX: tile.images[ m ].hotSpotX,
								hotSpotY: tile.images[ m ].hotSpotY,
							};
							this.sprites[ y ][ x ].push( spriteDefinition );
							this.spriteList.push( spriteDefinition );
							element.style.position = 'absolute';
							element.style.visibility = 'hidden';
							document.body.appendChild( element );
						}
					}
				}
				else
				{
					this.renderer.error( 'Map ' + this.renderItem.identifier + ' error: tile not defined X ' + x + ', Y ' + y + ', Value ' + tileNumber );
				}
			}
		}
	}
};
Tree.Renderer.Renderer_HTML.RendererItemMap.resize = function( width, height )
{
};






Tree.Renderer.Renderer_HTML.RendererItemCanvas = function( renderer, renderItem, options )
{
	Object.assign( this, Tree.Renderer.Renderer_HTML.RendererItemCanvas );
	Object.assign( this, Tree.Renderer.Utilities.Canvas2D );
	this.renderer = renderer;
	this.renderItem = renderItem;
	this.name = renderItem.name;
	this.className = 'Tree.Renderer.Renderer_HTML.RendererItemCanvas';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;

	this.utilities.setOptions( this, options );
	this.canvas = this.renderer.createCanvas( this.renderItem.width, this.renderItem.height, 'canvasItem', this.renderItem, this );
	this.width = this.canvas.width;
	this.height = this.canvas.height;
	this.canvas.style.position = 'absolute';
	this.canvas.style.zIndex = renderItem.z;
	this.canvas.style.visibility = 'hidden';
    document.body.appendChild( this.canvas );
    this.toRefresh = true;
}
Tree.Renderer.Renderer_HTML.RendererItemCanvas.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_HTML.RendererItemCanvas.update = function( options )
{
	this.canvas.style.zIndex = options.z;
    this.canvas.style.left = Math.floor( options.x ) + 'px';
    this.canvas.style.top = Math.floor( options.y ) + 'px';
    this.canvas.style.width = Math.floor( this.width ) + 'px';
    this.canvas.style.height = Math.floor( this.height ) + 'px';
    this.canvas.style.opacity = options.alpha.toString();
    this.canvas.style.visibility = this.visible ? 'visible' : 'hidden';
};
Tree.Renderer.Renderer_HTML.RendererItemCanvas.destroy = function( options )
{
    document.body.removeChild( this.canvas );
};
Tree.Renderer.Renderer_HTML.RendererItemCanvas.setVisible = function( flag )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_HTML.RendererItemCanvas.resize = function( width, height )
{
	if ( typeof width != 'undefined' )
	{
		this.canvas.width = width;
		this.canvas.height = height;
	}
	if ( typeof height != 'undefined' )
	{
		this.width = width;
		this.height = height;
	}
};
Tree.Renderer.Renderer_HTML.RendererItemCanvas.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_HTML.RendererItemCanvas.getVector = function()
{
    return { x: 0, y: 0 };
};
