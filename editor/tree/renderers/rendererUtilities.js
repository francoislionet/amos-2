 /*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * RenderItem canvas class
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 02/03/2018
 */
Tree.Renderer = Tree.Renderer || {};
Tree.Renderer.Utilities = Tree.Renderer.Utilities || {};

// Canvas function for RendererItemCanvas
Tree.Renderer.Utilities.Canvas2D =
{
    setStrokeStyle: function( options, style )
    {
        this.context.strokeStyle = this.getColor( style );
    },
    setFillStyle:function( options, color )
    {
        this.context.fillStyle = this.getColor( color );
    },
    setLineWidth: function( options, width )
    {
        this.context.lineWidth = width;
    },
    setGlobalAlpha: function( options, alpha )
    {
        this.context.globalAlpha = alpha;
    },
    setFont: function( options, font )
    {
        if ( typeof font == 'undefined' )
		      font = this.defaultFont;
        this.context.font = font;
    },
	beginPath: function( options )
	{
		this.context.beginPath();
	},
	moveTo: function( options, x, y )
	{
		this.context.moveTo( x, y );
	},
	lineTo: function( options, x, y )
	{
		this.context.lineTo( x, y );
	},
	closePath: function( options )
	{
		this.context.closePath();
	},
	clip: function( options )
	{
		this.context.clip();
		options.renderer.refresh = true;
	},
	stroke: function( options )
	{
 		this.context.stroke();
		this.toRefresh = true;
	},
	fillRect: function( options, x, y, width, height )
	{
 		this.context.fillRect( x, y, width, height );
		this.toRefresh = true;
	},
	clearRect: function( options, x, y, width, height )
	{
 		this.context.clearRect( x, y, width, height );
		this.toRefresh = true;
	},
	rect: function( options, x, y, width, height )
	{
 		this.context.rect( x, y, width, height );
	},
	ellipse: function( options, x, y, width, height, start, end, angle )
	{
 		this.context.ellipse( x, y, width, height, start, end, angle );
	},
	fill: function( options )
	{
 		this.context.fill();
		this.toRefresh = true;
	},
	save: function( options )
	{
 		this.context.save();
	},
	restore: function( options )
	{
 		this.context.restore();
	},
	fillText: function( options, text, x, y )
	{
 		this.context.fillText( text, x, y );
		options.renderer.refresh = true;
		this.toRefresh = true;
	},
	setTextAlign: function( options, align )
	{
 		this.context.textAlign = align;
	},
	setTextBaseline: function( options, base )
	{
 		this.context.textBaseline = base;
	},
	drawImage: function( options, image, x, y, width, height )
	{
 		this.context.drawImage( image, x, y, width, height );
		options.renderer.refresh = true;
		this.toRefresh = true;
	},
	scale: function( options, x, y )
	{
 		this.context.scale( x, y );
	},
	setAlpha: function( options, alpha )
	{
 		this.context.globalAlpha = alpha;
	},
	setLineCap: function( options, cap )
	{
 		this.context.lineCap = cap;
	},
	translate: function( options, x, y )
	{
 		this.context.translate( x, y );
	},
	rotate: function( options, angle )
	{
 		this.context.rotate( angle );
	},
	drawText: function ( options, x, y, text, font, color, hAlign, vAlign, size )
	{
	 	if ( typeof font === 'undefined' )
			font = this.defaultFont + '';
		if ( typeof color === 'undefined' )
			color = '#000000';
		if ( typeof hAlign === 'undefined' )
			hAlign = 'center';
		if ( typeof vAlign === 'undefined' )
			vAlign = 'middle';
	 	if ( typeof size !== 'undefined' )
	     	font = this.setFontSize( font, size );
		this.context.font = font;
		this.context.textAlign = hAlign;
	 	this.context.textBaseline = vAlign;
		this.context.fillStyle = color;
		this.context.fillText( text, x, y );
		options.renderer.refresh = true;
		this.toRefresh = true;
	},
	getColor: function ( color )
	{
		if ( typeof color == 'number' )
		{
			var result = ( ( color & 0xFF0000 ) >> 32 ).toString( 16 )
				+ ( ( color & 0x00FF00 ) >> 16 ).toString( 16 )
				+ ( color & 0x0000FF ).toString( 16 );
		}
		else
			result = color;

		return result;
	},
	setFontSize: function ( font, size )
	{
	 	if ( typeof font == 'undefined' )
			font = this.defaultFont;

		var pos = font.indexOf( 'px' );
		if ( pos >= 0 )
		{
			font = size + font.substring( pos );
		}
		else
		{
			font = size + 'px ' + font;
		}
		return font;
	},
	getFontSize: function ( font )
	{
		return this.renderer.getFontSize( font );
	},
	addColor: function ( color, modification, direction )
	{
	 	if ( typeof direction === 'undefined' )
	     	direction = 1;
		for ( var c = 1; c < 7; c += 2 )
		{
			var mod = parseInt( modification.substr( c, 2 ), 16 ) * direction;
			var col = parseInt( color.substr( c, 2 ), 16 );
			var temp = col + mod;
			if ( temp > 255 )
				temp = 255;
			if ( temp < 0 )
				temp = 0;
			temp = temp.toString( 16 );
			if ( temp.length < 2 )
				temp = '0' + temp;
			color = color.substring( 0, c ) + temp + color.substring( c + 2 )
		}
		return color;
	},
	measureText: function( text, font )
	{
	 	if ( typeof font == 'undefined' )
			font = this.defaultFont;
	 	this.context.font = font;
	 	var coords = this.context.measureText( text );
	 	coords.height = this.getFontSize( font );
	 	return coords;
	}
};
