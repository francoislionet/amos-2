 /*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine HTML Renderer
 * Renders all items in the document
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 02/03/2018
 */
Tree.Renderer = Tree.Renderer || {};

Tree.Renderer.Renderer_Canvas2D = function( localProperties, globalProperties )
{
	//Object.assign( this, Tree.Renderer.Renderer_Canvas2D );

	this.tree = globalProperties.tree;
	this.name = 'Renderer_Canvas2D';
	this.className = 'Tree.Renderer.Renderer_Canvas2D';
	this.utilities = globalProperties.utilities;
	this.resources = globalProperties.resources;
	this.renderingId = false;
	this.utilities.setOptions( this, globalProperties );

	// Direct copy of options, they are all set!
	Object.assign( this, localProperties );

	// Renderer variables
    this.pile = [];
    this.items = {};
    this.images = {};
	this.renderFlags = {};
	this.canvasses = {};
	this.pile = [];
	this.displayList = [];
	this.renderToList = {};
    if ( this.width == Tree.NOTDEFINED )
        this.width = this.tree.width;
    if ( this.height == Tree.NOTDEFINED )
        this.height = this.tree.height;
	this.refresh = true;

	// Attach canvas
	if ( this.renderingId )
	{
		var element = document.getElementById( this.renderingId );
		this.canvas = document.createElement( 'canvas' );
		this.canvas.width = this.width;
		this.canvas.height = this.height;
		element.appendChild( this.canvas );
		this.context = this.canvas.getContext( '2d' );
	}
};

// Default options. Will be send on renderer creation
Tree.Renderer.Renderer_Canvas2D.defaultProperties =
{
	x: 0,
	y: 0,
	z: 0,
	width: Tree.NOTDEFINED,
	height: Tree.NOTDEFINED,
	defaultFont: '12px Verdana',
	antialias: true,
};

Tree.Renderer.Renderer_Canvas2D.prototype.resize = function( newWidth, newHeight, mode )
{
	this.canvas.width = newWidth;
	this.canvas.height = newHeight;
	this.refresh = true;
};

Tree.Renderer.Renderer_Canvas2D.prototype.changeExposed = function( info )
{
	switch ( info.id )
	{
        default:
            break;
	}
};
Tree.Renderer.Renderer_Canvas2D.prototype.getScreenCoordinates = function( item )
{
    return rendererItem.getVector();
}
Tree.Renderer.Renderer_Canvas2D.prototype.add = function( item, rendererItem )
{
	this.items[ item.identifier ] = rendererItem;
};
Tree.Renderer.Renderer_Canvas2D.prototype.clear = function()
{
	this.canvasses = {};
    this.renderFlags = {};
	this.images = {};
	this.pile = [];
	this.renderToList = {};
	for ( var i in this.items )
		this.items[ i ].destroy();
	this.items = {};
	this.clearDisplayList();
};
Tree.Renderer.Renderer_Canvas2D.prototype.refreshItem = function( item )
{
	if ( this.items[ item.identifier ] )
		this.items[ item.identifier ].doRefresh();
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_Canvas2D.prototype.startRenderTo = function( nameImage, image )
{
	if ( !this.renderToList[ nameImage ] )
		this.renderToList[ nameImage ] = image;
};
Tree.Renderer.Renderer_Canvas2D.prototype.stopRenderTo = function( nameImage )
{
	this.renderToList[ nameImage ] = false;
	this.renderToList = this.utilities.cleanArray( this.renderToList );
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_Canvas2D.prototype.startDestroy = function()
{
};
Tree.Renderer.Renderer_Canvas2D.prototype.destroy = function( item )
{
	if ( this.items[ item.identifier ] )
	{
		this.items[ item.identifier ].destroy();
		this.items[ item.identifier ] = false;
	}
};
Tree.Renderer.Renderer_Canvas2D.prototype.endDestroy = function()
{
	this.items = this.utilities.cleanArray( this.items );
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_Canvas2D.prototype.setImage = function( srcImage, options, callback, extra )
{
	callback( true, srcImage, extra );
};
Tree.Renderer.Renderer_Canvas2D.prototype.getCanvas = function( id )
{
	var canvas = this.canvasses[ id ];
	if ( canvas )
		return canvas;
	return false;
};
Tree.Renderer.Renderer_Canvas2D.prototype.createCanvas = function( width, height, name, item, rendererItem, force )
{
	var id = item.identifier + '<>' + name;
	var canvas = this.canvasses[ id ];
	if ( !canvas || force  )
	{
		canvas = document.createElement( 'canvas' );
		canvas.treeName = name;
		canvas.treeRendererId = id;
		canvas.width = width;
		canvas.height = height;
		rendererItem.canvas = canvas;
		rendererItem.context = canvas.getContext( '2d' );
		this.canvasses[ id ] = canvas;
	}
	return canvas;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_Canvas2D.prototype.setFontSize = function( font, size )
{
	if ( typeof font == 'undefined' )
		font = this.defaultFont;

	var pos = font.indexOf( 'px' );
	if ( pos >= 0 )
	{
		font = size + font.substring( pos );
	}
	else
	{
		font = size + 'px ' + font;
	}
	return font;
};

Tree.Renderer.Renderer_Canvas2D.prototype.getFontSize = function( font )
{
 	if ( typeof font == 'undefined' )
		font = this.defaultFont;
	var pos = font.indexOf( 'px' );
	if ( pos > 0 )
	{
		var end = pos--;
		while ( pos >= 0 && font.charAt( pos ) == ' ' )
			pos--;
		while ( pos >= 0 && font.charAt( pos ) != ' ' )
			pos--;
		pos++;
		return parseInt( font.substring( pos, end ), 10 );
	}
	return 16;
};
Tree.Renderer.Renderer_Canvas2D.prototype.addColor = function( color, modification, direction )
{
	if ( typeof direction === 'undefined' )
		direction = 1;
	for ( var c = 1; c < 7; c += 2 )
	{
		var mod = parseInt( modification.substr( c, 2 ), 16 ) * direction;
		var col = parseInt( color.substr( c, 2 ), 16 );
		var temp = col + mod;
		if ( temp > 255 )
			temp = 255;
		if ( temp < 0 )
			temp = 0;
		temp = temp.toString( 16 );
		if ( temp.length < 2 )
			temp = '0' + temp;
		color = color.substring( 0, c ) + temp + color.substring( c + 2 )
	}
	return color;
};

Tree.Renderer.Renderer_Canvas2D.prototype.measureText = function( text, font )
{
	var canvas = document.createElement( 'canvas' );
	var context = canvas.getContext( '2d' );
	if ( typeof font == 'undefined' )
		font = this.defaultFont;
	context.font = font;
	var coords = context.measureText( text );
	coords.height = this.getFontSize( font );
	return coords;
}

Tree.Renderer.Renderer_Canvas2D.prototype.updateItem = function( renderItem )
{
	if ( renderItem.rendererItem )
	{
		if ( renderItem.rendererItem )
			renderItem.rendererItem.needsUpdate = true;
	}
};
Tree.Renderer.Renderer_Canvas2D.prototype.resizeItem = function( renderItem, width, height )
{
	if ( renderItem.rendererItem )
	{
		renderItem.rendererItem.resize( width, height );
	}
};

///////////////////////////////////////////////////////////////////////////////
//
// Rendering process
//
///////////////////////////////////////////////////////////////////////////////

Tree.Renderer.Renderer_Canvas2D.prototype.getRenderFlags = function( extraFlags )
{
	var options =
	{
		x: 0,
		y: 0,
		z: 0,
		offsetX: 0,
		offsetY: 0,
		renderer: this,
		zoomX: 1,
		zoomY: 1,
		rotation: 0,
		alpha: 1,
		xCenter: this.width / 2,
		yCenter: this.height / 2,
		perspective: 0
	};
	this.utilities.setOptions( options, extraFlags );
	return options;
};

Tree.Renderer.Renderer_Canvas2D.prototype.renderStart = function( options )
{
};

Tree.Renderer.Renderer_Canvas2D.prototype.renderUp = function( options, item )
{
	options.item = item;

	// Nothing to draw (security)
	if ( !item.rendererType )
	{
        options.rendererItem = false;
		this.pile.push( Object.assign( {}, options ) );
        this.renderFlags[ item.identifier ] = Object.assign( {}, options );
		options = this.renderPrepare( options, item );
		return options;
	}

	// A refresh is needed
	options.renderer.refresh = true;

	// Creates the renderingItem if it does not exist
	if ( !item.rendererItem )
	{
		item.rendererItem = new Tree.Renderer.Renderer_Canvas2D[ 'RendererItem' + item.rendererType ]( this, item, options );
		item.rendererItem.renderItem = item;
		this.add( item, item.rendererItem );
		item.rendererItem.visible = true;
	}

	// Context = rendererItem
	options.rendererItem = item.rendererItem;
	options.context = options.rendererItem;
	this.pile.push( Object.assign( {}, options ) );
	this.renderFlags[ item.identifier ] = Object.assign( {}, options );

	// Calculates coordinates
	options = this.renderPrepare( options, item );

	return options;
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderPrepare = function( options, item )
{
	var xx = item.x;
	var yy = item.y;
	if ( !item.noOffsets )
	{
		xx += options.offsetX;
		yy += options.offsetY;
	}
	options.offsetX = 0;
	options.offsetY = 0;

	if ( !options.noPerspective && !item.noPerspective )
	{
		// Calculates the x and y shift
		xx += ( xx - options.xCenter ) * options.perspective;
		yy += ( yy - options.yCenter ) * options.perspective;

		// Specific perspective for the children of the item?
		if ( item.perspective  )
		{
				options.perspective = item.perspective;
			if ( typeof item.xCenter != 'undefined' )
				options.xCenter = item.xCenter;
			if ( typeof item.yCenter != 'undefined' )
				options.yCenter = item.yCenter;
		}
		if ( typeof item.noPerspective != 'undefined' )
			options.noPerspective = item.noPerspective;
	}

	options.x += Math.floor( xx );
	options.y += Math.floor( yy );
	options.xReal = options.x;
	options.yReal = options.y;
	options.z = item.z;
	options.width = item.width;
	options.height = item.height;
	item.rect.x = options.x;
	item.rect.y = options.y;
	item.rect.width = item.width;
	item.rect.height = item.height;
	item.thisRect.x = 0;
	item.thisRect.y = 0;
	item.thisRect.width = item.width;
	item.thisRect.height = item.height;
	if ( !item.noRotation )
        options.rotation += item.rotation;
	else
        options.rotation = 0;
	options.zoomX *= item.zoomX;
    options.zoomY *= item.zoomY;
	options.alpha *= item.alpha;
	return options;
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderIt = function( options, item )
{
	if ( options.rendererItem )
	{
		// Visible or not?
		if ( item.visible != options.rendererItem.visible )
        	options.rendererItem.setVisible( item.visible );

		// Refreshes item if it has changed
		options.rendererItem.update( options );
	}
	return options;
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderDown = function( options, item )
{
	item.refresh = false;
	return this.pile.pop();
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderUpFast = function( options, item )
{
	if ( this.renderFlags[ item.identifier ] )
	{
		options = Object.assign( {}, this.renderFlags[ item.identifier ] );
		options = this.renderPrepare( options, item );
		return options;
	}
	return false;
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderDownFast = function( options, item )
{
	item.refresh = false;
	return options;
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderEnd = function ()
{
	if ( this.context )
	{
		this.renderDisplayList( this.context );

		for ( var i in this.renderToList )
		{
			var image = this.renderToList[ i ];
			var context = image.getContext( '2d' );
			context.drawImage( this.canvas, 0, 0, image.width, image.height );
		}
	}

};
Tree.Renderer.Renderer_Canvas2D.prototype.postProcess = function( imageOrCanvas, item )
{
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Display list
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
Tree.Renderer.Renderer_Canvas2D.prototype.clearDisplayList = function()
{
	this.displayList = [];
};
Tree.Renderer.Renderer_Canvas2D.prototype.appendChild = function( element )
{
	this.displayList.push( element );
};
Tree.Renderer.Renderer_Canvas2D.prototype.removeChild = function( element )
{
	for ( var d = 0; d < this.displayList.length; d++ )
	{
		if ( this.displayList[ d ] == element )
		{
			this.displayList.splice( d, 1 );
			break;
		}
	}
};
Tree.Renderer.Renderer_Canvas2D.prototype.renderDisplayList = function( context )
{
	// Sort the displaylist
	this.displayList = this.displayList.sort( function( a, b )
	{
		return a.z - b.z;
	} );

	// Display!
	for ( var d = 0; d < this.displayList.length; d++ )
	{
		var element = this.displayList[ d ];
		if ( element.visible )
		{
			context.globalAlpha = element.alpha;
			if ( element.angle == 0 )
				context.drawImage( element.image,
								   element.x - element.hotSpotX,
								   element.y - element.hotSpotY,
					               element.width, element.height );
			else
			{
				context.save();
				context.translate( element.x, element.y );
				if ( element.angle != 0)
					context.rotate( -element.angle * 0.0174532925);
				//context.scale( Math.max( 0.001, element.zoomX ), Math.max( 0.001, element.zoomY ) );
				context.translate( -element.hotSpotX, -element.hotSpotY );
				context.drawImage( element.image, 0, 0, element.width, element.height );
				context.restore();
			}
			element.refresh = false;
		}
	}
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// RendererItems
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


Tree.Renderer.Renderer_Canvas2D.RendererItemSprite = function( renderer, item, options )
{
	Object.assign( this, Tree.Renderer.Renderer_Canvas2D.RendererItemSprite );
	this.renderer = renderer;
	this.item = item;
	this.name = item.name;
	this.className = 'Tree.Renderer.Renderer_Canvas2D.RendererItemSprite';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.width = false;
	this.height = false;
	this.utilities.setOptions( this, options );

	// Adds the element
	this.element =
	{
		image: null,
		x: 0,
		y: 0,
		z: 0,
		angle: 0,
		width: 0,
		height: 0,
		zoomX: 1,
		zoomY: 1,
		hotSpotX: 0,
		hotSpotY: 0,
		alpha: 1,
		visible: false,
		refresh: false
	};
	this.checkImage();

	this.renderer.appendChild( this.element );
    this.toRefresh = true;
}
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.checkImage = function( z )
{
	if ( this.item.nameImage && ( this.nameImage != this.item.nameImage || this.needsUpdate ) || this.forceReset )
	{
        this.image = this.resources.getImage( this.item.nameImage );
        if ( this.image )
        {
			this.element.image = this.image;
            this.nameImage = this.item.nameImage;
			this.element.hotSpotX = this.image.hotSpotX;
			this.element.hotSpotY = this.image.hotSpotY;
       }
	}
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.update = function( options )
{
	this.checkImage();
	this.element.x = options.x;
	this.element.y = options.y;
	this.element.z = options.z;
	this.element.angle = options.rotation;
	this.element.alpha = options.alpha;
	this.element.visible = this.visible;
	this.element.width = options.width;
	this.element.height = options.height;
	this.element.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.destroy = function( options )
{
    this.renderer.removeChild( this.image );
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.setVisible = function( flag )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.getVector = function()
{
    return { x: 0, y: 0 };
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite.resize = function( width, height )
{
	if ( typeof width != 'undefined' )
		this.width = width;
	if ( typeof height != 'undefined' )
		this.height = height;
};






Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D = function( renderer, item, options )
{
	Object.assign( this, Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D );
	this.renderer = renderer;
	this.item = item;
	this.name = item.name;
	this.className = 'Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.width = false;
	this.height = false;
	this.utilities.setOptions( this, options );
	this.layerList = {};
	this.maxLayers = 0;
    this.checkImages();
    this.toRefresh = true;
}
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.reset = function()
{
	this.forceReset = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.checkImages = function( zBase )
{
	var maxLayers = this.maxLayers;
	if ( !this.layerList[ this.item.nameImage ] || this.nameImage != this.item.nameImage || this.forceReset )
	{
		this.forceReset = false;
		this.nameImage = this.item.nameImage;
		this.layerList[ this.item.nameImage ] = [];

		// Removes the previous images from the document
		for ( var l = 0; l < this.layerList.length; l++ )
		{
			var layer = this.layerList[ l ];
			if ( layer && layer.element )
			{
				layer.element = false;
				this.renderer.removeChild( layer.element );
			}
		}

		// Creates the new ones
		var imageList = this.resources.getImage( this.item.nameImage, this.item );
		for ( var z = 0; z < imageList.images.length; z ++ )
		{
			var image = this.resources.getImage( imageList.images[ z ].name );

			// Adds to renderer the first time
			this.added = true;
			var element =
			{
				image: image,
				x: 0,
				y: 0,
				z: 0,
				angle: 0,
				width: image.width,
				height: image.height,
				zoomX: 1,
				zoomY: 1,
				hotSpotX: image.hotSpotX,
				hotSpotY: image.hotSpotY,
				alpha: 1,
				visible: false,
				refresh: false
			}
			var layer =
			{
				element: element,
				width: image.width,
				height: image.height,
				z: z
			};
			if ( z == 0 )
			{
				this.width = image.width;
				this.height = image.height;
			}

			// Add to renderer
			this.renderer.appendChild( element );

			// Add to tables
			this.layerList[ this.item.nameImage ].push( layer );
			this.maxLayers = Math.max( this.maxLayers, z );
		}
	}
	if ( this.maxLayers < maxLayers )
	{
		for ( z = this.maxLayers; z < this.maxLayers; z++ )
		{
			var layer = this.layerList[ z ];
			if ( layer.element )
			{
				this.renderer.removeChild( layer.element );
				layer.element = false;
			}
		}
	}
	return this.layerList[ this.item.nameImage ];
}
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.update = function( options )
{
	this.checkImages();
	var z = 0;
	var layerList = this.layerList[ this.item.nameImage ];
	if ( layerList )
	{
		// All the layers one above the other by changing the Z order up, and adding perspective
		for ( z = 0; z < layerList.length && z < this.maxLayers; z++ )
		{
			var layer = layerList[ z ];
			var deltaX = ( ( options.x - options.xCenter ) * z * options.perspective ) * options.zoomX;
			var deltaY = ( ( options.y - options.yCenter ) * z * options.perspective ) * options.zoomY;

			var element = layer.element;
			element.x = options.x + deltaX;
			element.y = options.y + deltaY;
			element.z = options.z + 0.1 * z;
			element.with = options.width;
			element.height = options.height;
			element.zoomX = options.zoomX;
			element.zoomY = options.zoomY;
			element.alpha = options.alpha;
			element.angle = options.rotation;
			element.visible = this.visible;
			element.refresh = true;
		}
	}
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.destroy = function( options )
{
	// All the layers one above the other by changing the Z order up, and adding perspective
	for ( image in this.layerList )
	{
		var layerList = this.layerList[ image ];
		for ( z = 0; z < layerList.length; z++ )
		{
			if ( layerList[ z ].element )
				this.renderer.removeChild( layerList[ z ].element );
		}
	}
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.setVisible = function( options )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.getVector = function()
{
    return { x: 0, y: 0 };
};
Tree.Renderer.Renderer_Canvas2D.RendererItemSprite3D.resize = function( width, height )
{
	if ( typeof width != 'undefined' )
		this.width = width;
	if ( typeof height != 'undefined' )
		this.height = height;
};



Tree.Renderer.Renderer_Canvas2D.RendererItemMap = function( renderer, item, options )
{
	Object.assign( this, Tree.Renderer.Renderer_Canvas2D.RendererItemMap );
	this.renderer = renderer;
	this.item = item;
	this.name = item.name;
	this.className = 'Tree.Renderer.Renderer_Canvas2D.RendererItemMap';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.utilities.setOptions( this, options );

	this.tileWidth = this.item.tileWidth;
	this.tileHeight = this.item.tileHeight;
	this.mapWidth = this.item.mapWidth;
	this.mapHeight = this.item.mapHeight;
	this.tileCount = 0;
	this.reset();
	return this;
}
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.update = function( options )
{
	var offsetX = this.item.offsetX;
	var offsetY = this.item.offsetY;

	var xx = options.x - offsetX * options.zoomX;
	var yy = options.y - offsetY * options.zoomY;

	// Position the background
	this.backgroundCanvas.z = options.z;
    this.backgroundCanvas.x = xx;
    this.backgroundCanvas.y = yy;
    this.backgroundCanvas.alpha = options.alpha;
    this.backgroundCanvas.visible = this.visible;

	// Position the animated sprites. TODO: clipping and culling!
	for ( var y in this.sprites )
	{
		for ( var x in this.sprites[ y ] )
		{
			var spriteDefinition = this.sprites[ y ][ x ];
			for ( var z = 0; z < spriteDefinition.length; z++ )
			{
				var element = spriteDefinition[ z ];
				var x1 = xx + x * this.tileWidth;
				var y1 = yy + y * this.tileHeight;
				x1 += ( x1 - options.xCenter ) * options.perspective * z;
				y1 += ( y1 - options.yCenter ) * options.perspective * z;
				element.x = x1;
				element.y = y1;
				element.z = options.z + 0.1 + z * 0.1;
				element.alpha = options.alpha;
				element.visible = this.visible;
			}
		}
	}
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.destroy = function( options )
{
	this.renderer.removeChild( this.backgroundCanvas );
	for ( var s = 0; s < this.spriteList.length; s++ )
	{
		this.renderer.removeChild( this.spriteList[ s ] );
	}
};
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.setVisible = function( flag )
{
	if ( this.visible != flag )
	{
		this.visible = flag;
	}
};
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.doRefresh = function()
{
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.getVector = function()
{
	var position = new THREE.Vector3();
	return position.getPositionFromMatrix( this.backgroundSprite.matrixWorld );
};
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.reset = function()
{
	// Computes the tiles definition
	this.tiles = [];
    for ( var i = 0; i < this.item.tiles.length; i ++ )
    {
		var tile = this.item.tiles[ i ];
		var tileDefinition = {};
		if ( tile.nameImage )
		{
			var image = this.resources.getImage( tile.nameImage );
			tileDefinition.width = image.width;
			tileDefinition.height = image.height;
			tileDefinition.nameImage = tile.nameImage;
		}
		else if ( tile.images )
		{
			tileDefinition.images = [];
			for ( var ii = 0; ii < tile.images.length; ii++ )
			{
				var image = this.resources.getImage( tile.images[ ii ].nameImage );
				var def =
				{
					nameImage: tile.images[ ii ].nameImage,
					width: image.width,
					height: image.height,
					hotSpotX: image.hotSpotX,
					hotSpotY: image.hotSpotY,
					z: ii
				};
				tileDefinition.images.push( def );
			}
		}
		this.tiles.push( tileDefinition );
	}

	// Creates the static ground layer canvas
	this.width = this.mapWidth * this.tileWidth;
	this.height = this.mapHeight * this.tileHeight;
	var backgroundCanvas = this.renderer.createCanvas( this.width, this.height, 'mapBackground', this.item, this );

	// Draw the background if defined
	if ( this.item.background )
	{
		var backImage = this.resources.getImage( this.item.background, this.item );
		var repeatX = Math.floor( ( this.mapWidth * this.tileWidth ) / backImage.width ) + 1;
		var repeatY = Math.floor( ( this.mapHeight * this.tileHeight ) / backImage.height ) + 1;
		for ( var x = 0; x < repeatX; x++ )
		{
			for ( var y = 0; y < repeatY; y++ )
			{
				this.context.drawImage( backImage, x * backImage.width, y * backImage.height, backImage.width, backImage.height );
			}
		}
	}

	// Creates the background
	this.backgroundCanvas =
	{
		image: backgroundCanvas,
		x: 0,
		y: 0,
		z: 0,
		angle: 0,
		zoomX: 1,
		zoomY: 1,
		hotSpotX: 0,
		hotSpotY: 0,
		alpha: 1,
		visible: false,
		refresh: false
	}
	this.renderer.appendChild( this.backgroundCanvas );

	// Draw the tiles, and creates the animated sprites columns
	this.map = this.item.map;
	this.sprites = [];
	this.spriteList = [];
	var x = 0;
	var y = 0;
    for ( var y = 0; y < this.map.length; y++ )
    {
		for ( var x = 0; x < this.map[ y ].length; x++ )
		{
			var tileNumber = this.map[ y ][ x ];
			if ( tileNumber >= 0 )
			{
				if ( this.tiles[ tileNumber ] )
				{
					var tile = this.tiles[ tileNumber ];
					if ( tile.nameImage )
					{
						var image = this.resources.getImage( tile.nameImage );

						// A single image, draw into background
						this.context.drawImage( image, x * this.tileWidth, y * this.tileHeight );
					}
					else
					{
						// Animated tile, creates the column of sprites
						for ( var m = 0; m < tile.images.length; m++ )
						{
							if ( !this.sprites[ y ] )
								this.sprites[ y ] = {};
							if ( !this.sprites[ y ][ x ] )
								this.sprites[ y ][ x ] = [];

							var image = this.resources.getImage( tile.images[ m ].nameImage );
							var spriteDefinition =
							{
								image: image,
								x: 0,
								y: 0,
								z: 0,
								angle: 0,
								zoomX: 1,
								zoomY: 1,
								hotSpotX: 0,
								hotSpotY: 0,
								alpha: 1,
								width: image.width,
								height: image.height,
								visible: false,
								refresh: false
							}
							this.sprites[ y ][ x ].push( spriteDefinition );
							this.spriteList.push( spriteDefinition );
							this.renderer.appendChild( spriteDefinition );
						}
					}
				}
				else
				{
					this.renderer.error( 'Map ' + this.item.identifier + ' error: tile not defined X ' + x + ', Y ' + y + ', Value ' + tileNumber );
				}
			}
		}
	}
};
Tree.Renderer.Renderer_Canvas2D.RendererItemMap.resize = function( width, height )
{
};






Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas = function( renderer, item, options )
{
	Object.assign( this, Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas );
	Object.assign( this, Tree.Renderer.Utilities.Canvas2D );
	this.renderer = renderer;
	this.item = item;
	this.name = item.name;
	this.className = 'Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas';
	this.utilities = this.renderer.utilities;
	this.resources = this.renderer.resources;
	this.width = false;
	this.height = false;
	this.utilities.setOptions( this, options );
	this.width = this.item.width;
	this.height = this.item.height;
	this.canvas = this.renderer.createCanvas( this.item.width, this.item.height, 'canvasItem', this.item, this );
	this.element =
	{
		image: this.canvas,
		x: 0,
		y: 0,
		z: 0,
		angle: 0,
		zoomX: 1,
		zoomY: 1,
		hotSpotX: 0,
		hotSpotY: 0,
		width: this.item.width,
		height: this.item.height,
		alpha: 1,
		visible: false,
		refresh: false
	}
	this.renderer.appendChild( this.element );
    this.toRefresh = true;
}
Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.reset = function()
{
	this.forceReset = true;
};

Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.update = function( options )
{
    this.element.x = options.x;
	this.element.y = options.y;
	this.element.z = options.z;
	this.element.width = options.width;
	this.element.height = options.height;
	this.element.angle = options.rotation;
    this.element.alpha = options.alpha;
	this.element.visible = this.visible;
	this.element.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.destroy = function( options )
{
    this.renderer.removeChild( this.element );
};
Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.setVisible = function( flag )
{
	this.visible = flag;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.doRefresh = function( options )
{
	this.toRefresh = true;
	this.renderer.refresh = true;
};
Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.getVector = function()
{
    return { x: 0, y: 0 };
};
Tree.Renderer.Renderer_Canvas2D.RendererItemCanvas.resize = function( width, height )
{
	if ( typeof width != 'undefined' )
	{
		this.canvas.width = width;
		this.width = width;
		this.element.width = width;
	}
	if ( typeof height != 'undefined' )
	{
		this.canvas.height = height;
		this.height = height;
		this.element.height = height;
	}
};
