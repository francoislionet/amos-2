/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine Tree management elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.RenderItems = Tree.RenderItems || {};

// Fake item
Tree.Empty = function( tree, name, options )
{
    this.nameImage = false;
    this.renderItemName = 'Tree.RenderItems.Empty';
    Tree.Items.init( this, tree, name, 'Tree.Empty', options );
};
Tree.Empty.messageUp = function( message )
{
    return this.startProcess( message, [ ] );
};
Tree.Empty.messageDown = function( message )
{
    return this.endProcess( message, [] );
};

// Fake renderitem
Tree.RenderItems.Empty = function( tree, item, options )
{
	this.rendererName = '*';
	Tree.RenderItems.init( this, tree, item, 'Tree.RenderItems.Empty', options );

	this.setDefaultSize( 256, 256 );
};
Tree.RenderItems.Empty.render = function()
{
};
