/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine Tree management elements
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.RenderItems = Tree.RenderItems || {};

Tree.AsciiArt = function( tree, name, options )
{
    this.font = '12px sans serif';
    this.color = '#FFFF00';
    this.palette = ' .oO';
    this.horizontalResolution = 64;
    this.verticalResolution = 64 * 9 / 16;

	this.nameImage = false;
	this.renderItemName = 'Tree.RenderItems.AsciiArt';

    Tree.Items.init( this, tree, name, 'Tree.AsciiArt', options );
    this.registerEvents( 'refresh' );
};
Tree.AsciiArt.messageUp = function( message )
{
    if ( message.command == 'refresh' )
        this.doRefresh();
    return this.startProcess( message, [ 'x', 'y', 'z', 'rotation' ] );
};
Tree.AsciiArt.messageDown = function( message )
{
    return this.endProcess( message, [ 'x', 'y', 'z', 'rotation' ] );
};


Tree.RenderItems.AsciiArt_HTML = function( tree, name, options )
{
    this.font = false;
    this.color = false;
    this.palette = false;
    this.horizontalResolution = false;
	this.verticalResolution = false;
	this.nameImage = false;

    this.rendererName = 'Renderer_HTML';
    this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.RenderItems.AsciiArt_HTML', options );
};
Tree.RenderItems.AsciiArt_HTML.render = function( options )
{
    var canvas = this.resources.getImage( this.item.nameImage );
    if ( canvas )
    {
        var xx, yy;
        var context = canvas.getContext( '2d' );
        var imageData = context.getImageData( 0, 0, canvas.width, canvas.height );
        var srceSizeX = canvas.width / this.item.horizontalResolution;
        var srceSizeY = canvas.height / this.item.verticalResolution;
        var destSizeX = this.width / this.item.horizontalResolution;
        var destSizeY = this.height / this.item.verticalResolution;

        options.rendererItem.clearRect( options, 0, 0, this.width, this.height );
        for ( var x = 0, xx = 0; x < this.item.horizontalResolution; x++, xx += srceSizeX )
        {
            for ( var y = 0, yy = 0; y < this.item.verticalResolution; y++, yy += srceSizeY )
            {
                var colors = this.utilities.getPixelColor( imageData, xx, yy, srceSizeX, srceSizeY );
                var lum = ( colors.red * 0.2126 + colors.green * 0.7152 + colors.blue * 0.0722 ) / 255;
                var index = Math.floor( this.item.palette.length * lum );
                var letter = this.item.palette[ index ];

                options.rendererItem.drawText( options, x * destSizeX + destSizeX / 2, y * destSizeY, letter, this.item.font, '#FFFFFF', 'left', 'middle', destSizeY );
            }
        }
    }
    return options;
};

Tree.RenderItems.AsciiArt_Canvas2D = function( tree, name, options )
{
    this.font = false;
    this.color = false;
    this.palette = false;
    this.horizontalResolution = false;
	this.verticalResolution = false;
	this.nameImage = false;

	this.rendererName = 'Renderer_Canvas2D';
    this.rendererType = 'Canvas';
    Tree.RenderItems.init( this, tree, name, 'Tree.RenderItems.AsciiArt_Canvas2D', options );
};
Tree.RenderItems.AsciiArt_Canvas2D.render = Tree.RenderItems.AsciiArt_HTML.render;
