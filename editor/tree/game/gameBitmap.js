/*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree game objects
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.Game = Tree.Game || {};
Tree.Game.RenderItems = Tree.Game.RenderItems || {};

Tree.Game.Bitmap = function( tree, name, options )
{
    this.nameImage = '';
	this.fitImage = 'stretch';
	this.renderItemName = 'Tree.Game.RenderItems.Bitmap';
    Tree.Items.init( this, tree, name, 'Tree.Game.Bitmap', options );
};
Tree.Game.Bitmap.messageUp = function( message )
{
    return this.startProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'rotation', 'nameImage' ] );
};
Tree.Game.Bitmap.messageDown = function( message )
{
    var ret = this.endProcess( message, [ 'x', 'y', 'z', 'width', 'height', 'rotation', 'nameImage' ] );
	return ret;
};



Tree.Game.RenderItems.Bitmap_HTML = function( tree, item, options )
{
	this.nameImage = false;

	this.rendererName = 'Renderer_HTML';
	this.rendererType = 'Sprite';
	Tree.RenderItems.init( this, tree, item, 'Tree.Game.RenderItems.Bitmap_HTML', options );
	this.nameImage = ''; 	// Enforce loading the image
	this.setImage();

	this.item.width = this.image.width;
	this.item.height = this.image.height;
};
Tree.Game.RenderItems.Bitmap_HTML.render = function( options )
{
	this.setImage();
};
Tree.Game.RenderItems.Bitmap_HTML.message = function( message )
{
	switch ( message.command )
	{
		case 'resize':
			if ( typeof message.width != 'undefined')
				this.width = message.width;
			if ( typeof message.height != 'undefined' )
				this.height = message.height;
			this.renderer.resizeItem( this, message.width, message.height );
            this.item.doRefresh();
			break;
		default:
			break;
	}
};
Tree.Game.RenderItems.Bitmap_HTML.setImage = function()
{
	if ( this.nameImage != this.item.nameImage )
	{
		this.image = this.resources.getImage( this.item.nameImage );
		if ( image )
		{
			this.nameImage = this.item.nameImage;
			this.hotSpotX = image.hotSpotX;
			this.hotSpotY = image.hotSpotY;
			this.width = image.width;
			this.height = image.height;
			return true;
		}
		else
		{
		    Tree.log( this, { level: Tree.ERRORLEVEL_HIGH, error: 'non existant image: ' + this.image + '.' })
		}
	}
    return false;
};

Tree.Game.RenderItems.Bitmap_Canvas2D = function( tree, item, options )
{
	this.nameImage = false;

	this.rendererName = 'Renderer_Canvas2D';
	this.rendererType = 'Sprite';
    Tree.RenderItems.init( this, tree, item, 'Tree.Game.RenderItems.Bitmap_Canvas2D', options );
    this.nameImage = '';
	this.setImage();

	this.item.width = this.image.width;
	this.item.height = this.image.height;
};
Tree.Game.RenderItems.Bitmap_Canvas2D.setImage = Tree.Game.RenderItems.Bitmap_Canvas2D.setImage;
Tree.Game.RenderItems.Bitmap_Canvas2D.render = Tree.Game.RenderItems.Bitmap_Canvas2D.render;
Tree.Game.RenderItems.Bitmap_Canvas2D.message = Tree.Game.RenderItems.Bitmap_Canvas2D.message;
