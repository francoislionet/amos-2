 /*©agpl*************************************************************************
*                                                                              *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU Affero General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                 *
* GNU Affero General Public License for more details.                          *
*                                                                              *
* You should have received a copy of the GNU Affero General Public License     *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
*                                                                              *
*****************************************************************************©*/
/** @file
 *
 * Tree engine game items
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2018
 */
Tree.Game = Tree.Game || {};
Tree.Game.RenderItems = Tree.Game.RenderItems || {};

Tree.Game.Sprite = function( tree, name, options )
{
    this.end = false;
    this.nameImage = false;
    this.positionMode = 'none';
    this.resizeMode = 'none';
	this.renderItemName = 'Tree.Game.RenderItems.Sprite';
    Tree.Items.init( this, tree, name, 'Tree.Game.Sprite', options );
};
Tree.Game.Sprite.messageUp = function( message )
{
    return this.startProcess( message, [ 'x', 'y', 'z', 'rotation', 'nameImage' ] );
};
Tree.Game.Sprite.messageDown = function( message )
{
    if ( message.command == 'setSize' )
    {
        this.width = message.width;
        this.height = message.height;
    }
    return this.endProcess( message, [ 'x', 'y', 'z', 'rotation', 'nameImage' ] );
};

Tree.Game.RenderItems.Sprite_Three2D = function( tree, name, options )
{
	this.rendererType = 'Sprite';
	this.rendererName = 'Renderer_Three2D';
    Tree.RenderItems.init( this, tree, name, 'Tree.Game.RenderItems.Sprite_Three2D', options );
    this.nameImage = '';
    this.setImage();
};
Tree.Game.RenderItems.Sprite_Three2D.render = function( options )
{
    this.setImage();
	return options;
};
Tree.Game.RenderItems.Sprite_Three2D.setImage = function()
{
    if ( this.item.nameImage != this.nameImage )
    {
        var image = this.resources.getImage( this.item.nameImage );
        if ( image )
        {
            this.nameImage = this.item.nameImage;

            this.width = image.width;
            this.height = image.height;
            this.hotSpotX = image.hotSpotX;
            this.hotSpotY = image.hotSpotY;

            // Set item width and height
            this.tree.sendMessageToItem( this.item.root, this.item,
            {
                command: 'setSize',
                type: 'renderItemToItem',
                width: this.width,
                height: this.height
            });
            return true;
        }
        else
        {
            Tree.log( this.item,
            {
                level: Tree.ERRORLEVEL_HIGH,
                error: 'Image does not exist: ' + this.nameImage
            } );
        }
    }
	return false;
};


Tree.Game.RenderItems.Sprite_HTML = function( tree, name, options )
{
	this.rendererType = 'Sprite';
	this.rendererName = 'Renderer_HTML';
    Tree.RenderItems.init( this, tree, name, 'Tree.Game.RenderItems.Sprite_HTML', options );
    this.nameImage = '';
    this.setImage();
};
Tree.Game.RenderItems.Sprite_HTML.setImage = Tree.Game.RenderItems.Sprite_Three2D.setImage;
Tree.Game.RenderItems.Sprite_HTML.render = Tree.Game.RenderItems.Sprite_Three2D.render;


Tree.Game.RenderItems.Sprite_Canvas2D = function( tree, name, options )
{
	this.rendererType = 'Sprite';
	this.rendererName = 'Renderer_Canvas2D';
    Tree.RenderItems.init( this, tree, name, 'Tree.Game.RenderItems.Sprite_Canvas2D', options );
    this.setImage = Tree.Game.RenderItems.Sprite_Three2D.setImage;
    this.render = Tree.Game.RenderItems.Sprite_Three2D.render;
    this.nameImage = '';
    this.toto = 'prout';
    this.setImage();
};
Tree.Game.RenderItems.Sprite_Canvas2D.setImage = Tree.Game.RenderItems.Sprite_Three2D.setImage;
Tree.Game.RenderItems.Sprite_Canvas2D.render = Tree.Game.RenderItems.Sprite_Three2D.render;
