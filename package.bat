del amosc-x86.exe
del amosc-x64.exe
del amosc-win-x86.exe
del amosc-macos
REM del amosc-linux

cd compiler
call pkg --out-path ./.. --targets win,x86 amosc.js
call pkg --out-path ./.. --targets linux,x86 amosc.js
call pkg --out-path ./.. --targets linux,x64 amosc.js
call pkg --out-path ./.. --targets macos amosc.js
cd ..

timeout 1
del amosc-win.exe
del amosc-win-x86.exe
ren amosc-linux amosc-linux-x86
ren amosc amosc-macos
