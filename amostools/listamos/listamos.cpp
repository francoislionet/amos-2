// listamos.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "amoslib.h"
#include "listamos_.h"
#include "fileio.h"
#include "amoslib.h"

int main(int argc, char *argv[]) 
{
	uint8_t *buf;
	size_t len;
	int i;

	if (argc < 2) {
		printf("Usage: %s [options] <Filename.AMOS>\n", argv[0]);
		printf("Options:\n"
			"  -e extension     use this extension in its default slot\n"
			"  -eN=extension    use this extension in slot N\n"
			"  -c config        read extension filenames from config\n"
			"  -d dir           ...and extension files are in this dir\n");
		return 0;
	}

	i = read_extensions(argc, argv);
	read_config_extensions(argc, argv);
	read_default_extensions();

	/* list source file(s) */
	for (; i < argc; i++) {
		if ((buf = (uint8_t*)read_file(argv[i], &len))) {
			if (len >= 34 && amos_leek(buf) == 0x414D4F53) {
				uint32_t srclen = amos_leek(&buf[16]);
				if (srclen > (uint32_t)(len - 20)) srclen = (uint32_t)(len - 20);
				int err = AMOS_print_source(&buf[20], srclen, stdout, table);
				if (err & 4) {
					fprintf(stderr, "%s: missing extension instruction(s) - "
						"are the correct extensions loaded?\n", argv[i]);
				}
			}
			else {
				fprintf(stderr, "%s: not an AMOS source file\n", argv[i]);
			}
			free(buf);
		}
	}

	AMOS_free_tokens(table);
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
