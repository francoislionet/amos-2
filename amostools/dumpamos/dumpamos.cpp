// dumpamos.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "dumpamos_.h"
#include "fileio.h"

int main(int argc, char *argv[])
{
	if (argc <= 1) {
		printf("Usage: %s [-p] <file.AMOS, file.abk, file.abs, ...>\n", argv[0]);
		return 1;
	}

	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-p")) {
			use_prefix = 1;
		}
		else {
			char *fname = argv[i];
			uint8_t *buf;
			size_t len;
			if ((buf = (uint8_t*)read_file(fname, &len))) 
			{
				amos_file(fname, buf, len);
				free(buf);
			}
		}
	}
	return 0;
}

