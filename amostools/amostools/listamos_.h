#pragma once

void read_extension_file(char *filename, int slot);
int read_extensions(int argc, char *argv[]);
void read_config_extensions(int argc, char *argv[]);
void read_default_extensions();
extern struct AMOS_token *table[AMOS_TOKEN_TABLE_SIZE];
