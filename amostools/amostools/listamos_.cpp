/* listamos: prints AMOS source code as ASCII */
#include "pch.h"
#include <windows.h>
#include <sys/types.h>
#include <string.h>
#include <stdint.h>
#include "amoslib.h"
#include "listamos_.h"
#include "fileio.h"
#pragma warning(disable : 4996)

struct AMOS_token *table[AMOS_TOKEN_TABLE_SIZE] = { NULL };
char extensions_loaded[AMOS_EXTENSION_SLOTS + 1] = { 0 };

void read_extension_file(char *filename, int slot) {
    uint8_t *buf;
    size_t len;

    if ((buf = (uint8_t*)read_file(filename, &len))) {
        if (slot == -1) {
            slot = AMOS_find_slot(buf, len);
        }
        if (slot == -1) {
            fprintf(stderr, "%s: can't determine extension slot"
                            " (use -eNN=filename to set slot NN)\n", filename);
        }
        else if (extensions_loaded[slot]) {
            fprintf(stderr, "%s: extension already loaded in slot %d\n", filename, slot);
        }
        else if (AMOS_parse_extension(buf, len, slot, 6, table)) {
            fprintf(stderr, "%s: not an AMOS extension\n", filename);
        }
        else {
            fprintf(stderr, "%s: loaded into slot %d\n", filename, slot);
            extensions_loaded[slot] = 1;
        }
        free(buf);
    }
}

int read_extensions(int argc, char *argv[]) {
    int i, slot;

    /* look for -e options */
    for (i = 1; i < argc; i++) {
        if (*argv[i] != '-') break; /* end of arg processing */
        if (!strcmp(argv[i], "-e")) {
            read_extension_file(argv[++i], -1);
        }
        else if (sscanf(argv[i], "-e%d=", &slot) == 2) {
            if (slot < 1 || slot > 25) {
                fprintf(stderr, "invalid extension slot %d (must be 1-25)\n", slot);
            }
            else {
                read_extension_file(strchr(argv[i], '=') + 1, slot);
            }
        }
        else if (argv[i][1] == 'c' || argv[i][1] == 'd') {
            i++; /* skip arg */
        }
    }
    return i;
}

/* extensions to ignore in the config file, because we already loaded them */
#define NUM_DEFAULT_EXTENSIONS 9
const char *default_extensions[NUM_DEFAULT_EXTENSIONS] = {
    "Music.Lib",   "AMOSPro_Music.Lib",
    "Compact.Lib", "AMOSPro_Compact.Lib",
    "Request.Lib", "AMOSPro_Request.Lib",
    "Serial.Lib",  "AMOSPro_IOPorts.Lib",
    "" /* also ignore blank entries */
};

void read_config_extensions(int argc, char *argv[])
{
	char *dir = (char*)".", *config = NULL, *fullname = NULL, *fp;
	char *slots[AMOS_EXTENSION_SLOTS];
	uint8_t *buf = NULL;
	size_t len;
	int i, j;

	/* look for -c and -d options */
	for (i = 1; i < argc; i++)
	{
		if (argv[i][0] != '-') break; /* end of arg processing */
		if (argv[i][1] == 'c') config = argv[++i];
		else if (argv[i][1] == 'd') dir = argv[++i];
		else if (argv[i][1] == 'e' && !argv[i][2]) i++;
	}

	/* if no '-c' flag, do nothing */
	HANDLE hFind = NULL;
	if (!config) goto error_handler;

	/* open '-d' directory */
	WIN32_FIND_DATA FindFileData;
	if ((hFind = FindFirstFile(dir, &FindFileData)) != NULL)
	{
		/* read '-c' config file */
		if (!(buf = (uint8_t*)read_file(config, &len)))
		{
			goto error_handler;
		}
		if (AMOS_parse_config(buf, len, slots))
		{
			fprintf(stderr, "%s: not an AMOS config file\n", config);
			goto error_handler;
		}

		/* reduce the extension names to just NULL or a filename,
			* and remove default extensions */
		for (i = 0; i < AMOS_EXTENSION_SLOTS; i++)
		{
			if (slots[i] && *slots[i])
			{
				char *fname = strrchr(slots[i], '/');
				if (fname) slots[i] = fname + 1;
				/* ignore extensions loaded by default */
				for (j = 0; j < NUM_DEFAULT_EXTENSIONS; j++)
				{
					if (!stricmp(slots[i], default_extensions[j]))
					{
						slots[i] = NULL;
						break;
					}
				}
			}
			else
			{
				slots[i] = NULL;
			}
		}

		/* allocate space for dir + "/" + fname, pre-fill dir + "/" */
		fullname = (char*)malloc(strlen(dir) + 257);
		if (!fullname) goto error_handler;
		strcpy(fullname, dir);
		fp = &fullname[strlen(dir)];
		*fp++ = '/';

		/* find files in the directory that are listed as extensions */
		do
		{
			/* compare each extension's filename to the current dir entry */
			for (i = 0; i < AMOS_EXTENSION_SLOTS; i++)
			{
				if (!slots[i] || stricmp(FindFileData.cFileName, slots[i])) continue;
				/* build the full path and load extension */
				strncpy(fp, FindFileData.cFileName, 256);
				read_extension_file(fullname, i + 1);
				slots[i] = NULL;
			}
		} while (FindNextFile(hFind, &FindFileData));
		FindClose(hFind);

		/* complain about any listed extensions not loaded */
		for (i = 0; i < AMOS_EXTENSION_SLOTS; i++)
		{
			if (slots[i]) fprintf(stderr, "%s/%s: no such file\n", dir, slots[i]);
		}

		printf("%s\n", FindFileData.cFileName);
	}
	else
	{
		perror(dir);
		goto error_handler;
	}
error_handler:
	if (hFind != NULL) FindClose(hFind);
	free(buf);
	free(fullname);
}

/* read tokens for base language and default extensions */
#include "extensions/00_base.h"
#include "extensions/01_music.h"
#include "extensions/02_compact.h"
#include "extensions/03_request.h"
#include "extensions/06_ioports.h"
void read_default_extensions() 
{
    AMOS_parse_extension(ext00_base, sizeof(ext00_base), 0, -194, table);
    if (!extensions_loaded[1]) {
        AMOS_parse_extension(ext01_music, sizeof(ext01_music), 1, 6, table);
    }
    if (!extensions_loaded[2]) {
        AMOS_parse_extension(ext02_compact, sizeof(ext02_compact), 2, 6, table);
    }
    if (!extensions_loaded[3]) {
        AMOS_parse_extension(ext03_request, sizeof(ext03_request), 3, 6, table);
    }
    if (!extensions_loaded[6]) {
        AMOS_parse_extension(ext06_ioports, sizeof(ext06_ioports), 6, 6, table);
    }
}


