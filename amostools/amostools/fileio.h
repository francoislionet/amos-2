#pragma once

extern void *read_file(char *name, size_t *length);
extern int write_file(char *name, void *mem, size_t length);
