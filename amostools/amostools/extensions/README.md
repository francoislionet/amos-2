This directory contains [every AMOS extension](https://www.exotica.org.uk/wiki/AMOS_extensions)
I can find, cut down to just their token tables with the `make_extensions.pl` script.

They should be used with the `listamos -e` option.

| Filename                                              | Source |
|-------------------------------------------------------|--------|
|`3d.lib-V1.00`                                         |[Amiga Computing #66 coverdisk](http://amr.abime.net/issue_505_coverdisks)|
|`3d.lib-V1.50`                                         |[AMOS PD #266](http://cd.textfiles.com/amospd/251-275/APD266/AMOS_System/3d.lib)|
|`AMOSPro_3d.Lib-V1.02AP`                               |AMOS Pro Productivity2 disk|
|`AGA.Lib-V1.0`, `AMOSPro_AGA.Lib-V1.0`                 |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/AGA.LHA)|
|`AMOSPro_AMCAF.Lib-V1.40-deutsch`                      |[Aminet](http://aminet.net/package/dev/amos/AMCAFExt140InD)|
|`AMOSPro_AMCAF.Lib-V1.40-english`                      |Aminet CD 21: dev/amos/AMCAFInE.lha|
|`AMOSPro_AMCAF.Lib-V1.50beta4`                         |[Aminet](http://aminet.net/package/dev/amos/AMCAF150Final)|
|`Amon.Lib-V1.03`, `AMOSPro_Amon.Lib-V1.03`             |AMOSZine issue 8 (F1-106B)|
|`Amon.Lib-V1.04`, `AMOSPro_Amon.Lib-V1.04`             |AMOSZine issue 9 (F1-112C)|
|`BUtility.Lib-V1.1`                                    |Aminet CD 38: dev/amos/BUtility.lha|
|`BUtility.Lib-V1.2`                                    |Aminet CD 42: dev/amos/BUtility.lha|
|`BUtility.Lib-V1.21`                                   |[Aminet](http://aminet.net/package/dev/amos/BUtility)|
|`AMOSPro_Colours.Lib-V1.0`                             |[AMOS Factory](http://www.amigau.com/amigarealm/amos/amos/2301.htm)|
|`Compiler.Lib-V1.00`                                   |[AMOS PD #230](http://cd.textfiles.com/amospd/226-250/APD230/AMOS_System/)|
|`Compiler.Lib-V1.34`                                   |[CU Amiga April 1993 coverdisk](http://amr.abime.net/issue_602_coverdisks)|
|`Compiler.Lib-V1.35`                                   |[AMOS PD #373](http://cd.textfiles.com/amospd/351-375/APD373/) (Compiler 1.36 Update)|
|`AMOSPro_Compiler.Lib-V1.34`                           |[AMOS PD #466](http://cd.textfiles.com/amospd/451-475/APD466/APSystem/)|
|`AMOSPro_Compiler.Lib-V2.00`                           |AMOS Pro Productivity2 disk|
|`CRAFT.Lib-V1.00`, `AMOSPro_CRAFT.Lib-V1.00`, `MusiCRAFT.Lib-V1.00`, `AMOSPro_MusiCRAFT.Lib-V1.00` |[CU Amiga March 1994 coverdisk](http://amr.abime.net/issue_613_coverdisks)|
|`CTEXT.Lib-V1.1`                                       |[Ultimate Amiga](https://www.ultimateamiga.co.uk/index.php?action=tpmod;dl=item397)|
|`CTEXT.Lib-V1.32`                                      |[AMOS PD #426](http://cd.textfiles.com/amospd/426-450/APD426/AMOS_System/)|
|`AMOSPro_DBench.Lib-V0.42`                             |... unknown! ...|
|`D-Sam.Lib-V1.01`                                      |AMOS Pro 2.00 Update disk|
|`AMOSPro_Delta.Lib-V1.4`                               |Aminet CD 22: dev/amos/deltaext.lha|
|`AMOSPro_Delta.Lib-V1.6`                               |[Aminet](http://aminet.net/package/dev/amos/AMOSPro_Delta)|
|`AMOSPro_DOOM_Music.Lib-V2.0`                          |[AMOS Factory](http://www.amigau.com/amigarealm/amos/amos/1503.htm)|
|`Dump.Lib-V1.0`                                        |[AMOS PD #36](http://cd.textfiles.com/amospd/026-050/APD036/) (AMOS 1.36 Update disk)|
|`Dump.Lib-V1.1`                                        |[AMOS PD #565](http://cd.textfiles.com/amospd/551-575/APD565/JC/AMOS_System/)|
|`EasyLife.Lib-V1.0`                                    |[AMOS PD #600](http://cd.textfiles.com/amospd/576-600/APD600/easy/)|
|`AMOSPro_EasyLife.Lib-V1.09`                           |[Aminet](http://aminet.net/package/dev/amos/Easylife110_P1)|
|`AMOSPro_EasyLife.Lib-V1.10`                           |[Aminet](http://aminet.net/package/dev/amos/Easylife110pt1)|
|`AMOSPro_EasyLife.Lib-V1.44`                           |Aminet CD 4: dev/amos/easylife14b.lha|
|`Ercole.Lib-V1.6`, `AMOSPro_Ercole.Lib-V1.7`           |[Aminet](http://aminet.net/package/dev/amos/Ercole_ext)|
|`AMOSPro_Explode.Lib-V2.01`                            |[Author's website](https://testaware.wordpress.com/amiga/)|
|`FileID.Lib-V1.0`                                      |[Aminet](http://aminet.net/package/dev/amos/fileidex)|
|`AMOSPro_First.Lib-V0.10.1`                            |[Aminet](http://aminet.net/package/dev/amos/FirstExt)|
|`AMOSPro_Game.Lib-V0.9`                                |[Ultimate Amiga](https://www.ultimateamiga.co.uk/index.php?action=tpmod;dl=item364)|
|`AMOSPro_GameSupport.Lib-V1.2`                         |[Aminet](http://aminet.net/package/dev/amos/GameSupport)|
|`AMOSPro_GUI.Lib-V1.5`                                 |Aminet CD 12: dev/amos/GuiExt15.lha|
|`AMOSPro_GUI.Lib-V1.61`                                |Aminet CD 19: dev/amos/GuiExt.lha|
|`AMOSPro_GUI.Lib-V1.62`                                |[Aminet](http://aminet.net/package/dev/amos/GuiExt162)|
|`AMOSPro_GUI.Lib-V2.1`                                 |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/GUI_2.LHA)|
|`Int.Lib-V1.0`, `AMOSPro_Int.Lib-V1.0`                 |[AMOS Factory](http://www.amigau.com/amigarealm/amos/amos/2501.htm)|
|`IntuiExtend.Lib-V1.6`, `AMOSPro_IntuiExtend.Lib-V1.6` |[Aminet](http://aminet.net/package/dev/amos/intuiextend16)|
|`AMOSPro_IntuiExtend.Lib-V2.01b`                       |[Aminet](http://aminet.net/package/dev/amos/IntuiExtend20b)|
|`Intuition.Lib-V1.3`, `AMOSPro_Intuition.Lib-V1.3`     |F1 Licenseware disk F1-120|
|`Intuition.Lib-V1.3a`, `AMOSPro_Intuition.Lib-V1.3a`   |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/Intuition.LHA)|
|`Intuition.Lib-V1.3b`, `AMOSPro_Intuition.Lib-V1.3b`   |[Author's website](http://www.achurch.org/)|
|`AMOSPro_JD.Lib-V4.6`, `AMOSPro_Prt.Lib-V1.1`          |[Aminet](http://aminet.net/package/dev/amos/jdlib4_6)|
|`AMOSPro_JD.Lib-V5.3`, `AMOSPro_Prt.Lib-V1.3`, `AMOSPro_JDColour.Lib-V1.4` |[AMOS PD disk 599](http://cd.textfiles.com/amospd/576-600/APD599/)|
|`AMOSPro_JD.Lib-V5.9`, `AMOSPro_Prt.Lib-V1.4`, `AMOSPro_JDColour.Lib-V2.0`, `AMOSPro_JDInt.Lib-V1.3`, `AMOSPro_JDK3.Lib-V1.1` |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/JD_Extensions.LHA)|
|`AMOSPro_Jotre.Lib-V1.0-var1`, `AMOSPro_Jotre.Lib-V1.0-var2` |Aminet CD 23: dev/amos/jly_jtre.lha|
|`AMOSPro_JVP.Lib-V1.01`                                |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/JVP_1.01.LHA)|
|`LDos.Lib-V2.5`                                        |[AMOS PD #600](http://cd.textfiles.com/amospd/576-600/APD600/ldos_demo/)|
|`LDos.Lib-V2.5r`, `AMOSPro_LDos.Lib-V2.5r`             |[AMOS PD #606](http://cd.textfiles.com/amospd/601-625/APD606/Ldos_AMOS_Extensions/)|
|`AMOSPro_LDos.Lib-V2.6`, `AMOSPro_LSerial.Lib-V2.2`    |[Ultimate Amiga](https://www.ultimateamiga.co.uk/index.php?action=tpmod;dl=item371)|
|`AMOSPro_Locale.Lib-V0.26`                             |[Aminet](http://aminet.net/package/dev/amos/localeext)|
|`LSerial.Lib-V2.1`                                     |[AMOS PD #600](http://cd.textfiles.com/amospd/576-600/APD600/ldos_demo/)|
|`AMOSPro_Make.Lib-V1.2`                                |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/Make_1.2.LHA)|
|`AMOSPro_Make.Lib-V1.30`                               |[Aminet](http://aminet.net/package/dev/amos/intuiextend16) (bundled with IntuiExtend)|
|`MAXSDoorHandler.Lib-V0.20`                            |[Aminet](http://aminet.net/package/comm/bbs/ArisDoors4MAXs)
|`MED.Lib-V7.1`                                         |[Aminet](http://aminet.net/package/dev/amos/medext71)|
|`AMOSPro_Misc.Lib-V1.0`                                |[Aminet](http://aminet.net/package/dev/amos/MiscExt)|
|`Music.Lib-V3.0DEMO`, `AMOSPro_Music.Lib-V3.0DEMO`     |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/EME_3.0_DEMO.LHA)|
|`Opal.Lib-V1.1`                                        |[Aminet](http://aminet.net/package/dev/basic/amosopal)|
|`OrgAsm.Lib-V1.0demo`                                  |AMOSZine Supplemental Disk #4|
|`AMOSPro_OS_DevKit.Lib-V1.61`                          |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/OS_Dev_1.61.LHA)|
|`AMOSPro_P61.Lib-V1.1`                                 |Aminet CD 19: dev/amos/AMOSP61Ext.lha|
|`AMOSPro_P61.Lib-V1.2`                                 |[Aminet](http://aminet.net/package/dev/amos/AMOSP61Ext)|
|`AMOSPro_Personnal.Lib-V1.0b`, `Personnal.Lib-V1.0b`   |[Aminet](http://aminet.net/package/dev/amos/PersonnalDEMO)|
|`AMOSPro_Personnal.Lib-V1.1a`, `Personnal.Lib-V1.1a`, `Personnal-EXTRA.Lib` |[Ultimate Amiga](https://www.ultimateamiga.co.uk/index.php?action=tpmod;dl=item458)|
|`PowerBobs.Lib-V0.6`, `AMOSPro_PowerBobs.Lib-V0.6`     |AMOSZine issue 5/6 (F1-079A/B/C)|
|`PowerBobs.Lib-V1.0`, `AMOSPro_PowerBobs.Lib-V1.0`     |[Aminet](http://aminet.net/package/dev/amos/PowerBobs)|
|`RANGE.Lib-var1`                                       |[AMOS PD #435](http://cd.textfiles.com/amospd/426-450/APD435/AMOS_System/)|
|`RANGE.Lib-var2`                                       |[AMOS PD #435](http://cd.textfiles.com/amospd/426-450/APD447/AMOS_System/)|
|`Range.Lib-V2.6`                                       |[AMOS PD #575](http://cd.textfiles.com/amospd/551-575/APD574/AMOS_System/Range.Lib)|
|`AMOSPro_Range.Lib-V2.9Plus`                           |Created by [TOME4ProUpdate](http://aminet.net/package/dev/amos/TOME4ProUpdate)|
|`shuffle.lib`                                          |[AMOS PD #426](http://cd.textfiles.com/amospd/426-450/APD426/AMOS_System/)|
|`AMOSPro_SLN.Lib-V2.0`, `AMOSPro_SLN.Lib-V2.1`         |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/SLN_Extensions.LHA)|
|`stars.lib-V2.33`, `starspro.lib-V2.33`                |[Aminet](http://aminet.net/package/dev/amos/stars)|
|`Sticks.Lib-V1.01b`                                    |[AMOS PD CD](http://cd.textfiles.com/amospd/Extensions/Sticks/)|
|`AMOSPro_SymBase.Lib-V0.58`                            |Aminet CD 31: dev/amos/SymBase.lha|
|`AMOSPro_SymBase.Lib-V0.85`                            |Aminet CD 47: dev/amos/SymBase.lha|
|`AMOSPro_SymBase.Lib-V0.92`                            |Aminet CD 52: dev/amos/SymBase.lha|
|`AMOSPro_SymBase.Lib-V0.94`                            |[Aminet](http://aminet.net/package/dev/amos/SymBase)|
|`AMOSPro_TFT.Lib-V0.6`                                 |[Ultimate Amiga](http://www.aliensrcooluk.com/public/Amiga/AMOS/TFT_0.6.LHA)|
|`AMOSPro_TFT.Lib-V0.7`                                 |[Aminet](http://aminet.net/package/dev/amos/TFT_Extension)|
|`AMOSPro_THX.Lib-V0.6`                                 |Aminet CD 15: dev/amos/THXLib06.lha|
|`TOME.Lib-V3.1`                                        |[Ultimate Amiga](https://www.ultimateamiga.co.uk/index.php?action=tpmod;dl=item397)|
|`TOME.Lib-V4.23`                                       |[AMOS PD #574](http://cd.textfiles.com/amospd/551-575/APD574/AMOS_System/)|
|`AMOSPro_TOME.Lib-V4.24Plus`                           |Created by [TOME4ProUpdate](http://aminet.net/package/dev/amos/TOME4ProUpdate)|
|`AMOSPro_Tools.Lib-V1.00`                              |Aminet CD 18: dev/amos/Tools.lha|
|`AMOSPro_Tools.Lib-V1.01`                              |[Aminet](http://aminet.net/package/dev/amos/Tools)|
|`TURBO.Lib-V1.5`                                       |Aminet CD 1: dev/amos/turbo15.lha|
|`TURBO.Lib-V1.6`                                       |Aminet CD 1: dev/amos/turbo16.lha|
|`TURBO.Lib-V1.9`, `AMOSPro_TURBO.Lib-V1.9`             |Aminet CD 2: dev/amos/turbo19.lha|
|`TURBO.Lib-V1.11`                                      |Aminet CD 2: dev/amos/turbo1_11.lha|
|`TURBO_Plus.Lib-V1.0`, `AMOSPro_TURBO_Plus.Lib-V1.0`   |[CU Amiga August 1994 coverdisk](http://amr.abime.net/issue_618_coverdisks)|
|`AMOSPro_TURBO_Plus.Lib-V2.15`                         |[Ultimate Amiga](https://www.ultimateamiga.co.uk/index.php?action=tpmod;dl=item362)|
