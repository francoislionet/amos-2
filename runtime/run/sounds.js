/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Runtime
 *
 * The return of Basic!
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 22/09/2019
 */
function AMOS2Sounds( amos )
{
	this.amos = amos;
	this.manifest = amos.manifest;
	this.utilities = amos.utilities;
	this.banks = amos.banks;
/*
	var options = this.manifest.manifest.sounds;
	options.onload = onLoad;
	options.onloaderror = onLoadError;
	options.onplayerror = onPlayError;
	options.onplay = onPlay;
	options.onend = onEnd;
	options.onpause = onPause;
	options.onstop = onStop;
	options.onmute = onMute;
	options.onvolume = onVolume;
	options.onrate = onRate;
	options.onseek = onSeek;
	options.onfade = onFade;
	options.onunlock = onUnlock;
	this.howler = new Howl( options );

	this.soundsContext = new AMOSContext( this.amos, 'sounds', { sorted: false, howler: this.howler } );
	this.soundsContext.loadList
	this.howler = new Howl( {} );
*/
};
