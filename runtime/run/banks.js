/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Runtime
 *
 * The return of Basic!
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 28/02/2018
 */
function Banks( amos )
{
	this.amos = amos;
	this.utilities = amos.utilities;
	this.manifest = amos.manifest;
	this.banks = {};
	this.imageBank = new AMOSContext( this.amos, 'application', {} );
	this.banks[ 1 ] = this.imagesBank;
	INSERT_CODE
};

Banks.prototype.reserve = function( number, type, length )
{
	if ( number < 0 )
		throw 'illegal_function_call';
	if ( !this.manifest.unlimitedBanks && number > 16 )
		throw 'illegal_function_call';
	if ( length < 0 )
		throw 'illegal_function_call';
	var buffer = new ArrayBuffer( length );
	if ( buffer )
	{
		var memoryBlock = this.amos.allocMemoryBlock( buffer, this.manifest.compilation.endian );
		this.banks[ number ] =
		{
			type: type,
			length: length,
			memoryBlock: memoryBlock
		};
	}
	else
	{
		throw 'out_of_memory';
	}
};
Banks.prototype.erase = function( number )
{
	if ( number < 1 )
		throw 'illegal_function_call';
	if ( !this.manifest.unlimitedBanks && number > 16 )
		throw 'illegal_function_call';
	var bank = this.banks[ number ];
	if ( !bank )
		throw 'bank_not_reserved';
	this.amos.freeMemoryBlock( bank.memoryBlock );
	this.banks[ number ] = null;
};
Banks.prototype.getStart = function( number )
{
	if ( number < 1 )
		throw 'illegal_function_call';
	if ( !this.manifest.unlimitedBanks && number > 16 )
		throw 'illegal_function_call';
	var bank = this.banks[ number ];
	if ( !bank )
		throw 'bank_not_reserved';
	return bank.memoryBlock.memoryHash * this.amos.memoryHashMultiplier;
};
Banks.prototype.getLength = function( number )
{
	if ( number < 1 )
		throw 'illegal_function_call';
	if ( !this.manifest.unlimitedBanks && number > 16 )
		throw 'illegal_function_call';
	var bank = this.banks[ number ];
	if ( !bank )
		throw 'bank_not_reserved';
	return bank.length;
};
Banks.prototype.listBank = function()
{
	var result = '';
	for ( var b = 0; b < this.banks.length; b++ )
	{
		var bank = this.banks[ b ];
		if ( bank )
		{
			result = ' ' + b + ' - ' + bank.type + '     ' + 'S: ' + this.amos.hex$( bank.memoryBlock.memoryHash >> 8, 8 ) + ' L: ' + bank.length;
			this.amos.currentScreen.currentWindow.print( result, true );
		}
	}
};


// IMAGE BANK
Banks.prototype.setHotSpot = function( index, x, y )
{
	this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).setHotSpot( index, x, y );
};
Banks.prototype.getImage = function( index )
{
	return this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).get( index );
};
Banks.prototype.getImageWidth = function( index )
{
	return this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).get( index ).width;
};
Banks.prototype.getImageHeight = function( index )
{
	return this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).get( index ).height;
};
Banks.prototype.insertNewImage = function( index )
{
	this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).addNew( index, index + 1 );
};
Banks.prototype.insertNewImages = function( first, last )
{
	this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).addNew( first, last );
};
Banks.prototype.insertImage = function( index, canvas, tags )
{
	this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).addCanvas( index, canvas, tags );
};
Banks.prototype.deleteImage = function( index )
{
	this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).delete( index );
};
Banks.prototype.deleteImages = function( first, last )
{
	if ( typeof first == 'undefined' && typeof last == 'undefined' )
	{
		this.imageBank.reset( this.amos.currentContextName, 1 );
	}
	else
	{
		if ( typeof first == 'number' )
		{
			last = typeof last != 'undefined' ? last : first + 1;
			this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).deleteRange( first, last );
		}
		else if ( typeof first == 'string' )
		{
			this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).delete( first );
		}
	}
};
Banks.prototype.getImagesPalette = function()
{
	this.imageBank.getElement( this.amos.currentContextName, 1, 'bank_not_defined' ).getPalette();
};

function ImageBank( amos, imageList, palette, options )
{
	this.amos = amos;
	this.utilities = amos.utilities;
	this.palette = palette;
	this.options = options;
	this.domain = 'images';
	this.context = new AMOSContext( this.amos, this.domain, {} );
	if ( imageList )
		this.loadList( imageList, options.tags );
};
ImageBank.prototype.loadList = function( imageList, tags )
{
	var self = this;
	var count = imageList.length;
	tags = typeof tags == 'undefined' ? '' : tags;
	if ( count )
	{
		this.amos.loadingMax += count;
		for ( var i = 0; i < imageList.length; i++ )
		{
			var path = './resources/images/' + imageList[ i ];
			this.utilities.loadUnlockedImage( path, 'image/png', {}, function( response, imageLoaded, name )
			{
				if ( response )
				{
					var image =
					{
						amos: self.amos,
						name: name,
						canvas: imageLoaded,
						width: imageLoaded.width,
						height: imageLoaded.height,
						hotSpotX: 0,
						hotSpotY: 0,
						collisionMaskPrecision: self.amos.manifest.sprites.collisionPrecision,
						collisionMaskAlphaThreshold: self.amos.manifest.sprites.collisionAlphaThreshold,
						getCanvas: getImageCanvas
					};
					self.context.setElement( this.domain, image, name, true );
					self.setFromTag( name, tags );
					self.amos.loadingCount++;
				}
			}, this.utilities.getFilename( imageList[ i ] ) );
		}
	}
};
ImageBank.prototype.load = function( path, width, height, tags )
{
	var self = this;
	var imageName = this.utilities.getFilename( path );
	if ( imageName )
	{
		var image = new Image();
		this.amos.loadingMax++;
		image.onload = function()
		{
			var canvas = document.createElement( 'canvas' );
			canvas.width = typeof width != 'undefined' ? width : this.width;
			canvas.height = typeof height != 'undefined' ? height : this.height;
			var context = canvas.getContext( '2d' );
			context.imageSmoothingEnabled = self.utilities.isTag( tags, [ 'smooth' ] )
			context.drawImage( this, 0, 0 );
			var newImage =
			{
				amos: self.amos,
				name: imageName,
				canvas: canvas,
				width: canvas.width,
				height: canvas.height,
				hotSpotX: 0,
				hotSpotY: 0,
				collisionMaskPrecision: self.collisionPrecision,
				collisionMaskAlphaThreshold: self.collisionAlphaThreshold,
				getCanvas: getImageCanvas
			}
			self.context.setElement( this.domain, newImage, imageName, true );
			self.setFromTag( imageName, tags );
			this.amos.loadingCount++;
		};
		image.src = path;
	}
};
ImageBank.prototype.addNew = function( first, last, tags )
{
	var canvas = document.createElement( 'canvas' );
	canvas.width = 16;
	canvas.height = 16;
	if ( last < first || first < 0 || last < 0 )
		throw 'illegal_function_call';
	for ( var i = first; i < last; i++ )
		this.addCanvas( i, canvas, tags );
};
ImageBank.prototype.addCanvas = function( index, canvas, tags )
{
	var image =
	{
		amos: self.amos,
		name: index,
		canvas: canvas,
		width: canvas.width,
		height: canvas.height,
		hotSpotX: 0,
		hotSpotY: 0,
		collisionMaskPrecision: self.collisionPrecision,
		collisionMaskAlphaThreshold: self.collisionAlphaThreshold,
		getCanvas: getImageCanvas
	}
	this.context.setElement( this.domain, image, index, true );
	this.setFromTag( index, tags );
};
function getImageCanvas( hRev, vRev )
{
	var canvas = this.canvas;
	if ( hRev || vRev )
	{
		if ( hRev && vRev )
		{
			if ( !this.canvasRev )
				this.canvasRev = this.amos.utilities.flipCanvas( this.canvas, Sprites.HREV | Sprites.VREV );
			canvas = this.canvasRev;
		}
		else if ( hRev )
		{
			if ( !this.canvasHRev )
				this.canvasHRev = this.amos.utilities.flipCanvas( this.canvas, Sprites.HREV );
			canvas = this.canvasHRev;
		}
		else
		{
			if ( !this.canvasVRev )
				this.canvasVRev = this.amos.utilities.flipCanvas( this.canvas, Sprites.VREV );
			canvas = this.canvasVRev;
		}
	}
	return canvas;
};
ImageBank.prototype.get = function( index )
{
	return this.context.getElement( this.domain, index, 'image_not_defined' );
};
ImageBank.prototype.getWidth = function( index )
{
	return this.context.getElement( this.domain, index, 'image_not_defined' ).width;
};
ImageBank.prototype.getHeight = function( number )
{
	return this.context.getElement( this.domain, index, 'image_not_defined' ).height;
};
ImageBank.prototype.getPalette = function()
{
	return this.palette;
};
ImageBank.prototype.delete = function( index )
{
	return this.context.deleteElement( this.domain, index );
};
ImageBank.prototype.deleteRange = function( first, last )
{
	return this.context.deleteRange( this.domain, first, last );
};
ImageBank.prototype.addRangeNew = function( first, last, tags )
{
	if ( typeof first == 'number' && typeof last == 'number' )
	{
		if ( last < first )
			throw 'illegal_function_call';

		for ( var count = first; count < last; count++ )
		{
			this.addNew( count, tags );
		}
		return;
	}
	throw 'type_mismatch';
};
ImageBank.prototype.setFromTag = function( index, tags )
{
	var image = this.context.getElement( this.domain, index, 'image_not_defined' );
	if ( this.utilities.isTag( tags, [ 'hotSpotX', 'hotSpotY' ] ) )
	{
		var x = this.utilities.getTagParameter( tags, 'hotSpotX' );
		if ( typeof x == 'string' )
		{
			switch ( x )
			{
				case 'left':
					image.hotSpotX = 0;
					break;
				case 'center':
					image.hotSpotX = image.width / 2;
					break;
				case 'right':
					image.hotSpotX = image.width;
					break;
			}
		}
		else if ( typeof x == 'number' )
		{
			image.hotSpotX = x;
		}

		var y = this.utilities.getTagParameter( tags, 'hotSpotY' );
		if ( typeof y == 'string' )
		{
			switch ( y )
			{
				case 'top':
					image.hotSpotY = 0;
					break;
				case 'middle':
					image.hotSpotY = image.height / 2;
					break;
				case 'bottom':
					image.hotSpotY = image.height;
					break;
			}
		}
		else if ( typeof y == 'number' )
		{
			image.hotSpotY = y;
		}
	}
};
ImageBank.prototype.setHotSpot = function( index, x, y )
{
	var image = this.context.getElement( this.domain, index, 'image_not_defined' );
	if ( x == 'mask' )
	{
		switch ( ( y & 0x70 ) >> 4 )
		{
			case 0:
				image.hotSpotX = 0;
				break;
			case 1:
				image.hotSpotX = image.width / 2;
				break;
			case 2:
				image.hotSpotX = image.width;
				break;
		}
		switch ( y & 0x07 )
		{
			case 0:
				image.hotSpotY = 0;
				break;
			case 1:
				image.hotSpotY = image.height / 2;
				break;
			case 2:
				image.hotSpotX = image.height;
				break;
		}
	}
	else
	{
		if ( typeof x != 'undefined' )
			image.hotSpotX = x;
		if ( typeof y != 'undefined' )
			image.hotSpotY = y;
	}
};


/*
// SOUNDS BANK
function SoundBank( amos, soundList, options )
{
	this.amos = amos;
	this.utilities = amos.utilities;
	this.options = options;
	this.domain = 'sounds';
	this.howler = options.howler;
	this.context = new AMOSContext( this.amos, this.domain, {} );
	if ( soundList )
		this.loadList( soundList, options.tags );

	// -----------------------------------------------------------------
	// Cascade format of classes for automatic code production... (c) FL
	// -----------------------------------------------------------------
	// PRINCIPLE
	// 1. Specific, format for names and properties, and names of library classes to use
	// 2. If subclasses with other libraries use, use the name principle
	// 3. Formatting in names + consistency yet freedom in names -> replicable to the infinite.
	// 4. AUTO PRODUCTION OF CODE EASY
	// 5. End Idea  :)
};
SoundBank.prototype.loadList = function( soundList, tags )
{
	var count = soundList.length;
	tags = typeof tags == 'undefined' ? '' : tags;
	if ( count )
	{
		this.amos.loadingMax += count;
		for ( var i = 0; i < soundList.length; i++ )
		{
			var path = './resources/sounds/' + soundList[ i ];
			this.load( path, soundList, tags );
		}
	}
};
SoundBank.prototype.load = function( path, soundList, tags )	// Idea : between 'path' and 'tags' free parameter list (limited to 5? to check ;) : End Idea (c) FL
{
	var self = this;
	var soundName = this.utilities.getFilename( path );
	if ( soundName )
	{
		var sound =
		{
			amos: self.amos,
			name: name,
			element: undefined,
			options: {}
		};
		self.amos.loadingCount++;
		this.howler.load();

		for ( var i = 0; i < soundList.length; i ++ )			// Idea : index variable i to m included, with ii iii jj jjj indicating both depth and rank at a glance : End Idea (c) FL
		{
			self.context.setElement( this.domain, sound, name, false );
			self.setFromTag( name, tags );
		}


		self.amos.loadingCount++;

		function onload()
		{
			self.amos.loadingCount--;
		}
		function onloaderror()
		{
			// FLTODO#Sounds
		}
	}
};
SoundBank.prototype.addNew = function( index, tags )
{
};
SoundBank.prototype.get = function( index )
{
	return this.context.getElement( this.domain, index, 'sound_not_defined' );
};
SoundBank.prototype.getWidth = function( index )
{
	return this.context.getElement( this.domain, index, 'sound_not_defined' ).width;
};
SoundBank.prototype.getHeight = function( number )
{
	return this.context.getElement( this.domain, index, 'sound_not_defined' ).height;
};
// Idea
//     getABCXYZ where ABCXYZ is the name of extra elements
SoundBank.prototype.getPalette = function()
{
	return this.palette;
};
// End Idea (C) FL
SoundBank.prototype.delete = function( index )
{
	return this.context.deleteElement( this.domain, index );
};
SoundBank.prototype.deleteRange = function( first, last )
{
	return this.context.deleteRange( this.domain, first, last );
};
SoundBank.prototype.addRangeNew = function( first, last, tags )
{
	if ( typeof first == 'number' && typeof last == 'number' )
	{
		if ( last < first )
			throw 'illegal_function_call';

		for ( var count = first; count < last; count++ )
		{
			this.addNew( count, tags );
		}
		return;
	}
	throw 'type_mismatch';
};
SoundBank.prototype.setFromTag = function( index, tags )
{
};
*/