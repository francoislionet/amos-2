/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
/** @file
 *
 * AMOS 2 Runtime
 *
 * Sprite bank and sprites
 *
 * @author FL (Francois Lionet)
 * @date first pushed on 04/03/2019
 */

function Sprites( amos )
{
	this.amos = amos;
	this.banks = amos.banks;
	this.spriteContext = new AMOSContext( this.amos, 'sprites', { sorted: true } );
	this.spritesToUpdate = false;
	this.spritesUpdateOn = true;
	this.spritesPriorityOn = false;
	this.spritesPriorityReverseOn = false;
	this.collisionList = [];
	this.collisionBoxed = this.amos.manifest.sprites.collisionBoxed;
	this.collisionPrecision = this.amos.manifest.sprites.collisionPrecision;
	this.collisionAlphaThreshold = this.amos.manifest.sprites.collisionAlphaThreshold;
}
Sprites.HREV = 0x80000000;
Sprites.VREV = 0x40000000;

Sprites.prototype.sprite = function( index, x, y, image )
{
	var hRev = 0;
	var vRev = 0;
	if ( typeof index == 'number' )
	{
		hRev = ( index & Sprites.HREV ) != 0;
		vRev = ( index & Sprites.VREV ) != 0;
		index &= ~( Sprites.HREV | Sprites.VREV );
	}
	var sprite = this.spriteContext.getElement( this.amos.currentContextName, index );
	if ( !sprite )
	{
		sprite =
		{
			xDisplay: undefined,
			yDisplay: undefined,
			imageDisplay: undefined,
			xScaleDisplay: undefined,
			yScaleDisplay: undefined,
			xSkewDisplay: undefined,
			ySkewDisplay: undefined,
			angleDisplay: undefined,
			xNew: x,
			yNew: y,
			imageNew: image,
			angleNew: 0,
			xScaleNew: 1,
			yScaleNew: 1,
			xSkewNew: 0,
			ySkewNew: 0,
			hRev: hRev,
			vRev: vRev,
			toUpdate: true,
			toUpdateCollisions: true,
			collisions: {}
		};
		this.spriteContext.setElement( this.amos.currentContextName, sprite, index );
	}
	x = typeof x == 'undefined' ? sprite.xNew : x;
	y = typeof y == 'undefined' ? sprite.yNew : y;
	image = typeof image == 'undefined' ? sprite.imageNew : image;
	this.banks.getImage( image, this.amos.currentContextName, 'image_not_defined' );			// Check if undefined
	sprite.xNew = x;
	sprite.yNew = y;
	sprite.imageNew = image;
	sprite.toUpdate = true;
	sprite.toUpdateCollisions = true;
	this.spritesToUpdate = true;
	this.renderer.setModified();
};
Sprites.prototype.spriteOff = function( index )
{
	if ( typeof index != 'undefined' )
	{
		this.spriteContext.deleteElement( this.amos.currentContextName, index );
	}
	else
	{
		this.spriteContext.reset( this.amos.currentContextName );
	}
	this.spritesToUpdate = true;
	this.renderer.setModified();
};
Sprites.prototype.spritesUpdate = function( force, forceAll )
{
	if ( force || ( this.spritesUpdateOn && this.spritesToUpdate ) )
	{
		this.spritesToUpdate = false;

		var done = false;
		for ( var sprite = this.spriteContext.getFirstElement(); sprite != null; sprite = this.spriteContext.getNextElement() )
		{
			if ( sprite.toUpdate || forceAll )
			{
				sprite.toUpdate = false;
				sprite.xDisplay = sprite.xNew;
				sprite.yDisplay = sprite.yNew;
				sprite.imageDisplay = sprite.imageNew;
				sprite.xScaleDisplay = sprite.xScaleNew;
				sprite.yScaleDisplay = sprite.yScaleNew;
				sprite.xSkewDisplay = sprite.xSkewNew;
				sprite.ySkewDisplay = sprite.ySkewNew;
				sprite.angleDisplay = sprite.angleNew;
				done = true;
			}
		}
		if ( done )
		{
			this.sortSpritesPriority();
			this.renderer.setModified();
		}
	}
};
Sprites.prototype.setSpritesUpdate = function( yes_no )
{
	this.spritesUpdateOn = yes_no;
};
Sprites.prototype.setScale = function( index, xScale, yScale )
{
	var sprite = this.spriteContext.getElement( this.amos.currentContextName, index, 'sprite_not_defined' );
	if ( typeof xScale != 'undefined' )
		sprite.xScaleNew = xScale;
	if ( typeof yScale != 'undefined' )
		sprite.yScaleNew = yScale;

	sprite.toUpdate = true;
	sprite.toUpdateCollisions = true;
	this.spritesToUpdate = true;
};
Sprites.prototype.setSkew = function( index, xSkew, ySkew )
{
	var sprite = this.spriteContext.getElement( this.amos.currentContextName, index, 'sprite_not_defined' );
	if ( typeof xSkew != 'undefined' )
		sprite.xSkewNew = xSkew;
	if ( typeof ySkew != 'undefined' )
		sprite.ySkewNew = ySkew;

	sprite.toUpdate = true;
	sprite.toUpdateCollisions = true;
	this.spritesToUpdate = true;
};
Sprites.prototype.setAngle = function( index, angle )
{
	this.spriteContext.setProperty( this.amos.currentContextName, index, 'angleNew', angle, 'sprite_not_defined' );

	sprite.toUpdate = true;
	sprite.toUpdateCollisions = true;
	this.spritesToUpdate = true;
};
Sprites.prototype.xSprite = function( index )
{
	return this.spriteContext.getProperty( this.amos.currentContextName, index, 'xNew', 'sprite_not_defined' );
};
Sprites.prototype.ySprite = function( index )
{
	return this.spriteContext.getProperty( this.amos.currentContextName, index, 'yNew', 'sprite_not_defined' );
};
Sprites.prototype.iSprite = function( index )
{
	return this.spriteContext.getProperty( this.amos.currentContextName, index, 'iNew', 'sprite_not_defined' );
};
Sprites.prototype.getPalette = function( )
{
	return this.banks.getImagePalette( this.amos.currentContextName );
};
Sprites.prototype.isSprite = function( index )
{
	return this.spriteContext.getElement( this.amos.currentContextName, index ) != null;
};
Sprites.prototype.setSpritesPriority = function( on_off )
{
	this.spritesPriorityOn = on_off;
	this.spritesToUpdate = true;
};
Sprites.prototype.setSpritesPriorityReverse = function( on_off )
{
	this.spritesPriorityReverseOn  = on_off;
	this.spritesToUpdate = true;
};
Sprites.prototype.sortSpritesPriority = function()
{
	if ( this.spritesPriorityOn )
	{
		if ( this.spritesPriorityReverseOn )
		{
			this.spriteContext.sort( undefined, function( b1, b2 )
			{
				if ( b1.yNew == b2.yNew )
					return 0;
				return ( b1.yNew > b2.yNew ) ? -1 : 1;
			} );
		}
		else
		{
			this.spriteContext.sort( undefined, function( b1, b2 )
			{
				if ( b1.yNew == b2.yNew )
					return 0;
				return ( b1.yNew < b2.yNew ) ? -1 : 1;
			} );
		}
	}
};


// Collision detection
Sprites.prototype.spriteCol = function( number, from, to )
{
	if ( typeof number != 'number' )
		throw 'type_mismatch';
	if ( number < 0 )
		throw 'illegal_function_call';
	var sprite = this.sprites[ number ];
	if ( !sprite )
		throw 'sprite_not_defined';
	if ( sprite.toUpdateCollisions )
		this.setSpriteCollisionData( sprite );

	from = typeof from == 'undefined' ? 0 : from;
	to = typeof to == 'undefined' ? this.lastSpriteNumber : to;
	if ( from < 0 || to < 0 || from > to )
		throw 'illegal_function_call';

	this.collisionList = [];
	for ( var s = from; s <= to; s++ )
	{
		var testSprite = this.sprites[ s ];
		if ( testSprite && testSprite != sprite )
		{
			if ( testSprite.toUpdateCollisions )
				this.setSpriteCollisionData( testSprite );
			if ( this.isColliding( sprite, testSprite ) )
			{
				this.collisionList[ s ] = true;
			}
		}
	}
	return this.collisionList.length > 0;
};
Sprites.prototype.spriteBobCol = function( sprite, screen, from, to )
{
	if ( typeof sprite != 'number')
		throw 'type_mismatch';
	if ( sprite < 0 )
		throw 'illegal_function_call';
	sprite = this.sprites[ sprite ];
	if ( !sprite )
		throw 'sprite_not_defined';
	if ( sprite.toUpdateCollisions )
		this.setSpriteCollisionData( sprite );

	from = typeof from == 'undefined' ? 0 : from;
	to = typeof to == 'undefined' ? screen.lastBobNumber : to;
	if ( from < 0 || to < 0 || from > to )
		throw 'illegal_function_call';

	this.collisionList = [];
	for ( var b = from; b <= to; b++ )
	{
		var bob = screen.bobs[ b ];
		if ( bob )
		{
			if ( bob.toUpdateCollisions )
				this.setBobCollisionData( bob, screen );

			if ( this.collisionRectangle || this.isColliding( sprite, bob ) )
			{
				this.collisionList[ b ] = true;
			}
		}
	}
	return this.collisionList.length > 0;
};
Sprites.prototype.bobSpriteCol = function( bob, screen, from, to )
{
	if ( typeof bob != 'number' )
		throw 'type_mismatch';
	if ( bob < 0 )
		throw 'illegal_function_call';
	bob = screen.bobs[ bob ];
	if ( !bob )
		throw 'bob_not_defined';
	if ( bob.toUpdateCollisions )
		this.setBobCollisionData( bob, screen );

	from = typeof from == 'undefined' ? 0 : from;
	to = typeof to == 'undefined' ? this.lastSpriteNumber : to;
	if ( from < 0 || to < 0 || from > to )
		throw 'illegal_function_call';

	this.collisionList = [];
	for ( var s = from; s <= to; s++ )
	{
		var sprite = this.sprites[ s ];
		if ( sprite )
		{
			if ( sprite.toUpdateCollisions )
				this.setSpriteCollisionData( sprite );

			if ( this.isColliding( sprite, bob ) )
			{
				this.collisionList[ s ] = true;
			}
		}
	}
	return this.collisionList.length > 0;
};
Sprites.prototype.bobCol = function( bob, screen, from, to )
{
	// TODO: allow collision test with strings as bob reference.
	if ( typeof bob != 'number' )
		throw 'type_mismatch';
	if ( bob < 0 )
		throw 'illegal_function_call';
	bob = screen.bobs[ bob ];
	if ( !bob )
		throw 'bob_not_defined';
	if ( bob.toUpdateCollisions )
		this.setBobCollisionData( bob, screen );

	from = typeof from == 'undefined' ? 0 : from;
	to = typeof to == 'undefined' ? screen.lastBobNumber : to;
	if ( from < 0 || to < 0 || from > to )
		throw 'illegal_function_call';

	this.collisionList = [];
	for ( var b = from; b <= to; b++ )
	{
		var testBob = screen.bobs[ b ];
		if ( testBob && testBob != bob )
		{
			if ( testBob.toUpdateCollisions )
				this.setBobCollisionData( testBob, screen );

			if ( this.isColliding( bob, testBob ) )
			{
				this.collisionList[ b ] = true;
			}
		}
	}
	return this.collisionList.length > 0;
};
Sprites.prototype.col = function( number )
{
	if ( number < 0 )
	{
		for ( var c = 0; c < this.collisionList.length; c++ )
		{
			if ( this.collisionList[ c ] )
				return c;
		}
		return 0;
	}
	return this.collisionList[ number ] == true;
};
Sprites.prototype.setSpriteCollisionData = function( sprite )
{
	var collisions = sprite.collisions;
	var image = this.getImage( sprite.imageNew );
	if ( image )
	{
		if ( sprite.angleNew == 0 )
		{
			collisions.x1 = sprite.xNew - image.hotSpotX * sprite.xScaleNew;
			collisions.y1 = sprite.yNew - image.hotSpotY * sprite.yScaleNew;
			collisions.x2 = collisions.x1 + image.width * sprite.xScaleNew;
			collisions.y2 = collisions.y1 + image.height * sprite.yScaleNew;
		}
		else
		{
			var x1 = sprite.xNew - image.hotSpotX * sprite.xScaleNew;
			var y1 = sprite.yNew - image.hotSpotY * sprite.yScaleNew;
			var coords = this.utilities.rotate( x1, y1,	sprite.xNew, sprite.yNew, sprite.angleNew );
			collisions.x1 = coords.x;
			collisions.y1 = coords.y;
			coords = this.utilities.rotate( x1 + image.width * sprite.xScaleNew, y1 + image.height * sprite.yScaleNew, sprite.xNew, sprite.yNew, sprite.angle );
			collisions.x2 = coords.x;
			collisions.y2 = coords.y;
		}
	}
	else
	{
		collisions.x1 = 10000000;
		collisions.y1 = 10000000;
		collisions.x2 = -10000000;
		collisions.y2 = -10000000;
	}
	sprite.toUpdateCollisions = false;
};
Sprites.prototype.setBobCollisionData = function( bob, screen )
{
	var collisions = bob.collisions;
	var image = this.getImage( bob.imageNew );
	if ( image )
	{
		var xHard = bob.xNew * screen.renderScaleX + screen.x;
		var yHard = bob.yNew * screen.renderScaleY + screen.y;
		var xHotspotHard = image.hotSpotX * screen.renderScaleX;
		var yHotspotHard = image.hotSpotY * screen.renderScaleY;
		var widthHard = image.width * screen.renderScaleX;
		var heightHard = image.height * screen.renderScaleY;
		if ( bob.angleNew == 0 )
		{
			collisions.x1 = xHard - xHotspotHard * bob.xScaleNew;
			collisions.y1 = yHard - yHotspotHard * bob.yScaleNew;
			collisions.x2 = collisions.x1 + widthHard * bob.xScaleNew;
			collisions.y2 = collisions.y1 + heightHard * bob.yScaleNew;
		}
		else
		{
			var x1 = xHard - xHotspotHard * bob.xScaleNew;
			var y1 = yHard - yHotspothard * bob.yScaleNew;
			var coords = this.utilities.rotate( x1, y1,	xHard, yHard, bob.angleNew );
			collisions.x1 = coords.x;
			collisions.y1 = coords.y;
			coords = this.utilities.rotate( x1 + widthHard * bob.xScaleNew, y1 + heightHard * bob.yScaleNew, xHard, yHard, bob.angle );
			collisions.x2 = coords.x;
			collisions.y2 = coords.y;
		}
	}
	else
	{
		collisions.x1 = 10000000;
		collisions.y1 = 10000000;
		collisions.x2 = -10000000;
		collisions.y2 = -10000000;
	}
	bob.toUpdateCollisions = false;
};
Sprites.prototype.getCollisionMask = function( image )
{
	// #STARTHERE
	// Reprendre ici.
	// Il faut simplement convertir l'image en dataview. Trop fatigue.
	// :)
	//
	// #TODOIDE: Integrate this with Friend Network
	//
	// -> send a message 'inside of the code'
	// -> simply insert at the location of the question a remark
	//    like this:
	//
	// #CAL9L HT Can you tell me why it crashes here?
	// #END
	//
	// -> Francois clicks on the FRIEND NETWORK <CALL> button of the IDE
	// -> The IDE scans the code on Francois' machine current source
	// -> Finds the tag
	// -> Through Friend Network, contacts Hogne's machine where the IDE is open too
	//    or eventually open it
	//
	// On Hogne's machine
	// -> The <CALL> button flashes (possibility to pause the system and only check
	//    for the calls every hour or 2, a quiet mode when you have to think, etc.)
	// -> Hogne click on <ANSWER>
	// -> IDE loads source code from Francois' machine and displays it
	// -> A new #ANSWER FL line is inserted at the position of the call in the code
	// -> The original messagejhjhjkhj from #CALL to #END is inserted after,
	//    #CALL being replaces by #CALLED
	// -> Hogne types the answer, a simple conversation...
	//
	// #ANSWER FL Because, because... ;)
	// #CALLED HT Can you tell me why it crashes here?
	// #END
	//
	// Conversation carries on until one of the two press <END CALL>
	// -> A dialog box appears Delete conversation? YES / NO
	//    - YES: sources are restored
	//    - NO: tags are replaced by just the initials and the conversation
	//      becomes:
	//
	// #CONVERSATION FL-HT 28/01/2019-16:34
	// #FL ARGH Found it! Sorry Hogne :)
	// #HT Because, because... ;)
	// #FL Can you tell me why it crashes here?
	// #END
	//
	// -> On Hogne's machine, the window of the IDE containing the original
	//    code is closed if he was not editing it before the call
	//
	// -> You can re-open conversations... by simply adding #CALL
	//    above #CONVERSATION
	//
	// #CALL HT Hogne, I have found something very strange in your code...
	// #FL ARGH Found it! Sorry Hogne :)
	// #HT Because, because... ;)
	// #FL Can you tell me why it crashes here?
	// #END
	//
	// etc. ;)
	//
	// Quick, simple, deletable and archivable, a new communication channel
	// through Friend Network for programmers and them only, only available
	// in Friend Code! (good name for a new version!)
	//
	// -> work over the Internet and communicate while coding without disruption
	// -> enhance by written discussions common parts of the code together
	// -> progress faster without having to call, send emails, go to chat, move in
	//    office etc. Unique feature for teams.
	// -> exchange programmers jokes, add new team member by adding the tag #JOIN
	// -> clean code at the end and generate archive (IDE feature)
	// -> fantastic plus for AMOS, finally people will work in team on the same games
	// -> fantastic selling point for Friend and Friend Network, with a good IDE next year
	// -> If you support me financially through the AMOS-2 adventure up to the release
	//    of the Boxed version on November 29th 2019, at my previous salary, you will
	//    have that for your birthday.
	// -> If not, I decide of the date and it may be MY birthday! ;)
	// -> Friend Network will be functional and tested through a Visual Studio Code extension
	//    and my AMOS-2 fans... There is not much to do left, the project has matured in my head,
	//    I know exactly how to re-program it, including the server in node.js.
	// -> The VSCode extension could eventually be released later to promote Friend Network
	//    then Friend by a Friend extension to 30% of developers on Earth (your decision).
	// -> It will be in AMOS-2 IDE by default released summer 2020, in the
	//    AMOS VSCode extension but ONLY for .AMOS sources or code included in a
	//    functional AMOS program in mid-2020. Called Friend Network if you support me, or
	//    AMOS Network if not. I would rather have it called Friend, you know that...
	// -> Of course, AMOS-2 and IDE in Friend later and on the AMOS Community machine
	//    "powered by Friend" that I intend to make public later this year...
	//
	// #CALL HT -> Hogne, ready to support me financially and remove 'the burden of money' from my quest? <-
	// #END
	// <CALL> :)
	//
	if ( !image.collisionMask )
	{
		var context = image.getContext( '2d' );
		var dataView = context.getImageData( 0, 0, image.width, image.height );

		image.collisionMask = dataView;
		image.collisionMaskWidth = image.width;
		image.collisionMaskHeight = image.height;
		image.collisionMaskPrecision = 1;
	}
	return { mask: image.collisionMask, width: image.collisionMaskWidth, height: image.collisionMaskHeight, precision: image.collisionMaskPrecision };
};
Sprites.prototype.isColliding = function( object1, object2 )
{
	var collisions1 = object1.collisions;
	var collisions2 = object2.collisions;

	var colliding = ( collisions1.x2 > collisions2.x1 && collisions1.x1 <= collisions2.x2
				   && collisions1.y2 > collisions2.y1 && collisions1.y1 <= collisions2.y2 );

	if ( colliding && !this.collisionBoxed && ( object1.angleNew == 0 && object2.angleNew == 0 ) )		// TODO
	{
		var maskDefinition1 = this.getCollisionMask( this.getImage( object1.imageNew ) );
		var maskDefinition2 = this.getCollisionMask( this.getImage( object2.imageNew ) );
		if ( !maskDefinition1 || !maskDefinition2 )
			return colliging;

		colliding = false;
		var o1Left, o1Top, o1Right, o1Bottom;
		var o2Left, o2Top, o2Right, o2Bottom;
		if ( collisions1.x1 <= collisions2.x1 )
		{
			o2Left = 0;
			o1Left = collisions2.x1 - collisions1.x1;
			if ( collisions1.x2 < collisions2.x2 )
			{
				o1Right = collisions1.x2 - collisions1.x1;
				o2Right = collisions1.x2 - collisions2.x1;
			}
			else
			{
				o2Right = collisions2.x2 - collisions2.x1;
				o1Right = collisions2.x2 - collisions1.x1;
			}
		}
		else
		{
			o1Left = 0;
			o2Left = collisions1.x1 - collisions2.x1;
			if ( collisions1.x2 < collisions2.x2 )
			{
				o1Right = collisions1.x2 - collisions1.x1;
				o2Right = collisions1.x2 - collisions2.x1;
			}
			else
			{
				o1Right = collisions2.x2 - collisions1.x1;
				o2Right = collisions2.x2 - collisions2.x1;
			}
		}
		if ( collisions1.y1 <= collisions2.y1 )
		{
			o2Top = 0;
			o1Top = collisions2.y1 - collisions1.y1;
			if ( collisions1.y2 < collisions2.y2 )
			{
				o1Bottom = collisions1.y2 - collisions1.y1;
				o2Bottom = collisions1.y2 - collisions2.y1;
			}
			else
			{
				o2Bottom = collisions2.y2 - collisions2.y1;
				o1Bottom = collisions2.y2 - collisions1.y1;
			}
		}
		else
		{
			o1Top = 0;
			o2Top = collisions1.y1 - collisions2.y1;
			if ( collisions1.y2 < collisions2.y2 )
			{
				o1Bottom = collisions1.y2 - collisions1.y1;
				o2Bottom = collisions1.y2 - collisions2.y1;
			}
			else
			{
				o1Bottom = collisions2.y2 - collisions1.y1;
				o2Bottom = collisions2.y2 - collisions2.y1;
			}
		}

		o1Left = o1Left * maskDefinition1.precision / object1.xScaleNew;
		o1Top = o1Top * maskDefinition1.precision / object1.yScaleNew;
		o2Left = o2Left * maskDefinition2.precision / object2.xScaleNew;
		o2Top = o2Top * maskDefinition2.precision / object2.yScaleNew;
		o1Right = Math.floor( o1Right * maskDefinition1.precision / object1.xScaleNew );
		o1Bottom = Math.floor( o1Bottom * maskDefinition1.precision / object1.yScaleNew );
		o2Right = Math.floor( o2Right * maskDefinition2.precision / object2.xScaleNew );
		o2Bottom = Math.floor( o2Bottom * maskDefinition2.precision / object2.yScaleNew );
		var o1PlusX = maskDefinition1.precision / object1.xScaleNew;
		var o1PlusY = maskDefinition1.precision / object1.yScaleNew;
		var o2PlusX = maskDefinition2.precision / object2.xScaleNew;
		var o2PlusY = maskDefinition2.precision / object2.yScaleNew;

		var x1, y1, x2, y2;

		/*
		for ( y1 = o1Top, y2 = o2Top; y1 < o1Bottom && y2 < o2Bottom; y1 += o1PlusY, y2 += o2PlusY )
		{
			var offset1 = Math.floor( y1 ) * maskDefinition1.width;
			var offset2 = Math.floor( y2 ) * maskDefinition2.width;
			for ( x1 = o1Left, x2 = o2Left; x1 < o1Right && x2 < o2Right; x1 += o1PlusX, x2 += o2PlusX )
			{
				this.amos.currentScreen.plot( x1 - o1Left, y1 - o1Top, maskDefinition1.mask[ offset1 + Math.floor( x1 ) ] != 0 ? 2 : 0 );
				this.amos.currentScreen.plot( 100 + x2 - o2Left, y2 - o2Top, maskDefinition2.mask[ offset2 + Math.floor( x2 ) ] != 0 ? 2 : 0 );
				if ( ( maskDefinition1.mask[ offset1 + Math.floor( x1 ) ] & maskDefinition2.mask[ offset2 + Math.floor( x2 ) ] ) != 0 )
				{
					this.amos.currentScreen.plot( x1 - o1Left, y1 - o1Top, 5 );
					this.amos.currentScreen.plot( 100 + x2 - o2Left, y2 - o2Top, 5 );
				}
			}
		}
		*/

		for ( y1 = o1Top, y2 = o2Top; y1 < o1Bottom && y2 < o2Bottom && !colliding; y1 += o1PlusY, y2 += o2PlusY )
		{
			var offset1 = Math.floor( y1 ) * maskDefinition1.width;
			var offset2 = Math.floor( y2 ) * maskDefinition2.width;
			for ( x1 = o1Left, x2 = o2Left; x1 < o1Right && x2 < o2Right; x1 += o1PlusX, x2 += o2PlusX )
			{
				if ( ( maskDefinition1.mask[ offset1 + Math.floor( x1 ) ] & maskDefinition2.mask[ offset2 + Math.floor( x2 ) ] ) != 0 )
				{
					colliding = true;
					break;
				}
			}
		}
	}
	return colliding;
};
