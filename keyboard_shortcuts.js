
// Please cut and paste this block into "keybindings.json"
// (watch video at : https://youtu.be/lsxe-3O6NDs )

{
	"key": "ctrl+f1",
	"args": "amos_run",
	"when": "editorTextFocus"
},
{
	"key": "ctrl+f2",
	"command": "workbench.action.tasks.runTask",
	"args": "amos_test",
	"when": "editorTextFocus"
},
